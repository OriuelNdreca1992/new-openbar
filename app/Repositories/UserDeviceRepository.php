<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\UserDevice;

class UserDeviceRepository
{
    public function findAllByUser(User $user): array
    {
        return UserDevice::query()
            ->where('user_id', $user->id)
            ->pluck('fcm_token')
            ->toArray();
    }
}
