<?php

namespace App\Repositories;

use App\Models\Cheers;
use App\Models\CheersParagraph;
use App\Models\Company;
use App\Models\Venue;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

class CheersRepository
{
    protected Cheers $cheers;

    public function __construct(Cheers $cheers)
    {
        $this->cheers = $cheers;
    }

    /**
     * @throws Exception
     */
    public function save(array $data, ?Cheers $cheers = null): Cheers
    {
        if (is_null($cheers)) {
            $cheers = new $this->cheers;
        }

        $type = match ($data['promoted_type']) {
            'venue'     => Venue::class,
            'company'   => Company::class,
            default     => null,
        };

        $cheers->promoted_type = $type;
        $cheers->promoted_id = is_null($type) ? null : $data['promoted_id'];
        $cheers->title = $data['title'];
        $cheers->subtitle = $data['subtitle'];
        $cheers->status = (isset($data['status']) && $data['status']) ?: 0;
        $cheers->start_date = $data['start_date'] ? Carbon::parse($data['start_date']) : null;
        $cheers->end_date = $data['end_date'] ? Carbon::parse($data['end_date']) : null;
        $cheers->date_label = $data['date_label'];
        $cheers->time_label = $data['time_label'];

        $cheers->save();

        if (isset($data['image']) && $data['image']) {
            $cheers->addMedia($data['image'])->toMediaCollection(Cheers::COLLECTION);
        }

        $paragraphIds = [];
        foreach ($data['paragraphs'] as $paragraphData) {
            /** @var CheersParagraph $paragraph */
            $paragraph = CheersParagraph::query()->find($paragraphData['id']);
            if ($paragraph) {
                $paragraph->update([
                    'title' => $paragraphData['title'],
                    'description' => $paragraphData['description'],
                    'video_url' => $paragraphData['video_url'],
                ]);
            } else {
                $paragraph = CheersParagraph::query()->create([
                    'cheers_id' => $cheers->id,
                    'title' => $paragraphData['title'],
                    'description' => $paragraphData['description'],
                    'video_url' => $paragraphData['video_url'],
                ]);
            }

            if (isset($paragraphData['image']) && $paragraphData['image']) {
                $paragraph->addMedia($paragraphData['image'])->toMediaCollection(CheersParagraph::COLLECTION);
            }

            $paragraphIds[] = $paragraph->id;
        }

        $deleteCheersParagraphs = CheersParagraph::query()
            ->where('cheers_id', $cheers->id)
            ->get()
            ->except($paragraphIds);
        foreach ($deleteCheersParagraphs as $paragraph) {
            /** @var CheersParagraph $paragraph */
            $paragraph->delete();
        }

        DB::commit();

        return $cheers->refresh();
    }
}
