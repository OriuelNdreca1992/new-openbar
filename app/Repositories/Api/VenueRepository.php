<?php

namespace App\Repositories\Api;

use App\Models\Checkout\Cart;
use App\Models\Offer;
use App\Models\PremiumDrink;
use App\Models\User;
use App\Models\Venue;
use App\Services\Checkout\OfferService;
use App\Services\Checkout\PremiumService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class VenueRepository
{
    protected PremiumService $premiumService;
    protected OfferService $offerService;
    protected Carbon $oneWeek;

    public function __construct(PremiumService $premiumService, OfferService $offerService)
    {
        $this->premiumService = $premiumService;
        $this->offerService = $offerService;
        $this->oneWeek = Carbon::now()->addDays(6)->endOfDay();
    }

    public function findByGeo(
        float $latitude,
        float $longitude,
        array $filters = [],
        float $outerRadius = Venue::OUTER_RADIUS,
        float $innerRadius = Venue::INNER_RADIUS
    ): Collection {
        $shop = isset($filters['shop']) && filter_var($filters['shop'], FILTER_VALIDATE_BOOLEAN);
        $offer = isset($filters['offer']) && filter_var($filters['offer'], FILTER_VALIDATE_BOOLEAN);
        $today = isset($filters['today']) && filter_var($filters['today'], FILTER_VALIDATE_BOOLEAN);
        $all = !$shop && !$offer && !$today;

        $shop_callback = function($query) {
            $query->where('status', true);
        };
        $offer_callback = function($query) {
            $query
                ->where('status', true)
                ->where('quantity', '>', 0)
                ->whereHas('product', function($q) {
                    $q->where('status', true);
                })
                ->where(function($query) {
                    $query->whereNull('start')
                        ->orWhere('start', '<=', $this->oneWeek);
                })
                ->where(function($query) {
                    $query->whereNull('end')
                        ->orWhere('end', '>=', Carbon::now());
                });
        };
        $today_callback = function($query) {
            $query
                ->where('status', true)
                ->where('quantity', '>', 0)
                ->whereHas('product', function($q) {
                    $q->where('status', true);
                })
                ->where(function($query) {
                    $query->where(function ($query) {
                        $query
                            ->doesntHave('offerDays')
                            ->orWhereHas('offerDays', function ($query) {
                                $query->where('week_day', Carbon::now()->dayOfWeekIso);
                            });
                    });
                    $query->where(function ($query) {
                        $query
                            ->where(function ($q) {
                                $q->where('start', '<=', Carbon::now())
                                    ->where('end', '>=', Carbon::now());
                            })
                            ->orWhere(function ($q) {
                                $q->whereNull('start')
                                    ->whereNull('end');
                            })
                            ->orWhere(function ($q) {
                                $q->whereNull('start')
                                    ->where('end', '>=', Carbon::now());
                            })
                            ->orWhere(function ($q) {
                                $q->whereNull('end')
                                    ->where('start', '<=', Carbon::now());
                            });
                    });
                });
        };
        $array = [];
        if ($shop) $array[] = ['relation' => 'premiumDrinks', 'callback' => $shop_callback];
        if ($offer) $array[] = ['relation' => 'offers', 'callback' => $offer_callback];
        if ($today) $array[] = ['relation' => 'offers', 'callback' => $today_callback];

        $venues = Venue::query()
            ->geofence($latitude, $longitude, $innerRadius, $outerRadius)
            ->where('status', true);

        if (isset($filters['search']) && !empty($filters['search'])) {
            $venues->where(function ($q) use ($filters) {
                $q
                    ->where(function ($q) use ($filters) {
                        $q->whereLike(['name'], $filters['search']);
                    })
                    ->orWhere(function ($q) use ($filters) {
                        $q->whereLike(['city'], $filters['search']);
                    })
                    ->orWhere(function ($q) use ($filters) {
                        $q->whereLike(['state'], $filters['search']);
                    })
                    ->orWhere(function ($q) use ($filters) {
                        $q->whereLike(['address'], $filters['search']);
                    })
                    ->orWhere(function ($q) use ($filters) {
                        $q->whereLike(['types.name'], $filters['search']);
                    });
            });
        }

        $venues->where(function($q) use ($array) {
            foreach ($array as $key => $condition) {
                if ($key === 0) {
                    $q->whereHas($condition['relation'], $condition['callback']);
                } else {
                    $q->orWhereHas($condition['relation'], $condition['callback']);
                }
            }
        });

        return $venues
            ->orderBy('distance')
            ->orderByDesc('created_at')
            ->get();
    }

    public function find(int $id): ?Model
    {
        return Venue::query()
            ->with(['openingHours', 'types'])
            ->with('premiumDrinks', function($query) {
                $query->where('status', true);
                $query->withAndWhereHas('product', function($query) {
                    $query->where('status', true);
                    $query->with(['category']);
                });
            })
            ->with('offers', function($query) {
                $query
                    ->where('status', true)
                    ->where('quantity', '>', 0)
                    ->with('offerDays')
                    ->withAndWhereHas('product', function($query) {
                        $query->where('status', true);
                    })
                    ->where(function($query) {
//                        $query->where(function ($query) {
//                            $query
//                                ->doesntHave('offerDays')
//                                ->orWhereHas('offerDays', function ($query) {
//                                    $query->where('week_day', Carbon::now()->dayOfWeekIso);
//                                });
//                        });
                        $query->where(function ($query) {
                            $query
                                ->where(function ($q) {
                                    $q->whereNull('start')
                                        ->whereNull('end');
                                })
                                ->orWhere(function ($q) {
                                    $q->whereNull('start')
                                        ->where('end', '>=', Carbon::now());
                                })
                                ->orWhere(function ($q) {
                                    $q->where('start', '<=', $this->oneWeek)
                                        ->whereNull('end');
                                })
                                ->orWhere(function ($q) {
                                    $q->where('start', '<=', $this->oneWeek)
                                        ->where('end', '>=', Carbon::now());
                                });
                        });
                    });
            })
            ->where('status', true)
            ->find($id);
    }

    public function use2x1(int $venueId, int $offerId)
    {
        $today = Carbon::today();

        /** @var Venue $venue */
        $venue = Venue::query()
            ->where('status', true)
            ->find($venueId);

        if (empty($venue))
            return 404;

        /** @var Offer $offer */
        $offer = Offer::query()
            ->where('status', true)
            ->where('quantity', '>', 0)
            ->whereHas('product', function($q) {
                $q->where('status', true);
            })
            ->where(function($query) {
                $query->where(function ($query) {
                    $query
                        ->doesntHave('offerDays')
                        ->orWhereHas('offerDays', function ($query) {
                            $query->where('week_day', Carbon::now()->dayOfWeekIso);
                        });
                });
                $query->where(function ($query) {
                    $query
                        ->where(function ($q) {
                            $q->where('start', '<=', Carbon::now())
                                ->where('end', '>=', Carbon::now());
                        })
                        ->orWhere(function ($q) {
                            $q->whereNull('start')
                                ->whereNull('end');
                        })
                        ->orWhere(function ($q) {
                            $q->whereNull('start')
                                ->where('end', '>=', Carbon::now());
                        })
                        ->orWhere(function ($q) {
                            $q->whereNull('end')
                                ->where('start', '<=', Carbon::now());
                        });
                });
            })
            ->find($offerId);

        if (empty($offer))
            return 404;

        $isOfferUsedToday = Cart::query()
            ->where('user_id', auth()->id())
            ->whereHas('items', function ($query) use ($today) {
                return $query
                    ->where('drinkable_type', Offer::class)
                    ->whereDate('used_at', $today);
            })
            ->exists();

        if ($isOfferUsedToday)
            return 409;

        if (empty($result = $this->offerService->insert($offer))) {
            return 500;
        }

        return $result;
    }

    public function buyDrinks(Venue $venue, User $offered, array $drinks, ?string $message)
    {
        foreach ($drinks as &$drink) {
            $premiumDrinks = PremiumDrink::query()
                ->where('status', true)
                ->whereHas('product', function($q) {
                    $q->where('status', true);
                })
                ->find($drink['id']);

            if (empty($premiumDrinks))
                return 404;

            $drink['drink'] = $premiumDrinks;
        }

        if (empty($result = $this->premiumService->insert($offered, $drinks, $message))) {
            return 500;
        }

        return $result;
    }
}
