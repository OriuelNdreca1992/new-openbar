<?php

namespace App\Repositories\Api;

use App\Models\Checkout\Cart;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class PurchaseRepository
{
    public function findAll(bool $premium = false, bool $offer = false): Collection
    {
        $query = Cart::query()
            ->with('items.venue')
            ->where('user_id', auth()->id())
            ->with('offered')
            ->where('status', Cart::STATUS_SUCCEEDED);

        if ($premium && !$offer) {
            $query->where('type', Cart::TYPE_PREMIUM);
        } elseif ($offer && !$premium) {
            $query->where('type', Cart::TYPE_2X1);
        }

        return $query
            ->orderByDesc('created_at')
            ->get();
    }

    public function find(int $id): ?Model
    {
        return Cart::query()
            ->with('offered')
            ->with('items.venue')
            ->where('user_id', auth()->id())
            ->where('status', Cart::STATUS_SUCCEEDED)
            ->find($id);
    }
}
