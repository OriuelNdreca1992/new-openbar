<?php

namespace App\Repositories\Api;

use App\Models\UserActivity;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ActivityRepository
{
    public function findAll(): Collection
    {
        return UserActivity::query()
            ->with('cart.items.premiumDrink.product:id,image_url')
            ->with('cart.items.premiumDrink:id,product_id')
            ->with('cart.items:id,cart_id,name,price,drinkable_type,drinkable_id')
            ->with('cart:id,user_id,offered_to,amount,user_fee,message')
            ->with('userSender:id,first_name,last_name')
            ->where('user_id', auth()->id())
            ->orderByDesc('created_at')
            ->get();
    }

    public function find(int $id): ?Model
    {
        return UserActivity::query()
            ->with('cart.items.premiumDrink.product:id,image_url')
            ->with('cart.items.premiumDrink:id,product_id')
            ->with('cart.items:id,cart_id,name,price,drinkable_type,drinkable_id')
            ->with('cart:id,user_id,offered_to,amount,user_fee,message')
            ->with('userSender:id,first_name,last_name')
            ->where('user_id', auth()->id())
            ->find($id);
    }
}

