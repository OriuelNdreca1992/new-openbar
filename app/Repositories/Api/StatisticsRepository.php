<?php

namespace App\Repositories\Api;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;

class StatisticsRepository
{
    public function countGiftedDrinks(): int
    {
        return CartItem::query()
            ->whereHas('cart', function ($query) {
                $query->where([
                    'user_id'   => auth()->id(),
                    'type'      => Cart::TYPE_PREMIUM,
                    'status'    => Cart::STATUS_SUCCEEDED
                ]);
            })
            ->count();
    }

    public function countConsumedDrinks(): int
    {
        $countConsumedPremiumDrinks = CartItem::query()
            ->whereHas('cart', function ($query) {
                $query->where([
                    'offered_to' => auth()->id(),
                    'type'       => Cart::TYPE_PREMIUM,
                    'status'     => Cart::STATUS_SUCCEEDED
                ]);
            })
            ->whereNotNull('used_at')
            ->count();

        return $countConsumedPremiumDrinks + ($this->countObtainedOffers() * 2);
    }

    public function countObtainedOffers(): int
    {
        return CartItem::query()
            ->whereHas('cart', function ($query) {
                $query->where([
                    'user_id'    => auth()->id(),
                    'type'       => Cart::TYPE_2X1,
                    'status'     => Cart::STATUS_SUCCEEDED
                ]);
            })
            ->whereNotNull('used_at')
            ->count();
    }

    public function countVenueVisits(): int
    {
        return CartItem::query()
            ->whereHas('cart', function ($query) {
                $query
                    ->where(function($query) {
                        return $query->where([
                            'offered_to' => auth()->id(),
                            'type'       => Cart::TYPE_PREMIUM,
                            'status'     => Cart::STATUS_SUCCEEDED
                        ]);
                    })
                    ->orWhere(function($query) {
                        return $query->where([
                            'user_id'    => auth()->id(),
                            'type'       => Cart::TYPE_2X1,
                            'status'     => Cart::STATUS_SUCCEEDED
                        ]);
                    });
            })
            ->whereNotNull('used_at')
            ->distinct('venue_id')
            ->count();
    }
}

