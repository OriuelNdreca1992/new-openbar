<?php

namespace App\Repositories\Api;

use App\Models\Cheers;
use App\Models\Company;
use App\Models\Venue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class CheersRepository
{
    public function findAll(): Collection
    {
        return Cheers::query()
            ->where('status', true)
            ->orderByDesc('created_at')
            ->get();
    }

    public function find(int $id): ?Model
    {
        return Cheers::query()
            ->with('promoted')
            ->with('paragraphs')
            ->where('status', true)
            ->find($id);
    }
}
