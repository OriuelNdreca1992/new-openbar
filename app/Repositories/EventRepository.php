<?php

namespace App\Repositories;

use App\Models\DigitalEvent\Event;
use App\Models\DigitalEvent\EventDrink;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;

class EventRepository
{
    protected Event $event;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * @throws Exception
     */
    public function save(array $data, ?Event $event = null): Event
    {
        DB::beginTransaction();

        if (is_null($event)) {
            $event = new $this->event;
        }

        $event->type = $data['type'];
        $event->format = $data['format'];
        $event->promo = $data['promo'];
        $event->title = $data['title'];
        $event->subtitle = $data['subtitle'];
        $event->description = $data['description'];
        $event->start_date = Carbon::parse($data['start_date']);
        $event->end_date = Carbon::parse($data['end_date']);
        $event->date_label = $data['date_label'];
        $event->time_label = $data['time_label'];
        $event->status = (isset($data['status']) && $data['status']) ?: 0;
        $event->card_id = $data['card_id'];
        $event->company_id = $data['company_id'];
        $event->description = $data['description'];
        $event->budget = is_numeric($data['budget']) ? $data['budget'] : $data['custom'];

        $event->save();

        // event venues
        $venue_ids = (isset($data['venue_ids']) && is_array($data['venue_ids']) && !empty($data['venue_ids']))
            ? $data['venue_ids'] : [];
        $event->venues()->sync($venue_ids);

        // event details
        $event->drinks()->delete();
        if(isset($data['drinks']) && is_array($data['drinks']) && !empty($data['drinks'])) {
            $drinks = [];
            foreach ($data['drinks'] as $drink) {
                if($drink) {
                    $drinks[] = new EventDrink([
                        'title'         => $drink['title'],
                        'description'   => $drink['description'],
                        'product_id'    => (integer) $drink['product_id'],
                    ]);
                }
            }
            $event->drinks()->saveMany($drinks);
        }

        if(isset($data['image']) && $data['image']) {
            $event->addMedia($data['image'])->toMediaCollection(Event::COLLECTION);
        }

        DB::commit();

        return $event->refresh();
    }
}
