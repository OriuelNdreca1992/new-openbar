<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\Setting;
use Yajra\DataTables\DataTables;
use App\Http\Controllers\Controller;
use Symfony\Component\HttpFoundation\Request;

class SettingsController extends Controller
{
    public function index()
    {
        return view('admin.settings.index');
    }

    public function create()
    {
        return view('admin.settings.create');
    }

    public function edit(Setting $setting)
    {
        return view('admin.settings.edit', compact('setting'));
    }

    public function update(Setting $setting)
    {
        $requestData = request()->validate([
            'value' => 'required'
        ]);

        $setting->update($requestData);

        return redirect()
            ->route('admin.settings.index')
            ->with('flash', 'Setting has updated successfully!');
    }

    public function getSettings(Request $request)
    {
        $settings = Setting::select(['id', 'name', 'value', 'description', 'created_at', 'updated_at']);

        return Datatables::of($settings)
            ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('Y-m-d');
            })->editColumn('updated_at', function ($request) {
                    return $request->updated_at->format('Y-m-d');
            })->addColumn('actions', function ($row) {})
            ->make(true);
    }
}
