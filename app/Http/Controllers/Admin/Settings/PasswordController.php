<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Models\User;
use App\Jobs\UpdatePassword;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdatePasswordRequest;

class PasswordController extends Controller
{
    public function update(User $user)
    {
        request()->validate([
            'password' => 'required',
            'password_confirmation' => 'required',
        ]);

        $user->update([
            'password' => \Hash::make(request('password'))
        ]);

        return back()
            ->with('flash', 'Your password has updated successfully!');
    }
}
