<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ScheduledPushNotification;
use Illuminate\Http\RedirectResponse;
use Yajra\DataTables\Facades\DataTables;

class ScheduledPushNotificationsController extends Controller
{
    public function index()
    {
        return view('admin.push-notifications.index');
    }

    public function create()
    {
        return view('admin.push-notifications.create');
    }

    public function store(): RedirectResponse
    {
        $attributes = request()->validate([
            'title' => 'required',
            'body' => 'required',
            'scheduled_time' => 'date_format:Y-m-d H:i|required|unique:scheduled_push_notifications',
        ]);

        $attributes['status'] = ScheduledPushNotification::PENDING;

        ScheduledPushNotification::query()->create($attributes);

        return redirect()
            ->route('admin.push-notifications.index')
            ->with('flash', 'The push notification has created successfully!');
    }

    public function edit(ScheduledPushNotification $notification)
    {
        return view('admin.push-notifications.edit', compact('notification'));
    }

    public function update(ScheduledPushNotification $notification): RedirectResponse
    {
        $attributes = request()->validate([
            'title' => 'required',
            'body' => 'required',
            'scheduled_time' => 'required',
        ]);

        $notification->update($attributes);

        return redirect()
            ->route('admin.push-notifications.index')
            ->with('flash', 'The push notification has updated successfully!');
    }

    public function destroy(ScheduledPushNotification $notification): RedirectResponse
    {
        $notification->delete();

        return redirect()
            ->route('admin.push-notifications.index')
            ->with('flash', 'The push notification has deleted successfully!');
    }

    public function getScheduledPushNotifications()
    {
        $scheduledPushNotification = ScheduledPushNotification::query()
            ->where('status', ScheduledPushNotification::PENDING)
            ->latest();

        $datatables = Datatables::of($scheduledPushNotification);

        return $datatables
            ->editColumn('scheduled_time', function ($request) {
                    return $request->scheduled_time->format('Y-m-d H:i');
            })
            ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('Y-m-d H:i');
            })
            ->editColumn('updated_at', function ($request) {
                    return $request->updated_at->format('Y-m-d H:i');
            })
            ->editColumn('actions', function ($request) {
                    return $request->id;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function getExpiredPushNotifications()
    {
        $scheduledPushNotification = ScheduledPushNotification::query()
            ->where('status', ScheduledPushNotification::COMMITTED)
            ->latest();

        $datatables = Datatables::of($scheduledPushNotification);

        return $datatables
            ->editColumn('scheduled_time', function ($request) {
                    return $request->scheduled_time->format('Y-m-d H:i');
            })
            ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('Y-m-d H:i');
            })
            ->editColumn('updated_at', function ($request) {
                    return $request->updated_at->format('Y-m-d H:i');
            })
            ->addIndexColumn()
            ->make(true);
    }
}
