<?php

namespace App\Http\Controllers\Admin;

use App\Models\Venue;
use App\Models\Slider;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\RedirectResponse;
use Yajra\DataTables\Facades\DataTables;

class SliderTwoController extends Controller
{
    public $slider = null;

    public function __construct()
    {
        $this->slider = Slider::query()->skip(1)->first();
    }

    public function index()
    {
        return view('admin.sliders.slider-two.index')->with(['slider' => $this->slider]);
    }

    /**
     * @throws Exception
     */
    public function getVenues()
    {
        $venues = Venue::query()
            ->withCount(['sliders as slider_count' => function($q) {
                $q->where('slider_venue.slider_id', $this->slider->id);
            }]);

        return Datatables::of($venues)
            ->addIndexColumn()
            ->addColumn('actions', function () {})
            ->editColumn('created_at', function ($request) {
                 return $request->created_at->format('Y-m-d H:i');
            })
            ->make();
    }

    public function addVenue()
    {
        /** @var Venue $venue */
        $venue = Venue::query()->find(request('id'));
        $venue->sliders()->attach($this->slider->id);

        $venue->save();
    }

    public function removeVenue()
    {
        /** @var Venue $venue */
        $venue = Venue::query()->find(request('id'));
        $venue->sliders()->detach($this->slider->id);

        $venue->save();
    }

    public function update(): RedirectResponse
    {
        $validatedData = request()->validate([
            'title' => 'required',
            'subtitle' => 'required',
        ]);

        $this->slider->update($validatedData);

        return back()->with('flash', 'Slider has been updated successfully!');
    }
}
