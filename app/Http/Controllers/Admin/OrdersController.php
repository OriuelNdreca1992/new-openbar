<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Contracts\View\View;
use App\Models\Venue;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index(): View
    {
        $venues = Venue::query()->get();
        return view('admin.orders.index', compact('venues'));
    }
}
