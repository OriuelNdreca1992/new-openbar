<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Venue;
use App\Models\Company;
use App\Models\Invoice;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class InvoicesController extends Controller
{
    /**
     * @throws Exception
     */
    public function getInvoices(Request $request)
    {
        if ($request->ajax()) {
            $query = Invoice::with('subject')
                ->whereIn('subject_type', [Venue::class, Company::class])
                ->orderBy('created_at', 'desc')
                ->get();

            $datatables =  Datatables::of($query)
                ->addColumn('subject_type', function (Invoice $invoice) {
                    return class_basename($invoice->subject_type);
                })->addColumn('subject_name', function (Invoice $invoice) {
                    return ($invoice->subject && $invoice->subject->name)
                        ? class_basename($invoice->subject->name)
                        : '';
                })->addColumn('actions', function (Invoice $invoice) {
                    return $invoice->id;
                });

            return $datatables->addIndexColumn()->make(true);
        }

        return response()->json();
    }
}
