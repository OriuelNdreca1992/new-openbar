<?php

namespace App\Http\Controllers\Admin\Api;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\Offer;
use App\Models\PremiumDrink;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class OrdersController extends Controller
{
    /**
     * @throws Exception
     */
    public function getOrders(Request $request)
    {
        $orderItems = CartItem::query()
            ->select('cart_items.id as drink_id',
                'buyers.id as buyer_id',
                DB::raw('CONCAT(buyers.first_name, " ", buyers.last_name, " (ID: ", buyers.id, ")") as buyer'),
                'takers.id as taker_id',
                DB::raw('CONCAT(takers.first_name, " ", takers.last_name, " (ID: ", takers.id, ")") as taker'),
                'v.id as venue_id',
                'v.name as venue_name',
                'cart_items.cart_id as cart_id',
                'cart_items.name as drink_name',
                'cart_items.price as drink_price',
                'cart_items.used_at',
                'c.user_id',
                'c.offered_to',
                'c.status',
                'c.type as type',
                'c.expires_at',
                'c.created_at as purchased_at')
            ->leftJoin('carts as c', 'c.id', '=', 'cart_items.cart_id')
            ->leftJoin('offers as of', function ($join) {
                $join->on('of.id', '=', 'cart_items.drinkable_id')->where('cart_items.drinkable_type', '=', Offer::class);
            })
            ->leftJoin('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'cart_items.drinkable_id')->where('cart_items.drinkable_type', '=', PremiumDrink::class);
            })
            ->leftJoin('products as p', function ($join) {
                $join->on('p.id', '=', DB::raw('COALESCE(of.product_id, d.product_id)'));
            })
            ->leftJoin('venues as v', function ($join) {
                 $join->on('v.id', '=', DB::raw('COALESCE(of.venue_id, d.venue_id)'));
            })
            ->leftJoin('users as buyers', function ($join) {
                $join->on('buyers.id', '=', 'c.user_id');
            })
            ->leftJoin('users as takers', function ($join) {
                $join->on('takers.id', '=', 'c.offered_to');
            });

        $datatables = Datatables::of($orderItems)
            ->addColumn('status', function ($item) {
                if ($item->status === Cart::STATUS_FAILED) {
                    return 'failed';
                } elseif ($item->status === Cart::STATUS_PENDING) {
                    return 'pending';
                } elseif (!empty($item->used_at)) {
                    return 'used';
                } elseif ($item->expires_at < Carbon::now()) {
                    return 'expired';
                } else {
                    return 'available';
                }
            })->editColumn('purchased_at', function ($item) {
                return Carbon::parse($item->purchased_at)->format('Y-m-d H:i');
            })->addColumn('used_expires_at', function ($item) {
                if ($item->status === Cart::STATUS_SUCCEEDED && $item->used_at) {
                    return $item->used_at->format('Y-m-d H:i');
                } elseif ($item->status === Cart::STATUS_SUCCEEDED && $item->expires_at && $item->expires_at < Carbon::now()) {
                    return Carbon::parse($item->expires_at)->format('Y-m-d H:i');
                }
                return NULL;
            })->addColumn('actions', function ($item) {
                return route('admin.purchases.show', $item->cart_id);
            });

        if ($request['columns'][1]['search']['value']) {
            $datatables->filterColumn('drink_id', function($query, $drink_id) {
                $query->where('cart_items.id', $drink_id);
            });
        }

        if ($request['columns'][4]['search']['value']) {
            $datatables->filterColumn('venue_id', function($query, $id) {
                $query->where('v.id', $id);
            });
        }

        if ($request['columns'][7]['search']['value']) {
            $datatables->filterColumn('type', function($query, $type) {
                $query->where('c.type', $type);
            });
        }

        if ($request['columns'][8]['search']['value']) {
            $datatables->filterColumn('status', function($query, $status) {
                if ($status === 'failed') {
                    $query->where('c.status', Cart::STATUS_FAILED);
                } elseif ($status === 'pending') {
                    $query->where('c.status', Cart::STATUS_PENDING);
                } elseif ($status === 'used') {
                    $query->where('c.status', Cart::STATUS_SUCCEEDED)->whereNotNull('cart_items.used_at');
                } elseif ($status === 'available') {
                    $query->where('c.status', Cart::STATUS_SUCCEEDED)->whereNull('cart_items.used_at')->where('c.expires_at', '>', Carbon::now());
                } elseif ($status === 'expired') {
                    $query->where('c.status', Cart::STATUS_SUCCEEDED)->whereNull('cart_items.used_at')->where('c.expires_at', '<', Carbon::now());
                }
            });
        }

        if ($request['columns'][9]['search']['value']) {
            $datatables->filterColumn('purchased_at', function($query, $date_range) {
                [$start, $end] = explode('|', $date_range);
                if ($start) { $query->whereDate('c.created_at', '>=', $start); }
                if ($end) { $query->whereDate('c.created_at', '<=', $end); }
            });
        }

        return $datatables->addIndexColumn()->make(true);
    }
}
