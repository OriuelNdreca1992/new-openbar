<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Models\Company;
use App\Jobs\StoreCompany;
use App\Jobs\UpdateCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;
use Yajra\DataTables\Facades\DataTables;

class CompaniesController extends Controller
{
    public function index()
    {
        return view('admin.companies.index');
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.companies.create');
    }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $this->dispatchNow(new StoreCompany($request));

        return redirect()
            ->route('admin.companies.index')
            ->with('flash', 'Company has created successfully!');
    }

    public function show(Company $company)
    {
        return view('admin.companies.show', compact('company'));
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.companies.edit', compact('company'));
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        $this->dispatchNow(new UpdateCompany($request, $company));

        return redirect()
            ->route('admin.companies.index')
            ->with('flash', 'Company has updated successfully!');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return redirect()
            ->route('admin.companies.index')
            ->with('flash', 'Company has deleted successfully!');
    }

    public function getCompanies(Request $request)
    {
        if ($request->ajax()) {
            $query = Company::query();

            return Datatables::eloquent($query)
                ->editColumn('created_at', function ($item) {
                    return $item->created_at
                        ? date( config('rubik.base.datetime_format'), strtotime($item->created_at) )
                        : '';
                })
                ->addColumn('actions', function () {
                    return null;
                })
                ->toJson();
        }
    }
}
