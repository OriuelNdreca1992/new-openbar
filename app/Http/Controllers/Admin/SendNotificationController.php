<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserDevice;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SendNotificationController extends Controller
{
    protected PushNotification $push;

    public function __construct()
    {
        $this->push = new PushNotification('fcm');
        $this->push->setConfig([
            'priority' => 'high',
        ]);
    }

    public function edit(): View
    {
        $users = User::query()
            ->where('status', true)
            ->whereNotNull('phone')
            ->where('role', User::ROLE_USER)
            ->get(['id', 'phone', 'first_name', 'last_name'])
            ->toArray();

        return view('admin.settings.notifications.edit', compact('users'));
    }

    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'users' => 'required',
            'title' => 'required',
            'body'  => 'required'
        ]);

        $userIDs = $request->get('users');

        $fcmTokens = UserDevice::query()
            ->whereIn('user_id', $userIDs)
            ->pluck('fcm_token')
            ->toArray();

        if (empty($fcmTokens)) {
            return redirect()->route('admin.send-notification.edit')->with([
                'level'=>'warning',
                'flash'=>'No tokens for selected devices.'
            ]);
        }

        try {
            $this->push->setMessage([
                'notification' => [
                    'title' => $request->get('title'),
                    'body'  => $request->get('body'),
                ],
                'data' => [
                    'title' => $request->get('title'),
                    'body'  => $request->get('body'),
                    'type'  => 'manual',
                    'image' => config('fcm.image.openbar'),
                ],
            ])
            ->setApiKey(config('fcm.server_key'))
            ->setDevicesToken($fcmTokens)
            ->send();
        } catch (\Exception $e) {
            return redirect()->route('admin.send-notification.edit')->with([
                'level'=>'danger',
                'flash'=>'Something went wrong!'
            ]);
        }

        return redirect()->route('admin.send-notification.edit')->with([
            'level' => 'success',
            'flash' => 'Notification sent successfully!'
        ]);
    }
}
