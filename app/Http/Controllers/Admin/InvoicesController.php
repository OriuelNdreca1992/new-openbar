<?php

namespace App\Http\Controllers\Admin;

use App\Models\Invoice;
use App\Http\Controllers\Controller;

class InvoicesController extends Controller
{
    public function index()
    {
        return view('admin.invoices.index');
    }

    public function update(Invoice $invoice)
    {
        request('status') === 'true' ? $invoice->paid() : $invoice->unpaid();

        return back();
    }
}
