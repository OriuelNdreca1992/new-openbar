<?php

namespace App\Http\Controllers\Admin;

use App\Models\ExpiredDrinkReport;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\View;
use Yajra\DataTables\Facades\DataTables;

class ExpiredDrinksReportController extends Controller
{
    public function index(): View
    {
        return view('admin.reports.expired-drinks');
    }

    /**
     * @throws Exception
     */
    public function getReports()
    {
        $datatables = Datatables::of(ExpiredDrinkReport::query()->latest());

        return $datatables
            ->addIndexColumn()
            ->make(true);
    }
}
