<?php

namespace App\Http\Controllers\Admin\DigitalEvents;

use App\Helpers\Toastr;
use App\Http\Controllers\Controller;
use App\Models\DigitalEvent\Label;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class LabelsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request, $card)
    {
        if ($request->ajax()) {
            $data = Label::query()
                ->where('card_id', $card);
            return Datatables::eloquent($data)
                ->addIndexColumn()
                ->editColumn('image', function($item) {
                    /** @var Label $item */
                    return view('admin.metronic.layout.partials.common.list.image_preview')->with([
                        'alt' => $item->name,
                        'src' => $item->getImageAttribute()
                    ]);
                })
                ->editColumn('name', function($item) {
                    return $item->name;
                })
                ->editColumn('status', function ($item) {
                    return view('admin.metronic.layout.partials.common.list.status')->with([
                        'status' => $item->status
                    ]);
                })
                ->editColumn('order', function ($item) {
                    return view('admin.metronic.layout.partials.common.list.order')->with([
                        'order' => $item->order
                    ]);
                })
                ->addColumn('action', function($item) {
                    return view('admin.metronic.layout.partials.common.list.actions')->with([
                        'edit' => ['url' => route('admin.digitalEvents.labels.edit', [
                            'card' => $item->card_id,
                            'label' => $item->id
                        ]), 'id' => $item->id],
                        'delete' => ['id' => $item->id],
                    ]);
                })
                ->rawColumns(['image', 'action'])
                ->toJson();
        }

        return view('admin.metronic.digitalEvents.cards.labels.index', compact('card'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create($card): View
    {
        return view('admin.metronic.digitalEvents.cards.labels.form', compact('card'));
    }

    /**
     * Store a newly created resource.
     * @throws ValidationException
     */
    public function store(Request $request, $card): RedirectResponse
    {
        $data = $this->validateForm($request, $card);

        try {
            /** @var Label $item */
            $item = Label::query()->create($data);
            if($request->hasFile('image')) {
                $item->addMedia($request->file('image'))->toMediaCollection(Label::COLLECTION);
            }
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed(false);
            return redirect()->route('admin.digitalEvents.labels.create');
        }

        return redirect()->route('admin.digitalEvents.labels.index', compact('card'));
    }

    /**
     * Show the form for editing an existent resource.
     */
    public function edit($card, $id)
    {
        /** @var Label $item */
        $item = Label::query()->findOrFail($id);

        return view('admin.metronic.digitalEvents.cards.labels.form', [
            'card' => $card,
            'item' => $item,
            'image' => [$item->getFirstMedia(Label::COLLECTION)], // should be array
        ]);
    }

    /**
     * Update the given resource.
     * @throws ValidationException
     */
    public function update(Request $request, $card, $id): RedirectResponse
    {
        $data = $this->validateForm($request);
        try {
            /** @var Label $item */
            $item = Label::query()->findOrFail($id);
            $item->update($data);
            if($request->hasFile('image')) {
                $item->addMedia($request->file('image'))->toMediaCollection(Label::COLLECTION);
            }
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed(false);
            return redirect()->route('admin.digitalEvents.labels.edit', ['card' => $card, 'label' => $id]);
        }

        return redirect()->route('admin.digitalEvents.labels.index', compact('card'));
    }

    /**
     * Remove the specified resource.
     */
    public function destroy($card, $id): JsonResponse
    {
        try {
            /** @var Label $item */
            $item = Label::query()->findOrFail($id);
            $item->delete();
            $data = ['success' => config('constants.data_deleted_successfully')];
        } catch (Exception $e) {
            return response()->json($e->getMessage());
            Log::error($e);
            $data = ['error' => config('constants.data_not_deleted_successfully')];
        }

        return response()->json($data);
    }

    /**
     * Validate the resource.
     * @throws ValidationException
     */
    protected function validateForm(Request $request, $card = null): array
    {
        // Form validation
        $this->validate($request, [
            'name'   => 'required',
        ]);

        $data =  [
            'name'   => $request->input('name'),
            'status' => (bool)$request->input('status'),
        ];

        if ($card) {
            $data['card_id'] = $card;
        }

        return $data;
    }
}
