<?php

namespace App\Http\Controllers\Admin\DigitalEvents;

use App\Helpers\Toastr;
use App\Http\Controllers\Controller;
use App\Http\Requests\DigitalEvents\EventRequest;
use App\Models\Company;
use App\Models\DigitalEvent\Card;
use App\Models\DigitalEvent\Event;
use App\Models\Product\Product;
use App\Models\Venue;
use App\Services\EventService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class EventsController extends Controller
{
    protected EventService $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Event::query();
            return Datatables::eloquent($data)
                ->addIndexColumn()
                ->editColumn('status', function ($item) {
                    return view('admin.metronic.layout.partials.common.list.status')->with([
                        'status' => $item->status
                    ]);
                })
                ->editColumn('start_date', function ($item) {
                    return date( config('rubik.base.datetime_format'), strtotime($item->start_date) );
                })
                ->editColumn('end_date', function ($item) {
                    return date( config('rubik.base.datetime_format'), strtotime($item->end_date) );
                })
                ->addColumn('action', function($item) {
                    return view('admin.metronic.layout.partials.common.list.actions')->with([
                        'edit' => ['url' => route('admin.digitalEvents.events.edit', ['event' => $item->id]), 'id' => $item->id],
                        'delete' => ['id' => $item->id],
                    ]);
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('admin.metronic.digitalEvents.events.index');
    }

    public function create(): View
    {
        return view('admin.metronic.digitalEvents.events.form', [
            'cards' => $this->getCardsDropDown(),
            'companies' => $this->getCompaniesDropDown(),
            'budgets' => Event::BUDGETS,
            'venues' => $this->getVenuesDropDown(),
            'products' => $this->getProductsDropDown(),
        ]);
    }

    public function store(EventRequest $request): RedirectResponse
    {
        try {
            $this->eventService->saveEventData($request->all());
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed(false);
            return redirect()->route('admin.digitalEvents.events.create');
        }

        return redirect()->route('admin.digitalEvents.events.index');
    }

    public function edit($id)
    {
        /** @var Event $item */
        $item = Event::query()->findOrFail($id);
        return view('admin.metronic.digitalEvents.events.form', [
            'item' => $item,
            'image' => [$item->getFirstMedia(Event::COLLECTION)], // should be array
            'selected_details' => $item->details()->get()->toArray(),
            'selected_venues' => $item->venues()->pluck('venue_id')->toArray(),
            'selected_drinks' => $item->drinks()->get()->toArray(),
            'cards' => $this->getCardsDropDown(),
            'companies' => $this->getCompaniesDropDown(),
            'budgets' => Event::BUDGETS,
            'venues' => $this->getVenuesDropDown(),
            'products' => $this->getProductsDropDown(),
        ]);
    }

    public function update(Request $request, $id): RedirectResponse
    {
        try {
            /** @var Event $event */
            $event = Event::query()->findOrFail($id);
            $this->eventService->saveEventData($request->all(), $event);
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed(false);
            return redirect()->route('admin.digitalEvents.events.edit', $id);
        }

        return redirect()->route('admin.digitalEvents.events.index');
    }

    /**
     * Remove the specified resource.
     */
    public function destroy($id): JsonResponse
    {
        try {
            /** @var Event $item */
            $item = Event::query()->findOrFail($id);
            $item->delete();
            $data = ['success' => config('constants.data_deleted_successfully')];
        } catch (Exception $e) {
            Log::error($e);
            $data = ['error' => config('constants.data_not_deleted_successfully')];
        }

        return response()->json($data);
    }

    private function getCompaniesDropDown(): array {
        return Company::query()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
    }

    private function getCardsDropDown(): array {
        return Card::query()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
    }

    private function getVenuesDropDown(): array {
        return Venue::query()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
    }

    private function getProductsDropDown(): array {
        return Product::query()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
    }
}
