<?php

namespace App\Http\Controllers\Admin\DigitalEvents;

use App\Helpers\Toastr;
use App\Http\Controllers\Controller;
use App\Models\DigitalEvent\Card;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class CardsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Card::query();
            return Datatables::eloquent($data)
                ->addIndexColumn()
                ->editColumn('name', function($item) {
                    return $item->name;
                })->editColumn('status', function ($item) {
                    return view('admin.metronic.layout.partials.common.list.status')->with([
                        'status' => $item->status
                    ]);
                })->addColumn('action', function($item) {
                    return view('admin.metronic.layout.partials.common.list.actions')->with([
                        'edit' => ['url' => route('admin.digitalEvents.cards.edit', ['card' => $item->id]), 'id' => $item->id],
                        'labels' => ['url' => route('admin.digitalEvents.labels.index', ['card' => $item->id]), 'card_id' => $item->id],
                        'delete' => ['id' => $item->id],
                    ]);
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('admin.metronic.digitalEvents.cards.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.metronic.digitalEvents.cards.form');
    }

    /**
     * Store a newly created resource.
     * @throws ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $data = $this->validateForm($request);

        try {
            Card::query()->create($data);
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed(false);

            return redirect()->route('admin.digitalEvents.cards.create');
        }

        return redirect()->route('admin.digitalEvents.cards.index');
    }

    /**
     * Show the form for editing an existent resource.
     */
    public function edit($id)
    {
        /** @var Card $item */
        $item = Card::query()->findOrFail($id);
        return view('admin.metronic.digitalEvents.cards.form', ['item' => $item]);
    }

    /**
     * Update the given resource.
     * @throws ValidationException
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $data = $this->validateForm($request);

        try {
            /** @var Card $item */
            $item = Card::query()->findOrFail($id);
            $item->update($data);
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed(false);
            return redirect()->route('admin.digitalEvents.cards.edit', $id);
        }

        return redirect()->route('admin.digitalEvents.cards.index');
    }

    /**
     * Remove the specified resource.
     */
    public function destroy($id): JsonResponse
    {
        try {
            /** @var Card $item */
            $item = Card::query()->findOrFail($id);
            $item->delete();
            $data = ['success' => config('constants.data_deleted_successfully')];
        } catch (Exception $e) {
            Log::error($e);
            $data = ['error' => config('constants.data_not_deleted_successfully')];
        }

        return response()->json($data);
    }

    /**
     * Validate the resource.
     * @throws ValidationException
     */
    protected function validateForm(Request $request): array
    {
        // Form validation
        $this->validate($request, [
            'name'   => 'required',
        ]);

        return [
            'name'   => $request->input('name'),
            'status' => (bool)$request->input('status'),
        ];
    }
}
