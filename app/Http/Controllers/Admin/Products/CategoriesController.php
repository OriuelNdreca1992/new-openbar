<?php

namespace App\Http\Controllers\Admin\Products;

use App\Helpers\Toastr;
use App\Models\Product\Category;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Category::query();
            return Datatables::eloquent($data)
                ->addIndexColumn()
                ->editColumn('image_url', function($item) {
                    /** @var Category $item */
                    return view('admin.metronic.layout.partials.common.list.image_preview')->with([
                        'alt' => $item->name,
                        'src' => $item->image_url
                    ]);
                })
                ->editColumn('created_at', function ($item) {
                    return $item->created_at
                        ? date( config('rubik.base.datetime_format'), strtotime($item->created_at) )
                        : '';
                })
                ->editColumn('updated_at', function ($item) {
                    return $item->updated_at
                        ? date( config('rubik.base.datetime_format'), strtotime($item->updated_at) )
                        : '';
                })
                ->addColumn('action', function($item) {
                    return view('admin.metronic.layout.partials.common.list.actions')->with([
                        'edit' => ['url' => route('admin.product-categories.edit', ['product_category' => $item->id]), 'id' => $item->id],
                        'delete' => ['id' => $item->id],
                    ]);
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('admin.metronic.products.categories.index');
    }

    /**
     * Show the form for creating a new resource.
    */
    public function create()
    {
        return view('admin.metronic.products.categories.form');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validateRequest();

        try {
            Category::query()->create([
                'name'      => $request->get('name'),
                'image_url' => $request->hasFile('image_url')
                    ? $request->file('image_url')->store('categories', ['disk' => 'public'])
                    : null,
            ]);
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed();

            return redirect()->route('admin.product-categories.create');
        }

        return redirect()->route('admin.product-categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        /** @var Category $item */
        $item = Category::query()->findOrFail($id);
        return view('admin.metronic.products.categories.form', ['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        /** @var Category $category */
        $category = Category::query()->findOrFail($id);

        $this->validateRequest($id);

        $oldImageUrl = $category->image_url;

        $data = [
            'name' => $request->get('name'),
        ];

        if ($request->hasFile('image_url')) {
            $data['image_url'] = $request->file('image_url')->store('categories', ['disk' => 'public']);
        }

        try {
            $category->update($data);

            if ($request->hasFile('image_url') && $oldImageUrl) {
                File::delete(public_path(substr($oldImageUrl, strpos($oldImageUrl,'uploads/'))));
            }
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed();
            return redirect()->route('admin.product-categories.edit', $id);
        }

        return redirect()->route('admin.product-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        try {
            /** @var Category $item */
            $item = Category::query()->findOrFail($id);
            $imageUrl = $item->image_url;
            $item->delete();
            File::delete(public_path(substr($imageUrl, strpos($imageUrl,'uploads/'))));
            $data = ['status' => 'success', 'message' => config('constants.data_deleted_successfully')];
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                $data = ['status' => 'warning', 'message' => 'This category is already been used, delete the relations first!'];
            } else {
                Log::error($e);
                $data = ['status' => 'error', 'message' => config('constants.data_not_deleted_successfully')];
            }
        }

        return response()->json($data);
    }

    protected function validateRequest($id = null): array
    {
        return request()->validate([
            'name'      => 'required',
            'image_url' => 'file',
        ]);
    }
}
