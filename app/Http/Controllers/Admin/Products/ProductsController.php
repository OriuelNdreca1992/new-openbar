<?php

namespace App\Http\Controllers\Admin\Products;

use App\Helpers\Toastr;
use App\Models\Product\Product;
use App\Models\Product\Category;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Product::query();
            return Datatables::eloquent($data)
                ->addIndexColumn()
                ->editColumn('image_url', function($item) {
                    /** @var Product $item */
                    return view('admin.metronic.layout.partials.common.list.image_preview')->with([
                        'alt' => $item->name,
                        'src' => $item->image_url
                    ]);
                })
                ->addColumn('category', function ($item) {
                    return view('admin.metronic.layout.partials.common.list.badges.info')->with([
                        'value' => $item->category ? $item->category->name : ''
                    ]);
                })
                ->editColumn('status', function ($item) {
                    return view('admin.metronic.layout.partials.common.list.status')->with([
                        'status' => $item->status
                    ]);
                })
                ->editColumn('created_at', function ($item) {
                    return $item->created_at
                        ? date( config('rubik.base.datetime_format'), strtotime($item->created_at) )
                        : '';
                })
                ->editColumn('updated_at', function ($item) {
                    return $item->updated_at
                        ? date( config('rubik.base.datetime_format'), strtotime($item->updated_at) )
                        : '';
                })
                ->addColumn('action', function($item) {
                    return view('admin.metronic.layout.partials.common.list.actions')->with([
                        'edit' => ['url' => route('admin.products.edit', ['product' => $item->id]), 'id' => $item->id],
                        'delete' => ['id' => $item->id],
                    ]);
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('admin.metronic.products.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.metronic.products.form', ['categories' => $this->getCategoriesDropDown()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validateRequest();

        try {
            Product::query()->create([
                'category_id'   => $request->get('category_id'),
                'name'          => $request->get('name'),
                'description'   => $request->get('description'),
                'status'        => $request->get('status') == 'on',
                'image_url'     => $request->hasFile('image_url')
                    ? $request->file('image_url')->store('products', ['disk' => 'public'])
                    : null,
            ]);
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed();

            return redirect()->route('admin.products.create');
        }

        return redirect()->route('admin.products.index');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product): View
    {
        return view('admin.metronic.products.form', [
            'item' => $product,
            'categories' => $this->getCategoriesDropDown(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Product $product): RedirectResponse
    {
        $this->validateRequest($product->id);

        $oldImageUrl = $product->image_url;

        $data = [
            'category_id'   => $request->get('category_id'),
            'name'          => $request->get('name'),
            'description'   => $request->get('description'),
            'status'        => $request->get('status') == 'on',
        ];

        if ($request->hasFile('image_url')) {
            $data['image_url'] = $request->file('image_url')->store('products', ['disk' => 'public']);
        }

        try {
            $product->update($data);

            if ($request->hasFile('image_url') && $oldImageUrl) {
                File::delete(public_path(substr($oldImageUrl, strpos($oldImageUrl,'uploads/'))));
            }
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed();
            return redirect()->route('admin.products.edit', $product->id);
        }

        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product): JsonResponse
    {
        try {
            $imageUrl = $product->image_url;
            $product->delete();
            File::delete(public_path(substr($imageUrl, strpos($imageUrl,'uploads/'))));
            $data = ['status' => 'success', 'message' => config('constants.data_deleted_successfully')];
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                $data = ['status' => 'warning', 'message' => 'This product is already been used, delete the relations first!'];
            } else {
                Log::error($e);
                $data = ['status' => 'error', 'message' => config('constants.data_not_deleted_successfully')];
            }
        }

        return response()->json($data);
    }

    private function getCategoriesDropDown(): array {
        return Category::query()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
    }

    protected function validateRequest($id = null): array
    {
        return request()->validate([
            'category_id'  => 'required|integer',
            'name'         => 'required|unique:products,name,'.$id,
            'description'  => 'max:600',
            'status'       => 'sometimes',
            'image_url'    => 'sometimes|file',
        ]);
    }
}
