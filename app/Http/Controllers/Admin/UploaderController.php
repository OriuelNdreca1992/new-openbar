<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class UploaderController extends Controller
{
    /**
     * Remove a file from db & storage.
     */
    public function destroy($id): JsonResponse
    {
        try {
            $file = Media::query()->findOrFail($id);
            $file->delete();
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['success' => false, 'message' => 'File not deleted!']);
        }

        return response()->json(['success' => true, 'message' => 'File deleted successfully!']);
    }
}
