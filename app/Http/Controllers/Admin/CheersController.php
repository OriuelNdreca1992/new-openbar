<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Toastr;
use App\Http\Controllers\Controller;
use App\Http\Requests\CheersRequest;
use App\Models\Cheers;
use App\Models\Company;
use App\Models\Venue;
use App\Services\CheersService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class CheersController extends Controller
{
    protected CheersService $cheersService;

    public function __construct(CheersService $cheersService)
    {
        $this->cheersService = $cheersService;
    }

    public function index(Request $request): View|JsonResponse
    {
        if ($request->ajax()) {
            $data = Cheers::query();
            return Datatables::eloquent($data)
                ->addIndexColumn()
                ->editColumn('status', function ($item) {
                    return view('admin.metronic.layout.partials.common.list.status')->with([
                        'status' => $item->status
                    ]);
                })
                ->editColumn('start_date', function ($item) {
                    return $item->start_date
                        ? date( config('rubik.base.datetime_format'), strtotime($item->start_date) )
                        : '';
                })
                ->editColumn('end_date', function ($item) {
                    return $item->end_date
                        ? date( config('rubik.base.datetime_format'), strtotime($item->end_date) )
                        : '';
                })
                ->addColumn('action', function($item) {
                    return view('admin.metronic.layout.partials.common.list.actions')->with([
                        'preview' => ['url' => route('admin.cheers.preview', ['cheer' => $item->id]), 'id' => $item->id],
                        'edit' => ['url' => route('admin.cheers.edit', ['cheer' => $item->id]), 'id' => $item->id],
                        'delete' => ['id' => $item->id],
                    ]);
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('admin.metronic.cheers.index');
    }

    public function create(): View
    {
        return view('admin.metronic.cheers.form');
    }

    public function store(CheersRequest $request): RedirectResponse
    {
        try {
            $this->cheersService->save($request->all());
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed();
            return redirect()->route('admin.cheers.create');
        }

        return redirect()->route('admin.cheers.index');
    }

    public function edit($id): View
    {
        /** @var Cheers $item */
        $item = Cheers::query()->findOrFail($id);
        return view('admin.metronic.cheers.form', [
            'item' => $item,
            'image' => [$item->getFirstMedia(Cheers::COLLECTION)], // should be array
            'companies' => $this->getCompaniesDropDown(),
            'selected_paragraphs' => $item->paragraphs()->get()->toArray(),
        ]);
    }

    public function update(CheersRequest $request, $id): RedirectResponse
    {
        try {
            /** @var Cheers $item */
            $item = Cheers::query()->findOrFail($id);
            $this->cheersService->save($request->all(), $item);
            Toastr::itemProcessed(true);
        } catch (Exception $e) {
            Log::error($e);
            Toastr::itemProcessed();
            return redirect()->route('admin.cheers.edit', $id);
        }

        return redirect()->route('admin.cheers.index');
    }

    public function destroy($id): JsonResponse
    {
        try {
            /** @var Cheers $item */
            $item = Cheers::query()->findOrFail($id);
            $item->delete();
            $data = ['success' => config('constants.data_deleted_successfully')];
        } catch (Exception $e) {
            Log::error($e);
            $data = ['error' => config('constants.data_not_deleted_successfully')];
        }

        return response()->json($data);
    }

    public function promoters(Request $request): array
    {
        $type = $request->get('type');

        if (!$request->ajax() || is_null($type)) {
            return [];
        }

        return match ($type) {
            'venue' => Venue::query()
                ->where('status', true)
                ->orderBy('name', 'ASC')
                ->pluck('name', 'id')
                ->toArray(),
            'company' => Company::query()
                ->where('status', true)
                ->orderBy('name', 'ASC')
                ->pluck('name', 'id')
                ->toArray(),
            default => [],
        };
    }

    public function preview(Request $request, $id): View
    {
        /** @var Cheers $item */
        $item = Cheers::query()
            ->with('paragraphs')
            ->with('promoted')
            ->findOrFail($id);

        switch ($item->type) {
            case 'venue':
                $promoted = $item->promoted()->first();
                $promotedBy = $promoted->name;
                $promotedLogo = $promoted->image_publish_url;
                break;
            case 'company':
                $promoted = $item->promoted()->first();
                $promotedBy = $promoted->name;
                $promotedLogo = $promoted->image_url;
                break;
            default:
                $promotedBy = 'OpenBar';
                $promotedLogo = asset('assets/media/logos/openbar.png');
                break;
        }

        return view('admin.metronic.cheers.preview', [
            'item' => $item,
            'image' => [$item->getFirstMedia(Cheers::COLLECTION)], // should be array
            'promoted_by' => $promotedBy,
            'promoted_logo' => $promotedLogo
        ]);
    }

    private function getCompaniesDropDown(): array {
        return Company::query()->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
    }
}
