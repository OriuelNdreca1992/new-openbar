<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdministrativeData;

class AdministrativeDataController extends Controller
{
    public function edit()
    {
        $administrativeData = AdministrativeData::first();

        return view('admin.settings.administrative-data.edit', compact('administrativeData'));
    }

    public function update()
    {
        $validatedData = request()->validate([
            'name' => 'sometimes|string',
            'p_iva' => 'sometimes|string',
            'address' => 'sometimes|string',
            'email' => 'sometimes|email',
            'sdi' => 'sometimes|string',
            'bank_name' => 'sometimes|string',
            'abi' => 'sometimes|string',
            'iban' => 'sometimes|string',
            'cbi' => 'sometimes|string',

        ]);
        $administrativeData = AdministrativeData::first();

        $administrativeData->update($validatedData);

        return back()
            ->with('flash', 'Fiscal data has updated successfully!');
    }
}
