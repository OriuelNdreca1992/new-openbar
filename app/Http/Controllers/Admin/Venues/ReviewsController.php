<?php

namespace App\Http\Controllers\Admin\Venues;

use App\Models\Review;
use App\Models\Venue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class ReviewsController extends Controller
{
    public function index(Request $request, Venue $venue)
    {
        if ($request->ajax()) {
            $query = Review::query()
                ->with('user')
                ->where('venue_id', $venue->id);
            return Datatables::eloquent($query)
                ->addIndexColumn()
                ->addColumn('first_name', function ($item) {
                    return $item->user ? $item->user->first_name : '';
                })
                ->addColumn('last_name', function ($item) {
                    return $item->user ? $item->user->last_name : '';
                })
                ->editColumn('review', function ($item) {
                    $htmlStars = '<div class="rating ml-5">';
                    for ($star = 1; $star <= 5; $star++) {
                        $checked = ($item->review >= $star) ? ' checked' : '';
                        $htmlStars .= '<div class="rating-label' . $checked . '"><span class="bi bi-star-fill fs-6s"></span></div>';
                    }
                    $htmlStars .=  '</div>';
                    return $htmlStars;
                })->editColumn('created_at', function ($item) {
                    return $item->created_at->format('d.m.Y H:i');
                })
                ->rawColumns(['review'])
                ->toJson();
        }

        return view('admin.venues.reviews.index', compact('venue'));
    }
}
