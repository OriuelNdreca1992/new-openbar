<?php

namespace App\Http\Controllers\Admin\Venues;

use App\Helpers\Review;
use Carbon\Carbon;
use App\Models\Type;
use App\Models\Offer;
use App\Models\Venue;
use App\Models\Invoice;
use App\Jobs\StoreVenue;
use App\Jobs\UpdateVenue;
use App\Models\PremiumDrink;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Notifications\EnableVenue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\VenueRequest;
use App\Notifications\DisableVenue;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Notification;

class VenuesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $venues = Venue::all();

        return view('admin.venues.index', compact('venues'));
    }

      /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $types = Type::all();

        return view('admin.venues.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(VenueRequest $request): RedirectResponse
    {
        $this->dispatch(new StoreVenue($request));

        return redirect()
            ->route('admin.venues.index')
            ->with('flash', 'Venue has created successfully!');
    }

    /**
     * Display the specified resource.
     */
    public function show(Venue $venue)
    {
        return view('admin.venues.show')->with([
            'venue' => $venue,
            'score' => Review::getVenueScore($venue),
            'totalIncome' => $this->venueTotalIncome($venue->id),
            'purchasesStats' => $this->purchasesStats($venue->id),
            'clientsStats' => $this->clientsStats($venue->id),
            'premiumDrinksCurrentMonthStats' => $this->premiumDrinksCurrentMonthStats($venue->id),
            'premiumDrinksStats' => $this->premiumDrinksStats($venue->id),
            'offerDrinksStats' => $this->offerDrinksStats($venue->id),
            'opPremiumDrinksIncomeStats' => $this->opPremiumDrinksIncomeStats($venue->id),
            'venuePremiumDrinksIncomeStats' => $this->venuePremiumDrinksIncomeStats($venue->id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Venue $venue)
    {
        $types = Type::all();

        return view('admin.venues.edit', compact('venue', 'types'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(VenueRequest $request, Venue $venue): RedirectResponse
    {
        $this->dispatch(new UpdateVenue($request, $venue));

        return redirect()
            ->route('admin.venues.show', $venue->id)
            ->with('flash', 'Venue has updated successfully!');
    }

    public function updateStatus(Venue $venue): RedirectResponse
    {
        $status = request('status') == 'on';

        if ($status) {
            $venue->enable();
            Notification::send($venue->user()->first(), new EnableVenue($venue));
        } else {
            $venue->disable();
            Notification::send($venue->user()->first(), new DisableVenue($venue));
        }

        return redirect()
            ->route('admin.venues.show', $venue->id)
            ->with('flash', 'Venue status has updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Venue $venue): RedirectResponse
    {
        try {
            $venue->delete();
            return redirect()->route('admin.venues.index')->with([
                'level' => 'success',
                'flash' => 'Venue has been deleted successfully!'
            ]);
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                $message =  "Can't delete the venue due to a relationship constraint!";
            } else {
                Log::error($e);
                $message =  'Unexpected error occurred!';
            }

            return redirect()->route('admin.venues.show', $venue->id)->with([
                'level' => 'danger',
                'flash' => $message
            ]);
        }
    }

    /**
     * @throws Exception
     */
    public function getVenues(Request $request): JsonResponse
    {
        if (!$request->ajax()) {
            return new JsonResponse([]);
        }

        $query = Venue::query();

        return Datatables::eloquent($query)
            ->addIndexColumn()
            ->addColumn('review', function ($venue) {
                return Review::getVenueScore($venue);
            })
            ->addColumn('offers', function ($venue) {
                return $venue->offers->where('status', true)->count() ? 1 : 0;
            })
            ->addColumn('event', function ($venue) {
                return 1;
            })
            ->addColumn('shop', function ($venue) {
                return $venue->premiumDrinks->where('status', true)->count() ? 1 : 0;
            })
            ->editColumn('status', function ($venue) {
                return $venue->status ? 1 : 0;
            })
            ->addColumn('actions', function ($venue) {
                return $venue->id;
            })
            ->rawColumns(['review'])
            ->toJson();
    }


    //Statistics
    private function purchasesStats(int $id): Collection
    {
        return DB::table('cart_items as items')
            ->select('items.id',
                DB::raw("(count(items.id)) as total_drinks"),
                DB::raw("(count(d.id)) as total_premium"),
                DB::raw("(count(of.id)) as total_offer"),
                'c.user_id',
                'p.name as drink_name',
                'p.image_url as product_image',
                'v.name as venue_name',
                DB::raw('COALESCE(of.price, d.price) as drink_price'),
                'items.drinkable_id',
                'items.drinkable_type',
                'items.used_at',
                'c.expires_at',
                'c.type as type',
                'c.created_at as purchased_at')
            ->join('carts as c', function($join) use ($id) {
                $join->on('c.id', '=', 'items.cart_id')->where('items.venue_id', '=', $id);
            })
            ->leftJoin('offers as of', function ($join) {
                $join->on('of.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', Offer::class);
            })
            ->leftJoin('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);
            })
            ->leftJoin('products as p', function ($join) {
                $join->on('p.id', '=', DB::raw('COALESCE(of.product_id, d.product_id)'));
            })
            ->leftJoin('venues as v', function ($join) {
                $join->on('v.id', '=', DB::raw('COALESCE(of.venue_id, d.venue_id)'));
            })
            ->groupBy('drink_name')
            ->orderBy('total_drinks', 'desc')
            ->take(10)
            ->get();
    }

    private function clientsStats(int $id): array
    {
        $yearlyClients = DB::table('carts as c')
            ->select(
                DB::raw("(count(DISTINCT(`user_id`))) as total_clients"),
                DB::raw("(DATE_FORMAT(c.created_at, '%m-%Y')) as month")
            )
            ->join('cart_items as items', function($join) use ($id) {
                $join->on('items.cart_id', '=', 'c.id')->where('items.venue_id', '=', $id);
            })
            ->whereBetween('c.created_at', [Carbon::now()->subYear(), Carbon::now()])
            ->orderBy('c.created_at')
            ->groupBy(DB::raw("DATE_FORMAT(c.created_at, '%m-%Y')"))
            ->get();

        $totalClients = [];
        $months = [];
        foreach ($yearlyClients as $monthlyClients) {
            $totalClients[] = $monthlyClients->total_clients;
            $months[] = Carbon::create('1-' .$monthlyClients->month)->format('M');
        }

        $newClients = $yearlyClients->last()->total_clients ?? 0;

        return [
            'total_clients' => $totalClients,
            'months' => $months,
            'new_clients' => $newClients,
        ];
    }

    private function premiumDrinksCurrentMonthStats(int $id): array
    {
        $currentDate = Carbon::now();
        $agoDate = Carbon::parse('last day of last month')->setTime(00, 00, 00);

        $premiumDrinks = DB::table('cart_items as items')
            ->select('items.id', 'items.venue_id', 'items.used_at', 'c.expires_at')
            ->join('carts as c', function($join) use ($id) {
                $join->on('c.id', '=', 'items.cart_id')->where('items.venue_id', '=', $id);
            })
            ->where('items.drinkable_type', '=', PremiumDrink::class)
            ->get();

        $consumedDrinks = $premiumDrinks->filter(function ($drink) use ($agoDate, $currentDate) {
            return $drink->expires_at == null && $drink->used_at > $agoDate && $drink->used_at < $currentDate;
        });

        $expiredDrinks = $premiumDrinks->filter(function ($drink) use ($agoDate, $currentDate) {
            return $drink->used_at == null && $drink->expires_at > $agoDate && $drink->expires_at < $currentDate;
        });

        return [
            'consumed_drinks' => $consumedDrinks->count(),
            'expired_drinks' => $expiredDrinks->count(),
        ];
    }

    private function premiumDrinksStats(int $id): array
    {
        $agoDate = Carbon::now()->subYear();
        $currentDate = Carbon::now();

        $yearlyPremiumDrinksConsumed = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(items.used_at, '%m-%Y')) as month"),
                'items.venue_id',
                'items.used_at',
                'c.expires_at',
                'c.created_at'
            )
            ->join('carts as c', function($join) use ($id) {
                $join->on('c.id', '=', 'items.cart_id')->where('items.venue_id', '=', $id);
            })
            ->where('items.drinkable_type', PremiumDrink::class)
            ->whereNotNull('items.used_at')
            ->whereBetween('items.used_at', [$agoDate, $currentDate])
            ->groupBy('month')
            ->get();

        $yearlyPremiumDrinksExpired = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(c.expires_at, '%m-%Y')) as month"),
                'items.used_at',
                'c.expires_at',
                'c.created_at'
            )
            ->join('carts as c', function($join) use ($id) {
                $join->on('c.id', '=', 'items.cart_id')->where('items.venue_id', '=', $id);
            })
            ->where('items.drinkable_type', PremiumDrink::class)
            ->whereNull('items.used_at')
            ->whereBetween('c.expires_at', [$agoDate, $currentDate])
            ->groupBy('month')
            ->get();

        $monthsOfConsumed = [];
        $monthsOfExpired = [];
        $premiumConsumed = [];
        $premiumExpired = [];

        foreach ($yearlyPremiumDrinksConsumed as $drink) {
           $premiumConsumed[] = $drink->total;
           $monthsOfConsumed[] = Carbon::create($drink->used_at)->format('M-Y');
        }

        foreach ($yearlyPremiumDrinksExpired as $drink) {
           $premiumExpired[] = $drink->total;
           $monthsOfExpired[] = Carbon::create($drink->expires_at)->format('M-Y');
        }

        $premiumConsumedAndMonths = array_combine($monthsOfConsumed, $premiumConsumed);
        $premiumExpiredAndMonths = array_combine($monthsOfExpired, $premiumExpired);

        $months = array_unique(array_merge($monthsOfConsumed, $monthsOfExpired));

        usort($months, function($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        foreach ($months as $month) {
            if (! isset($premiumConsumedAndMonths[$month])) {
                $premiumConsumedAndMonths[$month] = 0;
            }

            if (! isset($premiumExpiredAndMonths[$month])) {
                $premiumExpiredAndMonths[$month] = 0;
            }
        }

        uksort($premiumConsumedAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        uksort($premiumExpiredAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        return [
            'premium_consumed' => array_values($premiumConsumedAndMonths),
            'premium_expired' => array_values($premiumExpiredAndMonths),
            'months' => $months,
        ];
    }

    private function offerDrinksStats(int $id): array
    {
        $agoDate = Carbon::now()->subYear();
        $currentDate = Carbon::now();

        $yearlyOfferDrinksConsumed = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(items.used_at, '%m-%Y')) as month"),
                'items.used_at',
                'c.expires_at',
                'c.created_at'
            )
            ->join('carts as c', function($join) use ($id) {
                $join->on('c.id', '=', 'items.cart_id')->where('items.venue_id', '=', $id);
            })
            ->where('items.drinkable_type', Offer::class)
            ->whereNotNull('items.used_at')
            ->whereBetween('items.used_at', [$agoDate, $currentDate])
            ->groupBy('month')
            ->get();

        $monthsOfConsumed = [];
        $offerConsumed = [];

        foreach ($yearlyOfferDrinksConsumed as $drink) {
           $offerConsumed[] = $drink->total;
           $monthsOfConsumed[] = Carbon::create($drink->used_at)->format('M-Y');
        }

        $offerConsumedAndMonths = array_combine($monthsOfConsumed, $offerConsumed);

        $months = array_unique(array_merge($monthsOfConsumed));

        usort($months, function($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        foreach ($months as $month) {
            if (! isset($offerConsumedAndMonths[$month])) {
                $offerConsumedAndMonths[$month] = 0;
            }
        }

        uksort($offerConsumedAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        return [
            'offer_consumed' => array_values($offerConsumedAndMonths),
            'months' => $months,
        ];
    }

    private function opPremiumDrinksIncomeStats(int $id): array
    {
        $yearlyIncome = DB::table('carts as c')
            ->select(
                DB::raw("(sum(c.user_fee)) as total_income"),
                DB::raw("(DATE_FORMAT(c.created_at, '%m-%Y')) as month")
            )
            ->join('cart_items as items', 'items.cart_id', '=', 'c.id')
            ->where('items.venue_id', $id)
            ->whereBetween('c.created_at', [Carbon::now()->subYear(), Carbon::now()])
            ->orderBy('c.created_at')
            ->groupBy('month')
            ->get();

        $totalIncome = [];
        $months = [];

        foreach ($yearlyIncome as $monthlyIncome) {
            $totalIncome[] = $monthlyIncome->total_income;
            $months[] = Carbon::create('1-' .$monthlyIncome->month)->format('M-Y');
        }

        return [
            'total_income' => $totalIncome,
            'months' => $months,
        ];
    }

    private function venuePremiumDrinksIncomeStats(int $id): array
    {
        $yearlyIncome = DB::table('cart_items as items')
            ->select(
                DB::raw("(sum(items.price)) as total_income"),
                DB::raw("(DATE_FORMAT(c.expires_at, '%m-%Y')) as month"),
            )
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.drinkable_type', PremiumDrink::class)
            ->where('items.venue_id', $id)
            ->whereNull('items.used_at')
            ->whereBetween('c.expires_at', [Carbon::now()->subYear(), Carbon::now()])
            ->groupBy('month')
            ->orderBy('c.expires_at')
            ->get();

        $totalIncome = [];
        $months = [];

        foreach ($yearlyIncome as $monthlyIncome) {
            $totalIncome[] = $monthlyIncome->total_income;
            $months[] = Carbon::create('1-' .$monthlyIncome->month)->format('M-Y');
        }

        return [
            'total_income' => $totalIncome,
            'months' => $months,
        ];
    }

    private function venueTotalIncome(int $id)
    {
        return DB::table('cart_items as items')
            ->join('carts as c', function($join) use ($id) {
                $join->on('c.id', '=', 'items.cart_id')->where('items.venue_id', '=', $id);
            })
            ->whereNotNull('items.used_at')
            ->join('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);
            })
            ->sum(DB::raw('items.price'));
    }
}
