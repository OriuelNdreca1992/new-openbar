<?php

namespace App\Http\Controllers\Admin\Venues;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PurchasesController extends Controller
{
    public function index(): View
    {
        return view('admin.purchases.index');
    }

    public function show($id): View
    {
        $cart = Cart::query()->findOrFail($id);;
        return view('admin.purchases.show', compact('cart'));
    }

    public function drinks(): View
    {
        $id = request('id');
        return view('admin.purchases.items.index', compact('id'));
    }

    /**
     * @throws Exception
     */
    public function getPurchases(Request $request): JsonResponse
    {
        if ($request->ajax()) {
            $query = Cart::query();
            $datatables = Datatables::eloquent($query)
                ->addIndexColumn()
                ->addColumn('user', function ($item) {
                    return $item->user?->getFullNameWithID();
                })->addColumn('actions', function ($item) {
                    return $item->id;
                })->addColumn('total_amount', function ($item) {
                    return number_format($item->amount + $item->user_fee, 2, '.', ',');
                })->editColumn('created_at', function ($item) {
                    return Carbon::parse($item->created_at)->format('Y-m-d H:i');
                });

            return $datatables->make();
        }

        return response()->json([]);
    }

    /**
     * @throws Exception
     */
    public function getPurchaseItems(Request $request): JsonResponse
    {
        if ($request->ajax()) {
            $query = CartItem::query()->where('cart_id', $request->id);

            $datatables = Datatables::eloquent($query)
                ->addIndexColumn()
                ->addColumn('drink_name', function ($item) {
                    return $item->name;
                })->addColumn('offered_to', function ($item) {
                    return ($item->cart && $item->cart->offered) ? $item->cart->offered->getFullNameWithID() : null;
                })->addColumn('venue_name', function ($item) {
                    return ($item->venue) ? $item->venue->name : '';
                })->addColumn('type', function ($item) {
                    return ($item->cart) ? $item->cart->type : null;
                })->addColumn('status', function ($item) {
                    return ($item->cart) ? $item->cart->status : null;
                })->addColumn('used_expires_at', function ($item) {
                    if ($item->cart->status === Cart::STATUSES[Cart::STATUS_SUCCEEDED]
                        && $item->used_at) {
                        return $item->used_at->format('Y-m-d H:i');
                    } elseif ($item->cart->status === Cart::STATUSES[Cart::STATUS_SUCCEEDED]
                        && $item->cart->expires_at
                        && $item->cart->expires_at < Carbon::now()) {
                        return $item->cart->expires_at?->format('Y-m-d H:i');
                    }
                    return NULL;
                })->editColumn('created_at', function ($item) {
                    return $item->cart->created_at?->format('Y-m-d H:i');
                });

            return $datatables->make();
        }

        return response()->json([]);
    }
}
