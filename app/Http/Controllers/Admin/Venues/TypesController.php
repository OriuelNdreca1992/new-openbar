<?php

namespace App\Http\Controllers\Admin\Venues;

use App\Models\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::get()->sortDesc();

        return view('admin.venues.venue-types.index', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        Type::create(request()->validate([
            'name' => 'required|unique:types',
        ]));

        return back()->with('flash', 'The type has created successfully!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VenueType  $venueType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $type->update(
            request()->validate([
                'name' => 'required|string',
        ]));

        return back()->with('flash', 'The type has updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VenueType  $venueType
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $type->delete();

        return back()->with('flash', 'The type has deleted successfully!');
    }

    public function getTypeById()
    {
        $type = Type::where('id', request('id'))->first();

        return response()
            ->json($type);
    }
}
