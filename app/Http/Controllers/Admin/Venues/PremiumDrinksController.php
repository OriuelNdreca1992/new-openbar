<?php

namespace App\Http\Controllers\Admin\Venues;

use App\Models\Venue;
use App\Models\PremiumDrink;
use App\Models\Product\Product;
use App\Models\Product\Category;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class PremiumDrinksController extends Controller
{
    public function index(Venue $venue): View
    {
        $premiumDrinks = $venue->premiumDrinks()->get()->sortDesc();

        return view('admin.venues.premium-drinks.index', compact('premiumDrinks', 'venue'));
    }

    public function create(Venue $venue): View
    {
        $defaultProducts = Product::query()->get()->sortDesc();
        $categories = Category::query()->get()->sortDesc();

        return view('admin.venues.premium-drinks.create', compact('defaultProducts', 'categories', 'venue'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function store(Venue $venue): RedirectResponse
    {
        $venue->premiumDrinks()->create($this->validateRequest());

        return redirect()
            ->route('admin.venue.premium-drinks.index', request()->route('venue'))
            ->with('flash', 'Premium Drink has created successfully!');
    }

    public function edit(Venue $venue, PremiumDrink $drink): View
    {
        $defaultProducts = Product::query()->get()->sortDesc();
        $categories = Category::query()->get()->sortDesc();

        return view('admin.venues.premium-drinks.edit', compact('drink', 'defaultProducts', 'categories', 'venue'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function update(Venue $venue, PremiumDrink $drink): RedirectResponse
    {
        $drink->update($this->validateRequest());

        return redirect()
            ->route('admin.venue.premium-drinks.index', request()->route('venue'))
            ->with('flash', 'Premium drink has updated successfully!');
    }

    public function destroy(Venue $venue, PremiumDrink $drink): RedirectResponse
    {
        $drink->delete();

        return redirect()
            ->route('admin.venue.premium-drinks.index', request()->route('venue'))
            ->with('flash', 'Premium drink has deleted successfully!');
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function validateRequest(): array
    {
        $validatedData = request()->validate([
            'product_id'        => 'required',
            'price'             => 'required',
            'promotional_price' => 'nullable|lt:price',
            'description'       => 'max:600',
        ]);

        $validatedData['status'] = request()->get('status') == 'on';

        return $validatedData;
    }
}
