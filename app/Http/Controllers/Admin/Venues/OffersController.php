<?php

namespace App\Http\Controllers\Admin\Venues;

use App\Models\Offer;
use App\Models\OfferDay;
use App\Models\Venue;
use App\Models\Product\Product;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;

class OffersController extends Controller
{
    public function index(Venue $venue): View
    {
        $offers = $venue->offers()->get()->sortDesc();

        return view('admin.venues.offers.index', compact('offers', 'venue'));
    }

    public function create(Venue $venue): View
    {
        $products = Product::query()
            ->where('status', true)
            ->get()
            ->sortDesc();

        return view('admin.venues.offers.create', compact('products', 'venue'));
    }

    public function store(Venue $venue): RedirectResponse
    {
        $this->validateRequest();

        DB::transaction(function () use ($venue) {
            /** @var Offer $offer */
            $offer = $venue->offers()->create([
                'product_id'    => request('product_id'),
                'price'         => request('price'),
                'quantity'      => request('quantity'),
                'status'        => request('status'),
                'start'         => request('start'),
                'end'           => request('end'),
                'all_the_time'  => request('all_the_time'),
                'description'   => request('description'),
            ]);

            if (request('days')) {
                foreach (request('days') as $day) {
                    $offer->offerDays()->create(['week_day' => $day]);
                }
            }
        });

        return redirect()
            ->route('admin.venue.offers.index', request()->route('venue'))
            ->with('flash', 'Offer has created successfully!');
    }

    public function edit(Venue $venue, Offer $offer): View
    {
        $products = Product::query()
            ->where('status', true)
            ->get()
            ->sortDesc();

        return view('admin.venues.offers.edit', compact('offer', 'products', 'venue'));
    }

    public function update(Venue $venue, Offer $offer): RedirectResponse
    {
        $this->validateRequest();

        DB::transaction(function () use ($offer) {
            $offer->update([
                'product_id'   => request('product_id'),
                'price'        => request('price'),
                'quantity'     => request('quantity'),
                'status'       => request('status'),
                'start'        => request('start'),
                'end'          => request('end'),
                'all_the_time' => request('all_the_time'),
                'description'  => request('description'),
            ]);

            $offerDayIds = [];
            if (request('days')) {
                foreach (request('days') as $day) {
                    $createdOrUpdated = $offer->offerDays()->updateOrCreate(['week_day' => $day]);
                    $offerDayIds[] = $createdOrUpdated->id;
                }
            }

            $deleteOfferDays = OfferDay::query()
                ->where('offer_id', $offer->id)
                ->get()
                ->except($offerDayIds);
            foreach ($deleteOfferDays as $offerDays) {
                $offerDays->delete();
            }
        });

        return redirect()
            ->route('admin.venue.offers.index', request()->route('venue'))
            ->with('flash', 'Offer has updated successfully!');
    }

    public function destroy(): RedirectResponse
    {
        Offer::query()->findOrFail(request('id'))->delete();

        return redirect()
            ->route('admin.venue.offers.index', request()->route('venue'))
            ->with('flash', 'Offer has deleted successfully!');
    }

    protected function validateRequest(): array
    {
        return request()->validate([
            'product_id'    => 'required|integer',
            'price'         => 'required',
            'quantity'      => 'required',
            'start'         => 'sometimes',
            'end'           => 'sometimes',
            'days'          => 'sometimes',
            'status'        => 'sometimes',
        ]);
    }
}
