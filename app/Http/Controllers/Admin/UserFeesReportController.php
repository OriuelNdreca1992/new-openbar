<?php

namespace App\Http\Controllers\Admin;

use App\Models\UserFeeReport;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\View\View;
use Yajra\DataTables\Facades\DataTables;

class UserFeesReportController extends Controller
{
    public function index(): View
    {
        return view('admin.reports.user-fees');
    }

    /**
     * @throws Exception
     */
    public function getReports()
    {
        $datatables = Datatables::of(UserFeeReport::query()->latest());

        return $datatables
            ->addIndexColumn()
            ->make(true);
    }
}
