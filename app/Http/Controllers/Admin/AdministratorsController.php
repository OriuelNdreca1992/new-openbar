<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Policies\AdministratorPolicy;
use App\Rules\ValidPhoneNumber;

class AdministratorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $administrators = User::with('roles')->whereHas("roles", function ($q) {
             $q->whereIn("name", ['super-admin', 'admin', 'digital-event']);
        })->get()->sortDesc();

        return view('admin.administrators.index', compact('administrators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('admin.administrators.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = $this->validateRequest();
        $data['password'] = \Hash::make($data['password']);
        $data['status'] = 1;

        $user = User::create($data);

        $user->assignRole($data['role']);

        return redirect()
            ->route('admin.administrators.index')
            ->with('flash', 'Administrator has created successfully!');
    }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $administrator)
    {
        return view('admin.administrators.edit', compact('administrator'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(User $administrator)
    {
        $data = $this->validateRequest($administrator->id);

        if ($data['role'] !== $administrator->role) {
            $administrator->syncRoles($data['role']);
        }

        $administrator->update($data);

        return redirect()
            ->route('admin.administrators.index')
             ->with('flash', 'Administrator has updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $administrator
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $administrator)
    {
        $this->authorize(AdministratorPolicy::DELETE, $administrator);

        $administrator->delete();

        return redirect()
            ->route('admin.administrators.index')
             ->with('flash', 'Administrator has deleted successfully!');;
    }

    protected function validateRequest($id = null)
    {
        $validateData = request()->validate([
            'first_name' => ['required', 'string', 'max:64'],
            'last_name'  => ['required', 'string', 'max:64'],
            'email'      => ['required', 'string', 'email', 'max:64', 'unique:users,email,'.$id],
            'phone'      => ['sometimes', 'string', 'min:10', new ValidPhoneNumber($id)],
            'role'       => ['required', 'numeric'],
            'password'   => ['sometimes', 'string', 'min:8', 'confirmed'],
        ]);

        return $validateData;
    }
}
