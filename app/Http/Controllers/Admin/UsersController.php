<?php

namespace App\Http\Controllers\Admin;

use App\Rules\ValidPhoneNumber;
use Carbon\Carbon;
use Exception;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Mail\RegisterAppUser;
use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;
USE Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    /**
     * @throws Exception
     */
    public function index(Request $request): View|JsonResponse
    {
        if ($request->ajax()) {
            $query = User::query()->where('role', 5);
            return Datatables::eloquent($query)
                ->addIndexColumn()
                ->addColumn('register_type', function ($request) {
                    if ($request->socials->count() === 0) {
                        if (empty($request->email)) {
                            return view('admin.metronic.layout.partials.common.list.badges.danger')->with(['value' => 'Unregistered ']);
                        }
                        return view('admin.metronic.layout.partials.common.list.badges.secondary')->with(['value' => 'Email']);
                    }
                    return match ($request->socials[0]->provider) {
                        'apple' => view('admin.metronic.layout.partials.common.list.badges.dark')->with(['value' => 'Apple']),
                        'facebook' => view('admin.metronic.layout.partials.common.list.badges.primary')->with(['value' => 'Facebook']),
                        default =>  view('admin.metronic.layout.partials.common.list.badges.warning')->with(['value' => $request->socials[0]->provider]),
                    };
                })
                ->editColumn('status', function ($request) {
                    return view('admin.metronic.layout.partials.common.list.status')->with(['status' => $request->status]);
                })
                ->editColumn('birthdate', function ($request) {
                    return view('admin.metronic.layout.partials.common.list.badges.secondary')->with(['value' => $request->birthdate?->format('d.m.Y')]);
                })
                ->editColumn('created_at', function ($request) {
                    return $request->created_at->format('d.m.Y');
                })
                ->addColumn('action', function($item) {
                    return view('admin.metronic.layout.partials.common.list.actions')->with([
                        'show' => ['url' => route('admin.users.show', ['user' => $item->id]), 'id' => $item->id],
                        'edit' => ['url' => route('admin.users.edit', ['user' => $item->id]), 'id' => $item->id],
                        'delete' => ['id' => $item->id],
                    ]);
                })
                ->rawColumns(['register_type', 'action'])
                ->toJson();
        }

        return view('admin.metronic.users.index');
    }

    public function create(): View
    {
        return view('admin.metronic.users.create');
    }

    public function store(): RedirectResponse
    {
        $data = $this->validateRequest();
        $data['password'] = Hash::make($data['password']);
        $data['role'] = 5;

        DB::transaction(function() use ($data) {
            /** @var User $user */
            $user = User::query()->create($data);
            $user->assignRole($data['role']);

            Mail::to($user->email)->send(new RegisterAppUser($user, request('password')));
        });

        return redirect()->route('admin.users.index')->with('flash', 'User has been created successfully!');
    }

    public function show(User $user): View
    {
        return view('admin.metronic.users.show', compact('user'));
    }

    public function edit(User $user): View
    {
        return view('admin.metronic.users.edit', compact('user'));
    }

    public function update(User $user): RedirectResponse
    {
        $data = $this->validateRequest($user->id);
        $user->update($data);

        return redirect()->route('admin.users.index')->with('flash', 'User has been updated successfully!');
    }

    public function destroy(User $user): JsonResponse
    {
        try {
            $user->delete();
            $data = ['status' => 'success', 'message' => config('constants.data_deleted_successfully')];
        } catch (Exception $e) {
            Log::error($e);
            $data = ['status' => 'error', 'message' => config('constants.data_not_deleted_successfully')];
        }

        return response()->json($data);
    }

    protected function validateRequest($id = null): array
    {
        return request()->validate([
            'first_name' => ['required', 'string', 'max:64'],
            'last_name'  => ['required', 'string', 'max:64'],
            'email'      => ['required', 'string', 'email', 'max:64', 'unique:users,email,'.$id],
            'phone'      => ['required', 'string', 'min:10', new ValidPhoneNumber($id)],
            'gender'     => ['required'],
            'birthdate'  => ['nullable', 'date', 'date_format:Y-m-d', 'before:today'],
            'password'   => ['sometimes', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * @throws Exception
     */
    public function getPurchased(Request $request): JsonResponse
    {
        if ($request->ajax()) {
            $query = Cart::query()->where('user_id', $request->get('id'));
            return Datatables::eloquent($query)
                ->addIndexColumn()
                ->addColumn('user', function ($item) {
                    return $item->offered?->getFullNameWithID();
                })->addColumn('actions', function ($item) {
                    return $item->id;
                })->editColumn('created_at', function ($item) {
                    return $item->created_at->format('Y-m-d H:i');
                })->make();
        }

        return response()->json([]);
    }

    /**
     * @throws Exception
     */
    public function getGifted(Request $request): JsonResponse
    {
        if ($request->ajax()) {
            $query = Cart::query()->where('offered_to', $request->get('id'));
            return Datatables::eloquent($query)
                ->addIndexColumn()
                ->addColumn('user', function ($item) {
                    return $item->user?->getFullNameWithID();
                })->addColumn('actions', function ($item) {
                    return $item->id;
                })->editColumn('created_at', function ($item) {
                    return $item->created_at->format('Y-m-d H:i');
                })->make();
        }

        return response()->json([]);
    }

    public function showPurchasedItems(User $user): View
    {
        return view('admin.metronic.users.purchased.index', compact('user'));
    }

    public function showGiftedItems(User $user): View
    {
        return view('admin.metronic.users.gifted.index', compact('user'));
    }

    /**
     * @throws Exception
     */
    public function getPurchasedItems(Request $request): JsonResponse
    {
        if ($request->ajax()) {
            $query = CartItem::query()->where('cart_id', $request->route()->parameter('cart'));

            return Datatables::eloquent($query)
                ->addIndexColumn()
                ->addColumn('drink_name', function ($item) {
                    return $item->name;
                })->addColumn('offered_to', function ($item) {
                    return ($item->cart && $item->cart->offered) ? $item->cart->offered->getFullNameWithID() : null;
                })->addColumn('venue_name', function ($item) {
                    return $item->venue?->name;
                })->addColumn('type', function ($item) {
                    return $item->cart?->type;
                })->addColumn('status', function ($item) {
                    return $item->cart?->status;
                })->addColumn('created_at', function ($item) {
                    return $item->cart?->created_at->format('Y-m-d H:i');
                })->addColumn('used_expires_at', function ($item) {
                    if ($item->cart->status === Cart::STATUSES[Cart::STATUS_SUCCEEDED]
                        && $item->used_at) {
                        return $item->used_at->format('Y-m-d H:i');
                    } elseif ($item->cart->status === Cart::STATUSES[Cart::STATUS_SUCCEEDED]
                        && $item->cart->expires_at
                        && $item->cart->expires_at < Carbon::now()) {
                        return $item->cart->expires_at->format('Y-m-d H:i');
                    }
                    return NULL;
                })->make();
        }

        return response()->json([]);
    }
}
