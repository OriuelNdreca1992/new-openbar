<?php

namespace App\Http\Controllers;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Traits\Encryption;
use Carbon\Carbon;
use Illuminate\Http\Request;


class DrinkController extends Controller
{
    use Encryption;

    public function details(Request $request, $transaction, $hash)
    {
        if ($error_message = $request->get('error_message')) {
            return view('front.drink-claim', ['error_message' => $error_message]);
        }

        /** @var CartItem $item */
        $item = CartItem::query()
            ->withAndWhereHas('cart', function ($query) use ($transaction) {
                $query
                    ->where('transaction_id', $transaction)
                    ->where('type', Cart::TYPE_PREMIUM)
                    ->where('status', Cart::STATUS_SUCCEEDED);
            })
            ->has('cart.offered')
            ->with('cart.offered')
            ->find($this->decrypt($hash));

        if (is_null($item)) {
            return view('front.drink-claim', ['error_message' => 'Drink non trovato.']);
        }

        if ($item->used_at) {
            return view('front.drink-claim', ['error_message' => 'Drink già servito.']);
        }

        if ($item->cart()->first()->expires_at < Carbon::now()) {
            return view('front.drink-claim', ['error_message' => 'Drink scaduto.']);
        }

        if ($request->get('confirm')) {
            $item->update(['used_at' => Carbon::now()]);
            return view('front.drink-claim');
        }

        return view('front.drink-details', [
            'item' => $item
        ]);
    }
}
