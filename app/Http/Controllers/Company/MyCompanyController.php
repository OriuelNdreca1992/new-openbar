<?php

namespace App\Http\Controllers\Company;

use App\Models\Company;
use App\Jobs\UpdateCompany;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;

class MyCompanyController extends Controller
{
    public function show()
    {
        $company = auth()->user()->company;

        return view('company.show', compact('company'));
    }

    public function edit()
    {
        $company = auth()->user()->company;

        return view('company.edit', compact('company'));
    }

    public function update(CompanyRequest $request)
    {
        $company = auth()->user()->company;

        $this->dispatchNow(new UpdateCompany($request, $company));

        return redirect()
            ->route('company.show')
            ->with('flash', 'Company has updated successfully!');
    }
}
