<?php

namespace App\Http\Controllers\Venue;

use App\Models\Offer;
use App\Models\Venue;
use App\Models\Product\Product;
use App\Http\Controllers\Controller;

class OffersController extends Controller
{
    public function index()
    {
        $offers = auth()->user()->venue->offers->sortDesc();

        return view('venue.offers.index', compact('offers'));
    }

    public function create()
    {
        $products = Product::get()->sortDesc();

        return view('venue.offers.create', compact('products'));
    }

    public function store()
    {
        $this->validateRequest();

        \DB::transaction(function () {
            $offer = auth()->user()->venue->offers()->create([
                'product_id'   => request('product_id'),
                'price'        => request('price'),
                'quantity'     => request('quantity'),
                'status'       => request('status'),
                'start'        => request('start'),
                'end'          => request('end'),
                'all_the_time' => request('all_the_time'),
                'description'  => request('description'),
            ]);

            if (request('days')) {
                foreach (request('days') as $key => $day) {
                    $offer->offerDays()->create(['week_day' => $day]);
                }
            }
        });

        return redirect()
            ->route('venue.offers.index', request()->route('venue'))
            ->with('flash', 'Offer has created successfully!');
    }

    public function edit(Venue $venue, Offer $offer)
    {
        $products = Product::get()->sortDesc();

        return view('venue.offers.edit', compact('offer', 'products'));
    }

    public function update(Venue $venue, Offer $offer)
    {
        $this->validateRequest();

        \DB::transaction(function () use ($offer) {
            $offer->update([
                'product_id'   => request('product_id'),
                'price'        => request('price'),
                'quantity'     => request('quantity'),
                'status'       => request('status'),
                'start'        => request('start'),
                'end'          => request('end'),
                'all_the_time' => request('all_the_time'),
                'description'  => request('description'),
            ]);

            $offer->offerDays()->delete();

            if (request('days')) {
                foreach (request('days') as $day) {
                    $offer->offerDays()->create(['week_day' => $day]);
                }
            }
        });

        return redirect()
            ->route('venue.offers.index', request()->route('venue'))
            ->with('flash', 'Offer has updated successfully!');
    }

    public function destroy(Offer $offer)
    {
        $offer->delete();

        return redirect()
            ->route('venue.offers.index', request()->route('venue'))
            ->with('flash', 'Offer has deleted successfully!');
    }

    protected function validateRequest()
    {
        return request()->validate([
            'product_id'  => 'required|integer',
            'price'       => 'required',
            'quantity'    => 'required',
            'start'       => 'sometimes',
            'end'         => 'sometimes',
            'days'        => 'sometimes',
            'status'      => 'sometimes',
            'description' => 'required',
        ]);
    }
}
