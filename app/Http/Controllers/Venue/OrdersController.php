<?php

namespace App\Http\Controllers\Venue;

use App\Models\Offer;
use App\Models\PremiumDrink;
use Exception;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    public function index()
    {
        return view('venue.orders.index');
    }

    /**
     * @throws Exception
     */
    public function getOrders(Request $request)
    {
        $orderItems = DB::table('cart_items as items')
            ->select('items.id as drink_id',
                'c.offered_to',
                'items.name as drink_name',
                'items.price as drink_price',
//                'c.status', ToDo
                'items.used_at',
                'c.expires_at',
                'c.type as type',
                'c.created_at as purchased_at')
            ->leftJoin('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.venue_id', auth()->user()->venue->id)
            ->whereNotNull('items.used_at')
            ->leftJoin('offers as of', function ($join) {
                $join->on('of.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', Offer::class);
            })
            ->leftJoin('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);
            })
            ->leftJoin('products as p', function ($join) {
                $join->on('p.id', '=', DB::raw('COALESCE(of.product_id, d.product_id)'));
            })
            ->orderByDesc('c.created_at');

        $datatables = Datatables::of($orderItems);

        if ($type = $request['columns'][5]['search']['value']) {
            $datatables->filterColumn('type', function($query, $type) {
                $query->where('c.type', $type);
            });
        }

        if ($date_range = $request['columns'][6]['search']['value']) {
            $datatables->filterColumn('purchased_at', function($query, $date_range) {
                [$start, $end] = explode('|', $date_range);
                $query->whereBetween('c.created_at', [$start, $end]);
            });
        }

        if ($drink_id = $request['columns'][1]['search']['value']) {
            $datatables->filterColumn('drink_id', function($query, $drink_id) {
                $query->where('items.id', $drink_id);
            });
        }

        return $datatables->addIndexColumn()->make(true);
    }
}
