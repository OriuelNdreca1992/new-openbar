<?php

namespace App\Http\Controllers\Venue;

use Carbon\Carbon;
use App\Models\Venue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CalendarController extends Controller
{
    public function index(Venue $venue)
    {
        return view('venue.calendar.index');
    }

    public function getOffers(Request $request)
    {
        $start = Carbon::parse($request->input('start'));
        $end = Carbon::parse($request->input('end'));

        $venue = auth()->user()->venue;
        $venue->load(['offers' => function ($query) use ($start, $end) {
            $query->where('start', '<=', $end->format('Y-m-d'))
                ->where('end', '>=', $start->format('Y-m-d'))
                ->orWhere('all_the_time', '=', true);
        }, 'offers.offerDays']);

        $events = [];

        foreach ($venue->offers as $offer) {
            if ($offer->all_the_time) {
                $events[] = [
                    'title' => $offer->product->name,
                    'start' => $offer->created_at,
                    'end' => $end,
                    'description' => $offer->description,
                    'className' => "fc-event-light fc-event-solid-primary",
                ];
            }
            $offer_start = $offer->start;
            $offer_end = $offer->end;
            $offer_days = $offer->offerDays->pluck('offer_id', 'week_day');

            while ($offer_start < $offer_end) {
                $day_of_week = $offer_start->dayOfWeek;

                if (isset($offer_days[$day_of_week])) {
                    while (isset($offer_days[$day_of_week + 1])) {
                        $day_of_week++;
                    }
                    $events[] = [
                        'title' => $offer->product->name,
                        'start' => $offer_start->format('Y-m-d 00:00:00'),
                        'end' => $offer_start->addDays($day_of_week - $offer_start->dayOfWeek)->format('Y-m-d 23:59:59'),
                        'description' => $offer->description,
                        'className' => "fc-event-light fc-event-solid-primary",
                    ];
                }

                $offer_start->addDay();
            }
        }

        return response()->json($events);
    }
}
