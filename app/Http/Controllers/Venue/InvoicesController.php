<?php

namespace App\Http\Controllers\Venue;

use App\Models\Venue;
use App\Models\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class InvoicesController extends Controller
{
    public function index()
    {
        return view('venue.invoices.index');
    }

    public function getInvoices(Request $request)
    {
        if ($request->ajax()) {
            $query = Invoice::select('*')
                ->with('subject')
                ->whereIn('subject_type', [Venue::class])
                ->where('subject_id', auth()->user()->venue->id)
                ->orderBy('created_at', 'desc')
                ->get();

                $datatables =  Datatables::of($query)
                    ->addColumn('subject_type', function (Invoice $invoice) {
                        return class_basename($invoice->subject_type);
                    })->addColumn('subject_name', function (Invoice $invoice) {
                        return class_basename($invoice->subject->name);
                    })->addColumn('actions', function (Invoice $invoice) {
                        return $invoice->id;
                    });

                return $datatables
                    ->addIndexColumn()
                    ->make(true);
        }
    }
}
