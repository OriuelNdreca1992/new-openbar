<?php

namespace App\Http\Controllers\Venue;

use App\Models\Venue;
use App\Models\PremiumDrink;
use App\Models\Product\Product;
use App\Models\Product\Category;
use App\Http\Controllers\Controller;

class PremiumDrinksController extends Controller
{
    public function index()
    {
        $premiumDrinks = auth()->user()->venue->premiumDrinks->sortDesc();

        return view('venue.premium-drinks.index', compact('premiumDrinks'));
    }

    public function create()
    {
        $defaultProducts = Product::get()->sortDesc();
        $categories = Category::get()->sortDesc();

        return view('venue.premium-drinks.create', compact('defaultProducts', 'categories'));
    }

    public function store(Venue $venue)
    {
        auth()->user()->venue->premiumDrinks()->create($this->validateRequest());

        return redirect()
            ->route('venue.premium-drinks.index', request()->route('venue'))
            ->with('flash', 'Premium Drink has created successfully!');
    }

    public function edit(Venue $venue, PremiumDrink $drink)
    {
        $defaultProducts = Product::get()->sortDesc();
        $categories = Category::get()->sortDesc();

        return view('venue.premium-drinks.edit', compact('drink', 'defaultProducts', 'categories'));
    }

    public function update(Venue $venue, PremiumDrink $drink)
    {
        $drink->update($this->validateRequest());

       return back()
            ->with('flash', 'Premium drink has updated successfully!');
    }

    public function destroy(Venue $venue, PremiumDrink $drink)
    {
       $drink->delete();

        return redirect()
            ->route('venue.premium-drinks.index', request()->route('venue'))
            ->with('flash', 'Premium drink has deleted successfully!');
    }

    public function validateRequest()
    {
        $validatedData = request()->validate([
            'product_id'        => 'required',
            'price'             => 'required',
            'description'       => 'required|max:600',
        ]);

        if (request('promotional_price') !== null) {
            request()->validate([
                'promotional_price' => 'lt:price',
            ]);

            $validatedData['promotional_price'] = request('promotional_price');
        }

        if (request()->has('status')) {
            $validatedData['status'] = 1;
        } else {
            $validatedData['status'] = 0;
        }

        return $validatedData;
    }
}
