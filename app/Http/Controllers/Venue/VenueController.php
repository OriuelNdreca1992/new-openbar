<?php

namespace App\Http\Controllers\Venue;

use App\Models\Type;
use App\Models\Venue;
use App\Jobs\UpdateVenue;
use App\Http\Requests\VenueRequest;
use App\Http\Controllers\Controller;

class VenueController extends Controller
{
    public function show()
    {
        $venue = auth()->user()->venue;

        return view('venue.show', compact('venue'));
    }

    public function edit()
    {
        $types = Type::get()->sortDesc();
        $venue = auth()->user()->venue;

        return view('venue.edit', compact('venue', 'types'));
    }

    public function update(VenueRequest $request)
    {
        $venue = auth()->user()->venue;

        $this->dispatchSync(new UpdateVenue($request, $venue));

        return redirect()
            ->route('venue.show')
            ->with('flash', 'Venue has updated successfully!');
    }
}
