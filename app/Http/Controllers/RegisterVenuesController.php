<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Jobs\StoreVenue;
use App\Http\Requests\VenueRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class RegisterVenuesController extends Controller
{
     /**
     * @return View
     */
    public function showRegisterVenueForm(): View
    {
        $types = Type::query()->get();

        return view('register-venue', compact('types'));
    }

    /**
     * @param VenueRequest $request
     * @return RedirectResponse
     */
    public function store(VenueRequest $request): RedirectResponse
    {
        $this->dispatchNow(new StoreVenue($request));

        return redirect()->route('thank-you');
    }
}
