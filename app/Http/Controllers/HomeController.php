<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Offer;
use App\Models\Venue;
use App\Models\Invoice;
use App\Models\PremiumDrink;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     */
    public function index()
    {
        if (auth()->user()->hasRole('super-admin|admin')) {
            $adminVenuesStats = $this->adminVenuesStats();
            $adminUsersStats = $this->adminUsersStats();
            $adminPurchasesStats = $this->adminPurchasesStats();
            $adminPremiumDrinksCurrentMonthStats = $this->adminPremiumDrinksCurrentMonthStats();
            $adminPremiumDrinksStats = $this->adminPremiumDrinksStats();
            $adminOfferDrinksStats = $this->adminOfferDrinksStats();
            $adminOpPremiumDrinksIncomeStats = $this->adminOpPremiumDrinksIncomeStats();
            $adminVenuePremiumDrinksIncomeStats = $this->adminVenuePremiumDrinksIncomeStats();

            return view('admin.home', compact('adminUsersStats', 'adminVenuesStats', 'adminPurchasesStats', 'adminPremiumDrinksCurrentMonthStats', 'adminPremiumDrinksStats', 'adminOfferDrinksStats', 'adminOpPremiumDrinksIncomeStats', 'adminVenuePremiumDrinksIncomeStats'));
        }

        if (auth()->user()->hasRole('venue')) {
            $venueTotalIncome = $this->venueTotalIncome();
            $venuePurchasesStats = $this->venuePurchasesStats();
            $venueClientsStats = $this->venueClientsStats();
            $venueDrinksStats = $this->venueDrinksStats();
            $opPremiumDrinksIncomeStats = $this->opPremiumDrinksIncomeStats();
            $venuePremiumDrinksIncomeStats = $this->venuePremiumDrinksIncomeStats();

            return view('venue.home', compact('venueTotalIncome', 'venuePurchasesStats', 'venueClientsStats', 'venueDrinksStats', 'opPremiumDrinksIncomeStats', 'venuePremiumDrinksIncomeStats'));
        }

        if (auth()->user()->hasRole('company')) {
            return view('company.home');
        }

        if (auth()->user()->hasRole('digital-event')) {
            return view('admin.metronic.digitalEvents.home');
        }
    }

    private function adminUsersStats(): array
    {
        $yearlyUsers = User::query()->select(
                DB::raw("(count(id)) as total_users"),
                DB::raw("(DATE_FORMAT(created_at, '%m-%Y')) as month")
            )
             ->whereBetween('created_at',
                [Carbon::now()->subYear(), Carbon::now()]
            )
            ->where('role', User::ROLE_USER)
            ->orderBy('created_at')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%m-%Y')"))
            ->get();

        $totalUsers = [];
        $months = [];
        foreach ($yearlyUsers as $monthlyUsers) {
            $totalUsers[] = $monthlyUsers->total_users;
            $months[] = Carbon::create('1-' .$monthlyUsers->month)->format('M');
        }

        return [
            'total_users' => $totalUsers,
            'months' => $months,
            'new_users' => $yearlyUsers->last() ? $yearlyUsers->last()->total_users: null,
        ];
    }

    private function adminVenuesStats(): array
    {
        $yearlyVenues = Venue::query()->select(
                DB::raw("(count(id)) as total_venues"),
                DB::raw("(DATE_FORMAT(created_at, '%m-%Y')) as month")
            )
             ->whereBetween('created_at',
                [Carbon::now()->subYear(), Carbon::now()]
            )
            ->orderBy('created_at')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%m-%Y')"))
            ->get();

        $totalVenues = [];
        $months = [];
        foreach ($yearlyVenues as $monthlyVenues) {
            $totalVenues[] = $monthlyVenues->total_venues;
            $months[] = Carbon::create('1-' .$monthlyVenues->month)->format('M');
        }

        return [
            'total_venues' => $totalVenues,
            'months' => $months,
            'new_venues' => $yearlyVenues->last() ? $yearlyVenues->last()->total_venues : null,
        ];
    }

    private function adminPurchasesStats(): Collection
    {
        $agoDate = Carbon::now()->subYear();
        $currentDate = Carbon::now();

        return DB::table('cart_items as items')
            ->select('items.id',
                DB::raw("(count(items.id)) as total_drinks"),
                DB::raw("(count(d.id)) as total_premium"),
                DB::raw("(count(of.id)) as total_offer"),
                'c.user_id',
                'items.name as drink_name',
                'p.image_url as product_image',
                'items.venue_id',
                'v.name as venue_name',
                DB::raw('COALESCE(of.price, d.price) as drink_price'),
                'items.drinkable_id',
                'items.drinkable_type',
                'items.used_at',
                'c.expires_at',
                'c.type as type',
                'c.created_at as purchased_at')
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->join('venues as v', 'v.id', '=', 'items.venue_id')
            ->leftJoin('offers as of', function ($join) {
                $join->on('of.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', Offer::class);
            })
            ->leftJoin('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);
            })
            ->leftJoin('products as p', function ($join) {
                $join->on('p.id', '=', DB::raw('COALESCE(of.product_id, d.product_id)'));
            })
            ->whereBetween('c.created_at', [$agoDate, $currentDate])
            ->groupBy('drink_name')
            ->orderBy('total_drinks', 'desc')
            ->take(10)
            ->get();
    }

    private function adminPremiumDrinksCurrentMonthStats(): array
    {
        $currentDate = Carbon::now();
        $agoDate = Carbon::parse('last day of last month')->setTime(00, 00, 00);

        $premiumDrinks = DB::table('cart_items as items')
            ->select('items.id', 'items.used_at', 'c.expires_at')
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.drinkable_type', '=', PremiumDrink::class)
            ->whereBetween('c.created_at', [$agoDate, $currentDate])
            ->get();

        $consumedDrinks = $premiumDrinks->filter(function ($drink) use ($agoDate, $currentDate) {
            return $drink->expires_at == null && $drink->used_at > $agoDate && $drink->used_at < $currentDate;
        });

        $expiredDrinks = $premiumDrinks->filter(function ($drink) use ($agoDate, $currentDate) {
            return $drink->used_at == null && $drink->expires_at > $agoDate && $drink->expires_at < $currentDate;
        });

        return [
            'consumed_drinks' => $consumedDrinks->count(),
            'expired_drinks' => $expiredDrinks->count(),
        ];
    }

    private function adminPremiumDrinksStats(): array
    {
        $agoDate = Carbon::now()->subYear();
        $currentDate = Carbon::now();

        $yearlyPremiumDrinksConsumed = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(items.used_at, '%m-%Y')) as month"),
                'items.used_at',
                'c.expires_at',
                'c.created_at'
            )
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.drinkable_type', PremiumDrink::class)
            ->whereNotNull('items.used_at')
            ->whereBetween('items.used_at', [$agoDate, $currentDate])
            ->groupBy('month')
            ->get();

        $yearlyPremiumDrinksExpired = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(c.expires_at, '%m-%Y')) as month"),
                'items.used_at',
                'c.expires_at',
                'c.created_at'
            )
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.drinkable_type', PremiumDrink::class)
            ->whereNull('items.used_at')
            ->whereBetween('c.expires_at', [$agoDate, $currentDate])
            ->groupBy('month')
            ->get();

        $monthsOfConsumed = [];
        $monthsOfExpired = [];
        $premiumConsumed = [];
        $premiumExpired = [];

        foreach ($yearlyPremiumDrinksConsumed as $drink) {
           $premiumConsumed[] = $drink->total;
           $monthsOfConsumed[] = Carbon::create($drink->used_at)->format('M-Y');
        }

        foreach ($yearlyPremiumDrinksExpired as $drink) {
           $premiumExpired[] = $drink->total;
           $monthsOfExpired[] = Carbon::create($drink->expires_at)->format('M-Y');
        }

        $premiumConsumedAndMonths = array_combine($monthsOfConsumed, $premiumConsumed);
        $premiumExpiredAndMonths = array_combine($monthsOfExpired, $premiumExpired);

        $months = array_unique(array_merge($monthsOfConsumed, $monthsOfExpired));

        usort($months, function($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        foreach ($months as $month) {
            if (! isset($premiumConsumedAndMonths[$month])) {
                $premiumConsumedAndMonths[$month] = 0;
            }

            if (! isset($premiumExpiredAndMonths[$month])) {
                $premiumExpiredAndMonths[$month] = 0;
            }
        }

        uksort($premiumConsumedAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        uksort($premiumExpiredAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        return [
            'premium_consumed' => array_values($premiumConsumedAndMonths),
            'premium_expired' => array_values($premiumExpiredAndMonths),
            'months' => $months,
        ];
    }

    private function adminOfferDrinksStats(): array
    {
        $agoDate = Carbon::now()->subYear();
        $currentDate = Carbon::now();

        $yearlyOfferDrinksConsumed = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(items.used_at, '%m-%Y')) as month"),
                'items.used_at',
                'c.expires_at',
                'c.created_at')
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.drinkable_type', Offer::class)
            ->whereNotNull('items.used_at')
            ->whereBetween('items.used_at', [$agoDate, $currentDate])
            ->groupBy('month')
            ->get();

        $monthsOfConsumed = [];
        $offerConsumed = [];

        foreach ($yearlyOfferDrinksConsumed as $drink) {
           $offerConsumed[] = $drink->total;
           $monthsOfConsumed[] = Carbon::create($drink->used_at)->format('M-Y');
        }

        $offerConsumedAndMonths = array_combine($monthsOfConsumed, $offerConsumed);

        $months = array_unique(array_merge($monthsOfConsumed));

        usort($months, function($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        foreach ($months as $month) {
            if (! isset($offerConsumedAndMonths[$month])) {
                $offerConsumedAndMonths[$month] = 0;
            }
        }

        uksort($offerConsumedAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        return [
            'offer_consumed' => array_values($offerConsumedAndMonths),
            'months' => $months,
        ];
    }

    private function adminOpPremiumDrinksIncomeStats(): array
    {
        $yearlyIncome = DB::table('carts as c')
            ->select(
                DB::raw("(sum(c.user_fee)) as total_income"),
                DB::raw("(DATE_FORMAT(c.created_at, '%m-%Y')) as month")
            )
            ->whereBetween('c.created_at', [Carbon::now()->subYear(), Carbon::now()])
            ->orderBy('created_at')
            ->groupBy('month')
            ->get();

        $totalIncome = [];
        $months = [];

        foreach ($yearlyIncome as $monthlyIncome) {
            $totalIncome[] = $monthlyIncome->total_income;
            $months[] = Carbon::create('1-' .$monthlyIncome->month)->format('M-Y');
        }

        return [
            'total_income' => $totalIncome,
            'months' => $months,
        ];
    }

    private function adminVenuePremiumDrinksIncomeStats(): array
    {
        $yearlyIncome = DB::table('cart_items as items')
            ->select(
                DB::raw("(sum(items.price)) as total_income"),
                DB::raw("(DATE_FORMAT(c.expires_at, '%m-%Y')) as month"),
            )
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.drinkable_type', PremiumDrink::class)
            ->whereNull('items.used_at')
            ->whereBetween('c.expires_at', [Carbon::now()->subYear(), Carbon::now()])
            ->groupBy('month')
            ->orderBy('c.expires_at')
            ->get();

        $totalIncome = [];
        $months = [];

        foreach ($yearlyIncome as $monthlyIncome) {
            $totalIncome[] = $monthlyIncome->total_income;
            $months[] = Carbon::create('1-' .$monthlyIncome->month)->format('M-Y');
        }

        return [
            'total_income' => $totalIncome,
            'months' => $months,
        ];
    }

    //Venue Stats

    private function venueTotalIncome()
    {
        return DB::table('cart_items as items')
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.venue_id', auth()->user()->venue->id)
            ->whereNotNull('items.used_at')
            ->leftJoin('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);

            })
            ->sum(DB::raw('items.price'));
    }

    private function venuePurchasesStats(): Collection
    {
        return DB::table('cart_items as items')
            ->select('items.id',
                DB::raw("(count(items.id)) as total_drinks"),
                DB::raw("(count(d.id)) as total_premium"),
                DB::raw("(count(of.id)) as total_offer"),
                'c.user_id',
                'items.name as drink_name',
                'p.image_url as product_image',
                'v.name as venue_name',
                DB::raw('COALESCE(of.price, d.price) as drink_price'),
                'items.drinkable_id',
                'items.drinkable_type',
                'items.used_at',
                'c.expires_at',
                'c.type as type',
                'c.created_at as purchased_at')
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.venue_id', auth()->user()->venue->id)
            ->leftJoin('offers as of', function ($join) {
                $join->on('of.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', Offer::class);
            })
            ->leftJoin('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);
            })
            ->leftJoin('products as p', function ($join) {
                $join->on('p.id', '=', DB::raw('COALESCE(of.product_id, d.product_id)'));
            })
            ->leftJoin('venues as v', function ($join) {
                $join->on('v.id', '=', DB::raw('COALESCE(of.venue_id, d.venue_id)'));
            })
            ->groupBy('drink_name')
            ->orderBy('total_drinks', 'desc')
            ->take(10)
            ->get();
    }

    private function venueClientsStats(): array
    {
        $yearlyClients = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(DISTINCT(c.user_id))) as total_clients"),
                DB::raw("(DATE_FORMAT(c.created_at, '%m-%Y')) as month"),
            )
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.venue_id', auth()->user()->venue->id)
            ->whereBetween('c.created_at',[Carbon::now()->subYear(), Carbon::now()])
            ->orderBy('c.created_at')
            ->groupBy(DB::raw("DATE_FORMAT(c.created_at, '%m-%Y')"))
            ->get();

        $totalClients = [];
        $months = [];
        foreach ($yearlyClients as $monthlyClients) {
            $totalClients[] = $monthlyClients->total_clients;
            $months[] = Carbon::create('1-' .$monthlyClients->month)->format('M');
        }

        $newClients = $yearlyClients->last()->total_clients ?? 0;

        return [
            'total_clients' => $totalClients,
            'months' => $months,
            'new_clients' => $newClients,
        ];
    }

    private function venueDrinksStats(): array
    {
        $agoDate = Carbon::now()->subYear();
        $currentDate = Carbon::now();

        $yearlyPremiumDrinksConsumed = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(items.used_at, '%m-%Y')) as month"),
                'items.used_at',
                'c.expires_at',
                'c.created_at'
            )
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.venue_id', auth()->user()->venue->id)
            ->where('items.drinkable_type', PremiumDrink::class)
            ->whereBetween('c.created_at', [$agoDate, $currentDate])
            ->whereNotNull('items.used_at')
            ->groupBy('month')
            ->get();

        $yearlyOfferDrinksConsumed = DB::table('cart_items as items')
            ->select(
                DB::raw("(count(items.id)) as total"),
                DB::raw("(DATE_FORMAT(items.used_at, '%m-%Y')) as month"),
                'items.used_at',
                'c.expires_at',
                'c.created_at'
            )
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->where('items.venue_id', auth()->user()->venue->id)
            ->where('items.drinkable_type', Offer::class)
            ->whereBetween('c.created_at', [$agoDate, $currentDate])
            ->whereNotNull('items.used_at')
            ->groupBy('month')
            ->get();

        $monthsOfPremium = [];
        $monthsOfOffers = [];
        $premiumConsumed = [];
        $offersConsumed = [];

        foreach ($yearlyPremiumDrinksConsumed as $drink) {
           $premiumConsumed[] = $drink->total;
           $monthsOfPremium[] = Carbon::create($drink->used_at)->format('M-Y');
        }

        foreach ($yearlyOfferDrinksConsumed as $drink) {
           $offersConsumed[] = $drink->total;
           $monthsOfOffers[] = Carbon::create($drink->expires_at)->format('M-Y');
        }

        $premiumConsumedAndMonths = array_combine($monthsOfPremium, $premiumConsumed);
        $offersConsumedAndMonths = array_combine($monthsOfOffers, $offersConsumed);

        $months = array_unique(array_merge($monthsOfPremium, $monthsOfOffers));

        usort($months, function($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        foreach ($months as $month) {
            if (! isset($premiumConsumedAndMonths[$month])) {
                $premiumConsumedAndMonths[$month] = 0;
            }

            if (! isset($offersConsumedAndMonths[$month])) {
                $offersConsumedAndMonths[$month] = 0;
            }
        }

        uksort($premiumConsumedAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        uksort($offersConsumedAndMonths, function ($a, $b) {
            return strtotime($a) <=> strtotime($b);
        });

        return [
            'premium_consumed' => array_values($premiumConsumedAndMonths),
            'offer_consumed' => array_values($offersConsumedAndMonths),
            'months' => $months,
        ];
    }

    private function opPremiumDrinksIncomeStats(): array
    {
        $yearlyIncome = Invoice::query()->select(
                DB::raw("(sum(open_bar_income)) as total_income"),
                DB::raw("(DATE_FORMAT(created_at, '%m-%Y')) as month")
            )
             ->whereBetween('created_at',
                [Carbon::now()->subYear(), Carbon::now()]
            )
            ->where('subject_type', Venue::class)
            ->where('subject_id', auth()->user()->venue->id)
            ->orderBy('created_at')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%m-%Y')"))
            ->get();

        $totalIncome = [];
        $months = [];

        foreach ($yearlyIncome as $monthlyIncome) {
            $totalIncome[] = $monthlyIncome->total_income;
            $months[] = Carbon::create('1-' .$monthlyIncome->month)->format('M-Y');
        }

        return [
            'total_income' => $totalIncome,
            'months' => $months,
        ];
    }

    private function venuePremiumDrinksIncomeStats(): array
    {
        $yearlyIncome = Invoice::query()->select(
                DB::raw("(sum(venue_income)) as total_income"),
                DB::raw("(DATE_FORMAT(created_at, '%m-%Y')) as month")
            )
             ->whereBetween('created_at',
                [Carbon::now()->subYear(), Carbon::now()]
            )
            ->where('subject_type', Venue::class)
            ->where('subject_id', auth()->user()->venue->id)
            ->orderBy('created_at')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%m-%Y')"))
            ->get();

        $totalIncome = [];
        $months = [];

        foreach ($yearlyIncome as $monthlyIncome) {
            $totalIncome[] = $monthlyIncome->total_income;
            $months[] = Carbon::create('1-' .$monthlyIncome->month)->format('M-Y');
        }

        return [
            'total_income' => $totalIncome,
            'months' => $months,
        ];
    }

    public function deleteMedia(Request $request): JsonResponse
    {
        $filename = $request->get('filename');
        if (empty($filename)) {
            return response()->json(false, 500);
        }

        try {
            $files = Media::query()->where('file_name', $filename)->get();
            foreach ($files as $file) {
                $file->delete();
            }
        } catch (Exception $e) {
            return response()->json(false, 500);
        }

        return response()->json(true);
    }
}

