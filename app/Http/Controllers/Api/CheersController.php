<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cheers;
use App\Repositories\Api\CheersRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class CheersController extends Controller
{
    protected CheersRepository $cheersRepository;

    public function __construct(CheersRepository $cheersRepository)
    {
        $this->cheersRepository = $cheersRepository;
    }

    /**
     * @OA\Get(
     *    tags={"Cheers"},
     *    path="/api/cheers",
     *    description="Get cheers.",
     *    security={ {"bearer": {} }},
     *    @OA\Parameter(name="page", in="query", description="Set pagination.",
     *       @OA\Schema(type="integer", example="2")
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="array", @OA\Items(allOf={
     *          @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Cheers")}),
     *       })),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $cheers = $this->cheersRepository->findAll();
        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($cheers->paginate()->getCollection());
    }

    /**
     * @OA\Get(
     *    tags={"Cheers"},
     *    path="/api/cheers/{id}",
     *    description="Get cheer data.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Cheers")}),
     *          @OA\Schema(
     *             @OA\Property(property="promoted", type="object", allOf={@OA\Schema(ref="#/components/schemas/CheersParagraph")}),
     *          ),
     *          @OA\Schema(
     *             @OA\Property(property="paragraphs", type="array", @OA\Items(allOf={@OA\Schema(ref="#/components/schemas/CheersParagraph")})),
     *          ),
     *       }),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Cheer not found!")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            /** @var Cheers $cheers */
            $cheers = $this->cheersRepository->find($id);

            if (empty($cheers)) return response()->json(['message' => trans('response.cheers.404')], 404);

        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($cheers);
    }
}
