<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\Offer;
use App\Traits\Encryption;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;

class WalletController extends Controller
{
    use Encryption;

    /**
     * @OA\Get(
     *    tags={"Wallet"},
     *    path="/api/wallet",
     *    description="Get wallet data.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(
     *             @OA\Property(property="isOfferUsedToday", type="boolean", example=false),
     *             @OA\Property(property="gifts", type="array", @OA\Items(allOf={
     *                @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/CartItem")}),
     *                @OA\Schema(
     *                   @OA\Property(property="premium_drink", type="object", allOf={
     *                      @OA\Schema(
     *                         @OA\Property(property="id", type="integer", example=1),
     *                         @OA\Property(property="price", type="string", example="10.00"),
     *                         @OA\Property(property="promotional_price", type="string", nullable=true, example="8.00"),
     *                         @OA\Property(property="description", type="string", example="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
     *                         @OA\Property(property="discount", type="string", example="20%"),
     *                         @OA\Property(property="product_id", type="integer", example=1),
     *                         @OA\Property(property="product", type="object", allOf={
     *                            @OA\Schema(
     *                               @OA\Property(property="id", type="integer", example=1),
     *                               @OA\Property(property="name", type="string", example="Beer"),
     *                               @OA\Property(property="image_url", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                            ),
     *                         }),
     *                      ),
     *                   }),
     *                   @OA\Property(property="venue", type="object", allOf={
     *                      @OA\Schema(
     *                         @OA\Property(property="id", type="integer", example=1),
     *                         @OA\Property(property="name", type="string", example="Larkin, Waters and Bahringer"),
     *                         @OA\Property(property="address", type="string", example="405 Charles Lane\nAngelicaview, TN 69121-1697"),
     *                         @OA\Property(property="lat", type="string", example="17.9401390000000"),
     *                         @OA\Property(property="lng", type="string", example="-2.249014000000"),
     *                         @OA\Property(property="city", type="string", example="East Elian"),
     *                         @OA\Property(property="image_publish_url", type="string", example=null),
     *                      ),
     *                   }),
     *                   @OA\Property(property="cart", type="object", allOf={
     *                      @OA\Schema(
     *                         @OA\Property(property="id", type="integer", example=1),
     *                         @OA\Property(property="user_id", type="integer", example=2),
     *                         @OA\Property(property="expires_at", type="string", example="2022-03-25T21:46:03.000000Z"),
     *                         @OA\Property(property="user", type="object", allOf={
     *                            @OA\Schema(
     *                               @OA\Property(property="id", type="integer", example=1),
     *                               @OA\Property(property="first_name", type="string", example="Name"),
     *                               @OA\Property(property="last_name", type="string", example="Surname"),
     *                               @OA\Property(property="avatar", type="object", allOf={
     *                                  @OA\Schema(
     *                                     @OA\Property(property="thumb", type="string", example=""),
     *                                     @OA\Property(property="original", type="string", example=""),
     *                                  ),
     *                               }),
     *                            ),
     *                         }),
     *                      ),
     *                   }),
     *                ),
     *             })),
     *          ),
     *       }),
     *    ),
     * )
     */
    public function wallet(): JsonResponse
    {
        $isOfferUsedToday = Cart::query()
            ->where('user_id', auth()->id())
            ->whereHas('items', function ($query) {
                return $query
                    ->where('drinkable_type', Offer::class)
                    ->whereDate('used_at', Carbon::today());
            })
            ->exists();

        $gifts = CartItem::query()
            ->whereHas('cart', function ($query) {
                $query
                    ->where('offered_to', auth()->id())
                    ->where('type', Cart::TYPE_PREMIUM)
                    ->where('status', Cart::STATUS_SUCCEEDED)
                    ->where('expires_at', '>', Carbon::now());
            })
            ->with('premiumDrink.product:id,name,image_url')
            ->with('premiumDrink:id,price,promotional_price,description,product_id')
            ->with('venue:id,name,address,lat,lng,city,image_publish_url')
            ->with('cart.user:id,first_name,last_name')
            ->with('cart:id,user_id,expires_at')
            ->whereNull('used_at')
            ->get();

        return response()->json([
            'isOfferUsedToday' => $isOfferUsedToday,
            'gifts' => $gifts
        ]);
    }

    /**
     * @OA\Get(
     *    tags={"Wallet"},
     *    path="/api/wallet/{id}/check",
     *    description="Check claim of the gift.",
     *    security={ {"bearer": {} }},
     *    @OA\Parameter(in="path", required=true, name="id", example="1"),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={@OA\Schema(
     *          @OA\Property(property="gift", type="object", allOf={
     *             @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/CartItem")}),
     *             @OA\Schema(
     *                @OA\Property(property="premium_drink", type="object", allOf={@OA\Schema(
     *                   @OA\Property(property="id", type="integer", example=1),
     *                   @OA\Property(property="description", type="string", example="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
     *                   @OA\Property(property="discount", type="string", example="20%"),
     *                   @OA\Property(property="product_id", type="integer", example=1),
     *                   @OA\Property(property="product", type="object", allOf={@OA\Schema(
     *                      @OA\Property(property="id", type="integer", example=1),
     *                      @OA\Property(property="image_url", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                   )}),
     *                )}),
     *                @OA\Property(property="venue", type="object", allOf={@OA\Schema(
     *                   @OA\Property(property="id", type="integer", example=1),
     *                   @OA\Property(property="name", type="string", example="Larkin, Waters and Bahringer"),
     *                   @OA\Property(property="image_publish_url", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                )}),
     *             ),
     *          }),
     *          @OA\Property(property="url", type="string", example="https://www.google.com/"),
     *       )}),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Not found!")),
     *    ),
     *    @OA\Response(response=409, description="Conflict",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Already used!")),
     *    ),
     * )
     */
    public function checkClaim(int $id): JsonResponse
    {
        /** @var CartItem $cartItem */
        $cartItem = CartItem::query()
            ->whereHas('cart', function ($query) {
                $query
                    ->where('offered_to', auth()->id())
                    ->where('type', Cart::TYPE_PREMIUM)
                    ->where('status', Cart::STATUS_SUCCEEDED)
                    ->where('expires_at', '>', Carbon::now());
            })
            ->with('premiumDrink.product:id,image_url')
            ->with('premiumDrink:id,description,product_id')
            ->with('venue:id,name,image_publish_url')
            ->find($id);

        if (empty($cartItem))
            return response()->json(['message' => trans('response.404')], 404);

        if (!empty($cartItem->used_at))
            return response()->json(['message' => trans('response.premium.409')], 409);

        return response()->json([
            'gift' => $cartItem,
            'url' => route('drink.details', [
                'transaction'=> $cartItem->cart()->first()->transaction_id,
                'hash' => $this->encrypt($cartItem->id),
            ]),
        ]);
    }
}
