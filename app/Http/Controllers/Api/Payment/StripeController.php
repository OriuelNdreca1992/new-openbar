<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use App\Models\Checkout\Cart;
use App\Models\User;
use App\Notifications\Activities\PurchasedDrinksActivity;
use App\Notifications\Push\PurchasedDrinksNotification;
use App\Traits\StripeHelper;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Stripe;
use Stripe\Exception\SignatureVerificationException;
use UnexpectedValueException;

class StripeController extends Controller
{
    use StripeHelper;

    /**
     * @OA\Get(
     *    tags={"Stripe"},
     *    path="/api/stripe/payment/cards",
     *    description="Get Stripe User Cards.",
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(
     *             @OA\Property(property="object", type="string", example="list"),
     *             @OA\Property(property="data", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="has_more", type="boolean", example=false),
     *             @OA\Property(property="url", type="string", example="/v1/payment_methods"),
     *          )
     *       })
     *    )
     * )
     *
     * @throws Stripe\Exception\ApiErrorException
     */
    public function getCards(Request $request): JsonResponse
    {
        $user = $request->user();

        Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        $this->createCustomer($user);

        $cards =  Stripe\PaymentMethod::all(['customer' => $user->stripe_id, 'type' => 'card']);

        return response()->json($cards);
    }

    /**
     * @OA\Post(
     *    tags={"Stripe"},
     *    path="/api/stripe/payment/method",
     *    description="Payment method for Stripe User.",
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"token"},
     *          @OA\Property(property="token", type="string", example="tok_threeDSecure2Required"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(
     *             @OA\Property(property="id", type="string", example="pm_1LGkAhBVN56aHDIQX5hOCcBD"),
     *             @OA\Property(property="object", type="string", example="payment_method"),
     *             @OA\Property(property="billing_details", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="card", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="created", type="string", example="1656683171"),
     *             @OA\Property(property="customer", type="string", example="cus_LuLODFN9ROYFp3"),
     *             @OA\Property(property="livemode", type="boolean", example=false),
     *             @OA\Property(property="metadata", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="type", type="string", example="card"),
     *          )
     *       })
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(type="object", allOf={@OA\Schema(
     *          @OA\Property(property="message", type="string", example="token"),
     *       )})
     *    )
     * )
     *
     * @throws Stripe\Exception\ApiErrorException
     */
    public function paymentMethod(Request $request): JsonResponse
    {
        $data = $request->all();
        $user = $request->user();

        Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        if (empty($data['token'])) {
            return response()->json(['message' => 'token'], 400);
        }

        $paymentMethod = StripeHelper::createPaymentMethod($data['token']);

        $this->createCustomer($user);

        return response()->json($paymentMethod);
    }

    /**
     *  @OA\Post(
     *    tags={"Stripe"},
     *    path="/api/stripe/payment/attach",
     *    description="Attach Stripe User Card.",
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"payment_method_id"},
     *          @OA\Property(property="payment_method_id", type="string", example="pm_card_authenticationRequiredOnSetup"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(
     *             @OA\Property(property="id", type="string", example="pm_1LGkAhBVN56aHDIQX5hOCcBD"),
     *             @OA\Property(property="object", type="string", example="payment_method"),
     *             @OA\Property(property="billing_details", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="card", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="created", type="string", example="1656683171"),
     *             @OA\Property(property="customer", type="string", example="cus_LuLODFN9ROYFp3"),
     *             @OA\Property(property="livemode", type="boolean", example=false),
     *             @OA\Property(property="metadata", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="type", type="string", example="card"),
     *          )
     *       })
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(type="object", allOf={@OA\Schema(
     *          @OA\Property(property="message", type="string", example="payment_method_id"),
     *       )})
     *    )
     * )
     *
     * @throws Stripe\Exception\ApiErrorException
     */
    public function attachCard(Request $request): JsonResponse
    {
        $user = $request->user();
        $data = $request->all();

        Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        if (empty($data['payment_method_id'])) {
            return response()->json(['message' => 'payment_method_id'], 400);
        }

        $this->createCustomer($user);

        $response = StripeHelper::attachCard($user->stripe_id, $data['payment_method_id']);

        return response()->json($response);
    }

    /**
     * @OA\Post(
     *    tags={"Stripe"},
     *    path="/api/stripe/payment/detach",
     *    description="Detach Stripe User Card.",
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"payment_method_id"},
     *          @OA\Property(property="payment_method_id", type="string", example="pm_card_authenticationRequiredOnSetup"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(
     *             @OA\Property(property="id", type="string", example="pm_1LGkAhBVN56aHDIQX5hOCcBD"),
     *             @OA\Property(property="object", type="string", example="payment_method"),
     *             @OA\Property(property="billing_details", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="card", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="created", type="string", example="1656683171"),
     *             @OA\Property(property="customer", type="string", example="cus_LuLODFN9ROYFp3"),
     *             @OA\Property(property="livemode", type="boolean", example=false),
     *             @OA\Property(property="metadata", type="array", @OA\Items(), example={}),
     *             @OA\Property(property="type", type="string", example="card"),
     *          )
     *       })
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(type="object", allOf={@OA\Schema(
     *          @OA\Property(property="message", type="string", example="payment_method_id"),
     *       )})
     *    )
     * )
     *
     *
     * @throws Stripe\Exception\ApiErrorException
     */
    public function detachCard(Request $request): JsonResponse
    {
        $user = $request->user();
        $data = $request->all();

        Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        if (empty($data['payment_method_id'])) {
            return response()->json(['message' => 'payment_method_id'], 400);
        }

        $response = StripeHelper::detachCard($user->stripe_id, $data['payment_method_id']);

        return response()->json($response);
    }

    /**
     * @OA\Post(
     *    tags={"Venue"},
     *    path="/api/stripe/payment/webhook",
     *    description="Stripe webhook.",
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(oneOf={
     *          @OA\Schema(type="json", example={"succeeded":"pi_1HeMdDBVN56aHDIQ375EQn4W"}),
     *          @OA\Schema(type="json", example={"payment_failed":"pi_1HeMdDBVN56aHDIQ375EQn4W"}),
     *       })
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="unexpected_event_type", type="string", example="type")),
     *    )
     * )
     */
    public function webhook(Request $request): JsonResponse
    {
        Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        $event = null;
        try {
            $event = Stripe\Webhook::constructEvent(
                $request->getContent(),
                $request->header('stripe-signature'),
                config('services.stripe.webhook.secret'),
                config('services.stripe.webhook.tolerance')
            );
        } catch(UnexpectedValueException $e) {
            // Invalid payload
            return response()->json(['UnexpectedValueException' => $event],400);
        } catch(SignatureVerificationException $e) {
            // Invalid signature
            return response()->json([
                'SignatureVerification' => $event,
                'data' => ['payload' => $request->getContent(), 'header' => $request->header('stripe-signature')]
            ],400);
        }

        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object;
                /** @var Cart $cart */
                $cart = Cart::query()->where('payment_intent_id', $paymentIntent->id)->first();
                if ($cart) {
                    $cart->status = Cart::STATUS_SUCCEEDED;
                    $cart->save();

                    // save activity
                    dispatch(new PurchasedDrinksActivity($cart));
                    // send notification
                    dispatch(new PurchasedDrinksNotification($cart));
                }
                return response()->json(['succeeded' => $paymentIntent->id]);
            case 'payment_intent.payment_failed':
                $paymentIntent = $event->data->object;
                $cart = Cart::query()->where('payment_intent_id', $paymentIntent->id)->first();
                if ($cart) {
                    $cart->status = Cart::STATUS_FAILED;
                    $cart->save();
                }
                return response()->json(['payment_failed' => $paymentIntent->id]);
            // handle other event types
            default:
                // unexpected event type
                return response()->json(['unexpected_event_type' => $event->type],400);
        }
    }

    /**
     * @throws Stripe\Exception\ApiErrorException
     */
    private function createCustomer(User $user): void
    {
        if (empty($user->stripe_id)) {
            $customer = Stripe\Customer::create([
                "description" => 'App User '.$user->first_name.' '.$user->last_name,
            ]);
            $user->stripe_id = $customer->id;
            $user->save();
        }
    }
}
