<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class SliderController extends Controller
{
    /**
     * @OA\Get(
     *    tags={"Cheers"},
     *    path="/api/sliders/{index}",
     *    description="Get slider dara.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Slider")}),
     *          @OA\Schema(
     *             @OA\Property(property="venues", type="array", @OA\Items(allOf={
     *                @OA\Schema(ref="#/components/schemas/Venue")
     *             })),
     *          ),
     *       }),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function show($position): JsonResponse
    {
        $index = (is_numeric($position) && $position > 0) ? $position - 1 : 0;

        try {
            $slider = Slider::query()
                ->with(['venues' => function($query) {
                    $query->where('status', true);
                    $query->take(10);
                }])
                ->skip($index)
                ->first();
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($slider);
    }
}
