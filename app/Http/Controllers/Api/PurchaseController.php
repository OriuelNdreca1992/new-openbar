<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Api\PurchaseRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PurchaseController extends Controller
{
    protected PurchaseRepository $purchaseRepository;

    public function __construct(PurchaseRepository $purchaseRepository)
    {
        $this->purchaseRepository = $purchaseRepository;
    }

    /**
     * @OA\Get(
     *    tags={"Purchase"},
     *    path="/api/auth/purchases",
     *    description="Get all purchases.",
     *    security={ {"bearer": {} }},
     *    @OA\Parameter(name="page", in="query", description="Set pagination.",
     *       @OA\Schema(type="integer", example=2)
     *    ),
     *    @OA\Parameter(name="premium", in="query", description="Request premium drinks in result.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Parameter(name="2x1", in="query", description="Request 2x1 drinks in result.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="array", @OA\Items(allOf={
     *          @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Cart")}),
     *          @OA\Schema(
     *             @OA\Property(property="offered", type="object", allOf={@OA\Schema(ref="#/components/schemas/User")}),
     *             @OA\Property(property="items", type="array", @OA\Items(allOf={
     *                @OA\Schema(ref="#/components/schemas/CartItem"),
     *                @OA\Schema(
     *                   @OA\Property(property="venue", type="object", allOf={@OA\Schema(ref="#/components/schemas/Venue")}),
     *                ),
     *             })),
     *          ),
     *       })),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function index(Request $request): JsonResponse
    {
        try {
            $purchases = $this->purchaseRepository->findAll(
                filter_var($request->get('premium'), FILTER_VALIDATE_BOOLEAN),
                filter_var($request->get('2x1'), FILTER_VALIDATE_BOOLEAN)
            );
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($purchases->paginate()->getCollection());
    }

    /**
     * @OA\Get(
     *    tags={"Purchase"},
     *    path="/api/auth/purchases/{id}",
     *    description="Get single purchase.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Cart")}),
     *          @OA\Schema(
     *             @OA\Property(property="offered", type="object", allOf={@OA\Schema(ref="#/components/schemas/User")}),
     *             @OA\Property(property="items", type="array", @OA\Items(allOf={
     *                @OA\Schema(ref="#/components/schemas/CartItem"),
     *                @OA\Schema(
     *                   @OA\Property(property="venue", type="object", allOf={@OA\Schema(ref="#/components/schemas/Venue")}),
     *                ),
     *             })),
     *          ),
     *       }),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Not found!")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $purchase = $this->purchaseRepository->find($id);

            if (empty($purchase)) return response()->json(['message' => trans('response.404')], 404);
        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($purchase);
    }
}
