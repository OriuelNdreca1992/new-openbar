<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Checkout\Cart;
use App\Models\User;
use App\Models\Venue;
use App\Repositories\Api\VenueRepository;
use App\Traits\PhoneNumber;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Stripe;

class PaymentController extends Controller
{
    use PhoneNumber;

    protected VenueRepository $venueRepository;

    public function __construct(VenueRepository $venueRepository)
    {
        $this->venueRepository = $venueRepository;
    }

    /**
     * @OA\Post(
     *    tags={"Venue"},
     *    path="/api/venues/{id}/drinks",
     *    description="Buy drinks.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true, description="Buy drinks.",
     *       @OA\JsonContent(required={"offered_to", "drinks"},
     *          @OA\Property(property="offered_to", type="string", example="+39 06 467 2607"),
     *          @OA\Property(property="message", type="string", example="This is a message."),
     *          @OA\Property(property="drinks", type="array", @OA\Items(allOf={
     *             @OA\Schema(
     *                @OA\Property(property="id", type="integer", example=1),
     *                @OA\Property(property="quantity", type="integer", example=2),
     *             ),
     *          })),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(
     *             @OA\Property(property="fee", type="string", example="1.00"),
     *             @OA\Property(property="amount", type="string", example="30.00"),
     *             @OA\Property(property="intent", type="object", allOf={}),
     *             @OA\Property(property="transaction", type="string", example="b9625994-3382-42e4-8462-746820f8d5c8"),
     *          ),
     *       }),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Not found!")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     *
     * @throws Stripe\Exception\ApiErrorException
     */
    public function buyDrinks(int $id, Request $request): JsonResponse
    {
        if (empty($request->get('drinks')))
            return response()->json(['message' => trans('response.404')], 404);

        /** @var Venue $venue */
        $venue = Venue::query()->where('status', true)->find($id);
        if (empty($venue))
            return response()->json(['message' => trans('response.404')], 404);

        $lastDigits = PhoneNumber::getLastDigitsPhone($request->get('offered_to'));
        /** @var User $giftUser */
        $giftUser = User::query()
            ->where(DB::raw("RIGHT(REPLACE(phone, ' ', ''), " . PhoneNumber::$lastPhoneDigits . ")"), $lastDigits)
            ->first();
        if (empty($giftUser)) {
            $giftUser = User::query()->create([
                'phone' => PhoneNumber::formatPhone($request->get('offered_to')),
                'role'  => User::ROLE_USER,
            ]);
        }

        /** @var Cart $cart */
        $cart = $this->venueRepository->buyDrinks(
            $venue,
            $giftUser,
            $request->get('drinks'),
            $request->get('message'),
        );

        if (is_numeric($cart)) {
            return match ($cart) {
                404 => response()->json(['message' => trans('response.404')], 404),
                default => response()->json(['message' => trans('response.500')], 500),
            };
        }

        /** @var User $user */
        $user = Auth::user();
        Stripe\Stripe::setApiKey(config('services.stripe.secret'));
        if (empty($user->stripe_id)) {
            $customer = Stripe\Customer::create(["description" => 'App User ' . $user->getFullName()]);
            $user->stripe_id = $customer->id;
            $user->save();
        }

        $intent = Stripe\PaymentIntent::create([
            'amount' => filter_var($cart->amount, FILTER_SANITIZE_NUMBER_INT) + filter_var($cart->user_fee, FILTER_SANITIZE_NUMBER_INT),
            'currency' => 'EUR',
            'payment_method_types' => ['card'],
            'confirmation_method' => 'automatic',
            'confirm' => false,
            'customer' => $user->stripe_id,
            'metadata' => [
                'openBar_transaction_id' => $cart->transaction_id,
            ],
        ]);

        Cart::query()
            ->where('transaction_id', $cart->transaction_id)
            ->first()
            ->update(['payment_intent_id' => $intent->id]);

        return response()->json([
            'fee' => $cart->user_fee,
            'amount' => $cart->amount,
            'transaction' => $cart->transaction_id,
            'intent' => $intent,
        ]);
    }
}
