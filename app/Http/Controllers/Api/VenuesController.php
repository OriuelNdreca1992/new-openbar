<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Venue;
use App\Repositories\Api\VenueRepository;
use App\Traits\PhoneNumber;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VenuesController extends Controller
{
    use PhoneNumber;

    protected VenueRepository $venueRepository;

    public function __construct(VenueRepository $venueRepository)
    {
        $this->venueRepository = $venueRepository;
    }

    /**
     * @OA\Get(
     *    tags={"Venue"},
     *    path="/api/venues",
     *    description="Get venues.",
     *    security={ {"bearer": {} }},
     *    @OA\Parameter(name="page", in="query", description="Set pagination.",
     *       @OA\Schema(type="integer", example=1)
     *    ),
     *    @OA\Parameter(name="lat", in="query", description="Set the latitude.",
     *       @OA\Schema(type="float", example=38.682287)
     *    ),
     *    @OA\Parameter(name="lng", in="query", description="Set the longitude.",
     *       @OA\Schema(type="float", example=128.916852)
     *    ),
     *    @OA\Parameter(name="shop", in="query", description="Filter venues with premium drinks.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Parameter(name="offer", in="query", description="Filter venues with 2x1 drinks.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Parameter(name="today", in="query", description="Filter venues with today 2x1 drinks.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Parameter(name="search", in="query", description="Filter venues by name.",
     *       @OA\Schema(type="string", example="Oslo")
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="array", @OA\Items(allOf={
     *          @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Venue")}),
     *          @OA\Schema(
     *             @OA\Property(property="distance", type="float", example=8.2789980398393),
     *          ),
     *       })),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function index(Request $request): JsonResponse
    {
        try {
            $venues = $this->venueRepository->findByGeo(
                $request->get('lat') ?: Venue::LATITUDE_DEFAULT,
                $request->get('lng') ?: Venue::LONGITUDE_DEFAULT,
                [
                    'shop' => $request->get('shop'),
                    'offer' => $request->get('offer'),
                    'today' => $request->get('today'),
                    'search' => $request->get('search'),
                ]
            );
        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($venues->paginate()->getCollection());
    }

    /**
     * @OA\Get(
     *    tags={"Venue"},
     *    path="/api/venues-v2",
     *    description="Get venues with total records counter.",
     *    security={ {"bearer": {} }},
     *    @OA\Parameter(name="page", in="query", description="Set pagination.",
     *       @OA\Schema(type="integer", example=1)
     *    ),
     *    @OA\Parameter(name="lat", in="query", description="Set the latitude.",
     *       @OA\Schema(type="float", example=38.682287)
     *    ),
     *    @OA\Parameter(name="lng", in="query", description="Set the longitude.",
     *       @OA\Schema(type="float", example=128.916852)
     *    ),
     *    @OA\Parameter(name="shop", in="query", description="Filter venues with premium drinks.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Parameter(name="offer", in="query", description="Filter venues with 2x1 drinks.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Parameter(name="today", in="query", description="Filter venues with today 2x1 drinks.",
     *       @OA\Schema(type="boolean", example=true)
     *    ),
     *    @OA\Parameter(name="search", in="query", description="Filter venues by name.",
     *       @OA\Schema(type="string", example="Oslo")
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={@OA\Schema(
     *          @OA\Property(property="total", type="integer", example="19"),
     *          @OA\Property(property="venues", type="array", @OA\Items(allOf={
     *             @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Venue")}),
     *             @OA\Schema(
     *                @OA\Property(property="distance", type="float", example=8.2789980398393),
     *             ),
     *          })),
     *       )}),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function index_v2(Request $request): JsonResponse
    {
        try {
            $venues = $this->venueRepository->findByGeo(
                $request->get('lat') ?: Venue::LATITUDE_DEFAULT,
                $request->get('lng') ?: Venue::LONGITUDE_DEFAULT,
                [
                    'shop' => $request->get('shop'),
                    'offer' => $request->get('offer'),
                    'today' => $request->get('today'),
                    'search' => $request->get('search'),
                ]
            );
        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json([
            'total' => $venues->count(),
            'venues' => $venues->paginate()->getCollection()
        ]);
    }

    /**
     * @OA\Get(
     *    tags={"Venue"},
     *    path="/api/venues/{id}",
     *    description="Get venue data.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(type="object", allOf={@OA\Schema(ref="#/components/schemas/Venue")}),
     *          @OA\Schema(
     *             @OA\Property(property="types", type="array", @OA\Items(allOf={@OA\Schema(ref="#/components/schemas/Type")})),
     *             @OA\Property(property="opening_hours", type="array", @OA\Items(allOf={@OA\Schema(ref="#/components/schemas/OpeningHour")})),
     *             @OA\Property(property="premium_drinks", type="array", @OA\Items(allOf={@OA\Schema(ref="#/components/schemas/PremiumDrink")})),
     *             @OA\Property(property="offers", type="array", @OA\Items(allOf={@OA\Schema(ref="#/components/schemas/Offer")})),
     *          ),
     *       }),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Venue not found!")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            /** @var Venue $venue */
            $venue = $this->venueRepository->find($id);

            if (empty($venue)) return response()->json(['message' => trans('response.venue.404')], 404);
        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($venue);
    }

    /**
     * @OA\Get(
     *    tags={"Venue"},
     *    path="/api/venues/{id}/offers/{id}/use",
     *    description="Use daily 2x1 offer.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(ref="#/components/schemas/Cart"),
     *          @OA\Schema(
     *             @OA\Property(property="items", type="array", @OA\Items(allOf={@OA\Schema(ref="#/components/schemas/CartItem")})),
     *          ),
     *       }),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Not found!")),
     *    ),
     *    @OA\Response(response=409, description="Conflict",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Already used!")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function use2x1($id, $offerID): JsonResponse
    {
        $result = $this->venueRepository->use2x1($id, $offerID);

        if (is_numeric($result)) {
            switch ($result) {
                case 404:
                    return response()->json(['message' => trans('response.404')], 404);
                case 409:
                    return response()->json(['message' => trans('response.offer.409')], 409);
                default:
                    return response()->json(['message' => trans('response.500')], 500);
            }
        }

        return response()->json($result);
    }
}
