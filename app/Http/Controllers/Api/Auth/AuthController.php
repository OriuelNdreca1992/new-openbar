<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\Api\DeviceRequest;
use App\Models\UserDevice;
use App\Notifications\Activities\WelcomeActivity;
use App\Rules\ValidPhoneNumber;
use App\Traits\PhoneNumber;
use App\Traits\SettingsHelper;
use Carbon\Carbon;
use App\Models\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/signup",
     *    description="Sign up.",
     *    summary="Sign up.",
     *    @OA\RequestBody(required=true, description="Register with credentials",
     *       @OA\JsonContent(required={"first_name", "last_name", "phone", "email", "password",
     *          "password_confirmation", "birthdate", "gender"},
     *          @OA\Property(property="first_name", type="string", example="User"),
     *          @OA\Property(property="last_name", type="string", example="User"),
     *          @OA\Property(property="email", type="string", example="user@user.com"),
     *          @OA\Property(property="phone", type="string", example="+12 123456789"),
     *          @OA\Property(property="birthdate", type="string", example="2000-01-01"),
     *          @OA\Property(property="gender", type="integer", example=0, description="0 (other), 1 (male), 2 (female)"),
     *          @OA\Property(property="password", type="string", example="123456"),
     *          @OA\Property(property="password_confirmation", type="string", example="123456"),
     *          @OA\Property(property="lang", type="string", example="en", enum={"en", "it"}),
     *       ),
     *    ),
     *    @OA\Response(response=201, description="Created",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="User created successfully!")),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The email has already been taken.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function signUp(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => 'required|string|email|unique:users',
            'phone'      => ['required', 'string', 'min:10', new ValidPhoneNumber()],
            'birthdate'  => 'required|date',
            'gender'     => 'required|integer|between:0,2',
            'password'   => 'required|string|confirmed',
            'lang'       => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        try {
            $data = [
                'first_name'         => $request->input('first_name'),
                'last_name'          => $request->input('last_name'),
                'email'              => $request->input('email'),
                'phone'              => PhoneNumber::formatPhone($request->input('phone')),
                'birthdate'          => $request->input('birthdate'),
                'gender'             => $request->input('gender'),
                'password'           => bcrypt($request->input('password')),
                'lang'               => $request->input('lang'),
                'role'               => User::ROLE_USER,
                'status'             => false,
                'email_verified_at'  => Date::now(), // automatic verification
            ];

            /** @var User $user */
            $user = User::query()->create($data);
            $user->sendEmailWelcomeNotification();
            $user->sendPhoneVerificationNotification();

            $userToken = $user->createToken('Personal Access Token');

            // save activity
            dispatch(new WelcomeActivity($user));

            return response()->json([
                'token_type' => 'Bearer',
                'access_token' => $userToken->plainTextToken,
                'expires_at' => Carbon::parse($userToken->accessToken->expires_at)->toDateTimeString(),
                'user' => $user
            ]);
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                $response = ['message' => trans('response.phone_email_taken'), 'status' => 400];
            } else {
                Log::error($e);
                $response = ['message' => trans('response.500'), 'status' => 500];
            }
        }

        return response()->json(['message' => $response['message']], $response['status']);
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/login",
     *    description="Sign in.",
     *    summary="Sign in.",
     *    @OA\RequestBody(required=true, description="Login with credentials",
     *       @OA\JsonContent(required={"email","password"},
     *          @OA\Property(property="email", type="string", example="user@user.com"),
     *          @OA\Property(property="password", type="string", example="123456"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(
     *          @OA\Property(property="token_type", type="string", example="Bearer"),
     *          @OA\Property(property="access_token", type="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ..."),
     *          @OA\Property(property="expires_at", type="string", example="2031-05-23 23:47:42"),
     *          @OA\Property(property="user", ref="#/components/schemas/User")
     *       ),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The email field is required.")),
     *    ),
     *    @OA\Response(response=401, description="Unauthorized",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Unauthorized")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function login(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'email'     => 'required|string',
            'password'  => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        $credentials = ['email' => $request->get('email'), 'password' => $request->get('password')];

        if (!Auth::attempt($credentials))
            return response()->json(['message' => trans('auth.failed')], 401);

        if (!($request->user()->role === User::USER_TYPE[User::ROLE_USER]))
            return response()->json(['message' => trans('response.401')], 401);

        try {
            $user = $request->user();
            $userToken = $user->createToken('Personal Access Token');
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json([
            'token_type' => 'Bearer',
            'access_token' => $userToken->plainTextToken,
            'expires_at' => Carbon::parse($userToken->accessToken->expires_at)->toDateTimeString(),
            'user' => $user
        ]);
    }

    /**
     * @OA\Get(
     *    tags={"Auth"},
     *    path="/api/auth/logout",
     *    description="Sign out.",
     *    summary="Sign out.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Successfully logged out")
     *       )
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Something went wrong!")
     *       )
     *    )
     * )
     */
    public function logout(Request $request): JsonResponse
    {
        try {
            $request->user()->currentAccessToken()->delete();
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json(['message' => trans('response.logged_out')]);
    }

    /**
     * @OA\Get(
     *    tags={"Auth"},
     *    path="/api/auth/user",
     *    description="Get user.",
     *    summary="Get user.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(
     *             @OA\Property(property="user", type="object", allOf={@OA\Schema(ref="#/components/schemas/User")}),
     *             @OA\Property(property="settings", type="object", allOf={@OA\Schema(
     *                @OA\Property(property="user_fee_fixed", type="string", example="1"),
     *                @OA\Property(property="user_fee_min", type="string", example="10"),
     *                @OA\Property(property="user_fee_percentage", type="string", example="10"),
     *             )})
     *          )
     *       })
     *    )
     * )
     */
    public function user(Request $request): JsonResponse
    {
        return response()->json([
            'user' => $request->user(),
            'settings' => SettingsHelper::getUserFeeSettings(),
        ]);
    }

    /**
     * @OA\Put(
     *    tags={"Auth"},
     *    path="/api/auth/update",
     *    description="Update user.",
     *    summary="Update user.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true, description="Update authenticated user",
     *       @OA\JsonContent(required={"first_name","last_name","phone"},
     *          @OA\Property(property="first_name", type="string", description="User"),
     *          @OA\Property(property="last_name", type="string", description="User"),
     *          @OA\Property(property="phone", type="string", description="123456789"),
     *          @OA\Property(property="birthdate", type="string", example="2000-01-01"),
     *          @OA\Property(property="gender", type="integer", example=0, description="0 (other), 1 (male), 2 (female)"),
     *          @OA\Property(property="lang", type="string", example="en", enum={"en", "it"}),
     *       )
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(ref="#/components/schemas/User"),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The email has already been taken.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function update(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'phone'      => ['required', 'string', 'min:10', new ValidPhoneNumber(auth()->user()->getAuthIdentifier())],
            'birthdate'  => 'nullable|date',
            'gender'     => 'nullable|integer|between:0,2',
            'lang'       => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        /** @var User $user */
        $user = $request->user();
        try {
            $user->update([
                'first_name' => $request->input('first_name'),
                'last_name'  => $request->input('last_name'),
                'phone'      => PhoneNumber::formatPhone($request->input('phone')),
                'birthdate'  => $request->input('birthdate'),
                'gender'     => $request->input('gender'),
                'lang'       => $request->input('lang'),
            ]);
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                $response = ['message' =>  trans('response.phone_taken'), 'status' => 400];
            } else {
                Log::error($e);
                $response = ['message' => trans('response.500'), 'status' => 500];
            }

            return response()->json(['message' => $response['message']], $response['status']);
        }

        return response()->json($user->fresh());
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/avatar/update",
     *    description="Update avatar.",
     *    summary="Update avatar.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true, description="Update authenticated user avatar",
     *       @OA\MediaType(mediaType="multipart/form-data",
     *          @OA\Schema(
     *              @OA\Property(property="image", type="file", description="image file [mimes:jpeg,jpg,png,heic | max:6MB]"),
     *          ),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(ref="#/components/schemas/User"),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function updateAvatar(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|max:6144',
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        /** @var User $user */
        $user = $request->user();
        if (!$request->hasFile('image')) {
            return response()->json($user);
        }

        try {
            $user->addMedia($request->file('image'))->toMediaCollection(User::COLLECTION);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($user->fresh());
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/phone/update",
     *    description="Update phone number.",
     *    summary="Update phone number.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true, description="Update phone number.",
     *       @OA\JsonContent(required={"phone"},
     *          @OA\Property(property="phone", type="string", description="+123456789"),
     *       )
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(ref="#/components/schemas/User"),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The phone number has already been taken.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function updatePhoneNumber(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'phone' => ['required', 'string', 'min:10', new ValidPhoneNumber($user->id)],
        ]);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        try {
            $user->update([
                'phone' => PhoneNumber::formatPhone($request->input('phone')),
                'phone_verified_at' => null,
                'status' => false,
            ]);

            $user->sendPhoneVerificationNotification();
        } catch (Exception $e) {
            if ($e->getCode() == 23000) {
                $response = ['message' =>  trans('response.phone_taken'), 'status' => 400];
            } else {
                Log::error($e);
                $response = ['message' => trans('response.500'), 'status' => 500];
            }

            return response()->json(['message' => $response['message']], $response['status']);
        }

        return response()->json($user->fresh());
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/password/change",
     *    description="Change the password.",
     *    summary="Change the password.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"old_password", "new_password", "password_confirmation"},
     *          @OA\Property(property="old_password", type="string", example="12345678"),
     *          @OA\Property(property="new_password", type="string", example="12345679"),
     *          @OA\Property(property="password_confirmation", type="string", example="12345679"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Password successfully changed!")),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The old_password field is required.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function changePassword(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'old_password'          => 'required',
                'new_password'          => 'required|min:8',
                'password_confirmation' => 'required|same:new_password',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->first()], 400);
            }

            // Current password provided is not correct
            if (!(Hash::check($request->get('old_password'), $request->user()->password))) {
                return response()->json(['message' => trans('passwords.not_matches')], 400);
            }

            // Current password and new password are same
            if (strcmp($request->get('old_password'), $request->get('new_password')) == 0) {
                return response()->json(['message' => trans('passwords.not_same')], 400);
            }

            // Change Password
            /** @var User $user */
            $user = $request->user();
            $user->update([
                'password' => bcrypt($request->input('new_password')),
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json(['message' =>  trans('passwords.changed')]);
    }

    /**
     * @OA\Delete(
     *    tags={"Auth"},
     *    path="/api/auth",
     *    description="Delete account and data.",
     *    summary="Delete account and data.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Account deleted successfully!"))
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function delete(Request $request): JsonResponse
    {
        try {
            $request->user()->delete();
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json(['message' => trans('auth.deleted')]);
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/devices",
     *    description="Save user device.",
     *    summary="Save user device.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"fcm_token"},
     *          @OA\Property(property="device_manufacturer", type="string", example="Samsung"),
     *          @OA\Property(property="device_model", type="string", example="S20"),
     *          @OA\Property(property="os", type="string", example="Android"),
     *          @OA\Property(property="os_version", type="string", example="12"),
     *          @OA\Property(property="fcm_token", type="string", example="bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1..."),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(ref="#/components/schemas/UserDevice"),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function saveDevice(DeviceRequest $request): JsonResponse
    {
        // @ToDo remove log.
        Log::info('Token sent from user with ID : ' .  auth()->id() . ' on ' . \Illuminate\Support\Carbon::now()->format('Y-m-d H:i'));
        try {
            $data = [
                'user_id'               => auth()->id(),
                'device_manufacturer'   => $request->input('device_manufacturer'),
                'device_model'          => $request->input('device_model'),
                'os'                    => $request->input('os'),
                'os_version'            => $request->input('os_version'),
                'fcm_token'             => $request->input('fcm_token'),
            ];

            $device = UserDevice::query()->where('fcm_token', $request->get('fcm_token'))->first();

            ($device) ? $device->update($data) : $device = UserDevice::query()->create($data);
        } catch(Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($device->refresh());
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/language",
     *    description="Save user language.",
     *    summary="Save user language.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"lang"},
     *          @OA\Property(property="lang", type="string", example="en", enum={"en", "it"}),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(ref="#/components/schemas/User"),
     *    ),
     *    @OA\Response(response=422, description="Unprocessable Entity",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The given data was invalid.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function saveLang(Request $request): JsonResponse
    {
        try {
            $lang = $request->get('lang');

            if(empty($lang)) return response()->json(['message' => trans('response.422')], 422);

            $user = $request->user();

            $user->update(['lang' => $lang]);
        } catch(Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($user->refresh());
    }
}
