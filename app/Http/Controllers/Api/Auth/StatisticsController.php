<?php

namespace App\Http\Controllers\Api\Auth;

use App\Repositories\Api\StatisticsRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class StatisticsController extends Controller
{
    protected StatisticsRepository $statisticsRepository;

    public function __construct(StatisticsRepository $statisticsRepository)
    {
        $this->statisticsRepository = $statisticsRepository;
    }

    /**
     * @OA\Get(
     *    tags={"Auth"},
     *    path="/api/auth/statistics",
     *    description="User statistics.",
     *    summary="User statistics.",
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(
     *          @OA\Property(property="gifted_drinks", type="integer", example=1),
     *          @OA\Property(property="consumed_drinks", type="integer", example=3),
     *          @OA\Property(property="venue_visits", type="integer", example=2),
     *       )
     *    )
     * )
     */
    public function general(): JsonResponse
    {
        return response()->json([
            'gifted_drinks'     => $this->statisticsRepository->countGiftedDrinks(),
            'consumed_drinks'   => $this->statisticsRepository->countConsumedDrinks(),
            'venue_visits'      => $this->statisticsRepository->countVenueVisits(),
        ]);
    }
}
