<?php

namespace App\Http\Controllers\Api\Auth;

use App\Helpers\ParseName;
use App\Models\UserSocial;
use App\Notifications\Activities\WelcomeActivity;
use Exception;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    /**
     * @OA\Get(
     *    tags={"Auth"},
     *    path="/api/auth/token",
     *    description="Sign in or sign up with Facebook, Google or Apple.",
     *    summary="Sign in or sign up with Facebook, Google or Apple.",
     *    @OA\Parameter(name="provider", in="query", required=true, description="Social provider, can be ['facebook', 'google', 'apple']"),
     *    @OA\Parameter(name="access_token", in="query", required=true, description="Long-Lived access token."),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(
     *          @OA\Property(property="token_type", type="string", example="Bearer"),
     *          @OA\Property(property="access_token", type="string", example="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ..."),
     *          @OA\Property(property="expires_at", type="string", example="2031-05-23 23:47:42"),
     *          @OA\Property(property="user", ref="#/components/schemas/User")
     *       ),
     *    ),
     *    @OA\Response(response=401, description="Unauthorized",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Unauthorized.")
     *       ),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Something went wrong!")
     *       ),
     *    )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function login(Request $request) : JsonResponse {
        $provider = $request->input('provider');

        try {
            $socialiteUser = Socialite::driver($provider)->userFromToken($request->input('access_token'));

            if (!$socialiteUser) return response()->json(["message" => trans('response.401')], 401);

            /** @var User $user */
            $user = User::query()->whereHas('socials', function($query) use ($provider, $socialiteUser) {
                $query
                    ->where('provider', $provider)
                    ->where('provider_id', $socialiteUser->id);
            })->first();

            DB::beginTransaction();

            if (empty($user)) {
                $fullNameArray = ParseName::splitFullName($socialiteUser->name);

                $userData = [
                    'first_name' => $fullNameArray['firstName'],
                    'last_name' => $fullNameArray['lastName'],
                    'password' => Hash::make(uniqid()),
                    'email' => uniqid(),
                    'status' => true,
                    'role' => User::ROLE_USER,
                    'email_verified_at' => Date::now()
                ];

                if ($request->has('first_name') && $request->has('last_name')) {
                    $userData['first_name'] = $request->input('first_name');
                    $userData['last_name'] = $request->input('last_name');
                }

                if (empty($socialiteUser->email)) {
                    /** @var User $user */
                    $user = User::query()->create($userData);
                    // save activity
                    dispatch(new WelcomeActivity($user));
                } else {
                    /** @var User $user */
                    $user = User::query()->where('email', $socialiteUser->email)->first();

                    if (empty($user)) {
                        $userData['email'] = $socialiteUser->email;
                        /** @var User $user */
                        $user = User::query()->create($userData);
                        // save activity
                        dispatch(new WelcomeActivity($user));
                    }
                }

                UserSocial::query()->create([
                    'user_id' => $user->id,
                    'provider' => $provider,
                    'provider_id' => $socialiteUser->id
                ]);

                if ($provider === 'facebook') {
                    $profileImg = $socialiteUser->avatar_original . "&access_token={$socialiteUser->token}";
                    $user->addMediaFromUrl($profileImg)->toMediaCollection(User::COLLECTION);
                }
            }

            $userToken = $user->createToken('Personal Access Token');

            DB::commit();

        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json([
            'token_type' => 'Bearer',
            'access_token' => $userToken->plainTextToken,
            'expires_at' => Carbon::parse($userToken->accessToken->expires_at)->toDateTimeString(),
            'user' => $user
        ]);
    }
}
