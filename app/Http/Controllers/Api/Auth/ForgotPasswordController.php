<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Services\CustomPasswordBroker;
use Exception;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    /**
     * Validate the email for the given request.
     */
    protected function validateEmail(Request $request): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($request->all(), [
            'email' => 'required|string'
        ]);
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/password/email",
     *    description="Forgot password.",
     *    summary="Forgot password.",
     *    @OA\RequestBody(required=true, description="Forgot Password",
     *       @OA\JsonContent(required={"email"},
     *          @OA\Property(property="email", type="string", example="user@user.com"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Your password has been reset! Please check your email.")
     *       ),
     *    ),
     *    @OA\Response(response=400, description="Bad request",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="The email field is required.")
     *       ),
     *    )
     * )
     *
     * @return JsonResponse|RedirectResponse
     * @throws  Exception
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validator = $this->validateEmail($request);

        if($validator->fails()) {
            return response()->json(['message' => $validator->errors()->first()], 400);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return ( ($response == Password::RESET_LINK_SENT) ||
            ($response == CustomPasswordBroker::PASSWORD_RESET_EMAIL) )
            ? $this->sendResetLinkResponse($request, $response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }
}
