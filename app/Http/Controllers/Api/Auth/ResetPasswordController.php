<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/password/reset",
     *    description="Reset password.",
     *    summary="Reset password.",
     *    @OA\RequestBody(required=true, description="Reset Password",
     *       @OA\JsonContent(required={"token","email","password","password_confirmation"},
     *          @OA\Property(property="token", type="string", example="d4of82"),
     *          @OA\Property(property="email", type="string", example="user@user.com"),
     *          @OA\Property(property="password", type="string", example="12345678"),
     *          @OA\Property(property="password_confirmation", type="string", example="12345678"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="Your password has been reset!")
     *       ),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(
     *          @OA\Property(property="message", type="string", example="The given data was invalid.")
     *       ),
     *    )
     * )
     */
    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     */
    protected string $redirectTo = RouteServiceProvider::HOME;
}
