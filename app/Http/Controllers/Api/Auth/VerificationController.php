<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\Checkout\Cart;
use App\Models\User;
use App\Models\UserActivity;
use App\Traits\PhoneNumber;
use Exception;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VerificationController extends Controller
{
    use VerifiesEmails;

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/email/verify",
     *    description="Verify e-mail.",
     *    summary="Verify e-mail.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true, description="Verify the email using the verification code.",
     *       @OA\JsonContent(required={"code"},
     *          @OA\Property(property="code", type="string", example="ThPA0hVP"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(ref="#/components/schemas/User"),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The verification code is invalid.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function verifyEmail(Request $request): JsonResponse
    {
        try {
            /** @var User $user */
            $user = $request->user();

            if ($request->get('code') != $user->email_verify_token) {
                return response()->json(['message' => trans('passwords.verify_email_invalid')], 400);
            }

            if (!$user->hasVerifiedEmail()) {
                $user->markEmailAsVerified();
            }
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($user->fresh());
    }

    /**
     * @OA\Post(
     *    tags={"Auth"},
     *    path="/api/auth/mobile/verify",
     *    description="Verify mobile number.",
     *    summary="Verify mobile number.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true, description="Verify the mobile number using the verification code.",
     *       @OA\JsonContent(required={"code"},
     *          @OA\Property(property="code", type="string", example="ThPA0hVP"),
     *       ),
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(ref="#/components/schemas/User"),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The verification code is invalid.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function verifyPhone(Request $request): JsonResponse
    {
        try {
            /** @var User $user */
            $user = $request->user();

            if ($request->get('code') != $user->phone_verify_token) {
                return response()->json(['message' => trans('passwords.verify_phone_invalid')], 400);
            }

            if (!$user->hasVerifiedPhone()) {
                $user->markPhoneAsVerified();

                if ($user->phone) {
                    $lastDigits = PhoneNumber::getLastDigitsPhone($user->phone);

                    $unregisteredUsers = User::query()
                        ->where(DB::raw("RIGHT(REPLACE(phone, ' ', ''), " . PhoneNumber::$lastPhoneDigits . ")"), $lastDigits)
                        ->where('id', '!=', $user->id)
                        ->whereNull('email')
                        ->get();

                    if ($unregisteredUsers) {
                        DB::beginTransaction();
                        /** @var User $unregisteredUser */
                        foreach ($unregisteredUsers as $unregisteredUser) {
                            Cart::query()
                                ->where('offered_to', $unregisteredUser->id)
                                ->where('type', Cart::TYPE_PREMIUM)
                                ->update(['offered_to' => $user->id]);

                            UserActivity::query()
                                ->where('user_id', $unregisteredUser->id)
                                ->update(['user_id' => $user->id]);

                            $unregisteredUser->delete();
                        }
                        DB::commit();
                    }
                }
            }
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($user->fresh());
    }


    /**
     * @OA\Get(
     *    tags={"Auth"},
     *    path="/api/auth/email/resend",
     *    description="Resend verification code.",
     *    summary="Resend verification code",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="We have e-mailed you the verification code.")),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="E-mail already verified.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function resend(): JsonResponse
    {
        try {
            /** @var User $user */
            $user = auth()->user();

            if ($user->hasVerifiedEmail()) {
                return response()->json(["message" => trans('passwords.email_already_verified')], 400);
            }

            $user->sendEmailVerificationNotification();
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json(["message" => trans('passwords.verify_email')]);
    }

    /**
     * @OA\Get(
     *    tags={"Auth"},
     *    path="/api/auth/mobile/resend",
     *    description="Resend mobile verification code.",
     *    summary="Resend mobile verification code",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="We have texted you the verification code.")),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Phone number already verified.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function resendPhone(): JsonResponse
    {
        try {
            /** @var User $user */
            $user = auth()->user();

            if ($user->hasVerifiedPhone()) {
                return response()->json(["message" => trans('passwords.phone_already_verified')], 400);
            }

            $user->sendPhoneVerificationNotification();
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json(["message" => trans('passwords.verify_phone')]);
    }
}
