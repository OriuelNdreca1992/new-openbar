<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Traits\PhoneNumber;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    use PhoneNumber;

    /**
     * @OA\Post(
     *    tags={"Contacts"},
     *    path="/api/contacts",
     *    description="Check openbar contacts.",
     *    summary="Check openbar contacts.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"contacts"},
     *          @OA\Property(property="contacts", type="array", @OA\Items(allOf={
     *             @OA\Schema(
     *                @OA\Property(property="name", type="string", example="John Due"),
     *                @OA\Property(property="phone", type="string", example="+39 06 467 2607"),
     *             )
     *          }))
     *       )
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="array", @OA\Items(allOf={
     *          @OA\Schema(
     *             @OA\Property(property="registered", type="array", @OA\Items(allOf={
     *                @OA\Schema(
     *                   @OA\Property(property="name", type="string", example="John Due"),
     *                   @OA\Property(property="phone", type="string", example="+39064672607"),
     *                   @OA\Property(property="avatar", type="object", allOf={
     *                      @OA\Schema(
     *                         @OA\Property(property="thumb", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                         @OA\Property(property="original", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                      )
     *                   })
     *                )
     *             })),
     *             @OA\Property(property="unregistered", type="array", @OA\Items(allOf={
     *                @OA\Schema(
     *                   @OA\Property(property="name", type="string", example="John Due"),
     *                   @OA\Property(property="phone", type="string", example="+39064672607"),
     *                )
     *             }))
     *          )
     *       }))
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The contacts field is required.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function checkContacts(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'contacts' => 'required|array',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->first()], 400);
            }

            // trim the whitespaces between the phone number digits
            $contacts = array_map(
                function($value) {$value['phone'] = str_replace(' ', '', $value['phone']); return $value;},
                $request->input('contacts')
            );

            // get the last digits of the phone numbers
            $lastDigits = array_map(function($value) {return substr($value,'-' . PhoneNumber::$lastPhoneDigits);}, array_column($contacts, 'phone'));

            // find the phone numbers that correspond to those digits
            $registeredPhones = User::query()
                ->where('status', true)
                ->whereIn(DB::raw("RIGHT(REPLACE(phone, ' ', ''), " . PhoneNumber::$lastPhoneDigits . ")"), $lastDigits)
                ->get(['id', 'phone', 'avatar'])
                ->toArray();

            // get the last digits from the phone numbers found in the DB
            $registeredPhones = array_map(
                function($value) {
                    $value['phone'] = substr(str_replace(' ', '', $value['phone']),'-' . PhoneNumber::$lastPhoneDigits);
                    return $value;
                },
                $registeredPhones
            );

            $registered = [];
            $notRegistered = [];
            foreach ($contacts as $contact) {
                $lastPhoneDigits = substr(str_replace(' ', '', $contact['phone']),'-' . PhoneNumber::$lastPhoneDigits);
                $key = array_search($lastPhoneDigits, array_column($registeredPhones, 'phone'));
                if ($key !== false) {
                    $contact['avatar'] = $registeredPhones[$key]['avatar'];
                    $registered[] = $contact;
                } else {
                    $notRegistered[] = $contact;
                }
            }
        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json([
            'registered' => $registered,
            'unregistered' => $notRegistered,
        ]);
    }
}
