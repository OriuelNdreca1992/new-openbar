<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Checkout\Cart;
use App\Models\UserActivity;
use App\Notifications\Activities\FeedbackDrinksActivity;
use App\Notifications\Push\FeedbackDrinksNotification;
use App\Repositories\Api\ActivityRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ActivityController extends Controller
{
    protected ActivityRepository $activityRepository;

    public function __construct(ActivityRepository $activityRepository)
    {
        $this->activityRepository = $activityRepository;
    }

    /**
     * @OA\Get(
     *    tags={"Activity"},
     *    path="/api/activities",
     *    description="Get activities.",
     *    security={ {"bearer": {} }},
     *    @OA\Parameter(name="page", in="query", description="Set pagination.",
     *       @OA\Schema(type="integer", example="2")
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="array", @OA\Items(allOf={
     *          @OA\Schema(ref="#/components/schemas/UserActivity"),
     *          @OA\Schema(
     *             @OA\Property(property="user_sender", type="object", allOf={@OA\Schema(
     *                @OA\Property(property="id", type="integer", example=1),
     *                @OA\Property(property="first_name", type="string", example="Name"),
     *                @OA\Property(property="last_name", type="string", example="Surname"),
     *                @OA\Property(property="avatar", type="object", allOf={
     *                   @OA\Schema(
     *                      @OA\Property(property="thumb", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                      @OA\Property(property="original", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                   ),
     *               }),
     *             )}),
     *          ),
     *          @OA\Schema(
     *             @OA\Property(property="cart", type="object", allOf={@OA\Schema(
     *                @OA\Property(property="id", type="integer", example=1),
     *                @OA\Property(property="user_id", type="integer", example=1),
     *                @OA\Property(property="offered_to", type="integer", example=2),
     *                @OA\Property(property="amount", type="string", example="22.00"),
     *                @OA\Property(property="user_fee", type="string", example="0.5"),
     *                @OA\Property(property="message", type="string", example="This is a message"),
     *                @OA\Property(property="items", type="array", @OA\Items(allOf={@OA\Schema(
     *                   @OA\Property(property="id", type="integer", example=1),
     *                   @OA\Property(property="cart_id", type="integer", example=1),
     *                   @OA\Property(property="name", type="string", example="Mojito"),
     *                   @OA\Property(property="price", type="string", example="8.00"),
     *                   @OA\Property(property="drinkable_type", type="string", example="App\Models\PremiumDrink"),
     *                   @OA\Property(property="drinkable_id", type="integer", example=1),
     *                   @OA\Property(property="premium_drink", type="object", allOf={@OA\Schema(
     *                      @OA\Property(property="id", type="integer", example=1),
     *                      @OA\Property(property="product_id", type="integer", example=1),
     *                      @OA\Property(property="discount", type="string", example=null),
     *                      @OA\Property(property="product", type="object", allOf={@OA\Schema(
     *                         @OA\Property(property="id", type="integer", example=1),
     *                         @OA\Property(property="image_url", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                      )}),
     *                   )}),
     *                )})),
     *             )}),
     *          ),
     *       })),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $activities = $this->activityRepository->findAll();
        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($activities->paginate()->getCollection());
    }

    /**
     * @OA\Get(
     *    tags={"Activity"},
     *    path="/api/activities/{id}",
     *    description="Get activity data.",
     *    security={ {"bearer": {} }},
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(type="object", allOf={
     *          @OA\Schema(ref="#/components/schemas/UserActivity"),
     *          @OA\Schema(
     *             @OA\Property(property="user_sender", type="object", allOf={@OA\Schema(
     *                @OA\Property(property="id", type="integer", example=1),
     *                @OA\Property(property="first_name", type="string", example="Name"),
     *                @OA\Property(property="last_name", type="string", example="Surname"),
     *                @OA\Property(property="avatar", type="object", allOf={
     *                   @OA\Schema(
     *                      @OA\Property(property="thumb", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                      @OA\Property(property="original", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                   ),
     *               }),
     *             )}),
     *          ),
     *          @OA\Schema(
     *             @OA\Property(property="cart", type="object", allOf={@OA\Schema(
     *                @OA\Property(property="id", type="integer", example=1),
     *                @OA\Property(property="user_id", type="integer", example=1),
     *                @OA\Property(property="offered_to", type="integer", example=2),
     *                @OA\Property(property="amount", type="string", example="22.00"),
     *                @OA\Property(property="user_fee", type="string", example="0.5"),
     *                @OA\Property(property="message", type="string", example="This is a message"),
     *                @OA\Property(property="items", type="array", @OA\Items(allOf={@OA\Schema(
     *                   @OA\Property(property="id", type="integer", example=1),
     *                   @OA\Property(property="cart_id", type="integer", example=1),
     *                   @OA\Property(property="name", type="string", example="Mojito"),
     *                   @OA\Property(property="price", type="string", example="8.00"),
     *                   @OA\Property(property="drinkable_type", type="string", example="App\Models\PremiumDrink"),
     *                   @OA\Property(property="drinkable_id", type="integer", example=1),
     *                   @OA\Property(property="premium_drink", type="object", allOf={@OA\Schema(
     *                      @OA\Property(property="id", type="integer", example=1),
     *                      @OA\Property(property="product_id", type="integer", example=1),
     *                      @OA\Property(property="discount", type="string", example=null),
     *                      @OA\Property(property="product", type="object", allOf={@OA\Schema(
     *                         @OA\Property(property="id", type="integer", example=1),
     *                         @OA\Property(property="image_url", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
     *                      )}),
     *                   )}),
     *                )})),
     *             )}),
     *          ),
     *       }),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Activity not found!")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            /** @var UserActivity $activity */
            $activity = $this->activityRepository->find($id);

            if (empty($activity)) return response()->json(['message' => trans('response.activity.404')], 404);

        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json($activity);
    }

    /**
     * @OA\Post(
     *    tags={"Activity"},
     *    path="/api/activities/{id}/read",
     *    description="Set read/unread the activity.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"read"},
     *          @OA\Property(property="read", type="boolean", example=true),
     *       )
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Success!")),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Activity not found!")),
     *    ),
     *    @OA\Response(response=422, description="Unprocessable Entity",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The given data was invalid.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function read(Request $request, $id): JsonResponse
    {
        try {
            /** @var UserActivity $activity */
            $activity = $this->activityRepository->find($id);

            if (empty($activity)) return response()->json(['message' => trans('response.activity.404')], 404);

            $read = $request->get('read');
            if (!is_bool($read)) return response()->json(['message' => trans('response.422')], 422);

            $activity->update(['read' => $read]);

        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json(['message' => 'Success!']);
    }

    /**
     * @OA\Post(
     *    tags={"Activity"},
     *    path="/api/activities/{id}/feedback",
     *    description="Give a feedback to the activity.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"feedback"},
     *          @OA\Property(property="feedback", type="string", example="cheers", enum={"cheers", "heart", "thumb"}),
     *       )
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Success!")),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Activity not found!")),
     *    ),
     *    @OA\Response(response=422, description="Unprocessable Entity",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The given data was invalid.")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function feedback(Request $request, $id): JsonResponse
    {
        try {
            /** @var UserActivity $activity */
            $activity = $this->activityRepository->find($id);
            if (empty($activity)) return response()->json(['message' => trans('response.activity.404')], 404);

            $feedback = $request->get('feedback');
            if (empty($feedback)) return response()->json(['message' => trans('response.422')], 422);

            $options = [
                UserActivity::ACTIVITY_FEEDBACK_CHEERS,
                UserActivity::ACTIVITY_FEEDBACK_HEART,
                UserActivity::ACTIVITY_FEEDBACK_THUMB,
            ];
            if (!in_array($feedback, $options)) return response()->json(['message' => trans('response.422')], 422);

        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        /** @var Cart $cart */
        $cart = $activity->cart()->first();

        if ($cart) {
            // save activity
            dispatch(new FeedbackDrinksActivity($cart, $feedback));
            // send notification
            dispatch(new FeedbackDrinksNotification($cart, $feedback));
        }

        return response()->json(['message' => 'Success!']);
    }
}
