<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Checkout\CartItem;
use App\Models\Review;
use App\Models\Venue;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    /**
     * @OA\Post(
     *    tags={"Review"},
     *    path="/api/review/{cartItemId}",
     *    description="Review a drink.",
     *    security={ {"bearer": {} }},
     *    @OA\RequestBody(required=true,
     *       @OA\JsonContent(required={"review"},
     *          @OA\Property(property="review", type="integer", example=5, enum={1, 2, 3, 4, 5}),
     *       )
     *    ),
     *    @OA\Response(response=200, description="Success",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Success!")),
     *    ),
     *    @OA\Response(response=400, description="Bad Request",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="The review field is required.")),
     *    ),
     *    @OA\Response(response=404, description="Not Found",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Drink not found!")),
     *    ),
     *    @OA\Response(response=409, description="Conflict",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Already reviewed!")),
     *    ),
     *    @OA\Response(response=500, description="Server Error",
     *       @OA\JsonContent(@OA\Property(property="message", type="string", example="Something went wrong!")),
     *    )
     * )
     */
    public function review(Request $request, $id): JsonResponse
    {
        try {
            $validator = Validator::make($request->all(), [
                'review' => 'required|integer|between:1,5',
            ]);

            if ($validator->fails()) {
                return response()->json(['message' => $validator->errors()->first()], 400);
            }

            /** @var CartItem $cartItem */
            $cartItem = CartItem::query()->find($id);
            if (empty($cartItem)) return response()->json(['message' => trans('response.drink.404')], 404);

            /** @var Review $review */
            $review = Review::query()->where('cart_item_id', $cartItem->id)->first();
            if (!empty($review)) return response()->json(['message' => trans('response.review.409')], 409);

            /** @var Venue $venue */
            $venue = $cartItem->venue()->first();

            Review::query()->create([
                'user_id' => auth()->id(),
                'venue_id' => $venue?->id,
                'cart_item_id' => $cartItem->id,
                'review' => $request->get('review'),
            ]);

        } catch (Exception $e) {
            Log::error($e);

            return response()->json(['message' => trans('response.500')], 500);
        }

        return response()->json(['message' => 'Success!']);
    }
}
