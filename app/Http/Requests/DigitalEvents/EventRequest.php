<?php
namespace App\Http\Requests\DigitalEvents;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'max:192'],
            'type' => ['required'],
            'format' => ['required'],
            'details.*.title' => ['required'],
            'venue_ids' => ['required'],
            'drinks.*.title' => ['required'],
            'drinks.*.product_id' => ['required'],
            'drinks.*.price' => ['required'],
            'drinks.*.quantity' => ['required'],
            'drinks.*.user_quantity' => ['required'],
        ];
    }
}
