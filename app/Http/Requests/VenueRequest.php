<?php

namespace App\Http\Requests;

use App\Rules\ValidPhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class VenueRequest extends FormRequest
{
        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->check()) {
            if (auth()->user()->hasRole('super-admin|admin')) {
                $userId = $this->venue->user->id ?? null;
                $venueId = $this->venue->id ?? null;
            } else {
                $userId = auth()->user()->id;
                $venueId = auth()->user()->venue->id;
            }
        } else {
            $userId = null;
            $venueId = null;
        }

        $rules = [
            'first_name'           => ['required', 'string', 'max:64'],
            'last_name'            => ['required', 'string', 'max:64'],
            'phone'                => ['required', 'string', 'min:10', new ValidPhoneNumber($userId)],
            'email'                => ['required', 'string', 'email', 'max:64', 'unique:users,email,'.$userId],
            'address'              => ['required', 'string', 'max:255'],
            'description'          => ['sometimes', 'nullable', 'max:600'],
            'image_event_url'      => ['sometimes'],
            'image_publish_url'    => ['sometimes'],
            'name'                 => ['required', 'string', 'max:64'],
            'city'                 => ['required', 'string', 'max:64'],
            'postal_code'          => ['required', 'string'],
            'state'                => ['sometimes', 'nullable', 'string', 'max:64'],
            'country'              => ['required', 'string', 'max:64'],
            'phone_number'         => ['required', 'string', 'max:64'],
            'seats_number'         => ['required', 'integer'],
            'premium_drink_fee'    => ['sometimes'],
            'types'                => ['required', 'array', 'min:1'],
            'fiscal_name'          => ['required', 'string', 'max:64'],
            'venue_email'          => ['sometimes', 'nullable', 'string', 'email', 'max:64', 'unique:venues,email,'.$venueId],
            'fiscal_address'       => ['required', 'string', 'max:64'],
            'fiscal_country'       => ['required', 'string', 'max:64'],
            'fiscal_city'          => ['required', 'string', 'max:64'],
            'fiscal_prov'          => ['sometimes', 'nullable', 'string', 'max:64'],
            'fiscal_cap'           => ['required', 'string', 'max:64'],
            'fiscal_vat'           => ['required', 'string', 'max:64'],
            'fiscal_code'          => ['sometimes', 'nullable', 'string', 'max:64'],
            'sdi_code'             => ['sometimes', 'nullable', 'string', 'max:64'],
            'certified_email'      => ['sometimes', 'nullable', 'string', 'email', 'max:64', 'unique:venues,certified_email,'.$venueId],
            'opening_hours'        => 'sometimes',
            'bic'                  => ['sometimes', 'nullable', 'string', 'max:64'],
            'iban'                 => ['sometimes', 'nullable', 'string', 'max:64'],
            'lat'                  => 'required',
            'lng'                  => 'required',
        ];

        if (request()->route()->getName() === 'venues.store') {
            $remove = ['description', 'image_url', 'image_event_url', 'image_publish_url', 'fiscal_prov', 'status', 'fiscal_code', 'bic', 'iban', 'lat', 'lng'];
            $rules = array_diff_key($rules, array_flip($remove));
        }

        if (($this->getMethod() == 'GET') || ($this->getMethod() == 'POST')) {
            $rules += [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ];
        }

        return $rules;
    }

    public function firstName()
    {
        return $this->get('first_name');
    }

    public function lastName()
    {
        return $this->get('last_name');
    }

    public function phone(): ?string
    {
        return $this->get('phone');
    }

    public function email()
    {
        return $this->get('email');
    }

    public function password()
    {
        return $this->get('password');
    }

    public function address()
    {
        return $this->get('address');
    }

    public function description(): ?string
    {
        return $this->get('description');
    }

    public function name()
    {
        return $this->get('name');
    }

    public function city()
    {
        return $this->get('city');
    }

    public function postalCode()
    {
        return $this->get('postal_code');
    }

    public function state()
    {
        return $this->get('state');
    }

    public function country()
    {
        return $this->get('country');
    }

    public function phoneNumber(): ?string
    {
        return $this->get('phone_number');
    }

    public function seatsNumber(): int
    {
        return $this->get('seats_number');
    }

    public function premiumDrinkFee()
    {
        return $this->get('premium_drink_fee');
    }

    public function types(): array
    {
        return $this->get('types');
    }

    public function status()
    {
        return $this->get('status');
    }

    public function fiscalName()
    {
        return $this->get('fiscal_name');
    }

    public function venueEmail()
    {
        return $this->get('venue_email');
    }

    public function fiscalAddress()
    {
        return $this->get('fiscal_address');
    }

    public function fiscalCountry()
    {
        return $this->get('fiscal_country');
    }

    public function fiscalCity()
    {
        return $this->get('fiscal_city');
    }

    public function fiscalProv()
    {
        return $this->get('fiscal_prov');
    }

    public function fiscalCap()
    {
        return $this->get('fiscal_cap');
    }

    public function fiscalVat()
    {
        return $this->get('fiscal_vat');
    }

    public function fiscalCode()
    {
        return $this->get('fiscal_code');
    }

    public function sdiCode()
    {
        return $this->get('sdi_code');
    }

    public function certifiedEmail()
    {
        return $this->get('certified_email');
    }

    public function bic()
    {
        return $this->get('bic');
    }

    public function iban()
    {
        return $this->get('iban');
    }

    public function lat()
    {
        return $this->get('lat');
    }

    public function lng()
    {
        return $this->get('lng');
    }

    public function openingHours(): array
    {
        return $this->get('opening_hours');
    }

    public function googleOpeningHours(): array
    {
        return $this->get('google_opening_hours');
    }
}
