<?php

namespace App\Http\Requests;

use App\Rules\ValidPhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (auth()->user()->hasRole('super-admin|admin')) {
            $userId = $this->company->user->id ?? null;
            $companyId = $this->company->id ?? null;
        } else {
            $userId = auth()->user()->id;
            $companyId = auth()->user()->company->id;
        }

        $rules =  [
            'first_name'      => ['required', 'string', 'max:64'],
            'last_name'       => ['required', 'string', 'max:64'],
            'phone'           => ['required', 'string', 'min:10', new ValidPhoneNumber($userId)],
            'email'           => ['required', 'email', 'max:64', 'unique:users,email,'.$userId],
            'name'            => ['required', 'string', 'max:64'],
            'company_email'   => ['required', 'string', 'email', 'max:64', 'unique:companies,email,'.$companyId],
            'city'            => ['required', 'string', 'max:64'],
            'phone_number'    => ['required', 'string', 'max:64'],
            'event_setup_fee' => ['sometimes'],
            'event_drink_fee' => ['sometimes'],
            'event_venue_fee' => ['sometimes'],
            'address'         => ['required', 'string', 'max:64'],
            'status'          => ['sometimes'],
            'image_url'       => ['sometimes'],
            'fiscal_name'     => ['required', 'string', 'max:64'],
            'fiscal_address'  => ['required', 'string', 'max:64'],
            'fiscal_city'     => ['required', 'string', 'max:64'],
            'fiscal_prov'     => ['required', 'string', 'max:64'],
            'fiscal_cap'      => ['required', 'string', 'max:64'],
            'fiscal_vat'      => ['required', 'string', 'max:64'],
            'fiscal_code'     => ['required', 'string', 'max:64'],
            'sdi_code'        => ['required', 'string', 'max:64'],
            'certified_email' => ['required', 'string', 'email', 'max:64', 'unique:companies,certified_email,'.$companyId],
            'bic'             => ['required', 'string', 'max:64'],
            'iban'            => ['required', 'string', 'max:64']
        ];


        if (($this->getMethod() == 'GET') || ($this->getMethod() == 'POST')) {
            $rules += [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ];
        }

        return $rules;
    }

    public function firstName(): string
    {
        return $this->get('first_name');
    }

    public function lastName(): string
    {
        return $this->get('last_name');
    }

    public function phone()
    {
        return $this->get('phone');
    }

    public function email(): string
    {
        return $this->get('email');
    }

    public function password(): string
    {
        return $this->get('password');
    }

    public function address(): string
    {
        return $this->get('address');
    }

    public function name(): string
    {
        return $this->get('name');
    }

    public function companyEmail(): string
    {
        return $this->get('company_email');
    }

    public function city(): string
    {
        return $this->get('city');
    }

    public function phoneNumber()
    {
        return $this->get('phone_number');
    }

    public function eventSetupFee()
    {
        return $this->get('event_setup_fee');
    }

    public function eventDrinkFee()
    {
        return $this->get('event_drink_fee');
    }

    public function eventVenueFee()
    {
        return $this->get('event_venue_fee');
    }

    public function status()
    {
        return $this->get('status');
    }

    public function fiscalName(): string
    {
        return $this->get('fiscal_name');
    }

    public function fiscalAddress(): string
    {
        return $this->get('fiscal_address');
    }

    public function fiscalCity(): string
    {
        return $this->get('fiscal_city');
    }

    public function fiscalProv(): string
    {
        return $this->get('fiscal_prov');
    }

    public function fiscalCap(): string
    {
        return $this->get('fiscal_cap');
    }

    public function fiscalVat(): string
    {
        return $this->get('fiscal_vat');
    }

    public function fiscalCode(): string
    {
        return $this->get('fiscal_code');
    }

    public function sdiCode(): string
    {
        return $this->get('sdi_code');
    }

    public function certifiedEmail(): string
    {
        return $this->get('certified_email');
    }

    public function bic(): string
    {
        return $this->get('bic');
    }

    public function iban(): string
    {
        return $this->get('iban');
    }
}
