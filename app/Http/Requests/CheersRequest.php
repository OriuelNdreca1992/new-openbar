<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CheersRequest extends FormRequest
{
    public function rules(): array
    {
        $rules = [
            'title' => ['required', 'string'],
            'promoted_type' => ['required'],
            'start_date' => ['required'],
        ];

        $paragraphs = $this->paragraphs;
        if ($paragraphs && is_array($paragraphs)) {
            foreach($paragraphs as $key => $val) {
                $rules['paragraphs.'.$key.'.title'] = 'required_without_all:paragraphs.'.$key.'.description,paragraphs.'.$key.'.video_url,paragraphs.'.$key.'.image';
            }
        }

        return $rules;
    }
}
