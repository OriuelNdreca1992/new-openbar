<?php

namespace App\Http\Requests;

use App\Rules\PasscheckRule;
use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'current_password' => ['required', new PasscheckRule($this->user)],
            'password' => ['required', 'confirmed', Password::min(8)->uncompromised()],
        ];
    }

    public function newPassword(): string
    {
        return $this->get('password');
    }
}
