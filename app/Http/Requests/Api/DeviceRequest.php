<?php
namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class DeviceRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'device_manufacturer'   => [],
            'device_model'          => [],
            'os'                    => [],
            'os_version'            => [],
            'fcm_token'             => ['required'],
        ];
    }
}
