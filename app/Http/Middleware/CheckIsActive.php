<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckIsActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->venue()->exists()) {
            if($request->user()->venue->status === 0) {
                \Auth::logout();
                $request->session()->invalidate();

                return redirect()
                    ->to('/')
                    ->with('warning', 'Your session has expired because your account is deactivated.');
            }
        }

        if ($request->user()->company()->exists()) {
            if($request->user()->company->status === 0) {
                \Auth::logout();
                $request->session()->invalidate();

                return redirect()
                    ->to('/')
                    ->with('warning', 'Your session has expired because your account is deactivated.');
            }
        }

        return $next($request);
    }
}
