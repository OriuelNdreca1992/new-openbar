<?php

namespace App\Handlers;

class LfmHandler
{
    public function userField(): string
    {
        if (auth()->user()->hasRole('admin')) {
            return '';
        } else {
            return auth()->user()->id;
        }
    }
}
