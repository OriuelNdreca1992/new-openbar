<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdministratorPolicy
{
    use HandlesAuthorization;

    const DELETE = 'delete';

    public function delete(User $user, User $administrator): bool
    {
        return $user->isSuperAdmin() && $user->id !== $administrator->id;
    }

    public function update(User $user, User $administrator): bool
    {
        return $user->isSuperAdmin() && $user->id !== $administrator->id;
    }
}
