<?php

namespace App\Notifications\Activities;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\User;
use App\Models\UserActivity;
use App\Models\Venue;
use App\Notifications\Notification;
use Exception;
use Illuminate\Support\Facades\Log;

final class ReminderDrinkActivity extends Notification
{
    protected int $days;
    protected User $user;
    protected CartItem $cartItem;
    protected ?Cart $cart;
    protected ?Venue $venue;

    /**
     * Create a new activity instance.
     */
    public function __construct(User $user, CartItem $cartItem, int $days)
    {
        parent::__construct();
        $this->days = $days;
        $this->user = $user;
        $this->cartItem = $cartItem;
        $this->cart = $cartItem->cart()->first();
        $this->venue = $this->cartItem->venue()->first();
    }

    /**
     * Store the activity.
     */
    public function handle()
    {
        try {
            $this->reminderDrinkActivity();
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    private function reminderDrinkActivity()
    {
        $message_data = [
            'days' => $this->days,
            'drink_name' => $this->getDrinkName($this->cartItem),
            'venue_name' => $this->getVenueName($this->venue, true),
        ];

        $data = [
            'type' => UserActivity::ACTIVITY_TYPE_INFO_EXPIRES,
            'sender' => UserActivity::ACTIVITY_SENDER_SYSTEM,
            'cart_id' => $this->cart->id,
            'cart_item_id' => $this->cartItem->id,
            'user_id' => $this->user->id,
            'sender_id' => null,
            'receiver_id' => null,
            'venue_id' => $this->venue?->id,
            'read' => 0,
            'message' => trans('activities.reminder.body', $message_data, 'it'),
            'message_en' => trans('activities.reminder.body', $message_data, 'en'),
            'venue_name' => $this->getVenueName($this->venue),
            'venue_address' => $this->getVenueAddress($this->venue),
            'drink_name' => $this->getDrinkName($this->cartItem),
            'drink_image' => $this->getDrinkImage($this->cartItem),
            'expires_at' => $this->cart->expires_at,
        ];

        UserActivity::query()->create($data);
    }
}
