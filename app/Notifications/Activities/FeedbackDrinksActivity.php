<?php

namespace App\Notifications\Activities;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\User;
use App\Models\UserActivity;
use App\Models\Venue;
use App\Notifications\Notification;
use Exception;
use Illuminate\Support\Facades\Log;

final class FeedbackDrinksActivity extends Notification
{
    protected Cart $cart;
    protected string $emoji;
    protected ?CartItem $cartItem;
    protected ?User $user;
    protected ?User $offered;
    protected ?Venue $venue;

    /**
     * Create a new activity instance.
     */
    public function __construct(Cart $cart, string $emoji)
    {
        parent::__construct();
        $this->cart = $cart;
        $this->emoji = $emoji;
        $this->user = $cart->user()->first();
        $this->offered = $cart->offered()->first();
        $this->cartItem = $cart->items()->first();
        $this->venue = $this->cartItem?->venue()->first();
    }

    /**
     * Store the activity.
     */
    public function handle()
    {
        try {
            $this->feedbackDrinksActivity();
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    private function feedbackDrinksActivity(): void
    {
        $data = [
            'type' => UserActivity::ACTIVITY_TYPE_INFO_FEEDBACK,
            'sender' => UserActivity::ACTIVITY_SENDER_USER,
            'cart_id' => $this->cart->id,
            'user_id' => $this->user->id,
            'sender_id' => $this->offered->id,
            'receiver_id' =>  $this->user->id,
            'venue_id' => $this->venue ? $this->venue->id : null,
            'read' => 0,
            'feedback' => $this->emoji,
            'venue_name' => $this->getVenueName($this->venue),
            'venue_address' => $this->getVenueAddress($this->venue),
            'expires_at' => $this->cart->expires_at,
        ];

        $message_data = [
            'full_name' => $this->getUserFullName($this->offered),
            'emoji' => $this->getEmoji($this->emoji),
        ];

        if ($this->cart->items()->count() > 1) {
            $data['message'] = trans('activities.feedback_multi.body', $message_data, 'it');
            $data['message_en'] = trans('activities.feedback_multi.body', $message_data, 'en');
        } else {
            $message_data['drink_name'] = $this->getDrinkName($this->cartItem);
            $data['message'] = trans('activities.feedback.body', $message_data, 'it');
            $data['message_en'] = trans('activities.feedback.body', $message_data, 'en');
            $data['drink_name'] = $this->getDrinkName($this->cartItem);
            $data['drink_image'] = $this->getDrinkImage($this->cartItem);
        }

        UserActivity::query()->create($data);
    }
}
