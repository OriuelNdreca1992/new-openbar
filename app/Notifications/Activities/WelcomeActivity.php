<?php

namespace App\Notifications\Activities;

use App\Models\UserActivity;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Log;

final class WelcomeActivity
{
    protected User $user;

    /**
     * Create a new activity instance.
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Store the activity.
     */
    public function handle(): void
    {
        try {
            $this->welcomeActivity();
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    private function welcomeActivity(): void
    {
        UserActivity::query()->create([
            'type' => UserActivity::ACTIVITY_TYPE_WELCOME,
            'sender' => UserActivity::ACTIVITY_SENDER_SYSTEM,
            'user_id' =>  $this->user->id,
            'read' => 0,
            'message' => trans('activities.welcome.title', [], 'it'),
            'message_en' => trans('activities.welcome.title', [], 'en'),
        ]);
    }
}
