<?php

namespace App\Notifications;

use App\Models\Checkout\CartItem;
use App\Models\User;
use App\Models\Venue;
use App\Repositories\UserDeviceRepository;
use Edujugon\PushNotification\PushNotification;

class Notification
{
    const LANG = 'it';
    protected PushNotification $push;
    protected UserDeviceRepository $userDeviceRepository;

    /**
     * Create a new notification instance.
     */
    public function __construct()
    {
        $this->push = new PushNotification('fcm');
        $this->push->setConfig([
            'priority' => 'high',
        ]);
        $this->userDeviceRepository = new UserDeviceRepository();
    }

    /**
     * Find Localization.
     */
    public function getLocalization(User $user): string
    {
        if (isset($user->lang) && $user->lang === 'en') {
            return 'en';
        }

        return self::LANG;
    }

    public function getUserFullName(?User $user): string
    {
        return $user ? $user->first_name.' '.$user->last_name : '';
    }

    public function getUserAvatar(?User $user): string
    {
        return $user && $user->avatar && isset($user->avatar['thumb']) ? $user->avatar['thumb'] : '';
    }

    public function getDrinkName(?CartItem $cartItem): string
    {
        return $cartItem ? $cartItem->name : '';
    }

    public function getDrinkImage(?CartItem $cartItem): string
    {
        return ($cartItem && $cartItem->premiumDrink && $cartItem->premiumDrink->product)
            ? $cartItem->premiumDrink->product->image_url
            : '';
    }

    public function getVenueName(?Venue $venue, bool $withSign = false): string
    {
        $sign = $withSign ? '@' : '';
        return ($venue && $venue->name) ? $sign.$venue->name : '';
    }

    public function getVenueAddress(?Venue $venue): string
    {
        return $venue ? $venue->address : '';
    }

    public function getEmoji(?string $type): string
    {
        if (empty($type)) return '';

        return match ($type) {
            'cheers' => html_entity_decode('&#127867;',ENT_NOQUOTES,'UTF-8'),
            'heart' => html_entity_decode('&#128155;',ENT_NOQUOTES,'UTF-8'),
            'thumb' => html_entity_decode('&#128077;',ENT_NOQUOTES,'UTF-8'),
            default => '',
        };
    }
}
