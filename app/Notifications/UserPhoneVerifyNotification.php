<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use NotificationChannels\Twilio\TwilioChannel;
use NotificationChannels\Twilio\TwilioSmsMessage;

class UserPhoneVerifyNotification extends Notification
{
    public $user;

    /**
     * Create a new notification instance.
     */
    public function __construct($user = null)
    {
        $this->user =  $user ?: Auth::user(); //if user is not supplied, get from session
    }

    /**
     * Get the notification's delivery channels.
     */
    public function via(): array
    {
        return [TwilioChannel::class];
    }

    public function toTwilio()
    {
        return (new TwilioSmsMessage())->content(
            trans('passwords.phone_verification_code_is', ['token' => $this->user->phone_verify_token])
        );
    }
}
