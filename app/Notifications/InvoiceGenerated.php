<?php

namespace App\Notifications;

use App\Models\Venue;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;

class InvoiceGenerated extends Notification
{
    use Queueable;

    protected Venue $venue;
    protected string $docPath;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($venue, $docPath)
    {
        $this->venue = $venue;
        $this->docPath = $docPath;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(mixed $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail(mixed $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject(Lang::get('Report settimanale'))
            ->markdown('emails.weekly_report', [
                'venue'     => $this->venue,
                'docPath'   => $this->docPath,
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray(mixed $notifiable): array
    {
        return [
            //
        ];
    }
}
