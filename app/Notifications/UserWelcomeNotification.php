<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class UserWelcomeNotification extends VerifyEmail implements ShouldQueue
{
    use Queueable;
    public $user;

    /**
     * Create a new notification instance.
     */
    public function __construct($user = null)
    {
        $this->user =  $user ?: Auth::user(); //if user is not supplied, get from session
    }

    /**
     * Get the notification's delivery channels.
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)
            ->from(env('USER_REGISTRATION_ADMIN_EMAIL'))
            ->subject(Lang::get('Benvenuto su Openbar'))
            ->markdown('emails.welcome', [
                'user' => $this->user,
                'footer_email' => 'appsupport@openbar.life'
            ]);
    }

    /**
     * Get the array representation of the notification.
     */
    public function toArray($notifiable): array
    {
        return [
            //
        ];
    }
}
