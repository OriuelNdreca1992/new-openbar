<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class UserVerifyNotification extends VerifyEmail implements ShouldQueue
{
    use Queueable;
    public $user;

    /**
     * Create a new notification instance.
     */
    public function __construct($user = null)
    {
        $this->user =  $user ?: Auth::user(); //if user is not supplied, get from session
    }

    /**
     * Get the notification's delivery channels.
     */
    public function via($notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail($notifiable): MailMessage
    {
        return (new MailMessage)->subject(Lang::get('Verify your account'))
            ->line(Lang::get('You are receiving this email because you have registered to our app.'))
            ->line(Lang::get('Verification code').': **'.$this->user->email_verify_token.'**')
            ->line(Lang::get('If you did not register, no further action is required.'));
    }

    /**
     * Get the array representation of the notification.
     */
    public function toArray($notifiable): array
    {
        return [
            //
        ];
    }
}
