<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Request;

class CustomResetPasswordNotification extends ResetPassword
{
    public string $fullName;

    /**
     * Create a notification instance.
     *
     * @param  string  $fullName
     * @param  string  $token
     * @return void
     */
    public function __construct(string $fullName, string $token)
    {
        parent::__construct($token);
        $this->fullName = $fullName;
    }

    /**
     * Build the mail representation of the notification.
     */
    public function toMail($notifiable): MailMessage
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        if (Request::wantsJson()) {
            // return JSON-formatted response
            return (new MailMessage)
                ->subject(Lang::get('Reset Password'))
                ->markdown('emails.password_reset_verification', [
                    'token'     => $this->token,
                    'fullName'  => $this->fullName,
                    'count'     => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')
                ]);
        } else {
            // return HTML response
            if (static::$createUrlCallback) {
                $url = call_user_func(static::$createUrlCallback, $notifiable, $this->token);
            } else {
                $url = url(route('password.reset', [
                    'token' => $this->token,
                    'email' => $notifiable->getEmailForPasswordReset(),
                ], false));
            }

            return (new MailMessage)
                ->subject(Lang::get('Reset Password'))
                ->markdown('emails.password_reset', [
                    'url'       => $url,
                    'fullName'  => $this->fullName,
                    'count'     => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')
                ]);
        }
    }
}
