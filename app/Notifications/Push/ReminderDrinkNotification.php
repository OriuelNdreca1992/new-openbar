<?php

namespace App\Notifications\Push;

use App\Models\Checkout\CartItem;
use App\Models\User;
use App\Notifications\Notification;
use Exception;
use Illuminate\Support\Facades\Log;

final class ReminderDrinkNotification extends Notification
{
    protected int $days;
    protected User $user;
    protected CartItem $cartItem;

    /**
     * Create a new notification instance.
     */
    public function __construct(User $user, CartItem $cartItem, int $days)
    {
        parent::__construct();
        $this->days = $days;
        $this->user = $user;
        $this->cartItem = $cartItem;
    }

    /**
     * Send notifications.
     */
    public function handle()
    {
        try {
            $this->reminderDrinkNotification();
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    private function reminderDrinkNotification()
    {
        $tokens = $this->userDeviceRepository->findAllByUser($this->user);

        $data = [
            'drink' => $this->cartItem->name,
            'venue' => $this->cartItem->venue ? $this->cartItem->venue->name : null,
        ];

        $this->push->setMessage([
            'notification' => [
                'title' => trans('activities.reminder.title'),
                'body'  => trans('activities.reminder.body', [
                    'venue_name' => $data['venue'],
                    'drink_name' => $data['drink'],
                    'days' => $this->days,
                ]),
            ],
            'data' => [
                'title' => trans('activities.reminder.title'),
                'body'  => trans('activities.reminder.body', [
                    'venue_name' => $data['venue'],
                    'drink_name' => $data['drink'],
                    'days' => $this->days,
                ]),
                'image' => config('fcm.image.info'),
            ],
        ])
        ->setApiKey(config('fcm.server_key'))
        ->setDevicesToken($tokens)
        ->send();
    }
}
