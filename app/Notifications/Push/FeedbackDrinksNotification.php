<?php

namespace App\Notifications\Push;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\User;
use App\Notifications\Notification;
use Exception;
use Illuminate\Support\Facades\Log;

final class FeedbackDrinksNotification extends Notification
{
    protected Cart $cart;
    protected string $emoji;
    protected ?CartItem $cartItem;
    protected ?User $user;
    protected ?User $offered;

    /**
     * Create a new activity instance.
     */
    public function __construct(Cart $cart, string $emoji)
    {
        parent::__construct();
        $this->cart = $cart;
        $this->emoji = $emoji;
        $this->user = $cart->user()->first();
        $this->offered = $cart->offered()->first();
        $this->cartItem = $this->cart->items()->first();
    }

    /**
     * Store the activity.
     */
    public function handle()
    {
        try {
            $this->feedbackDrinksNotification();
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    private function feedbackDrinksNotification(): void
    {
        $tokens = $this->userDeviceRepository->findAllByUser($this->user);

        if (empty($tokens)) return;

        $locale = $this->getLocalization($this->user);

        $message_data = [
            'full_name' => $this->getUserFullName($this->offered),
            'emoji' => $this->getEmoji($this->emoji),
        ];

        if ($this->cart->items()->count() > 1) {
            $data = [
                'title' => trans('activities.feedback_multi.title', $message_data, $locale),
                'body' => trans('activities.feedback_multi.body', $message_data, $locale),
            ];
        } else {
            $message_data['drink_name'] = $this->getDrinkName($this->cartItem);
            $data = [
                'title' => trans('activities.feedback.title', $message_data, $locale),
                'body' => trans('activities.feedback.body', $message_data, $locale),
            ];
        }

        $this->push->setMessage([
            'notification' => [
                'title' => $data['title'],
                'body' => $data['body'],
            ],
            'data' => [
                'title' => $data['title'],
                'body' => $data['body'],
                'image' => $this->getUserAvatar($this->offered),
            ],
        ])
        ->setApiKey(config('fcm.server_key'))
        ->setDevicesToken($tokens)
        ->send();
    }
}
