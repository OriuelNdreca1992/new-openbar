<?php

namespace App\Notifications\Push;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\User;
use App\Models\Venue;
use App\Notifications\Notification;
use Edujugon\PushNotification\PushNotification;
use Exception;
use Illuminate\Support\Facades\Log;

final class PurchasedDrinksNotification extends Notification
{
    protected Cart $cart;
    protected ?CartItem $cartItem;
    protected ?User $user;
    protected ?User $offered;
    protected ?Venue $venue;
    protected PushNotification $push;

    /**
     * Create a new notification instance.
     */
    public function __construct(Cart $cart)
    {
        parent::__construct();
        $this->cart = $cart;
        $this->user = $cart->user()->first();
        $this->offered = $cart->offered()->first();
        $this->cartItem = $cart->items()->first();
        $this->venue = $this->cartItem?->venue()->first();
    }

    /**
     * Send notifications.
     */
    public function handle()
    {
        try {
            $this->purchasedDrinksNotification();
        } catch (Exception $exception) {
            Log::error($exception);
        }
    }

    /**
     * Notify the sender.
     */
    private function purchasedDrinksNotification()
    {
        $tokens = $this->userDeviceRepository->findAllByUser($this->offered);

        if (empty($tokens)) return;

        $locale = $this->getLocalization($this->offered);

        $message_data = [
            'full_name' => $this->getUserFullName($this->user),
            'venue_name' => $this->getVenueName($this->venue, true),
        ];

        if ($this->cart->items()->count() > 1) {
            $data = [
                'title' => trans('activities.premium_multi.title', $message_data, $locale),
                'body' => trans('activities.premium_multi.body', $message_data, $locale),
            ];
        } else {
            $message_data['drink_name'] = $this->getDrinkName($this->cartItem);
            $data = [
                'title' => trans('activities.premium.title', $message_data, $locale),
                'body' => trans('activities.premium.body', $message_data, $locale),
            ];
        }

        $this->push->setMessage([
            'notification' => [
                'title' => $data['title'],
                'body' => $data['body'],
            ],
            'data' => [
                'title' => $data['title'],
                'body' => $data['body'],
                'image' => $this->getUserAvatar($this->user),
            ],
        ])
        ->setApiKey(config('fcm.server_key'))
        ->setDevicesToken($tokens)
        ->send();
    }
}
