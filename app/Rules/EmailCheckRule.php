<?php

namespace App\Rules;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule as CustomRule;
use Illuminate\Contracts\Validation\Rule;

class EmailCheckRule implements Rule
{
    protected $email;

    const ROLE = 5;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $count = DB::table('users')
                ->where(['email' => $this->email, 'role' => static::ROLE])
                ->count();

        return ! (bool)$count;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'This email exists on app user';
    }
}
