<?php

namespace App\Rules;

use App\Models\User;
use App\Traits\PhoneNumber;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class ValidPhoneNumber implements Rule
{
    protected ?int $id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(?int $id = null)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        $lastDigits = PhoneNumber::getLastDigitsPhone($value);
        $query = User::query()
            ->whereNotNull('email')
            ->where(DB::raw("RIGHT(REPLACE(phone, ' ', ''), " . PhoneNumber::$lastPhoneDigits . ")"), $lastDigits);

        if ($this->id) {
            $query->whereKeyNot($this->id);
        }

        $usersCount = $query->count();

        return $usersCount === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return "The :attribute can't be accepted.";
    }
}
