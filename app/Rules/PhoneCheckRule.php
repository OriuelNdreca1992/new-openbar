<?php

namespace App\Rules;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Validation\Rule;

class PhoneCheckRule implements Rule
{
    protected $phone;

    const ROLE = 5;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
            $count = DB::table('users')
                ->where(['phone' => $this->phone, 'role' => static::ROLE])
                ->count();

        return ! (bool)$count;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The phone number exists on app user.';
    }
}
