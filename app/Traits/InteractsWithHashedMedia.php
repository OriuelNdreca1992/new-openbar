<?php

namespace App\Traits;

use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;

trait InteractsWithHashedMedia
{
    use InteractsWithMedia {
        InteractsWithMedia::addMedia as parentAddMedia;
        InteractsWithMedia::addMediaFromUrl as parentAddMediaFromUrl;
        InteractsWithMedia::addMediaConversion as parentAddMediaConversion;
    }

    public function addMedia($file): FileAdder
    {
        return $this->parentAddMedia($file)
            ->usingFileName($file->hashName());
    }

    public function addMediaFromUrl($url): FileAdder
    {
        return $this->parentAddMediaFromUrl($url)
            ->usingFileName(md5($url).'.jpg');
    }

    /**
     * @throws InvalidManipulation
     */
    public function addMediaConversion(string $name): Conversion
    {
        return $this->parentAddMediaConversion($name)
            ->width(400)
            ->height(400)
            ->sharpen(10)
            ->keepOriginalImageFormat()
            ->nonOptimized();
    }
}
