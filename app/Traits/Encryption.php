<?php

namespace App\Traits;

trait Encryption
{
    private static string $encrypt_method = 'AES-256-CBC';
    private static string $secret_key = 'fe67d68ee1e09b47acd8810b880d537034c10c15344433a992b9c79002666844';
    private static string $secret_iv = 'fdd3345455fffgffffhkkyoife67d68ee1e09b47acd8810b880d537034c10c15344433a992b9c79002666844';

    public function encrypt($message): string
    {
        $key = hash('sha256', self::$secret_key);
        $iv = substr(hash('sha256', self::$secret_iv), 0, 16);

        return base64_encode(openssl_encrypt($message, self::$encrypt_method, $key, 0, $iv));
    }

    public function decrypt($encryptedMessage): string
    {
        $key = hash('sha256', self::$secret_key);
        $iv = substr(hash('sha256', self::$secret_iv), 0, 16);

        return openssl_decrypt(base64_decode($encryptedMessage), self::$encrypt_method, $key, 0, $iv);
    }
}
