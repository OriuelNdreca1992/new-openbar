<?php

namespace App\Traits;

use Stripe;

trait StripeHelper
{
    /**
     * @throws Stripe\Exception\ApiErrorException
     */
    public static function createPaymentMethod($token): Stripe\PaymentMethod
    {
        return Stripe\PaymentMethod::create([
            'type' => 'card',
            'card' => [
                'token' => $token
            ]
        ]);
    }

    /**
     * @throws Stripe\Exception\ApiErrorException
     */
    public static function attachCard($customer_id, $payment_method_id): Stripe\PaymentMethod
    {
        $payment_method = Stripe\PaymentMethod::retrieve($payment_method_id);

        return $payment_method->attach(['customer' => $customer_id]);
    }

    /**
     * @throws Stripe\Exception\ApiErrorException
     */
    public static function detachCard($customer_id, $payment_method_id): Stripe\PaymentMethod
    {
        $payment_method = Stripe\PaymentMethod::retrieve($payment_method_id);

        return $payment_method->detach();
    }
}
