<?php

namespace App\Traits;

trait PhoneNumber
{
    static int $lastPhoneDigits = 7;

    public static function formatPhone(string $phoneNumber): string
    {
        return str_replace(' ', '', $phoneNumber);
    }

    public static function getLastDigitsPhone(string $phoneNumber): string
    {
        $phoneWithoutSpaces = str_replace(' ', '', $phoneNumber);

        return substr($phoneWithoutSpaces, '-' . self::$lastPhoneDigits);
    }
}
