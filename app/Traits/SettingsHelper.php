<?php

namespace App\Traits;

use App\Models\Setting;

trait SettingsHelper
{
    public static function getUserFeeSettings(): array
    {
        $settings = Setting::query()
            ->select(['setting', 'value'])
            ->whereIn('setting', ['user_fee_fixed', 'user_fee_min', 'user_fee_percentage'])
            ->get();

        $fees = [];
        foreach($settings as $item) {
            $fees[$item->setting] = $item->value;
        }

        return $fees;
    }
}
