<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Geographical
{
    public function scopeDistance(Builder $query, float $latitude, float $longitude): Builder
    {
        $latName = $this->getQualifiedLatitudeColumn();
        $lonName = $this->getQualifiedLongitudeColumn();

        // Adding already selected columns to query, all columns will be selected by default
        if ($query->getQuery()->columns === null) {
            $query->select($this->getTable() . '.*');
        } else {
            $query->select($query->getQuery()->columns);
        }

        $sql = "((ACOS(SIN(? * PI() / 180) * SIN(" . $latName . " * PI() / 180) + COS(? * PI() / 180) * COS(" .
            $latName . " * PI() / 180) * COS((? - " . $lonName . ") * PI() / 180)) * 180 / PI()) * 60 * ?) as distance";

        $miles = false;
        if (property_exists(static::class, 'miles')) {
            $miles = static::$miles;
        }

        if ($miles) {
            // miles
            $query->selectRaw($sql, [$latitude, $latitude, $longitude, 1.1515]);
        } else {
            // kilometers
            $query->selectRaw($sql, [$latitude, $latitude, $longitude, 1.1515 * 1.609344]);
        }

        return $query;
    }

    public function scopeGeofence($query, $latitude, $longitude, $inner_radius, $outer_radius): Builder
    {
        $query = $this->scopeDistance($query, $latitude, $longitude);
        return $query->havingRaw('distance BETWEEN ? AND ?', [$inner_radius, $outer_radius]);
    }

    protected function getQualifiedLatitudeColumn(): string
    {
        return $this->getConnection()->getTablePrefix() . $this->getTable() . '.' . $this->getLatitudeColumn();
    }

    protected function getQualifiedLongitudeColumn(): string
    {
        return $this->getConnection()->getTablePrefix() . $this->getTable() . '.' . $this->getLongitudeColumn();
    }

    public function getLatitudeColumn(): string
    {
        return defined('static::LATITUDE_FIELD') ? static::LATITUDE_FIELD : 'latitude';
    }

    public function getLongitudeColumn(): string
    {
        return defined('static::LONGITUDE_FIELD') ? static::LONGITUDE_FIELD : 'longitude';
    }
}
