<?php

namespace App\Console\Commands;

use App\Models\Checkout\Cart;
use App\Models\PremiumDrink;
use Illuminate\Support\Facades\DB;
use PDF;
use Carbon\Carbon;
use App\Models\ExpiredDrinkReport;
use Illuminate\Console\Command;

class GenerateExpiredDrinksReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:expired-drinks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Expired Drinks Reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $now = Carbon::now();
        $startOfMonth = $now->copy()->subMonth()->startOfMonth();
        $endOfMonth = $now->copy()->subMonth()->endOfMonth();

        $drinks = DB::table('cart_items as items')
            ->select('items.id',
                'items.name as drink_name',
                'items.price',
                'v.name as venue_name',
                'buyers.id as buyer_id',
                'buyers.first_name',
                'buyers.last_name',
                'c.user_fee',
                'c.created_at',
                'c.expires_at')
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->join('venues as v', 'v.id', '=', 'items.venue_id')
            ->join('users as buyers', 'buyers.id', '=', 'c.user_id')
            ->join('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);
            })
            ->where('c.status', Cart::STATUS_SUCCEEDED)
            ->whereNull('items.used_at')
            ->whereBetween('c.expires_at', [$startOfMonth, $endOfMonth])
            ->get();

        if ($drinks->count()) {
            $data = [
                'drinks' => $drinks,
                'from'   => $startOfMonth,
                'to'     => $endOfMonth,
            ];

            $path = '/documents/reports/report_expired_drinks_'.$startOfMonth->format('Y-m').'_'.$now->getTimestamp().'.pdf';

            PDF::loadView('admin.pdf-reports.expired-drinks', $data)->setPaper('a4')->save(public_path($path));

            ExpiredDrinkReport::query()->create(['report_pdf' => $path, 'from' => $startOfMonth, 'to' => $endOfMonth]);
        }
    }
}
