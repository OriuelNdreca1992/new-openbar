<?php

namespace App\Console\Commands;

use App\Models\Checkout\Cart;
use App\Models\PremiumDrink;
use PDF;
use Carbon\Carbon;
use App\Models\UserFeeReport;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateUserFeesReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:user-fees';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Report User Fees';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $now = Carbon::now();
        $startOfMonth = $now->copy()->subMonth()->startOfMonth();
        $endOfMonth = $now->copy()->subMonth()->endOfMonth();

        $drinks = DB::table('cart_items as items')
            ->select('items.id',
                'items.name as drink_name',
                'items.price',
                'v.name as venue_name',
                'buyers.id as buyer_id',
                'buyers.first_name',
                'buyers.last_name',
                'c.user_fee',
                'c.created_at')
            ->join('carts as c', 'c.id', '=', 'items.cart_id')
            ->join('venues as v', 'v.id', '=', 'items.venue_id')
            ->join('users as buyers', 'buyers.id', '=', 'c.user_id')
            ->join('premium_drinks as d', function ($join) {
                $join->on('d.id', '=', 'items.drinkable_id')->where('items.drinkable_type', '=', PremiumDrink::class);
            })
            ->where('c.status', Cart::STATUS_SUCCEEDED)
            ->whereNotNull('c.user_fee')
            ->whereBetween('c.created_at', [$startOfMonth, $endOfMonth])
            ->get();

        if ($drinks->count()) {
            $data = [
                'drinks' => $drinks,
                'from'   => $startOfMonth,
                'to'     => $endOfMonth,
            ];

            $path = '/documents/reports/report_user_fees_'.$startOfMonth->format('Y-m').'_'.$now->getTimestamp().'.pdf';

            PDF::loadView('admin.pdf-reports.user-fees', $data)->setPaper('a4')->save(public_path($path));

            UserFeeReport::query()->create(['report_pdf' => $path, 'from' => $startOfMonth, 'to' => $endOfMonth]);
        }
    }
}
