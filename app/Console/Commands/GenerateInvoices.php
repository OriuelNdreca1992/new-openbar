<?php

namespace App\Console\Commands;

use App\Models\Checkout\Cart;
use App\Models\Offer;
use App\Models\PremiumDrink;
use Carbon\CarbonInterface;
use PDF;
use Carbon\Carbon;
use App\Models\Venue;
use App\Models\Invoice;
use App\Models\Setting;
use Illuminate\Console\Command;
use App\Models\AdministrativeData;
use App\Notifications\InvoiceGenerated;
use Illuminate\Support\Facades\Notification;

class GenerateInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoice:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate weekly invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->generateInvoices();
    }

    private function generateInvoices()
    {
        $now = Carbon::now();
        $weekOfYear = $now->copy()->subWeek()->weekOfYear;
        $weekStartDate = $now->copy()->subWeek()->startOfWeek(CarbonInterface::MONDAY);
        $weekEndDate = $now->copy()->subWeek()->endOfWeek(CarbonInterface::SUNDAY);
        $settings = Setting::all();
        $taxableSale = $settings->where('setting', 'taxable_sale')->first()->value;
        $openBarPremiumDrinkFee = $settings->where('setting', 'op_premium_fee')->first()->value;
        $taxableVat = $settings->where('setting', 'taxable_vat')->first()->value;
        $openBarAdministrativeData = AdministrativeData::query()->first();

        $venues = Venue::query()
            ->withAndWhereHas('cartItems.cart', function ($query) {
                $query->where('status', Cart::STATUS_SUCCEEDED);
            })
            ->withAndWhereHas('cartItems', function ($query) use ($weekStartDate, $weekEndDate) {
                $query->whereNotNull('used_at')
                    ->whereBetween('used_at', [$weekStartDate, $weekEndDate]);
            })
            ->get();

        foreach ($venues as $venue) {
            /** @var Venue $venue */
            if ($venue->cartItems->count()) {
                $twoPerOneOrders = $venue->cartItems->filter(function ($item) {
                    return $item->drinkable_type === Offer::class;
                });

                $premiumDrinkOrders = $venue->cartItems->filter(function ($item) {
                    return $item->drinkable_type === PremiumDrink::class;
                });

                $premiumDrinkFee = !is_null($venue->premium_drink_fee)
                    ? $venue->premium_drink_fee
                    : $openBarPremiumDrinkFee;

                if ($premiumDrinkOrders->count() > 0) {
                    $invoiceNo = !is_null($venue->invoice->last())
                        ? $venue->invoice->last()->invoice_no + 1
                        : 1;
                } else {
                    $invoiceNo = null;
                }

                $data = [
                    'venue' => $venue,
                    'orders' => $venue->cartItems,
                    'twoPerOneOrders' => $twoPerOneOrders,
                    'premiumDrinkOrders' => $premiumDrinkOrders,
                    'taxableSale' => $taxableSale,
                    'premiumDrinkFee' => $premiumDrinkFee,
                    'taxableVat' => $taxableVat,
                    'openBarAdministrativeData' => $openBarAdministrativeData,
                    'invoiceNo' => $invoiceNo,
                    'from' => $weekStartDate,
                    'to' => $weekEndDate,
                    'week' => $weekOfYear,
                ];

                $adminPath = '/documents/reports/report_admin_'.$venue->id.'_'.$now->getTimestamp().'.pdf';
                $venuePath = '/documents/reports/report_venue_'.$venue->id.'_'.$now->getTimestamp().'.pdf';

                $customPaper = [0, 0, 900.00, 650.80];
                $adminPdf = PDF::loadView('admin.pdf-invoices.admin', $data)->setPaper('a4')->save(public_path($adminPath));
                $venuePdf = PDF::loadView('admin.pdf-invoices.venue', $data)->setPaper('a4')->save(public_path($venuePath));

                $sumOfPrice = 0;
                foreach ($premiumDrinkOrders as $drink) {
                   $sumOfPrice += $drink->price;
                }

                $openBarIncome = $this->openBarIncome($sumOfPrice, $taxableVat, $premiumDrinkFee, $taxableSale);
                $venueIncome = $this->venueIncome($sumOfPrice, $taxableVat, $premiumDrinkFee, $taxableSale);

                Invoice::query()->create([
                    'subject_id' => $venue->id,
                    'subject_type' => Venue::class,
                    'subject_invoice' => $venuePath,
                    'admin_invoice' => $adminPath,
                    'from' => $weekStartDate,
                    'to' => $weekEndDate,
                    'open_bar_income' => $openBarIncome,
                    'venue_income' => $venueIncome,
                    'invoice_no' => $invoiceNo,
                    'status' => false
                ]);

                // Send E-mail
                if ($premiumDrinkOrders->count()) {
                    Notification::send($venue->user()->first(), new InvoiceGenerated($venue, $venuePath));
                }
           }
       }
    }

    private function venueIncome($sumOfPrice, $taxableVat, $premiumDrinkFee, $taxableSale): float
    {
        $ivaInponibile = $sumOfPrice / (1 + 1 * ($taxableVat / 100));
        $obFee = ($premiumDrinkFee / 100) * $ivaInponibile;
        $iva = ($taxableSale / 100) * $obFee;
        $total = $obFee + $iva;
        $result = $sumOfPrice - $total;

        return number_format($result, 2, '.', '');
    }

    private function openBarIncome($sumOfPrice, $taxableVat, $premiumDrinkFee, $taxableSale): float
    {
        $ivaImponibile = $sumOfPrice/(1 + 1 * ($taxableVat / 100));
        $obFee = ($premiumDrinkFee / 100) * $ivaImponibile;
        $iva = ($taxableSale / 100) * $obFee;
        $result = $obFee + $iva;

        return number_format($result, 2, '.', '');
    }
}
