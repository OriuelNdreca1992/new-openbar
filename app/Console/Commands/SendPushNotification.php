<?php

namespace App\Console\Commands;

use App\Models\ScheduledPushNotification;
use App\Models\UserDevice;
use Carbon\Carbon;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Console\Command;

class SendPushNotification extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'notification:send';

    /**
     * The console command description.
     */
    protected $description = 'Send scheduled push notifications';

    protected PushNotification $push;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        $this->push = new PushNotification('fcm');
        $this->push->setConfig([
            'priority' => 'high',
        ]);
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $notifications = ScheduledPushNotification::query()
            ->where('status', false)
            ->whereBetween('scheduled_time', [Carbon::now()->sub('minutes',5), Carbon::now()])
            ->get();

        if (empty($notifications)) return;

        $fcmTokens = UserDevice::query()
            ->pluck('fcm_token')
            ->toArray();

        $chunks = array_chunk($fcmTokens, config('fcm.firebase_tokens_limit'));

        foreach ($notifications as $notification) {

            foreach ($chunks as $tokens) {
                $this->push->setMessage([
                    'notification' => [
                        'title' => $notification->title,
                        'body' => $notification->body,
                    ],
                    'data' => [
                        'title' => $notification->title,
                        'body'  => $notification->body,
                        'type'  => 'scheduled',
                        'image' => config('fcm.image.openbar'),
                    ],
                ])
                ->setApiKey(config('fcm.server_key'))
                ->setDevicesToken($tokens)
                ->send();
            }

            $notification->status = ScheduledPushNotification::COMMITTED;
            $notification->save();
        }
    }
}
