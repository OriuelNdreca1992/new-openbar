<?php

namespace App\Console\Commands;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\User;
use App\Notifications\Activities\ReminderDrinkActivity;
use App\Notifications\Push\ReminderDrinkNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendReminderNotification extends Command
{
    /**
     * The name and signature of the console command.
     */
    protected $signature = 'reminder:send {days}';

    /**
     * The console command description.
     */
    protected $description = 'Send reminder push notifications';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $days = (int) $this->argument('days');

        if ($days !== 2 && $days !== 15) {
            $this->error($days . ' days not allowed!');
            return;
        }

        $drinks = CartItem::query()
            ->withAndWhereHas('cart', function ($query) use ($days) {
                $query
                    ->where('type', Cart::TYPE_PREMIUM)
                    ->whereDate('expires_at', Carbon::now()->add('days', $days)->format('Y-m-d'));
            })
            ->with('venue')
            ->whereNull('used_at')
            ->get();

        /** @var CartItem $drink */
        foreach ($drinks as $drink) {
            $user_id = $drink->cart->offered_to;
            if (empty($user_id)) continue;

            /** @var User $user */
            $user = User::query()->find($user_id);
            if (empty($user)) continue;

            // save activity
            dispatch(new ReminderDrinkActivity($user, $drink, $days));
            // send notification
            dispatch(new ReminderDrinkNotification($user, $drink, $days));
        }

        $this->info('Processed successfully ' . count($drinks) . ' drinks!');
    }
}
