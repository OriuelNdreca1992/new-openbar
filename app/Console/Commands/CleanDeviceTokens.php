<?php

namespace App\Console\Commands;

use App\Models\UserDevice;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Console\Command;

class CleanDeviceTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'device:tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command cleans the expired device tokens';

    protected PushNotification $push;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->push = new PushNotification('fcm');
        $this->push->setConfig([
            'priority' => 'high',
        ]);
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $fcmTokens = UserDevice::query()
            ->pluck('fcm_token')
            ->toArray();

        $chunks = array_chunk($fcmTokens, config('fcm.firebase_tokens_limit'));

        foreach ($chunks as $tokens) {
            $unregisteredDeviceTokens = $this->push->setMessage([
                'notification' => [
                    'title' => '',
                    'body' => ''
                ],
                'data' => [
                    'message' => 'Clearing unregistered tokens!',
                ],
                'dry_run' => true,
            ])
            ->setApiKey(config('fcm.server_key'))
            ->setDevicesToken($tokens)
            ->send()
            ->getUnregisteredDeviceTokens();

            UserDevice::query()
                ->whereIn('fcm_token', $unregisteredDeviceTokens)
                ->forceDelete();
        }
    }
}
