<?php

namespace App\Console;

use App\Console\Commands\GenerateInvoices;
use App\Console\Commands\SendReminderNotification;
use App\Console\Commands\SendPushNotification;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     */
    protected $commands = [
        GenerateInvoices::class,
        SendPushNotification::class,
        SendReminderNotification::class,
    ];

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('notification:send')->everyFiveMinutes();
        $schedule->command('reminder:send 15')->dailyAt('12:00');
        $schedule->command('reminder:send 2')->dailyAt('10:00');
        $schedule->command('invoice:generate')->weeklyOn(1, '1:00');
        $schedule->command('report:user-fees')->monthlyOn(1, '07:00');
        $schedule->command('report:expired-drinks')->monthlyOn(1, '08:00');
        $schedule->command('device:tokens')->monthlyOn(1, '10:00');
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
