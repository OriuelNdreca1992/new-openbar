<?php

namespace App\Generators;

use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\Support\PathGenerator\PathGenerator;

class CustomPathGenerator implements PathGenerator
{
    public function getPath(Media $media) : string
    {
        $collection = (empty($media->collection_name) || $media->collection_name == null) ? 'collection' : $media->collection_name;
        return $collection.DIRECTORY_SEPARATOR.$media->model_id.DIRECTORY_SEPARATOR.md5($media->id).DIRECTORY_SEPARATOR;
    }

    public function getPathForConversions(Media $media) : string
    {
        return $this->getPath($media) . 'conversions/';
    }

    public function getPathForResponsiveImages(Media $media): string
    {
        return $this->getPath($media) . 'responsive/';
    }
}
