<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterCompany extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $company;
    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company, $password)
    {
        $this->company = $company;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.register-company');
    }
}
