<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterVenue extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user, $venue, $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $venue, $password)
    {
        $this->user = $user;
        $this->venue = $venue;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this
            ->subject('Conferma registrazione')
            ->markdown('emails.register-venue');
    }
}
