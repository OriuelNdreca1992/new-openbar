<?php

namespace App\Helpers;

class ParseName
{
    /**
     * Split full name
     */
    public static function splitFullName(?string $fullName): array
    {
        if (is_null($fullName)) {
            return ['firstName' => '', 'lastName' => ''];
        }

        $parts = explode(" ", $fullName);
        if (count($parts) > 1) {
            $lastName = array_pop($parts);
            $firstName = implode(" ", $parts);
        } else {
            $firstName = $fullName;
            $lastName = " ";
        }

        return [
            'firstName' => $firstName,
            'lastName' => $lastName
        ];
    }
}
