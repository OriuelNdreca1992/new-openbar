<?php

namespace App\Helpers;

use App\Models\Venue;

class Review
{
    /**
     * Find venue score
     */
    public static function getVenueScore(?Venue $venue): int
    {
        if (empty($venue)) return 0;

        $reviews = $venue->reviews->pluck('review')->toArray();

        $score = $reviews
            ? number_format((float) array_sum($reviews) / count($reviews), 1, '.', '')
            : 0;

        return round($score);
    }
}
