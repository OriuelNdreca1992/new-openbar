<?php

namespace App\Helpers;

class Math
{
    /**
     * Find the average number of array
     */
    public static function getAverageOfArray(array $array): int {
        return round(array_sum($array) / count($array));
    }

    /**
     * Create verification code
     */
    public static function createVerificationCode($digits): string
    {
        return str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
    }
}
