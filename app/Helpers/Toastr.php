<?php

namespace App\Helpers;

class Toastr
{
    /**
     * Created.
     */
    public static function itemCreated(bool $status = false): void
    {
        if($status) {
            toastr()->success(config('constants.item_created_successfully'));
        } else {
            toastr()->error(config('constants.item_not_created_successfully'));
        }
    }

    /**
     * Updated.
     */
    public static function itemUpdated(bool $status = false): void
    {
        if($status) {
            toastr()->success(config('constants.item_updated_successfully'));
        } else {
            toastr()->error(config('constants.item_not_updated_successfully'));
        }
    }

    /**
     * Processed.
     */
    public static function itemProcessed(bool $status = false): void
    {
        if($status) {
            toastr()->success(config('constants.data_processed_successfully'));
        } else {
            toastr()->error(config('constants.data_not_processed_successfully'));
        }
    }

    /**
     * Deleted.
     */
    public static function itemDeleted(bool $status = false): void
    {
        if($status) {
            toastr()->success(config('constants.data_deleted_successfully'));
        } else {
            toastr()->error(config('constants.data_not_deleted_successfully'));
        }
    }
}
