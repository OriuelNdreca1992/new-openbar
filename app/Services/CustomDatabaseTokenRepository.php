<?php

namespace App\Services;

use App\Helpers\Math;
use Illuminate\Auth\Passwords\DatabaseTokenRepository;
use Illuminate\Support\Facades\Request;

class CustomDatabaseTokenRepository extends DatabaseTokenRepository
{
    // Overrides the standard token creation function

    public function createNewToken()
    {
        if (Request::wantsJson()) {
            // return JSON-formatted response
            return Math::createVerificationCode(6);
        } else {
            // return HTML response
            return substr(parent::createNewToken(), 0, 40);
        }
    }
}
