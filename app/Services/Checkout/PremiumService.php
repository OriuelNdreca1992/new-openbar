<?php

namespace App\Services\Checkout;

use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use App\Models\PremiumDrink;
use App\Models\User;
use App\Traits\SettingsHelper;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class PremiumService
{
    public function insert(User $user, array $drinks, ?string $message): ?Model
    {
        try {
            DB::beginTransaction();

            $amount = 0;
            foreach ($drinks as $drink) {
                $price = $drink['drink']->promotional_price ?: $drink['drink']->price;
                $amount += $price * $drink['quantity'];
            }

            $cartCreated = Cart::query()->create([
                'user_id' => auth()->id(),
                'offered_to' => $user->id,
                'amount' => $amount,
                'user_fee' => $this->calculateFee($amount),
                'transaction_type' => Cart::TRANSACTION_STRIPE,
                'transaction_id' => Str::uuid(),
                'payment_intent_id' => null,
                'status' => Cart::STATUS_PENDING,
                'last4' => null,
                'brand' => null,
                'type' => Cart::TYPE_PREMIUM,
                'expires_at' => Carbon::now()->add('day', 30),
                'message' => $message,
            ]);

            foreach ($drinks as $drink) {
                for ($i = 0 ; $i < (int) $drink['quantity']; $i++) {
                    CartItem::query()->insert([
                        'cart_id' => $cartCreated->id,
                        'venue_id' => $drink['drink']->venue_id,
                        'name' => $drink['drink']->product->name,
                        'price' => $drink['drink']->promotional_price ?: $drink['drink']->price,
                        'drinkable_type' => PremiumDrink::class,
                        'drinkable_id' => $drink['drink']->id,
                    ]);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            Log::error($e);

            DB::rollBack();

            return null;
        }

        return Cart::query()
            ->with('items')
            ->find($cartCreated->id);
    }

    public function calculateFee(float $amount): float
    {
        if ($amount <= 0) {
            return 0;
        }

        $settings = SettingsHelper::getUserFeeSettings();
        $fee = $settings['user_fee_fixed'];
        if ($amount > $settings['user_fee_min']) {
            $fee = ($settings['user_fee_percentage'] / 100) * $amount;
        }

        return $fee;
    }
}
