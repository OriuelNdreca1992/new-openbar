<?php

namespace App\Services\Checkout;

use App\Models\Offer;
use App\Models\Checkout\Cart;
use App\Models\Checkout\CartItem;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class OfferService
{
    public function insert(Offer $offer): ?Model
    {
        try {
            DB::beginTransaction();

            if ($offer->quantity <= 0) {
                throw new Exception('Insufficient 2x1 quantity!');
            }

            $cartCreated = Cart::query()->create([
                'user_id' => auth()->id(),
                'amount' => $offer->price,
                'user_fee' => 0, //ToDo
                'transaction_type' => Cart::TRANSACTION_CASH,
                'transaction_id' => Str::uuid(),
                'payment_intent_id' => null,
                'status' => Cart::STATUS_SUCCEEDED,
                'last4' => null,
                'brand' => null,
                'type' => Cart::TYPE_2X1,
                'expires_at' => Carbon::now(),
            ]);

            CartItem::query()->insert([
                'cart_id' => $cartCreated->id,
                'venue_id' => $offer->venue_id,
                'name' => $offer->product->name,
                'price' => $offer->price,
                'drinkable_type' => Offer::class,
                'drinkable_id' => $offer->id,
                'used_at' => Carbon::now(),
            ]);

            $offer->forceFill(['quantity' => $offer->quantity - 1])->save();

            DB::commit();
        } catch (Exception $e) {
            Log::error($e);

            DB::rollBack();

            return null;
        }

        return Cart::query()
            ->with('items')
            ->find($cartCreated->id);
    }
}
