<?php

namespace App\Services;

use App\Http\Requests\CheersRequest;
use App\Models\Cheers;
use App\Repositories\CheersRepository;
use Exception;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class CheersService
{
    protected CheersRepository $cheersRepository;

    public function __construct(CheersRepository $cheersRepository)
    {
        $this->cheersRepository = $cheersRepository;
    }

    /**
     * @throws Exception
     */
    public function save(array $data, ?Cheers $cheers = null): Cheers
    {
        $validate = Validator::make($data, (new CheersRequest())->rules());

        if ($validate->fails()) {
            throw new InvalidArgumentException($validate->errors()->first());
        }

        return $this->cheersRepository->save($data, $cheers);
    }
}
