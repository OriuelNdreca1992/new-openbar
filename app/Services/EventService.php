<?php

namespace App\Services;

use App\Http\Requests\DigitalEvents\EventRequest;
use App\Models\DigitalEvent\Event;
use App\Repositories\EventRepository;
use Exception;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class EventService
{
    protected EventRepository $eventRepository;

    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @throws Exception
     */
    public function saveEventData(array $data, ?Event $event = null): Event
    {
        $validate = Validator::make($data, (new EventRequest)->rules());

        if ($validate->fails()) {
            throw new InvalidArgumentException($validate->errors()->first());
        }

        return $this->eventRepository->save($data, $event);
    }
}
