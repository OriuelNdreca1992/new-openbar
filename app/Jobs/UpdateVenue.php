<?php

namespace App\Jobs;

use App\Traits\PhoneNumber;
use Carbon\Carbon;
use App\Models\Venue;
use App\Http\Requests\VenueRequest;
use Illuminate\Support\Facades\DB;

final class UpdateVenue
{
    private VenueRequest $request;
    private Venue $venue;

    /**
     * Create a new job instance.
     */
    public function __construct(VenueRequest $request, Venue $venue)
    {
        $this->request = $request;
        $this->venue = $venue;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $data = $this->request;
        $venue = $this->venue;

        DB::transaction(function () use ($data, $venue) {
            $venue->user()->update([
                'first_name' => $data->firstName(),
                'last_name'  => $data->lastName(),
                'phone'      => PhoneNumber::formatPhone($data->phone()),
                'email'      => $data->email(),
            ]);

            $fields = [
                'user_id'           => $venue->user->id,
                'name'              => $data->name(),
                'address'           => $data->address(),
                'city'              => $data->city(),
                'email'             => $data->venueEmail(),
                'postal_code'       => $data->postalCode(),
                'state'             => $data->state(),
                'country'           => $data->country(),
                'phone_number'      => $data->phoneNumber(),
                'seats_number'      => $data->seatsNumber(),
                'premium_drink_fee' => $data->premiumDrinkFee(),
                'description'       => $data->description(),
                'fiscal_name'       => $data->fiscalName(),
                'fiscal_address'    => $data->fiscalAddress(),
                'fiscal_city'       => $data->fiscalCity(),
                'fiscal_prov'       => $data->fiscalProv(),
                'fiscal_cap'        => $data->fiscalCap(),
                'fiscal_vat'        => $data->fiscalVat(),
                'fiscal_code'       => $data->fiscalCode(),
                'bic'               => $data->bic(),
                'iban'              => $data->iban(),
                'sdi_code'          => $data->sdiCode(),
                'certified_email'   => $data->certifiedEmail(),
                'lat'               => $data->lat(),
                'lng'               => $data->lng(),
            ];

            if (!is_null($data->status())) {
                $fields['status'] = $data->status();
            }
            if ($data->hasFile('image_event_url')) {
                $fields['image_event_url'] = $data->file('image_event_url')->store('venues', ['disk' => 'public']);
            }
            if ($data->hasFile('image_publish_url')) {
                $fields['image_publish_url'] = $data->file('image_publish_url')->store('venues', ['disk' => 'public']);
            }

            $venue->update($fields);

            $venue->types()->sync($data->types());

            foreach ($data['opening_hours'] as $key => $hour) {
                $venue->openingHours()->updateOrCreate([
                    'week_day' => $key
                ], [
                    'start' => $hour['start_hour'] ? Carbon::parse($hour['start_hour'])->format('H:i:s') : null,
                    'end' => $hour['end_hour'] ? Carbon::parse($hour['end_hour'])->format('H:i:s') : null
                ]);
            }
        });
    }
}
