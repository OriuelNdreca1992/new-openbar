<?php

namespace App\Jobs;

use App\Traits\PhoneNumber;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Venue;
use App\Mail\RegisterVenue;
use App\Http\Requests\VenueRequest;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

final class StoreVenue
{
    private VenueRequest $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(VenueRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     * @throws Exception
     */
    public function handle(): void
    {
        $data = $this->request;
        DB::transaction(function () use ($data) {
            $user = User::query()->create([
                'first_name' => $data->firstName(),
                'last_name'  => $data->lastName(),
                'phone'      => PhoneNumber::formatPhone($data->phone()),
                'email'      => $data->email(),
                'role'       => User::ROLE_VENUE,
                'status'     => $data->status() == 'on',
                'password'   => Hash::make($data->password()),
            ]);

            $user->assignRole('venue');

            $attributes = [
                'user_id'         => $user->id,
                'name'            => $data->name(),
                'email'           => $data->venueEmail() ? $data->venueEmail() : $data->email(),
                'address'         => $data->address(),
                'city'            => $data->city(),
                'postal_code'     => $data->postalCode(),
                'state'           => $data->state(),
                'country'         => $data->country(),
                'phone_number'    => $data->phoneNumber(),
                'seats_number'    => $data->seatsNumber(),
                'fiscal_name'     => $data->fiscalName(),
                'sdi_code'        => $data->sdiCode(),
                'fiscal_vat'      => $data->fiscalVat(),
                'certified_email' => $data->certifiedEmail(),
                'fiscal_address'  => $data->fiscalAddress(),
                'fiscal_city'     => $data->fiscalCity(),
                'fiscal_country'  => $data->fiscalCountry(),
                'fiscal_cap'      => $data->fiscalCap(),
                'pin_code'        => random_int(100000, 999999),
                'iban'            => $data->iban(),
                'lat'             => $data->lat(),
                'lng'             => $data->lng(),
            ];

            if (request()->route()->getName() === 'admin.venues.store') {
                $attributes += [
                    'image_event_url'   => $data->hasFile('image_event_url')
                        ? $data->file('image_event_url')->store('venues', ['disk' => 'public'])
                        : null,
                    'image_publish_url' => $data->hasFile('image_publish_url')
                        ? $data->file('image_publish_url')->store('venues', ['disk' => 'public'])
                        : null,
                    'status'            => $data->status() == 'on',
                    'description'       => $data->description(),
                    'fiscal_prov'       => $data->fiscalProv(),
                    'fiscal_code'       => $data->fiscalCode(),
                    'bic'               => $data->bic(),
                    'iban'              => $data->iban(),
                ];
            }

            $venue = Venue::query()->create($attributes);

            $venue->types()->sync($data->types());

            foreach ($data->openingHours() as $key => $hour) {
                $venue->openingHours()->create([
                    'week_day' => $key,
                    'start' => $hour['start_hour'] ? Carbon::parse($hour['start_hour'])->format('H:i') : null,
                    'end' => $hour['end_hour'] ? Carbon::parse($hour['end_hour'])->format('H:i') : null
                ]);
            }

            $emails = explode(',', config('mail.admin_email'));
            $emails[] = $data->email();

            Mail::to($emails)->send(new RegisterVenue($user, $venue, $data->password()));
        });
    }
}
