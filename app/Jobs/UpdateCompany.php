<?php

namespace App\Jobs;

use App\Models\Company;
use App\Http\Requests\CompanyRequest;
use App\Traits\PhoneNumber;

final class UpdateCompany
{

    private $request;
    private $company;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CompanyRequest $request, Company $company)
    {
        $this->request = $request;
        $this->company = $company;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->request;
        $company = $this->company;

        \DB::transaction(function () use ($data, $company) {
            $company->user()->update([
                'first_name' => $data->firstName(),
                'last_name'  => $data->lastName(),
                'phone'      => PhoneNumber::formatPhone($data->phone()),
                'status'     => $data->status() == 'on' ? true : false,
                'email'      => $data->email(),
            ]);

            $company->update([
                'user_id'         => $company->user->id,
                'name'            => $data->name(),
                'email'           => $data->companyEmail(),
                'city'            => $data->city(),
                'address'         => $data->address(),
                'phone_number'    => $data->phoneNumber(),
                'event_setup_fee' => $data->eventSetupFee(),
                'event_drink_fee' => $data->eventDrinkFee(),
                'event_venue_fee' => $data->eventVenueFee(),
                'status'          => $data->status() == 'on',
                'image_url'       => $data->hasFile('image_url')
                    ? $data->file('image_url')->store('companies', ['disk' => 'public'])
                    : $company->image_url,
                'fiscal_name'     => $data->fiscalName(),
                'fiscal_address'  => $data->fiscalAddress(),
                'fiscal_city'     => $data->fiscalCity(),
                'fiscal_prov'     => $data->fiscalProv(),
                'fiscal_cap'      => $data->fiscalCap(),
                'fiscal_vat'      => $data->fiscalVat(),
                'fiscal_code'     => $data->fiscalCode(),
                'sdi_code'        => $data->sdiCode(),
                'certified_email' => $data->certifiedEmail(),
                'bic'             => $data->bic(),
                'iban'            => $data->iban(),
            ]);
        });
    }
}
