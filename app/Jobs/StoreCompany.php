<?php

namespace App\Jobs;

use App\Models\User;
use App\Models\Company;
use App\Mail\RegisterCompany;
use App\Http\Requests\CompanyRequest;
use App\Traits\PhoneNumber;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

final class StoreCompany
{
    private CompanyRequest $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CompanyRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        DB::transaction(function() {
            $user = $this->createUser();
            $company = $this->createCompany($user->id);

            $emails = explode(',', config('mail.admin_email'));
            $emails[] = $this->request->email();

            Mail::to($emails)->send(new RegisterCompany($company, $this->request->password()));
        });
    }

    private function createUser(): Model|Builder
    {
        return User::query()->create([
            'first_name' => $this->request->firstName(),
            'last_name'  => $this->request->lastName(),
            'phone'      => PhoneNumber::formatPhone($this->request->phone()),
            'email'      => $this->request->email(),
            'status'     => $this->request->status() == 'on',
            'role'       => User::ROLE_COMPANY,
            'password'   => Hash::make($this->request->password()),
        ]);
    }

    private function createCompany($userId): Builder|Model
    {
       return Company::query()->create([
            'user_id'         => $userId,
            'name'            => $this->request->name(),
            'email'           => $this->request->companyEmail(),
            'city'            => $this->request->city(),
            'address'         => $this->request->address(),
            'phone_number'    => $this->request->phoneNumber(),
            'event_setup_fee' => $this->request->eventSetupFee(),
            'status'          => $this->request->status() == 'on',
            'image_url'       => $this->request->hasFile('image_url')
                ? $this->request->file('image_url')->store('companies', ['disk' => 'public'])
                : null,
            'fiscal_name'     => $this->request->fiscalName(),
            'fiscal_address'  => $this->request->fiscalAddress(),
            'fiscal_city'     => $this->request->fiscalCity(),
            'fiscal_prov'     => $this->request->fiscalProv(),
            'fiscal_cap'      => $this->request->fiscalCap(),
            'fiscal_vat'      => $this->request->fiscalVat(),
            'fiscal_code'     => $this->request->fiscalCode(),
            'sdi_code'        => $this->request->sdiCode(),
            'certified_email' => $this->request->certifiedEmail(),
            'bic'             => $this->request->bic(),
            'iban'            => $this->request->iban(),
        ]);
    }
}
