<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $table = 'product_categories';

    public function getImageUrlAttribute($value): ?string
    {
        return $value ? asset('uploads/' . $value) : null;
    }
}
