<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="category_id", type="integer", example=1),
 *    @OA\Property(property="name", type="string", example="Beer"),
 *    @OA\Property(property="image_url", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
 *    @OA\Property(property="status", type="boolean", example=true),
 *    @OA\Property(property="description", type="string", example="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 * )
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'status',
        'description',
        'image_url',
        'category_id',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected $guarded = [];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function getImageUrlAttribute($value): ?string
    {
        return $value ? asset('uploads/' . $value) : null;
    }

    public function getIosImageAttribute($value): ?string
    {
        return $value ? asset('uploads/' . $value) : null;
    }

    public function getAndroidImageAttribute($value): ?string
    {
        return $value ? asset('uploads/' . $value) : null;
    }
}
