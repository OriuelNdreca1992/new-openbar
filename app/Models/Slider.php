<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="title", type="string", example="This is the title."),
 *    @OA\Property(property="subtitle", type="string", example="This is the subtitle."),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 * )
 */
class Slider extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'subtitle',
        'created_at',
        'updated_at',
    ];

    protected $guarded = [];

    protected $hidden = ['pivot'];

    public function venues(): BelongsToMany
    {
        return $this->belongsToMany(Venue::class, 'slider_venue');
    }
}
