<?php

namespace App\Models\Checkout;

use App\Models\Offer;
use App\Models\PremiumDrink;
use App\Models\Venue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="venue_id", type="integer", example=1),
 *    @OA\Property(property="cart_id", type="integer", example=1),
 *    @OA\Property(property="name", type="string", example="Mojito"),
 *    @OA\Property(property="price", type="string", example="8.00"),
 *    @OA\Property(property="drinkable_type", type="string", example="App\Models\PremiumDrink"),
 *    @OA\Property(property="drinkable_id", type="integer", example=1),
 *    @OA\Property(property="used_at", type="string", example="2022-02-25T21:46:03.000000Z"),
 * )
 */
class CartItem extends Model
{
    protected $table = 'cart_items';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'id',
        'venue_id',
        'cart_id',
        'name',
        'price',
        'drinkable_id',
        'drinkable_type',
        'used_at',
    ];

    protected $casts = [
        'price' => 'decimal:2',
    ];

    protected $dates = ['used_at'];

    public $timestamps = false;

    public function cart(): BelongsTo
    {
        return $this->belongsTo(Cart::class, 'cart_id');
    }

    public function premiumDrink(): BelongsTo
    {
        return $this->belongsTo(PremiumDrink::class, 'drinkable_id');
    }

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class, 'drinkable_id');
    }

    public function venue(): BelongsTo
    {
        return $this->belongsTo(Venue::class, 'venue_id');
    }

    public function drinkable(): MorphTo
    {
        return $this->morphTo();
    }
}
