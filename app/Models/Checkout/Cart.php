<?php

namespace App\Models\Checkout;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="user_id", type="integer", example=1),
 *    @OA\Property(property="offered_to", type="integer", example=2),
 *    @OA\Property(property="amount", type="string", example="22.00"),
 *    @OA\Property(property="user_fee", type="string", example="0.5"),
 *    @OA\Property(property="type", type="string", example="premium"),
 *    @OA\Property(property="transaction_type", type="string", example="stripe"),
 *    @OA\Property(property="transaction_id", type="string", example="784bef2e-bba8-4914-9fad-9d0458ce8f3c"),
 *    @OA\Property(property="payment_intent_id", type="string", nullable=true, example=null),
 *    @OA\Property(property="status", type="string", example="pending"),
 *    @OA\Property(property="last4", type="string", nullable=true, example=null),
 *    @OA\Property(property="brand", type="string", nullable=true, example=null),
 *    @OA\Property(property="message", type="string", nullable=true, example="The message"),
 *    @OA\Property(property="expires_at", type="string", example="2022-03-25T21:46:03.000000Z"),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-25T21:46:03.000000Z"),
 *    @OA\Property(property="deleted_at", type="string", nullable=true, example=null),
 * )
 */
class Cart extends Model
{
    use HasFactory;

    protected $table = 'carts';

    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'id',
        'user_id',
        'offered_to',
        'amount',
        'user_fee',
        'type',
        'transaction_type',
        'transaction_id',
        'payment_intent_id',
        'status',
        'last4',
        'brand',
        'message',
        'expires_at',
        'created_at',
    ];

    protected $dates = ['created_at', 'expires_at'];

    public const STATUS_PENDING = 0;
    public const STATUS_FAILED = -1;
    public const STATUS_SUCCEEDED = 1;

    const STATUSES = [
        self::STATUS_PENDING => 'pending',
        self::STATUS_FAILED => 'failed',
        self::STATUS_SUCCEEDED => 'succeeded',
    ];

    public const TRANSACTION_CASH = 'cash';
    public const TRANSACTION_STRIPE = 'stripe';

    public const TYPE_2X1 = '2x1';
    public const TYPE_PREMIUM = 'premium';

    public function getStatusAttribute($value): string
    {
        return self::STATUSES[$value];
    }

    public function items(): HasMany
    {
        return $this->hasMany(CartItem::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function offered(): BelongsTo
    {
        return $this->belongsTo(User::class, 'offered_to');
    }
}
