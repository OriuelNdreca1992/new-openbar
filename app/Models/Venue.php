<?php

namespace App\Models;

use App\Models\Checkout\CartItem;
use App\Models\DigitalEvent\Event;
use App\Traits\Geographical;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="user_id", type="integer", example=1),
 *    @OA\Property(property="name", type="string", example="Larkin, Waters and Bahringer"),
 *    @OA\Property(property="address", type="string", example="405 Charles Lane\nAngelicaview, TN 69121-1697"),
 *    @OA\Property(property="email", type="string", example="chilpert@example.org"),
 *    @OA\Property(property="city", type="string", example="East Elian"),
 *    @OA\Property(property="postal_code", type="string", example="02807-5117"),
 *    @OA\Property(property="state", type="string", example="Texas"),
 *    @OA\Property(property="country", type="string", example="Burkina Faso"),
 *    @OA\Property(property="image_event_url", type="string", example=null),
 *    @OA\Property(property="image_publish_url", type="string", example=null),
 *    @OA\Property(property="phone_number", type="string", example="1-681-434-8884"),
 *    @OA\Property(property="description", type="string", example="Laudantium aut ut sit similique sed."),
 *    @OA\Property(property="seats_number", type="integer", example=60),
 *    @OA\Property(property="premium_drink_fee", type="integer", example=null),
 *    @OA\Property(property="fiscal_name", type="string", example="Williamson, Stark and West"),
 *    @OA\Property(property="fiscal_address", type="string", example="661 Karina Field\nSouth Chelsiestad, NE 24491-2203"),
 *    @OA\Property(property="fiscal_country", type="string", example="Armenia"),
 *    @OA\Property(property="sdi_code", type="string", example="16-9515960"),
 *    @OA\Property(property="certified_email", type="string", example="zcummerata@example.net"),
 *    @OA\Property(property="fiscal_city", type="string", example="Bernhardton"),
 *    @OA\Property(property="fiscal_prov", type="string", example="83-7240370"),
 *    @OA\Property(property="fiscal_cap", type="string", example="72-6889021"),
 *    @OA\Property(property="fiscal_vat", type="string", example="43-4414927"),
 *    @OA\Property(property="fiscal_code", type="string", example="22-3084019"),
 *    @OA\Property(property="bic", type="string", example="OOMIQIXA"),
 *    @OA\Property(property="iban", type="string", example="LT529369707906161887"),
 *    @OA\Property(property="lat", type="string", example="17.9401390000000"),
 *    @OA\Property(property="lng", type="string", example="-2.249014000000"),
 *    @OA\Property(property="pin_code", type="string", example="754551"),
 *    @OA\Property(property="status", type="boolean", example=true),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 * )
 */
class Venue extends Model
{
    use HasFactory, Notifiable, Geographical;

    const LATITUDE_FIELD = 'lat';
    const LONGITUDE_FIELD = 'lng';
    const LATITUDE_DEFAULT = 45.464165;
    const LONGITUDE_DEFAULT = 9.1874523;
    const INNER_RADIUS = 0;
    const OUTER_RADIUS  = 100000000000;

    protected $guarded = [];

    protected $hidden = ['pivot'];

    protected $casts = [
        'status' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        self::deleting(function($venue) {
            $venue->user()->delete();
        });

        self::creating(function($venue) {
            $setting = Setting::query()->where('setting', 'premium_fee')->first();
            if (! empty($setting)) {
                $venue->premium_drink_fee = $setting->value;
            }
        });
    }

    public function getImageEventUrlAttribute($value): ?string
    {
        return $value ? asset('uploads/' . $value) : null;
    }

    public function getImagePublishUrlAttribute($value): ?string
    {
        return $value ? asset('uploads/' . $value) : null;
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function invoice(): MorphMany
    {
        return $this->morphMany(Invoice::class, 'subject');
    }

    public function premiumDrinks(): HasMany
    {
        return $this->hasMany(PremiumDrink::class);
    }

    public function offers(): HasMany
    {
        return $this->hasMany(Offer::class);
    }

    public function cartItems(): HasMany
    {
        return $this->hasMany(CartItem::class);
    }

    public function openingHours(): HasMany
    {
        return $this->hasMany(OpeningHour::class);
    }

    public function types(): BelongsToMany
    {
        return $this->belongsToMany(Type::class, 'type_venue');
    }

    public function sliders(): BelongsToMany
    {
        return $this->belongsToMany(Slider::class, 'slider_venue');
    }

    public function events(): BelongsToMany
    {
        return $this->belongsToMany(Event::class, 'de_event_venues');
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(Review::class);
    }

    public function enable()
    {
        $this->user()->update(['status' => true]);
        $this->update(['status' => true]);
    }

    public function disable()
    {
        $this->user()->update(['status' => false]);
        $this->update(['status' => false]);
    }
}
