<?php

namespace App\Models;

use App\Models\Checkout\Cart;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="type", type="string", example="info_purchase"),
 *    @OA\Property(property="sender", type="string", example="user"),
 *    @OA\Property(property="message", type="string", example="message"),
 *    @OA\Property(property="message_en", type="string", example="message"),
 *    @OA\Property(property="sender_message", type="string", example=null),
 *    @OA\Property(property="offer_id", type="integer", example=1),
 *    @OA\Property(property="cart_id", type="integer", example=1),
 *    @OA\Property(property="cart_item_id", type="integer", example=1),
 *    @OA\Property(property="read", type="boolean", example=true),
 *    @OA\Property(property="feedback", type="string", example="cheers", enum={"cheers", "heart", "thumb"}),
 *    @OA\Property(property="phone", type="string", example=null),
 *    @OA\Property(property="drink_name", type="string", example="Drink"),
 *    @OA\Property(property="drink_image", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
 *    @OA\Property(property="venue_name", type="string", example="Venue"),
 *    @OA\Property(property="venue_address", type="string", example="Address"),
 *    @OA\Property(property="created_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 *    @OA\Property(property="expires_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 * )
 */
class UserActivity extends Model
{
    /**
     * The database table used by the model.
     */
    protected $table = 'user_activities';

    /**
     * The attributes that are not mass assignable.
     */
    protected $guarded = [
        'id',
    ];

    /**
     * Fillable fields for a Profile.
     */
    protected $fillable = [
        'type',
        'sender',
        'message',
        'message_en',
        'sender_message',
        'user_id',
        'sender_id',
        'receiver_id',
        'venue_id',
        'offer_id',
        'cart_id',
        'cart_item_id',
        'read',
        'feedback',
        'phone',
        'drink_name',
        'drink_image',
        'venue_name',
        'venue_address',
        'expires_at',
    ];

    protected $hidden = [
        'user_id',
        'sender_id',
        'receiver_id',
        'venue_id',
    ];

    /**
     * The attributes that should be cast.
     */
    protected $casts = [
        'read' => 'boolean',
    ];

    const ACTIVITY_TYPE_WELCOME = 'welcome';
    const ACTIVITY_TYPE_INFO_FEEDBACK = 'info_feedback';
    const ACTIVITY_TYPE_INFO_PURCHASE = 'info_purchase';
    const ACTIVITY_TYPE_INFO_EXPIRES = 'info_expires';

    const ACTIVITY_SENDER_USER = 'user';
    const ACTIVITY_SENDER_SYSTEM = 'system';

    const ACTIVITY_FEEDBACK_HEART = 'heart';
    const ACTIVITY_FEEDBACK_THUMB = 'thumb';
    const ACTIVITY_FEEDBACK_CHEERS = 'cheers';

    public function cart(): BelongsTo
    {
        return $this->belongsTo(Cart::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function userSender(): BelongsTo
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    public function userReceiver(): BelongsTo
    {
        return $this->belongsTo(User::class, 'receiver_id', 'id');
    }

    public function venue(): BelongsTo
    {
        return $this->belongsTo(Venue::class);
    }
}
