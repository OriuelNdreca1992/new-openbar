<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="name", type="string", example="Name"),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 * )
 */
class Type extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $hidden = ['pivot'];
}
