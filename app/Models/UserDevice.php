<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="device_manufacturer", type="string", example="Samsung"),
 *    @OA\Property(property="device_model", type="string", example="S20"),
 *    @OA\Property(property="os", type="string", example="Android"),
 *    @OA\Property(property="os_version", type="string", example="12"),
 *    @OA\Property(property="fcm_token", type="string", example="bk3RNwTe3H0:CI2k_HHwgIpoDKCIZvvDMExUdFQ3P1..."),
 *    @OA\Property(property="created_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 * )
 */
class UserDevice extends Model
{
    protected $table = 'user_devices';
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'device_manufacturer',
        'device_model',
        'os',
        'os_version',
        'fcm_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $hidden = [
        'id',
        'user_id',
        'created_at',
        'updated_at',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
