<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="venue_id", type="integer", example=1),
 *    @OA\Property(property="week_day", type="integer", example=1),
 *    @OA\Property(property="start", type="string", example="08:00:00"),
 *    @OA\Property(property="end", type="string", example="22:00:00"),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 * )
 */
class OpeningHour extends Model
{
    use HasFactory;

    protected $guarded = [];
}
