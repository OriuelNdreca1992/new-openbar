<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="user_id", type="integer", example=1),
 *    @OA\Property(property="name", type="string", example="Company name"),
 *    @OA\Property(property="email", type="string", example="example@email.com"),
 *    @OA\Property(property="city", type="string", example="Roma"),
 *    @OA\Property(property="address", type="string", example="Via Longhena 52"),
 *    @OA\Property(property="phone_number", type="string", example="+3928478327"),
 *    @OA\Property(property="image_url", type="string", nullable=true, example=null),
 *    @OA\Property(property="status", type="boolean", example=true),
 *    @OA\Property(property="fiscal_name", type="string", example="OpenBar Group"),
 *    @OA\Property(property="event_setup_fee", type="integer", example=5),
 *    @OA\Property(property="event_drink_fee", type="integer", example=7),
 *    @OA\Property(property="event_venue_fee", type="integer", example=4),
 *    @OA\Property(property="fiscal_address", type="string", example="00020 Via Longhena 52, Rome"),
 *    @OA\Property(property="fiscal_city", type="string", example="Rome"),
 *    @OA\Property(property="fiscal_prov", type="string", example="91-6215272"),
 *    @OA\Property(property="fiscal_cap", type="string", example="35-7697382"),
 *    @OA\Property(property="fiscal_vat", type="string", example="34-5604738"),
 *    @OA\Property(property="fiscal_code", type="string", example="95-2120162"),
 *    @OA\Property(property="sdi_code", type="string", example="01-9883216"),
 *    @OA\Property(property="certified_email", type="string", example="example@email.com"),
 *    @OA\Property(property="bic", type="string", example="ABCOITMM"),
 *    @OA\Property(property="iban", type="string", example="IT60X0542811101000000123456"),
 *    @OA\Property(property="created_at", type="string", example="2022-01-01T13:00:00.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-01-01T13:00:00.000000Z"),
 * )
 */
class Company extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'status' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        self::deleting(function($company) {
            $company->user()->delete();
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function invoice()
    {
        return $this->morphMany(Invoice::class, 'subject');
    }

    public function imageUrl()
    {
        return $this->image_url ? asset('uploads/' . $this->image_url) : null;
    }

    public function getImageUrlAttribute($value): ?string
    {
        return $value ? asset('uploads/' . $value) : null;
    }
}
