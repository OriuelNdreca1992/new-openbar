<?php

namespace App\Models;

use App\Models\Checkout\CartItem;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="product_id", type="integer", example=1),
 *    @OA\Property(property="venue_id", type="integer", example=1),
 *    @OA\Property(property="price", type="string", example="10.00"),
 *    @OA\Property(property="quantity", type="integer", example=10),
 *    @OA\Property(property="status", type="boolean", example=true),
 *    @OA\Property(property="all_the_time", type="boolean", example=true),
 *    @OA\Property(property="description", type="string", example="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
 *    @OA\Property(property="start", type="string", example="2022-02-23T00:00:00.000000Z"),
 *    @OA\Property(property="end", type="string", example="2022-02-25T00:00:00.000000Z"),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="offer_days", type="array", @OA\Items(allOf={@OA\Schema(ref="#/components/schemas/OfferDay")})),
 *    @OA\Property(property="product", type="object", allOf={@OA\Schema(ref="#/components/schemas/Product")}),
 * )
 */
class Offer extends Model
{
    public const DAILY_OFFER_PER_USER = 1;
    use HasFactory;

    protected $guarded = [];

    protected $dates = [
        'start',
        'end'
    ];

    protected $casts = [
        'status' => 'boolean',
        'all_the_time' => 'boolean',
    ];

    const DAYS = [
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
        0 => 'Sunday',
    ];

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = $value === 'on';
    }

    public function setAllTheTimeAttribute($value)
    {
        $this->attributes['all_the_time'] = $value === 'on';
    }

    public function offerDays(): HasMany
    {
        return $this->hasMany(OfferDay::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function drink(): MorphMany
    {
        return $this->morphMany(CartItem::class, 'drinkable');
    }
}
