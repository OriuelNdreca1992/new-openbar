<?php

namespace App\Models;

use App\Traits\InteractsWithHashedMedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="cheers_id", type="integer", example=1),
 *    @OA\Property(property="title", type="string", example="This is the title."),
 *    @OA\Property(property="description", type="string", example="This is the description."),
 *    @OA\Property(property="video_url", type="string", example="https://www.youtube.com/watch?v=PeUXBvRExic"),
 *    @OA\Property(property="image", type="string", nullable=true, example="http://openbar.localhost/uploads/cheers_paragraphs/1/c4ca4238a0b923820dcc509a6f75849b/JoYn8hJWabqlSIh9grrcGWNMEbUEeaoQbthY3uXc.png"),
 * )
 */
class CheersParagraph extends Model implements HasMedia
{
    use InteractsWithHashedMedia;

    const COLLECTION = 'cheers_paragraphs';

    protected $table = 'cheers_paragraphs';
    protected $primaryKey = 'id';

    protected $fillable = [
        'cheers_id',
        'title',
        'description',
        'video_url',
    ];

    protected $appends = ['image'];

    protected $hidden = ['media'];

    public $timestamps = false;

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::COLLECTION)->singleFile();
    }

    /**
     * @throws InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb');
    }

    public function getImageAttribute(): string
    {
        return $this->getFirstMediaUrl(self::COLLECTION);
    }

    public function getOriginalImage(): ?Media
    {
        return $this->getFirstMedia(self::COLLECTION);
    }

    public function cheers(): BelongsTo
    {
        return $this->belongsTo(Cheers::class);
    }
}
