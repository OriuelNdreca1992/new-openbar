<?php

namespace App\Models;

use App\Traits\InteractsWithHashedMedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="title", type="string", example="This is the title."),
 *    @OA\Property(property="subtitle", type="string", example="This is the subtitle."),
 *    @OA\Property(property="status", type="boolean", example=true),
 *    @OA\Property(property="start_date", type="string", example="2022-01-01T21:00:00.000000Z"),
 *    @OA\Property(property="end_date", type="string", example="2022-01-01T23:00:00.000000Z"),
 *    @OA\Property(property="date_label", type="string", nullable=true, example=null),
 *    @OA\Property(property="time_label", type="string", nullable=true, example=null),
 *    @OA\Property(property="order", type="integer", nullable=true, example=1),
 *    @OA\Property(property="created_at", type="string", example="2022-01-01T13:00:00.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-01-01T13:00:00.000000Z"),
 *    @OA\Property(property="image", type="string", nullable=true, example="http://openbar.localhost/uploads/cheers/1/c4ca4238a0b923820dcc509a6f75849b/JoYn8hJWabqlSIh9grrcGWNMEbUEeaoQbthY3uXc.png"),
 *    @OA\Property(property="type", type="string", example="openbar|company|venue"),
 * )
 */
class Cheers extends Model implements HasMedia
{
    use InteractsWithHashedMedia;

    const COLLECTION = 'cheers';

    protected $table = 'cheers';
    protected $primaryKey = 'id';

    public const CHEER_TYPE_OPENBAR = 'openbar';
    public const CHEER_TYPE_VENUE = 'venue';
    public const CHEER_TYPE_COMPANY = 'company';
    public const CHEER_TYPES = [
        self::CHEER_TYPE_OPENBAR => 'Openbar',
        self::CHEER_TYPE_VENUE => 'Venue',
        self::CHEER_TYPE_COMPANY => 'Company',
    ];

    protected $fillable = [
        'promoted_type',
        'promoted_id',
        'order',
        'type',
        'title',
        'subtitle',
        'status',
        'start_date',
        'end_date',
        'date_label',
        'time_label',
        'created_at',
        'updated_at',
    ];

    protected $appends = ['image', 'type'];

    protected $hidden = ['media', 'promoted_type', 'promoted_id'];

    protected $casts = [
        'status' => 'boolean',
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::COLLECTION)->singleFile();
    }

    /**
     * @throws InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb');
    }

    public function getImageAttribute(): string
    {
        return $this->getFirstMediaUrl(self::COLLECTION);
    }

    public function getOriginalImage(): ?Media
    {
        return $this->getFirstMedia(self::COLLECTION);
    }

    public function getTypeAttribute(): string
    {
        if ($this->promoted_type === Venue::class) {
            return self::CHEER_TYPE_VENUE;
        } elseif ($this->promoted_type === Company::class) {
            return self::CHEER_TYPE_COMPANY;
        }

        return self::CHEER_TYPE_OPENBAR;
    }

    public function paragraphs(): HasMany
    {
        return $this->hasMany(CheersParagraph::class);
    }

    public function promoted(): MorphTo
    {
        return $this->morphTo();
    }
}
