<?php

namespace App\Models;

use App\Helpers\Math;
use App\Models\Checkout\Cart;
use App\Notifications\CustomResetPasswordNotification;
use App\Notifications\UserPhoneVerifyNotification;
use App\Notifications\UserVerifyNotification;
use App\Notifications\UserWelcomeNotification;
use App\Traits\InteractsWithHashedMedia;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="first_name", type="string", example="Name"),
 *    @OA\Property(property="last_name", type="string", example="Surname"),
 *    @OA\Property(property="email", type="string", example="user@user.com"),
 *    @OA\Property(property="phone", type="string", example="123456789"),
 *    @OA\Property(property="birthdate", type="string", example="2000-01-01"),
 *    @OA\Property(property="gender", type="string", example="Male"),
 *    @OA\Property(property="status", type="boolean", example=true),
 *    @OA\Property(property="role", type="string", example="admin"),
 *    @OA\Property(property="city", type="string", example="Milano"),
 *    @OA\Property(property="location", type="string", example="Via Longhena 52"),
 *    @OA\Property(property="lang", type="string", example="en"),
 *    @OA\Property(property="phone_verified_at", type="string", nullable=true, example="2022-03-15T00:22:25.000000Z"),
 *    @OA\Property(property="email_verified_at", type="string", nullable=true, example="2022-03-15T00:22:25.000000Z"),
 *    @OA\Property(property="created_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 *    @OA\Property(property="avatar", type="object",
 *        @OA\Property(property="thumb", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
 *        @OA\Property(property="original", type="string", example="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png"),
 *    ),
 * )
 */
class User extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, InteractsWithHashedMedia;

    const COLLECTION = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'role',
        'birthdate',
        'gender',
        'status',
        'password',
        'lang',
        'email_verified_at',
        'phone_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'media',
        'password',
        'remember_token',
        'email_verify_token',
        'phone_verify_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'boolean',
        'birthdate' => 'date',
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];

    protected $dates = ['created_at'];

    protected $appends = ['avatar'];

    const ROLE_ADMIN = 1;
    const ROLE_DIGITAL_EVENT = 2;
    const ROLE_VENUE = 3;
    const ROLE_COMPANY = 4;
    const ROLE_USER = 5;
    const ROLE_SUPER_ADMIN = 6;

    const USER_TYPE = [
        self::ROLE_ADMIN => 'admin',
        self::ROLE_DIGITAL_EVENT => 'digital-event',
        self::ROLE_VENUE => 'venue',
        self::ROLE_COMPANY => 'company',
        self::ROLE_USER => 'app-user',
        self::ROLE_SUPER_ADMIN => 'super-admin',
    ];

    const USER_GENDER_UNFULFILLED = 0;
    const USER_GENDER_MALE = 1;
    const USER_GENDER_FEMALE = 2;
    const USER_GENDER = [
        self::USER_GENDER_UNFULFILLED   => 'Unfulfilled',
        self::USER_GENDER_MALE          => 'Male',
        self::USER_GENDER_FEMALE        => 'Female',
    ];

    public function isSuperAdmin(): bool
    {
        return $this->role === self::USER_TYPE[self::ROLE_SUPER_ADMIN];
    }

    public function venue(): HasOne
    {
        return $this->hasOne(Venue::class);
    }

    public function company(): HasOne
    {
        return $this->hasOne(Company::class);
    }

    public function carts(): HasMany
    {
        return $this->hasMany(Cart::class);
    }

    public function benefitedCarts(): HasMany
    {
        return $this->hasMany(Cart::class, 'offered_to');
    }

    public function getFullName(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getFullNameWithID(): string
    {
        return $this->getFullName() . ' (ID: ' . $this->id . ')';
    }

    public function socials(): HasMany
    {
        return $this->hasMany(UserSocial::class);
    }

    public function devices(): HasMany
    {
        return $this->hasMany(UserDevice::class);
    }

    public function activities(): HasMany
    {
        return $this->hasMany(UserActivity::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::COLLECTION)->singleFile();
    }

    /** @throws InvalidManipulation */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb');
    }

    public function getGenderLabel(): string
    {
        return self::USER_GENDER[$this->gender];
    }

    public function getRoleAttribute($value): ?string
    {
        return $value ? self::USER_TYPE[$value] : $value;
    }

    public function getAvatarAttribute(): array
    {
        return [
            'thumb' => $this->getFirstMediaUrl(self::COLLECTION, 'thumb'),
            'original' => $this->getFirstMediaUrl(self::COLLECTION),
        ];
    }

    public function getOriginalImage(): ?Media
    {
        return $this->getFirstMedia(self::COLLECTION);
    }

    public function sendPasswordResetNotification($token): void
    {
        $this->notify(new CustomResetPasswordNotification($this->getFullName(), $token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->forceFill([
            'email_verify_token' => Math::createVerificationCode(6),
        ])->save();

        $this->notify(new UserVerifyNotification($this));
    }

    public function sendEmailWelcomeNotification()
    {
        $this->notify(new UserWelcomeNotification($this));
    }

    public function sendPhoneVerificationNotification()
    {
        $this->forceFill([
            'phone_verify_token' => Math::createVerificationCode(6),
        ])->save();

        $this->notify(new UserPhoneVerifyNotification($this));
    }

    public function markEmailAsVerified(): bool
    {
        return $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
            'email_verify_token' => null,
            'status' => $this->hasVerifiedPhone(),
        ])->save();
    }

    public function markPhoneAsVerified(): bool
    {
        return $this->forceFill([
            'phone_verified_at' => $this->freshTimestamp(),
            'phone_verify_token' => null,
            'status' => $this->hasVerifiedEmail(),
        ])->save();
    }

    public function hasVerifiedPhone(): bool
    {
        return ! is_null($this->phone_verified_at);
    }

    public function delete(): void
    {
        if (!str_starts_with($this->email, 'DELETED_')) {
            $uniqueCode = $this->uniqueCode();
            $this->email = 'DELETED_' . $uniqueCode . '_' . $this->email;
            $this->phone = $this->phone . '_' . $uniqueCode . '_DELETED';
            $this->status = false;
            $this->save();
        }
        $this->socials()->delete();
        $this->tokens()->delete();
    }

    function uniqueCode(int $limit = 9): string
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }

    public function routeNotificationForTwilio()
    {
        return $this->phone;
    }
}
