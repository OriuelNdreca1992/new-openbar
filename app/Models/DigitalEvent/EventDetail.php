<?php

namespace App\Models\DigitalEvent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class EventDetail extends Model
{
    protected $table = 'de_event_details';
    protected $primaryKey = 'id';

    protected $fillable = [
        'title',
        'description',
    ];

    public $timestamps = false;

    /**
     * Get the event.
     */
    public function event(): HasOne
    {
        return $this->hasOne(Event::class, 'id','event_id');
    }
}
