<?php

namespace App\Models\DigitalEvent;

use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class EventDrink extends Model
{
    protected $table = 'de_event_drinks';
    protected $primaryKey = 'id';

    protected $fillable = [
        'event_id',
        'product_id',
        'title',
        'description',
        'price',
        'quantity',
        'user_quantity',
    ];

    public $timestamps = false;

    /**
     * Get the event.
     */
    public function event(): HasOne
    {
        return $this->hasOne(Event::class, 'id','event_id');
    }

    /**
     * Get the product.
     */
    public function product(): HasOne
    {
        return $this->hasOne(Product::class, 'id','product_id');
    }
}
