<?php

namespace App\Models\DigitalEvent;

use App\Models\Venue;
use App\Traits\InteractsWithHashedMedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Image\Exceptions\InvalidManipulation;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Event extends Model implements HasMedia
{
    use InteractsWithHashedMedia;

    const COLLECTION = 'events';

    protected $table = 'de_events';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
        'promo',
        'status',
        'created_at',
        'updated_at',
    ];

    protected $appends = ['image'];

    public const TYPOLOGY_OPENBAR = 'openbar';
    public const TYPOLOGY_VENUE = 'venue';
    public const TYPOLOGY_COMPANY = 'company';
    public const TYPOLOGY_TYPES = [
        self::TYPOLOGY_OPENBAR => 'Openbar',
        self::TYPOLOGY_VENUE => 'Venue',
        self::TYPOLOGY_COMPANY => 'Company',
    ];

    public const PROMO_TYPE_EVENT = 'event';
    public const PROMO_TYPE_FREE_DRINK = 'free-drink';
    public const PROMO_TYPE_SPECIAL_PRICE = 'special-price';
    public const PROMO_TYPE_CUSTOM = 'custom';
    public const PROMO_TYPES = [
        self::PROMO_TYPE_EVENT => 'Event',
        self::PROMO_TYPE_FREE_DRINK => 'Free Drink',
        self::PROMO_TYPE_SPECIAL_PRICE => 'Special Price',
        self::PROMO_TYPE_CUSTOM => 'Custom Label',
    ];

    public const FORMAT_SMALL = 'small';
    public const FORMAT_BIG = 'big';
    public const FORMAT_TYPES = [
        self::FORMAT_SMALL => 'Small',
        self::FORMAT_BIG => 'Big',
    ];

    public const BUDGET_SMALL = 5000;
    public const BUDGET_MEDIUM = 12000;
    public const BUDGET_BIG = 30000;
    public const BUDGET_CUSTOM = 'custom';
    public const BUDGETS = [
        self::BUDGET_SMALL => '€ 5,000',
        self::BUDGET_MEDIUM => '€ 12,000',
        self::BUDGET_BIG => '€ 30,000',
        self::BUDGET_CUSTOM => 'Custom',
    ];

    /**
     * Get the details of the event.
     */
    public function details(): HasMany
    {
        return $this->hasMany(EventDetail::class, 'event_id','id');
    }

    /**
     * Get the venues of the event.
     */
    public function venues(): BelongsToMany
    {
        return $this->belongsToMany(Venue::class, 'de_event_venues');
    }

    /**
     * Get the drinks of the event.
     */
    public function drinks(): HasMany
    {
        return $this->hasMany(EventDrink::class, 'event_id','id');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::COLLECTION)->singleFile();
    }

    /**
     * @throws InvalidManipulation
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb');
    }

    public function getImageAttribute(): string
    {
        return $this->getFirstMediaUrl(self::COLLECTION);
    }

    public function getOriginalImage(): ?Media
    {
        return $this->getFirstMedia(self::COLLECTION);
    }
}
