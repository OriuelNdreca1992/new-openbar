<?php

namespace App\Models\DigitalEvent;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'de_cards';

    protected $fillable = [
        'name',
        'status',
        'created_at',
        'updated_at',
    ];
}
