<?php

namespace App\Models\DigitalEvent;

use App\Traits\InteractsWithHashedMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Label extends Model implements HasMedia
{
    use InteractsWithHashedMedia;

    const COLLECTION = 'card-labels';

    protected $table = 'de_card_labels';

    protected $fillable = [
        'card_id',
        'name',
        'icon',
        'status',
        'order',
        'created_at',
        'updated_at',
    ];

    protected $appends = ['image'];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection(self::COLLECTION)->singleFile();
    }

    public function getImageAttribute(): string
    {
        return $this->getFirstMediaUrl(self::COLLECTION);
    }

    public function getOriginalImage(): ?Media
    {
        return $this->getFirstMedia(self::COLLECTION);
    }
}
