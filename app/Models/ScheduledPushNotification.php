<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduledPushNotification extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $fillable = [
        'title',
        'body',
        'scheduled_time',
        'status',
        'created_at',
        'updated_at',
    ];

    protected $dates = ['scheduled_time'];

    const PENDING = 0;
    const COMMITTED = 1;
}
