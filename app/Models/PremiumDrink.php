<?php

namespace App\Models;

use App\Models\Checkout\CartItem;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="venue_id", type="integer", example=1),
 *    @OA\Property(property="product_id", type="integer", example=1),
 *    @OA\Property(property="price", type="string", example="10.00"),
 *    @OA\Property(property="promotional_price", type="string", nullable=true, example="8.00"),
 *    @OA\Property(property="status", type="boolean", example=true),
 *    @OA\Property(property="description", type="string", example="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."),
 *    @OA\Property(property="created_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-23T21:46:03.000000Z"),
 *    @OA\Property(property="discount", type="string", example="20%"),
 *    @OA\Property(property="product", type="object", allOf={@OA\Schema(ref="#/components/schemas/Product")}),
 * )
 */
class PremiumDrink extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'status' => 'boolean',
        'price' => 'decimal:2',
        'promotional_price' => 'decimal:2',
    ];

    protected $appends = ['discount'];

    protected $table = 'premium_drinks';

    public function getDiscountAttribute(): ?string
    {
        return $this->promotional_price > 0
            ? (($this->price - $this->promotional_price) / $this->price) * 100 . '%'
            : null;
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function venue(): BelongsTo
    {
        return $this->belongsTo(Venue::class);
    }

    public function imageUrl(): ?string
    {
        return $this->image_url ? asset('uploads/' . $this->image_url) : null;
    }

    public function drink(): MorphMany
    {
        return $this->morphMany(CartItem::class, 'drinkable');
    }
}
