<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function subject()
    {
        return $this->morphTo();
    }

    public function paid()
    {
        $this->update(['status' => true]);
    }

    public function unpaid()
    {
        $this->update(['status' => false]);
    }
}
