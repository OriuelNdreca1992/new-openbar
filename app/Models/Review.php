<?php

namespace App\Models;

use App\Models\Checkout\CartItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @OA\Schema(
 *    @OA\Property(property="id", type="integer", example=1),
 *    @OA\Property(property="user_id", type="integer", example=1),
 *    @OA\Property(property="venue_id", type="integer", example=1),
 *    @OA\Property(property="cart_item_id", type="integer", example=1),
 *    @OA\Property(property="review", type="integer", example=5),
 *    @OA\Property(property="created_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 *    @OA\Property(property="updated_at", type="string", example="2022-02-07T00:33:55.000000Z"),
 * )
 */
class Review extends Model
{
    /**
     * The database table used by the model.
     */
    protected $table = 'reviews';

    /**
     * The attributes that are not mass assignable.
     */
    protected $guarded = [
        'id',
    ];

    /**
     * Fillable fields for a Profile.
     */
    protected $fillable = [
        'user_id',
        'venue_id',
        'cart_item_id',
        'review',
    ];

    protected $hidden = [
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function venue(): BelongsTo
    {
        return $this->belongsTo(Venue::class);
    }

    public function cartItem(): BelongsTo
    {
        return $this->belongsTo(CartItem::class);
    }
}
