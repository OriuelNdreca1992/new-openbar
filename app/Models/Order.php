<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;

    protected $dates = ['expires_at'];

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function premiumDrink()
    {
        return $this->belongsTo(PremiumDrink::class, 'drinkable_id');
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class, 'drinkable_id');
    }

    public function venue()
    {
        return $this->belongsTo(Venue::class, 'venue_id');
    }

    public function drinkable()
    {
        return $this->morphTo();
    }
}
