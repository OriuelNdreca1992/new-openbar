<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormGroupServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('radioGroup', 'admin.metronic.layout.components.radio', ['params', 'errors']);
        Form::component('textGroup', 'admin.metronic.layout.components.text', ['params', 'errors']);
        Form::component('translatableTextGroup', 'admin.metronic.layout.components.translatable_text', ['params', 'errors', 'item']);
        Form::component('selectGroup', 'admin.metronic.layout.components.select', ['params', 'errors']);
        Form::component('switchGroup', 'admin.metronic.layout.components.switch', ['params', 'errors']);
        Form::component('checkboxesGroup', 'admin.metronic.layout.components.checkboxes', ['params', 'errors', 'items']);
        Form::component('textareaGroup', 'admin.metronic.layout.components.textarea', ['params', 'errors']);
        Form::component('translatableTextareaGroup', 'admin.metronic.layout.components.translatable_textarea', ['params', 'errors', 'item']);
        Form::component('fileGroup', 'admin.metronic.layout.components.file', ['params', 'errors', 'item']);
        Form::component('imageGroup', 'admin.metronic.layout.components.image', ['params', 'errors', 'item']);
        Form::component('dateGroup', 'admin.metronic.layout.components.date', ['params', 'errors']);
        Form::component('dateTimeGroup', 'admin.metronic.layout.components.datetime', ['params', 'errors']);
        Form::component('dateTimeRangeGroup', 'admin.metronic.layout.components.datetimerange', ['params', 'errors']);
        Form::component('arrayTextGroup', 'admin.metronic.layout.components.array.text', ['params', 'errors']);
    }
}
