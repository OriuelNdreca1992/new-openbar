'use strict';

// Class definition
var KTImageInputDemo = function () {
	// Private functions
	var initDemos = function () {
		// Example 1
		var companyImage = new KTImageInput('company_image');

		// Example 2
		var venueImageUrl = new KTImageInput('venue_image_url');

		// Example 3
		var venueEventImageUrl = new KTImageInput('venue_event_image_url');

		// Example 4
		var venuePublishImageUrl = new KTImageInput('venue_publish_image_url');

        var productCategoryImage = new KTImageInput('product_category_image');

        var productImageUrl = new KTImageInput('product_image_url');
        var productImageIos = new KTImageInput('product_image_ios');
        var productImageAndroid = new KTImageInput('product_image_android');

		avatar4.on('cancel', function(imageInput) {
			swal.fire({
                title: 'Image successfully canceled !',
                type: 'success',
                buttonsStyling: false,
                confirmButtonText: 'Awesome!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
		});

		avatar4.on('change', function(imageInput) {
			swal.fire({
                title: 'Image successfully changed !',
                type: 'success',
                buttonsStyling: false,
                confirmButtonText: 'Awesome!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
		});

		avatar4.on('remove', function(imageInput) {
			swal.fire({
                title: 'Image successfully removed !',
                type: 'error',
                buttonsStyling: false,
                confirmButtonText: 'Got it!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
		});

		// Example 5
		var avatar5 = new KTImageInput('kt_image_5');

		avatar5.on('cancel', function(imageInput) {
			swal.fire({
                title: 'Image successfully changed !',
                type: 'success',
                buttonsStyling: false,
                confirmButtonText: 'Awesome!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
		});

		avatar5.on('change', function(imageInput) {
			swal.fire({
                title: 'Image successfully changed !',
                type: 'success',
                buttonsStyling: false,
                confirmButtonText: 'Awesome!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
		});

		avatar5.on('remove', function(imageInput) {
			swal.fire({
                title: 'Image successfully removed !',
                type: 'error',
                buttonsStyling: false,
                confirmButtonText: 'Got it!',
                confirmButtonClass: 'btn btn-primary font-weight-bold'
            });
		});
	}

	return {
		// public functions
		init: function() {
			initDemos();
		}
	};
}();

KTUtil.ready(function() {
	KTImageInputDemo.init();
});
