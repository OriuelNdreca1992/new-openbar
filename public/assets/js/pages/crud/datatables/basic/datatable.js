"use strict";

var KTDatatablesBasicPaginations = function() {

	var initTable1 = function() {
		var table = $('#kt_datatable');

		// begin first table
		table.DataTable({
			responsive: true,
			pagingType: 'full_numbers',
			columnDefs: [
				// {
				// 	targets: -1,
				// 	width: '125px',
				// 	title: 'Actions',
				// 	orderable: false,
				// },
				// {
				// 	targets: 5,
				// 	render: function(data, type, full, meta) {
				// 		var status = {
				// 			'admin' : {'title': 'Admin', 'class': ' label-light-info'},
				// 			'digital-event' : {'title': 'Digital Event', 'class': ' label-light-warning'},
				// 		};
				// 		if (typeof status[data] === 'undefined') {
				// 			return data;
				// 		}
				// 		return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
				// 	},
				// },
			]
		});
	};

	return {
		init: function() {
			initTable1();
		},
	};
}();

jQuery(document).ready(function() {
	KTDatatablesBasicPaginations.init();
});
