// Class definition
var KTFormControlsAdministrator = function () {
    // Private functions
    var _manageAdministratorForm = function () {
        var form = document.getElementById('manage_administrator_form');
        FormValidation.formValidation(
            form,
            {
               fields: {
                    first_name: {
                        validators: {
                            notEmpty: {
                                message: 'First Name is required'
                            }
                        }
                    },
                    last_name: {
                        validators: {
                            notEmpty: {
                                message: 'Last Name is required'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email address is required'
                            },
                            emailAddress: {
                                message: 'The value is not a valid email address'
                            }
                        }
                    },
                    phone: {
                        validators: {
                            notEmpty: {
                                message: 'Phone is required'
                            }
                        }
                    },
                    role: {
                        validators: {
                            notEmpty: {
                                message: 'Role is required'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The password must be at least 8 characters.'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );
    }

    var _validatePassword = function () {
        var form = document.getElementById('password_validation');
        FormValidation.formValidation(
            form,
            {
               fields: {
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The password must be at least 8 characters.'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit()
                }
            }
        );
    }
    return {
        init: function() {
            _manageAdministratorForm();
            _validatePassword();
        }
    };
}();

// Class definition
var KTFormControlsCompany = function () {
    // Private functions
    var _validateCompanyRequest = function () {
        var form = document.getElementById('validate_company_request');
        FormValidation.formValidation(
            form,
            {
               fields: {
                first_name: {
                        validators: {
                            notEmpty: {
                                message: 'First Name is required'
                            }
                        }
                    },
                    last_name: {
                        validators: {
                            notEmpty: {
                                message: 'Last Name is required'
                            }
                        }
                    },
                    phone: {
                        validators: {
                            notEmpty: {
                                message: 'Phone is required'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email address is required'
                            },
                            emailAddress: {
                                message: 'The value is not a valid email address'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The password must be at least 8 characters.'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                                        name: {
                        validators: {
                            notEmpty: {
                                message: 'Company Name is required'
                            }
                        }
                    },
                    company_email: {
                        validators: {
                            notEmpty: {
                                message: 'Company Email is required'
                            }
                        }
                    },
                    city: {
                        validators: {
                            notEmpty: {
                                message: 'City is required'
                            }
                        }
                    },
                    phone_number: {
                        validators: {
                            notEmpty: {
                                message: 'Phone Number is required'
                            }
                        }
                    },
                    phone_number: {
                        validators: {
                            notEmpty: {
                                message: 'Phone Number is required'
                            }
                        }
                    },
                    company_logo: {
                        validators: {
                            notEmpty: {
                                message: 'Logo is required'
                            }
                        }
                    },
                    fiscal_name: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal Name is required'
                            }
                        }
                    },
                    fiscal_address: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal Address is required'
                            }
                        }
                    },
                    fiscal_address: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal Address is required'
                            }
                        }
                    },
                    fiscal_city: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal City is required'
                            }
                        }
                    },
                    fiscal_prov: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal Prov is required'
                            }
                        }
                    },
                    fiscal_cap: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal Cap is required'
                            }
                        }
                    },
                    fiscal_vat: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal Vat is required'
                            }
                        }
                    },
                    fiscal_code: {
                        validators: {
                            notEmpty: {
                                message: 'Fiscal Code is required'
                            }
                        }
                    },
                    sdi_code: {
                        validators: {
                            notEmpty: {
                                message: 'Sdi Code is required'
                            }
                        }
                    },
                    certified_email: {
                        validators: {
                            notEmpty: {
                                message: 'Pec is required'
                            }
                        }
                    },
                    bic: {
                        validators: {
                            notEmpty: {
                                message: 'Bic/Swift is required'
                            }
                        }
                    },
                    iban: {
                        validators: {
                            notEmpty: {
                                message: 'Iban is required'
                            }
                        }
                    },
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit()
                }
            }
        );
    }

    return {
        init: function() {
            _validateCompanyRequest();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormControlsAdministrator.init();
});

jQuery(document).ready(function() {
    KTFormControlsCompany.init();
});
















// var KTFormControls = function () {
//     // Private functions
//     var _manageAdministratorForm = function () {
//         var form = document.getElementById('manage_administrator_form');
//         FormValidation.formValidation(
//             form,
//             {
//                fields: {
//                     first_name: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'First Name is required'
//                             }
//                         }
//                     },
//                     last_name: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Last Name is required'
//                             }
//                         }
//                     },
//                     email: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Email address is required'
//                             },
//                             emailAddress: {
//                                 message: 'The value is not a valid email address'
//                             }
//                         }
//                     },
//                     phone: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Phone is required'
//                             }
//                         }
//                     },
//                     role: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Role is required'
//                             }
//                         }
//                     },
//                     password: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'The password is required'
//                             },
//                             stringLength: {
//                                 min: 8,
//                                 message: 'The password must be at least 8 characters.'
//                             }
//                         }
//                     },
//                     password_confirmation: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'The password confirmation is required'
//                             },
//                             identical: {
//                                 compare: function() {
//                                     return form.querySelector('[name="password"]').value;
//                                 },
//                                 message: 'The password and its confirm are not the same'
//                             }
//                         }
//                     },
//                 },

//                 plugins: { //Learn more: https://formvalidation.io/guide/plugins
//                     trigger: new FormValidation.plugins.Trigger(),
//                     // Bootstrap Framework Integration
//                     bootstrap: new FormValidation.plugins.Bootstrap(),
//                     // Validate fields when clicking the Submit button
//                     submitButton: new FormValidation.plugins.SubmitButton(),
//                     // Submit the form when all fields are valid
//                     defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
//                 }
//             }
//         );
//     }

//     var _validatePassword = function () {
//         var form = document.getElementById('password_validation');
//         FormValidation.formValidation(
//             form,
//             {
//                fields: {
//                     password: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'The password is required'
//                             },
//                             stringLength: {
//                                 min: 8,
//                                 message: 'The password must be at least 8 characters.'
//                             }
//                         }
//                     },
//                     password_confirmation: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'The password confirmation is required'
//                             },
//                             identical: {
//                                 compare: function() {
//                                     return form.querySelector('[name="password"]').value;
//                                 },
//                                 message: 'The password and its confirm are not the same'
//                             }
//                         }
//                     },
//                 },

//                 plugins: { //Learn more: https://formvalidation.io/guide/plugins
//                     trigger: new FormValidation.plugins.Trigger(),
//                     // Bootstrap Framework Integration
//                     bootstrap: new FormValidation.plugins.Bootstrap(),
//                     // Validate fields when clicking the Submit button
//                     submitButton: new FormValidation.plugins.SubmitButton(),
//                     // Submit the form when all fields are valid
//                     defaultSubmit: new FormValidation.plugins.DefaultSubmit()
//                 }
//             }
//         );
//     }

//     var _validateCompanyRequest = function () {
//         var form = document.getElementById('validate_company_request');
//         FormValidation.formValidation(
//             form,
//             {
//                fields: {
//                 first_name: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'First Name is required'
//                             }
//                         }
//                     },
//                     last_name: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Last Name is required'
//                             }
//                         }
//                     },
//                     phone: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Phone is required'
//                             }
//                         }
//                     },
//                     email: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Email address is required'
//                             },
//                             emailAddress: {
//                                 message: 'The value is not a valid email address'
//                             }
//                         }
//                     },
//                     password: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'The password is required'
//                             },
//                             stringLength: {
//                                 min: 8,
//                                 message: 'The password must be at least 8 characters.'
//                             }
//                         }
//                     },
//                     password_confirmation: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'The password confirmation is required'
//                             },
//                             identical: {
//                                 compare: function() {
//                                     return form.querySelector('[name="password"]').value;
//                                 },
//                                 message: 'The password and its confirm are not the same'
//                             }
//                         }
//                     },
//                                         name: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Company Name is required'
//                             }
//                         }
//                     },
//                     company_email: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Company Email is required'
//                             }
//                         }
//                     },
//                     city: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'City is required'
//                             }
//                         }
//                     },
//                     phone_number: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Phone Number is required'
//                             }
//                         }
//                     },
//                     phone_number: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Phone Number is required'
//                             }
//                         }
//                     },
//                     company_logo: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Logo is required'
//                             }
//                         }
//                     },
//                     fiscal_name: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal Name is required'
//                             }
//                         }
//                     },
//                     fiscal_address: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal Address is required'
//                             }
//                         }
//                     },
//                     fiscal_address: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal Address is required'
//                             }
//                         }
//                     },
//                     fiscal_city: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal City is required'
//                             }
//                         }
//                     },
//                     fiscal_prov: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal Prov is required'
//                             }
//                         }
//                     },
//                     fiscal_cap: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal Cap is required'
//                             }
//                         }
//                     },
//                     fiscal_vat: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal Vat is required'
//                             }
//                         }
//                     },
//                     fiscal_code: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Fiscal Code is required'
//                             }
//                         }
//                     },
//                     sdi_code: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Sdi Code is required'
//                             }
//                         }
//                     },
//                     certified_email: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Pec is required'
//                             }
//                         }
//                     },
//                     bic: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Bic/Swift is required'
//                             }
//                         }
//                     },
//                     iban: {
//                         validators: {
//                             notEmpty: {
//                                 message: 'Iban is required'
//                             }
//                         }
//                     },
//                 },

//                 plugins: { //Learn more: https://formvalidation.io/guide/plugins
//                     trigger: new FormValidation.plugins.Trigger(),
//                     // Bootstrap Framework Integration
//                     bootstrap: new FormValidation.plugins.Bootstrap(),
//                     // Validate fields when clicking the Submit button
//                     submitButton: new FormValidation.plugins.SubmitButton(),
//                     // Submit the form when all fields are valid
//                     defaultSubmit: new FormValidation.plugins.DefaultSubmit()
//                 }
//             }
//         );
//     }

//     return {
//         init: function() {
//             _manageAdministratorForm();
//             _validatePassword();
//             _validateCompanyRequest();
//         }
//     };
// }();

// jQuery(document).ready(function() {
//     KTFormControls.init();
// });

