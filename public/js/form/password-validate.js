// Class definition
var KTFormControlsUser = function () {
    var _validatePassword = function () {
        var form = document.getElementById('password_validation');
        FormValidation.formValidation(
            form,
            {
               fields: {
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The password must be at least 8 characters.'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'The password confirmation is required'
                            },
                            identical: {
                                compare: function() {
                                    return form.querySelector('[name="password"]').value;
                                },
                                message: 'The password and its confirm are not the same'
                            }
                        }
                    },
                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit()
                }
            }
        );
    }
    return {
        init: function() {
            _validatePassword();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormControlsUser.init();
});
