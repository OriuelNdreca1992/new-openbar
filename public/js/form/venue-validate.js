"use strict";

// Class definition
var KTWizard3 = function () {
    // Base elements
    var _wizardEl;
    var _formEl;
    var _wizardObj;
    var _validations = [];

    // Private functions
    var _initWizard = function () {
        // Initialize form wizard
        _wizardObj = new KTWizard(_wizardEl, {
            startStep: 1, // initial active step number
            clickableSteps: true  // allow step clicking
        });

        // Validation before going to next page
        _wizardObj.on('change', function (wizard) {
            if (wizard.getStep() > wizard.getNewStep()) {
                return; // Skip if stepped back
            }

            // Validate form before change wizard step
            var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step

            if (validator) {
                validator.validate().then(function (status) {
                    if (status === 'Valid') {
                        wizard.goTo(wizard.getNewStep());
                        KTUtil.scrollTop();
                    }
                });
            }

            return false;  // Do not change wizard step, further action will be handled by he validator
        });

        // Changed event
        _wizardObj.on('changed', function (wizard) {
            KTUtil.scrollTop();
        });

        // Submit event
        _wizardObj.on('submit', function (wizard) {
            // Validate form before submit
            var validator = _validations[wizard.getStep() - 1]; // get validator for currnt step
            // var terms_and_conditions = $('#terms_and_conditions').is(':checked');
            // var privacy_policy = $('#privacy_policy').is(':checked');

            if (validator) {
                validator.validate().then(function (status) {
                    // if ((terms_and_conditions == false) || (privacy_policy == false)) {
                    //  status = 'Invalid';
                    // }

                    if (status === 'Valid') {
                        _formEl.submit(); // submit form
                    }
                });
            }
        });
    }

    var _initValidation = function () {
        // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
        // Step 1
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    first_name: {
                        validators: {
                            notEmpty: {
                                message: 'Nome è obbligatorio'
                            }
                        }
                    },
                    last_name: {
                        validators: {
                            notEmpty: {
                                message: 'Cognome è obbligatorio'
                            }
                        }
                    },
                    phone: {
                        validators: {
                            notEmpty: {
                                message: 'Cellulare è obbligatorio'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Email è obbligatorio'
                            },
                            emailAddress: {
                                message: 'Il valore non è un indirizzo email valido'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Password è obbligatorio'
                            },
                            regexp: {
                                regexp: "(?=[A-Za-z0-9@#$%^&+!=]+$)^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@#$%^&+!=])(?=.{8,}).*$",
                                message: 'Minimo otto caratteri, almeno una lettera maiuscola, una lettera minuscola, un numero e un carattere speciale',
                            }
                        }
                     },
                      password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'Conferma password è obbligatorio'
                            },
                            identical: {
                                compare: function() {
                                    return _formEl.querySelector('[name="password"]').value;
                                },
                                message: 'La password e la sua conferma non sono la stessa cosa'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //eleInvalidClass: '',
                        eleValidClass: '',
                    })
                }
            }
        ));

        // Step 2
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    address: {
                        validators: {
                            notEmpty: {
                                message: 'Venue indirizzo è obbligatorio'
                            }
                        }
                    },
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Nome Locale è obbligatorio'
                            }
                        }
                    },
                    city: {
                        validators: {
                            notEmpty: {
                                message: 'Città è obbligatorio'
                            }
                        }
                    },
                    postal_code: {
                        validators: {
                            notEmpty: {
                                message: 'Codice Postale è obbligatorio'
                            }
                        }
                    },
                    country: {
                        validators: {
                            notEmpty: {
                                message: 'Nazione è obbligatorio'
                            }
                        }
                    },
                    phone_number: {
                        validators: {
                            notEmpty: {
                                message: 'Telefono Venue è obbligatorio'
                            }
                        }
                    },
                    seats_number: {
                        validators: {
                            notEmpty: {
                                message: 'Posti a Sedere è obbligatorio'
                            }
                        }
                    },
                    'types[]': {
                        validators: {
                            notEmpty: {
                                message: 'Tipologia è obbligatorio'
                            }
                        }
                    }
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //eleInvalidClass: '',
                        eleValidClass: '',
                    })
                }
            }
        ));

        // Step 3
        _validations.push(FormValidation.formValidation(
            _formEl,
            {
                fields: {
                    fiscal_name: {
                        validators: {
                            notEmpty: {
                                message: 'Ragione Sociale è obbligatorio'
                            }
                        }
                    },
                    fiscal_vat: {
                        validators: {
                            notEmpty: {
                                message: 'Partita Iva è obbligatorio'
                            }
                        }
                    },
                    certified_email: {
                        validators: {
                            emailAddress: {
                                message: 'Il valore non è un indirizzo email valido'
                            }
                        }
                    },
                    fiscal_address: {
                        validators: {
                            notEmpty: {
                                message: 'Indirizzo di fatturazione è obbligatorio'
                            }
                        }
                    },
                    fiscal_city: {
                        validators: {
                            notEmpty: {
                                message: 'Città è obbligatorio'
                            }
                        }
                    },
                    fiscal_country: {
                        validators: {
                            notEmpty: {
                                message: 'Nazione è obbligatorio'
                            }
                        }
                    },
                    fiscal_cap: {
                        validators: {
                            notEmpty: {
                                message: 'Cap è obbligatorio'
                            }
                        }
                    },
                    terms_and_conditions: {
                        validators: {
                            notEmpty: {
                                message: 'Devi accettare i termini e le condizioni'
                            }
                        }
                    },

                    privacy_policy: {
                        validators: {
                            notEmpty: {
                                message: 'Devi accettare l\'informativa sulla privacy'
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //eleInvalidClass: '',
                        eleValidClass: '',
                    })
                }
            }
        ));
    }

    return {
        // public functions
        init: function () {
            _wizardEl = KTUtil.getById('kt_wizard_v3');
            _formEl = KTUtil.getById('kt_form');

            _initWizard();
            _initValidation();
        }
    };
}();

jQuery(document).ready(function () {
    KTWizard3.init();
});
