"use strict";

const KTDatatablesInvoice = function() {
    const invoice = function() {
        // begin first table
        let table = $('#invoice').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: BASE_URL + '/my-venue/invoices',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex', 'admin_invoice', 'invoice_no', 'from', 'to', 'status',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'admin_invoice'},
                {data: 'invoice_no'},
                {data: 'from'},
                {data: 'to'},
                {data: 'status'},
            ],

            columnDefs: [
                {
                    targets: 1,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + data + '" class="btn btn-sm btn-clean btn-icon" target="_blank" title="View Report">\
                                <i class="fa fa-eye icon-md"></i>\
                            </a>'
                        ;
                    },
                },
                {
                    targets: 2,
                    className: 'dt-body-center',
                    render: function (data, type, full, meta) {
                        return (data !== null) ? String(data).padStart(7,"0") : data;
                    },
                },
                {
                    targets: 5,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        let status = {
                            0: {'title': 'Not Paid', 'class': ' label-light-warning'},
                            1: {'title': 'Paid', 'class': ' label-light-success'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            invoice();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesInvoice.init();
});
