function initMap() {
    if (typeof venue !== 'undefined') {
        var lat = parseFloat(venue.lat);
        var lng = parseFloat(venue.lng);
        var address = venue.address;
    } else if ((oldLat !== null) && (oldLng !== null)) {
        var lat = parseFloat(oldLat);
        var lng = parseFloat(oldLng);
        var address = oldAddress;
    } else {
        var lat = -33.8688;
        var lng = 151.2195;
        var address = 'No selected';
    }

    var map = map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: lat, lng: lng },
        zoom: 17
    });

    var input = document.getElementById('pac-input');

    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
    autocomplete.setFields([
        'address_components',
        'geometry',
        'name',
        'opening_hours',
        'business_status',
        'international_phone_number',
        'formatted_phone_number'
    ]);

    autocomplete.setTypes([]);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);

    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    if (typeof venue !== 'undefined') {
        myLatlng = new google.maps.LatLng(parseFloat(venue.lat), parseFloat(venue.lng));
        marker.setPosition(myLatlng);
        marker.setVisible(true);

        infowindowContent.children['place-address'].textContent = venue.address;
        infowindow.open(map, marker);
    }

    if (oldLat !== null && oldLng !== null) {
        myLatlng = new google.maps.LatLng(oldLat, oldLng);
        marker.setPosition(myLatlng);
        marker.setVisible(true);

        infowindowContent.children['place-address'].textContent = oldAddress;
        infowindow.open(map, marker);
    }

    autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (! place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");

            return;
        }

        map.setCenter(place.geometry.location);
        map.setZoom(17);

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        document.querySelector("#lat").value = marker.getPosition().lat();
        document.querySelector("#lng").value = marker.getPosition().lng();

        if (typeof place.opening_hours !== 'undefined' &&
            typeof place.opening_hours.weekday_text !== 'undefined') {
            const inputs = document.querySelectorAll('#kt_timepicker_1_modal');
            inputs.forEach(input => {
                input.value = '';
            });

            var week_opening_hours = place.opening_hours.periods;
            week_opening_hours.forEach(function(day_period) {
                var start_hour = moment(day_period.open.time, "HHmm").format("HH:mm");
                var end_hour = moment(day_period.close.time, "HHmm").format("HH:mm");

                if (day_period.open.day === 0) {
                    document.querySelector("input[name='opening_hours[7][start_hour]']").value = start_hour;
                } else {
                    document.querySelector("input[name='opening_hours["+day_period.open.day+"][start_hour]']").value = start_hour;
                }

                if (day_period.close.day === 0) {
                    document.querySelector("input[name='opening_hours[7][end_hour]']").value = end_hour;
                } else {
                    document.querySelector("input[name='opening_hours["+day_period.close.day+"][end_hour]']").value = end_hour;
                }
            });
        }

        // console.log(place.address_components);
        if (typeof place.address_components !== 'undefined') {
            // document.querySelector("#name").value = place.name;

            parseGoogleResponse(place.address_components);

            if (place.international_phone_number) {
                document.querySelector("#phone_number").value = place.international_phone_number;
            }

            document.querySelector("#lat").value = place.geometry.location.lat();
            document.querySelector("#lng").value = place.geometry.location.lng();

            if (infowindowContent) {
                infowindowContent.children['place-address'].textContent = [
                    place.name,
                    place.address_components[1].long_name,
                    place.address_components[2].long_name,
                    place.address_components[6].long_name
                ].join(', ');
                infowindow.open(map, marker);
            }
        }
    });
}

function parseGoogleResponse(components) {
    _.each(components, function(component) {
        _.each(component.types, function(type) {
            if (type === 'country') {
                document.querySelector("#country").value = component.long_name
            }
            if (type === 'administrative_area_level_1') {
                document.querySelector("#state").value = component.long_name
            }
            if (type === 'locality') {
                document.querySelector("#city").value = component.long_name
            }
            if (type === 'postal_code') {
                document.querySelector("#postal_code").value = component.long_name
            }
            // if (type === 'route') {
            //     $("input[name=street]").val(component.long_name)
            // }
            // if (type === 'street_number') {
            //     $("input[name=nr]").val(component.long_name)
            // }
        })
    })
}

