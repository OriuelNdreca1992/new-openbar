"use strict";

// Shared Colors Definition
const primary = '#6993FF';
const success = '#1BC5BD';
const info = '#8950FC';
const warning = '#FFA800';
const danger = '#F64E60';

// Class definition
var KTCharts = function () {
    var _clientsChart = function () {
        var element = document.getElementById("clients_stats");

        var height = parseInt(KTUtil.css(element, 'height'));
        var color = KTUtil.hasAttr(element, 'data-color') ? KTUtil.attr(element, 'data-color') : 'primary';

        if (!element) {
            return;
        }

        var options = {
            series: [{
                name: 'Number',
                data: clientsStats.total_clients,
            }],
            chart: {
                type: 'area',
                height: height,
                toolbar: {
                    show: false
                },
                zoom: {
                    enabled: false
                },
                sparkline: {
                    enabled: true
                }
            },
            plotOptions: {},
            legend: {
                show: false
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: 'solid',
                opacity: 1
            },
            stroke: {
                curve: 'smooth',
                show: true,
                width: 3,
                colors: [KTApp.getSettings()['colors']['theme']['base'][color]]
            },
            xaxis: {
                categories: clientsStats.months,
                axisBorder: {
                    show: false,
                },
                axisTicks: {
                    show: false
                },
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                },
                crosshairs: {
                    show: false,
                    position: 'front',
                    stroke: {
                        color: KTApp.getSettings()['colors']['gray']['gray-300'],
                        width: 1,
                        dashArray: 3
                    }
                },
                tooltip: {
                    enabled: true,
                    formatter: undefined,
                    offsetY: 0,
                    style: {
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            yaxis: {
                min: 0,
                max: 55,
                labels: {
                    show: false,
                    style: {
                        colors: KTApp.getSettings()['colors']['gray']['gray-500'],
                        fontSize: '12px',
                        fontFamily: KTApp.getSettings()['font-family']
                    }
                }
            },
            states: {
                normal: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                hover: {
                    filter: {
                        type: 'none',
                        value: 0
                    }
                },
                active: {
                    allowMultipleDataPointsSelection: false,
                    filter: {
                        type: 'none',
                        value: 0
                    }
                }
            },
            tooltip: {
                style: {
                    fontSize: '12px',
                    fontFamily: KTApp.getSettings()['font-family']
                },
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            },
            colors: [KTApp.getSettings()['colors']['theme']['light'][color]],
            markers: {
                colors: [KTApp.getSettings()['colors']['theme']['light'][color]],
                strokeColor: [KTApp.getSettings()['colors']['theme']['base'][color]],
                strokeWidth: 3
            }
        };

        var chart = new ApexCharts(element, options);
        chart.render();
    }

    var _premiumDrinksCurrentMonthStats = function () {
        const apexChart = "#premium_drinks";
        var options = {
            series: [premiumDrinksCurrentMonthStats.consumed_drinks, premiumDrinksCurrentMonthStats.expired_drinks],
            chart: {
                width: 380,
                type: 'pie',
            },
            labels: ['Consumed', 'Expired'],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }],
            colors: [primary, success, info]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }

    var _premiumDrinksStats = function () {
        const apexChart = "#premium_drinks_stats";
        var options = {
            series: [{
                name: 'Premium Consumed',
                data: premiumDrinksStats.premium_consumed
            }, {
                name: 'Premium Expired',
                data: premiumDrinksStats.premium_expired
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: premiumDrinksStats.months,
            },
            yaxis: {
                title: {
                    text: '(quantity)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            },
            colors: [primary, success, warning]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }

    var _offerDrinksStats = function () {
        const apexChart = "#offer_drinks_stats";
        var options = {
            series: [{
                name: '2x1 Consumed',
                data: offerDrinksStats.offer_consumed
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: offerDrinksStats.months,
            },
            yaxis: {
                title: {
                    text: '(quantity)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            },
            colors: [warning, info]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }

    var _opPremiumDrinksIncomeStats = function () {
        const apexChart = "#op_income_stats";
        var options = {
            series: [{
                name: 'Income',
                data: opPremiumDrinksIncomeStats.total_income
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: opPremiumDrinksIncomeStats.months,
            },
            yaxis: {
                title: {
                    text: '(€)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            },
            colors: [info]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }

    var _venuePremiumDrinksIncomeStats = function () {
        const apexChart = "#venue_income_stats";
        var options = {
            series: [{
                name: 'Income',
                data: venuePremiumDrinksIncomeStats.total_income
            }],
            chart: {
                type: 'bar',
                height: 350
            },
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '55%',
                    endingShape: 'rounded'
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            xaxis: {
                categories: venuePremiumDrinksIncomeStats.months,
            },
            yaxis: {
                title: {
                    text: '(€)'
                }
            },
            fill: {
                opacity: 1
            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return val;
                    }
                }
            },
            colors: [info]
        };

        var chart = new ApexCharts(document.querySelector(apexChart), options);
        chart.render();
    }

    // Public methods
    return {
        init: function () {
            _clientsChart();
            _premiumDrinksCurrentMonthStats();
            _premiumDrinksStats();
            _offerDrinksStats();
            _opPremiumDrinksIncomeStats();
            _venuePremiumDrinksIncomeStats();
        }
    }
}();

// Webpack support
if (typeof module !== 'undefined') {
    module.exports = KTCharts;
}

jQuery(document).ready(function () {
    KTCharts.init();
});
