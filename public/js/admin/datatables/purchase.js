"use strict";

let KTDatatablesPurchase = function() {
    let purchase = function() {
        let table = $('#purchase').DataTable({
            // responsive: true,
            // processing: true,
            // serverSide: true,
            responsive: true,
            paging: true,
            order: [[ 1, "desc" ]],
            ajax: {
                url: getPurchases,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex',
                        'id',
                        'user',
                        'transaction_type',
                        'transaction_id',
                        'payment_intent_id',
                        'status',
                        'total_amount',
                        'user_fee',
                        'created_at',
                        'actions',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: true},
                {data: 'id'},
                {data: 'user'},
                {data: 'transaction_type'},
                {data: 'transaction_id'},
                {data: 'payment_intent_id'},
                {data: 'status'},
                {data: 'total_amount'},
                {data: 'user_fee'},
                {data: 'created_at'},
                {data: 'actions', responsivePriority: -1},
            ],
            columnDefs: [
                {
                    targets: 3,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var status = {
                            0: {'title': 'Disable', 'state': 'label-light-danger'},
                            1: {'title': 'Enable', 'state': 'label-light-success'},
                        };
                        if (typeof type[data] === 'undefined') {
                            return data;
                        }
                        return ' <span class="label label-lg font-weight-bold label-inline '+ status[data].state + '">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: 6,
                    render: function(data) {
                        let status = {
                            'succeeded': {'title': 'Succeeded', 'state': 'success'},
                            'pending': {'title': 'Pending', 'state': 'default'},
                            'failed': {'title': 'Failed', 'state': 'danger'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + purchaseDrinks.replace(':id', data) + '" class="btn btn-sm btn-clean btn-icon" title="View Drinks">\
                                <i class="fa fa-glass-cheers icon-md"></i>\
                            </a>\
                            ';
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            purchase();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesPurchase.init();
});
