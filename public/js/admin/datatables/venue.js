"use strict";

var KTDatatablesVenues = function() {
    var venues = function() {
        var table = $('#venue').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            paging: true,
            order: [[ 1, "DESC" ]],
            ajax: {
                url: getVenues,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex',
                        'id',
                        'name',
                        'review',
                        'email',
                        'city',
                        'offers',
                        'shop',
                        'event',
                        'status',
                        'actions',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'id', searchable: false, orderable: true},
                {data: 'name'},
                {data: 'review', searchable: false, orderable: false},
                {data: 'email'},
                {data: 'city'},
                {data: 'offers'},
                {data: 'shop'},
                {data: 'event'},
                {data: 'status'},
                {data: 'actions', responsivePriority: -1},
            ],

            columnDefs: [
                {
                    targets: 3,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        if (typeof type[data] === 'undefined') {
                            return data;
                        }
                        let htmlStars = '<div class="rating">';
                        for (let star = 1; star <= 5; star++) {
                            let checked = (data >= star) ? ' checked' : '';
                            htmlStars = htmlStars.concat('<div class="rating-label' + checked + '"><span class="bi bi-star-fill fs-6s"></span></div>');
                        }
                        htmlStars = htmlStars.concat('</div>');
                        return htmlStars;
                    },
                },
                {
                    targets: 6,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var offerStatus = {
                            0: {'state': 'label-circle-danger'},
                            1: {'state': 'label-circle-success'},
                        };
                        if (typeof type[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-circle '+ offerStatus[data].state + '"></span>';
                    },
                },
                {
                    targets: 7,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var shopStatus = {
                            0: {'state': 'label-circle-danger'},
                            1: {'state': 'label-circle-success'},
                        };
                        if (typeof type[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-circle '+ shopStatus[data].state + ' "></span>';
                    },
                },
                {
                    targets: 8,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        var eventStatus = {
                            0: {'state': 'label-circle-danger'},
                            1: {'state': 'label-circle-success'},
                        };
                        if (typeof type[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-circle '+ eventStatus[data].state + ' "></span>';
                    },
                },
                {
                    targets: 9,
                    className: 'dt-body-center',
                    render: function(data, type, full, meta) {
                        let status = {
                            0: {'title': 'OFF', 'state': 'label-light-danger'},
                            1: {'title': 'ON', 'state': 'label-light-success'},
                        };
                        if (typeof type[data] === 'undefined') {
                            return data;
                        }
                        return ' <span class="label label-lg font-weight-bold label-inline '+ status[data].state + '">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + venuesShow.replace(':venue', data) + '" class="btn btn-sm btn-clean btn-icon" title="View details">\
                                <i class="fa fa-eye icon-md"></i>\
                            </a>\
                        ';
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            venues();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesVenues.init();
});
