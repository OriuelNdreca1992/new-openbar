"use strict";

let KTDatatablesPurchaseItem = function() {
    let purchaseItems = function() {
        let table = $('#purchase-item').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            paging: true,
            order: [[ 1, "desc" ]],
            ajax: {
                url: getPurchaseItems,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    columnsDef: [
                        'DT_RowIndex',
                        'id',
                        'drink_name',
                        'offered_to_id',
                        'offered_to_name',
                        'venue_name',
                        'price',
                        'type',
                        'status',
                        'created_at',
                        'used_expires_at',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'id'},
                {data: 'drink_name'},
                {data: 'offered_to_id'},
                {data: 'offered_to_name'},
                {data: 'venue_name'},
                {data: 'price'},
                {data: 'type'},
                {data: 'status'},
                {data: 'created_at'},
                {data: 'used_expires_at'},
            ],
        });
    };

    return {
        init: function() {
            purchaseItems();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesPurchaseItem.init();
});
