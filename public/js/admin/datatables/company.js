"use strict";

var KTDatatablesCompany = function() {
    var company = function() {
        // begin first table
        var table = $('#company').DataTable({
            // responsive: true,
            // processing: true,
            // serverSide: true,
            responsive: true,
            paging: true,
            order: [[ 0, "DESC" ]],
            ajax: {
                url: getCompanies,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'id', 'name', 'email', 'city', 'status', 'phone_number', 'created_at', 'actions',
                    ],
                },
            },
            columns: [
                {data: 'id', searchable: false, orderable: true},
                {data: 'name'},
                {data: 'email'},
                {data: 'city'},
                {data: 'status'},
                {data: 'phone_number'},
                {data: 'created_at'},
                {data: 'actions', responsivePriority: -1},
            ],

            columnDefs: [
            {
                targets: 4,
                className: 'dt-body-center',
                render: function(data, type, full, meta) {
                    var status = {
                        0: {'title': 'Disable', 'state': 'label-light-danger'},
                        1: {'title': 'Enable', 'state': 'label-light-success'},
                    };
                    if (typeof type[data] === 'undefined') {
                        return data;
                    }
                    return ' <span class="label label-lg font-weight-bold label-inline '+ status[data].state + '">' + status[data].title + '</span>';
                },
            },
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + companiesShow.replace(':company', full.id) + '" class="btn btn-sm btn-clean btn-icon" title="View details">\
                                <i class="fa fa-eye icon-md"></i>\
                            </a>\
                            <a href="' + companiesEdit.replace(':company', full.id) + '" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
                                <i class="la la-edit"></i>\
                            </a>\
                            <a href="javascript:;"\
                                data-toggle="modal"\
                                data-target="#deleteCompany"\
                                data-id="' + full.id + '"\
                                id="delete-company"\
                                class="btn btn-sm btn-clean btn-icon"\
                                title="Delete">\
                                <i class="la la-trash"></i>\
                            </a>\
                        ';
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            company();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesCompany.init();
});
