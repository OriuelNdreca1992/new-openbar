"use strict";

let KTDatatablesUserDrinks = function() {
    let purchasedDrinks = function() {
        let table = $('#purchased_drink').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            paging: true,
            order: [[ 1, "desc" ]],
            ajax: {
                url: purchasedUrl,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex',
                        'id',
                        'user',
                        'type',
                        'transaction_type',
                        'transaction_id',
                        'payment_intent_id',
                        'amount',
                        'created_at',
                        'actions',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'id'},
                {data: 'user'},
                {data: 'type'},
                {data: 'transaction_type'},
                {data: 'transaction_id'},
                {data: 'payment_intent_id'},
                {data: 'amount'},
                {data: 'created_at'},
                {data: 'actions', responsivePriority: -1},
            ],

            columnDefs: [
                {
                    targets: 0,
                    className: 'text-center',
                },
                {
                    targets: 1,
                    className: 'text-center',
                },
                {
                    targets: 2,
                    className: 'text-center',
                },
                {
                    targets: 3,
                    className: 'text-center',
                    render: function ( data ) {
                        if (data === '2x1') {
                            return '<span class="font-weight-bold text-primary">2x1</span>';
                        } else if (data === 'premium') {
                            return '<span class="font-weight-bold text-success">Premium</span>';
                        }
                        return data;
                    },
                },
                {
                    targets: 4,
                    className: 'text-center',
                },
                {
                    targets: 5,
                    className: 'text-center',
                },
                {
                    targets: 6,
                    className: 'text-center',
                },
                {
                    targets: 7,
                    className: 'text-center',
                },
                {
                    targets: 8,
                    className: 'text-center',
                },
                {
                    targets: -1,
                    className: 'text-center',
                    title: 'Actions',
                    orderable: false,
                    render: function(data) {
                        return '<a href="' + purchaseDrinks.replace(':cart', data) + '" class="btn btn-sm btn-clean btn-icon" title="View details"><i class="fa fa-eye icon-md"></i></a>';
                    },
                },
            ],
        });
    };

    let giftedDrinks = function() {
        let table = $('#gifted_drink').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            paging: true,
            order: [[ 1, "desc" ]],
            ajax: {
                url: giftedUrl,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    columnsDef: [
                        'DT_RowIndex',
                        'id',
                        'user',
                        'type',
                        'transaction_type',
                        'transaction_id',
                        'payment_intent_id',
                        'amount',
                        'created_at',
                        'actions',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'id'},
                {data: 'user'},
                {data: 'type'},
                {data: 'transaction_type'},
                {data: 'transaction_id'},
                {data: 'payment_intent_id'},
                {data: 'amount'},
                {data: 'created_at'},
                {data: 'actions', responsivePriority: -1},
            ],

            columnDefs: [
                {
                    targets: 0,
                    className: 'text-center',
                },
                {
                    targets: 1,
                    className: 'text-center',
                },
                {
                    targets: 2,
                    className: 'text-center',
                },
                {
                    targets: 3,
                    className: 'text-center',
                    render: function ( data ) {
                        if (data === '2x1') {
                            return '<span class="font-weight-bold text-primary">2x1</span>';
                        } else if (data === 'premium') {
                            return '<span class="font-weight-bold text-success">Premium</span>';
                        }
                        return data;
                    },
                },
                {
                    targets: 4,
                    className: 'text-center',
                },
                {
                    targets: 5,
                    className: 'text-center',
                },
                {
                    targets: 6,
                    className: 'text-center',
                },
                {
                    targets: 7,
                    className: 'text-center',
                },
                {
                    targets: 8,
                    className: 'text-center',
                },
                {
                    targets: -1,
                    className: 'text-center',
                    title: 'Actions',
                    orderable: false,
                    render: function(data) {
                        return '<a href="' + giftedShow.replace(':cart', data) + '" class="btn btn-sm btn-clean btn-icon" title="View details"><i class="fa fa-eye icon-md"></i></a>';
                    },
                },
            ],
        });
    };

    return {
        init: function() {
            purchasedDrinks();
            giftedDrinks();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesUserDrinks.init();
});
