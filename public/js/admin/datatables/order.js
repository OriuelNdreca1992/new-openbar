"use strict";

let KTDatatablesOrder = function() {
    $.fn.dataTable.Api.register('column().title()', function() {
        return $(this.header()).text().trim();
    });
    let order = function() {
        // begin first table
        let table = $('#order').DataTable({
            responsive: true,
            paging: true,
            order: [[ 1, "DESC" ]],
            // Pagination settings
            dom: `<'row'<'col-sm-12'tr>>
            <'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
            // read more: https://datatables.net/examples/basic_init/dom.html

            buttons: [
                'print',
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
            ],

            lengthMenu: [5, 10, 25, 50],

            pageLength: 10,

            language: {
                'lengthMenu': 'Display _MENU_',
            },

            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: BASE_URL + '/orders',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex',
                        'drink_id',
                        'drink_name',
                        'drink_price',
                        'venue_id',
                        'buyer',
                        'taker',
                        'type',
                        'status',
                        'purchased_at',
                        'used_expires_at',
                        'actions'
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'drink_id'},
                {data: 'drink_name'},
                {data: 'drink_price'},
                {data: 'venue_id'},
                {data: 'buyer'},
                {data: 'taker'},
                {data: 'type'},
                {data: 'status'},
                {data: 'purchased_at'},
                {data: 'used_expires_at'},
                {data: 'actions'},
            ],

            columnDefs: [
                {
                    targets: 4,
                    render: function(data, type, full, meta) {
                         return '<a href="' + venuesShow.replace(':venue', full.venue_id) + '">' + full.venue_name + '</a>';
                    },
                },
                {
                    targets: 7,
                    render: function(data) {
                        let type = {
                            '2x1': {'title': '2x1', 'state': 'primary'},
                            'premium': {'title': 'Premium', 'state': 'success'},
                        };
                        if (typeof type[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="font-weight-bold text-' + type[data].state + '">' + type[data].title + '</span>';
                    },
                },
                {
                    targets: 8,
                    render: function(data) {
                        let status = {
                            'available': {'title': 'Available', 'state': 'primary'},
                            'pending': {'title': 'Pending', 'state': 'default'},
                            'used': {'title': 'Used', 'state': 'success'},
                            'failed': {'title': 'Failed', 'state': 'warning'},
                            'expired': {'title': 'Expired', 'state': 'danger'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + data + '" class="btn btn-sm btn-clean btn-icon" title="View Purchase">\
                                <i class="fa fa-shopping-basket icon-md"></i>\
                            </a>';
                    },
                },
            ],
        });

        var filter = function() {
            var val = $.fn.dataTable.util.escapeRegex($(this).val());
            table.column($(this).data('col-index')).search(val ? val : '', false, false).draw();
        };

        var asdasd = function(value, index) {
            var val = $.fn.dataTable.util.escapeRegex(value);
            table.column(index).search(val ? val : '', true, true);
        };

        $('#kt_search').on('click', function(e) {
            e.preventDefault();

            let start = $("input[name='start']").val();
            let end = $("input[name='end']").val();
            table.columns(1).search($("input[name='drink_id']").val()).draw();
            table.columns(4).search($("select[name='venue_id'] option:selected").val()).draw();
            table.columns(7).search($("select[name='type'] option:selected").val()).draw();
            table.columns(8).search($("select[name='status'] option:selected").val()).draw();
            table.columns(9).search(start  + '|' + end).draw();

            table.table().draw();
        });

        $('#kt_reset').on('click', function(e) {
            e.preventDefault();
            $('.datatable-input').each(function() {
                $(this).val('');
                table.column($(this).data('col-index')).search('', false, false);
            });
            table.table().draw();
        });

        $('#kt_datepicker').datepicker({
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>',
            },
        });

        $('#export_print').on('click', function(e) {
            e.preventDefault();
            table.button(0).trigger();
        });

        $('#export_copy').on('click', function(e) {
            e.preventDefault();
            table.button(1).trigger();
        });

        $('#export_excel').on('click', function(e) {
            e.preventDefault();
            table.button(2).trigger();
        });

        $('#export_csv').on('click', function(e) {
            e.preventDefault();
            table.button(3).trigger();
        });

        $('#export_pdf').on('click', function(e) {
            e.preventDefault();
            table.button(4).trigger();
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            order();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesOrder.init();
});
