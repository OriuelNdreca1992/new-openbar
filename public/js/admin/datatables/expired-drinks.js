"use strict";

var KTDatatablesExpiredDrinks = function() {
    var report = function() {
        // begin first table
        var table = $('#expired-drinks').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: getReports,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex', 'report_pdf', 'from', 'to',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'report_pdf'},
                {data: 'from'},
                {data: 'to'},
            ],

            columnDefs: [
                {
                    targets: 1,
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + data + '" class="btn btn-sm btn-clean btn-icon" target="_blank" title="View Invoice">\
                                <i class="fa fa-eye icon-md"></i>\
                            </a>'
                        ;
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            report();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesExpiredDrinks.init();
});
