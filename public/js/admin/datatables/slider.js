"use strict";

let KTDatatablesSlider = function () {
    let slider = function () {
        // begin first table
        let table = $('#venue').DataTable({
            serverSide: true,
            responsive: true,
            paging: true,
            order: [[ 1, "DESC" ]],
            ajax: {
                url: getVenues,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    columnsDef: [
                        'DT_RowIndex', 'id', 'name', 'address', 'city', 'slider_count', 'created_at', 'actions',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'id'},
                {data: 'name'},
                {data: 'address'},
                {data: 'city'},
                {data: 'slider_count'},
                {data: 'created_at'},
                {data: 'actions', responsivePriority: -1},
            ],
            columnDefs: [
                {
                    targets: 2,
                    render: function (data, type, full, meta) {
                        return '<a href="' + venuesShow.replace(':venue', full.id) + '">' + data + '</a>';
                    },
                },
                {
                    targets: 5,
                    render: function (data, type, full, meta) {
                        const status = {
                            0: {'title': 'Not Selected', 'state': 'danger'},
                            1: {'title': 'Selected', 'state': 'success'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        let output;
                        console.log(full.slider_count);
                        if (full.slider_count === 0) {
                            output = '\
                                <a href="javascript:;"\
                                    id="add-venue"\
                                    data-id="' + full.id + '"\
                                    class="btn btn-icon">\
                                    <i class="ki ki-plus icon-nm text-success"></i>\
                                </a>\
                            ';
                        } else {
                            output = '\
                                <a href="javascript:;"\
                                    id="remove-venue"\
                                    data-id="' + full.id + '"\
                                    class="btn btn-icon">\
                                    <i class="ki ki-minus icon-nm text-danger"></i>\
                                </a>\
                            ';
                        }

                        return output;
                    },
                },
            ],
        });

        $(document).on("click", "#add-venue", function (e) {
            e.preventDefault();
            let id = $(this).attr('data-id');
            $.ajax({
                url: addVenue,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    table.ajax.reload(null, false);
                }
            });
        });

        $(document).on("click", "#remove-venue", function (e) {
            e.preventDefault();
            let id = $(this).attr('data-id');
            $.ajax({
                url: removeVenue,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    table.ajax.reload(null, false);
                }
            });
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            slider();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesSlider.init();
});
