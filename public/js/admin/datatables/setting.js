"use strict";

var KTDatatablesSetting = function() {
    var setting = function() {
        // begin first table
        var table = $('#setting').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            paging: true,
            ajax: {
                url: getSettings,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'id', 'name', 'value', 'description', 'created_at', 'updated_at', 'actions'
                    ],
                },
            },
            columns: [
                {data: 'id'},
                {data: 'name'},
                {data: 'value'},
                {data: 'description'},
                {data: 'created_at'},
                {data: 'updated_at'},
                {data: 'actions', responsivePriority: -1},
            ],

            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + settingsEdit.replace(':setting', full.id) + '" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
                                <i class="la la-edit"></i>\
                            </a>\
                        ';
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            setting();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesSetting.init();
});
