"use strict";

var KTDatatablesPushNotification = function() {
    var scheduledPushNotification = function() {
        // begin first table
        var table = $('#scheduled-push-notifications').DataTable({
            // responsive: true,
            // processing: true,
            // serverSide: true,
            responsive: true,
            paging: true,
            ajax: {
                url: BASE_URL + '/admin/scheduled-push-notifications',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex', 'title', 'body', 'scheduled_time', 'status', 'created_at', 'actions',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'title'},
                {data: 'body'},
                {data: 'scheduled_time'},
                {data: 'status'},
                {data: 'created_at'},
                {data: 'actions', responsivePriority: -1},
            ],

            columnDefs: [
                {
                    targets: -1,
                    title: 'Actions',
                    orderable: false,
                    render: function(data, type, full, meta) {
                        return '\
                            <a href="' + notificationsEdit.replace(':notification', data) + '" class="btn btn-sm btn-clean btn-icon" title="Edit details">\
                                <i class="la la-edit"></i>\
                            </a>\
                            <a href="javascript:;"\
                                data-toggle="modal"\
                                data-target="#deleteNotification"\
                                data-id="' + data + '"\
                                id="delete-notification"\
                                class="btn btn-sm btn-clean btn-icon"\
                                title="Delete">\
                                <i class="la la-trash"></i>\
                            </a>\
                        ';
                    },
                },
                {
                    targets: 4,
                    render: function(data, type, full, meta) {
                        var status = {
                            0: {'title': 'Pending', 'state': 'danger'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    };

     var expiredPushNotification = function() {
        // begin first table
        var table = $('#expired-push-notifications').DataTable({
            // responsive: true,
            // processing: true,
            // serverSide: true,
            responsive: true,
            paging: true,
            ajax: {
                url: BASE_URL + '/admin/expired-push-notifications',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex', 'title', 'body', 'scheduled_time', 'status', 'created_at',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'title'},
                {data: 'body'},
                {data: 'scheduled_time'},
                {data: 'status'},
                {data: 'created_at'},
            ],

            columnDefs: [
                {
                    targets: 4,
                    render: function(data, type, full, meta) {
                        var status = {
                            1: {'title': 'Committed', 'state': 'success'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function() {
            scheduledPushNotification();
            expiredPushNotification();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesPushNotification.init();
});
