"use strict";

const KTDatatablesInvoice = function () {
    const invoice = function () {
        // begin first table
        let table = $('#invoice').DataTable({
            responsive: true,
            searchDelay: 500,
            processing: true,
            serverSide: true,
            ajax: {
                url: getInvoices,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    // parameters for custom backend script demo
                    columnsDef: [
                        'DT_RowIndex', 'admin_invoice', 'invoice_no', 'from', 'to', 'status', 'actions',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'admin_invoice'},
                {data: 'invoice_no'},
                {data: 'from'},
                {data: 'to'},
                {data: 'status'},
                {data: 'actions', responsivePriority: -1},
            ],

            columnDefs: [
                {
                    targets: 1,
                    className: 'dt-body-center',
                    render: function (data, type, full, meta) {
                        return '\
                            <a href="' + data + '" class="btn btn-sm btn-clean btn-icon" target="_blank" title="View Report">\
                                <i class="fa fa-eye icon-md"></i>\
                            </a>'
                            ;
                    },
                },
                {
                    targets: 2,
                    className: 'dt-body-center',
                    render: function (data, type, full, meta) {
                        return (data !== null) ? String(data).padStart(7,"0") : data;
                    },
                },
                {
                    targets: -2,
                    className: 'dt-body-center',
                    render: function (data, type, full, meta) {
                        const status = {
                            0: {'title': 'Not Paid', 'class': ' label-light-warning'},
                            1: {'title': 'Paid', 'class': ' label-light-success'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="label label-lg font-weight-bold' + status[data].class + ' label-inline">' + status[data].title + '</span>';
                    },
                },
                {
                    targets: -1,
                    className: 'dt-body-center',
                    orderable: false,
                    render: function (data, type, full, meta) {
                        const status = {
                            0: '',
                            1: ' checked',
                        };

                        return '\
                            <div class="form-group mb-0">\
                                <div class="col-12">\
                                    <span class="switch switch-icon">\
                                        <label>\
                                            <input\
                                                data-toggle="modal"\
                                                data-target="#confirmStatus"\
                                                data-id="' + data + '"\
                                                id="confirm-status"\
                                                title="Confirm Status"\
                                                type="checkbox"\
                                                name="status"' + status[full.status] + '/>\
                                            <span></span>\
                                        </label>\
                                    </span>\
                                </div>\
                            </div>\
                        ';
                    },
                },
            ],
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            invoice();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesInvoice.init();
});
