"use strict";

let KTDatatablesPurchaseItem = function() {
    let purchaseItems = function() {
        let table = $('#purchase-item').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            paging: true,
            order: [[ 1, "DESC" ]],
            ajax: {
                url: getPurchaseItems,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'POST',
                data: {
                    columnsDef: [
                        'DT_RowIndex',
                        'id',
                        'drink_name',
                        'offered_to',
                        'venue_name',
                        'price',
                        'type',
                        'status',
                        'created_at',
                        'used_expires_at',
                    ],
                },
            },
            columns: [
                {data: 'DT_RowIndex', searchable: false, orderable: false},
                {data: 'id'},
                {data: 'drink_name'},
                {data: 'offered_to'},
                {data: 'venue_name'},
                {data: 'price'},
                {data: 'type'},
                {data: 'status'},
                {data: 'created_at'},
                {data: 'used_expires_at'},
            ],

            columnDefs: [
                {
                    targets: 6,
                    render: function ( data ) {
                        if (data === '2x1') {
                            return '<span class="font-weight-bold text-primary">2x1</span>';
                        } else if (data === 'premium') {
                            return '<span class="font-weight-bold text-success">Premium</span>';
                        }
                        return data;
                    },
                },
                {
                    targets: 7,
                    render: function(data) {
                        let status = {
                            'succeeded': {'title': 'Succeeded', 'state': 'success'},
                            'pending': {'title': 'Pending', 'state': 'default'},
                            'failed': {'title': 'Failed', 'state': 'danger'},
                        };
                        if (typeof status[data] === 'undefined') {
                            return data;
                        }
                        return '<span class="font-weight-bold text-' + status[data].state + '">' + status[data].title + '</span>';
                    },
                },
            ],
        });
    };

    return {
        init: function() {
            purchaseItems();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatablesPurchaseItem.init();
});
