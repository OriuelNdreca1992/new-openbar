<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::query()->insert([
            ['name' => 'Open Bar Default 2x1 fee %', 'setting' => 'op_two_per_one_fee', 'value' => 15, 'description' => 'The fee applied at the venue for 2x1 drink', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Open Bar Default Premium fee %', 'setting' => 'op_premium_fee', 'value' => 15, 'description' => 'The fee applied at the venue for premium drink', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Services VAT % in Italy', 'setting' => 'taxable_sale', 'value' => 22, 'description' => 'Services VAT % in Italy', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Food VAT % in Italy', 'setting' => 'taxable_vat', 'value' => 10, 'description' => ' Food VAT in Italy', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Event drink % Fee', 'setting' => 'event_drink_fee', 'value' => 12, 'description' => 'Event drink % Fee', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Event setup Fee', 'setting' => 'event_setup_fee', 'value' => 12, 'description' => 'Event setup Fee', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'Event Venue Fee', 'setting' => 'event_venue_fee', 'value' => 15, 'description' => 'Event Venue Fee', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'User fee fixed', 'setting' => 'user_fee_fixed', 'value' => 1, 'description' => 'User fee fixed (Example: €1)', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'User fee min', 'setting' => 'user_fee_min', 'value' => 10, 'description' => 'User fee min amount (Example: €10)', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name' => 'User fee %', 'setting' => 'user_fee_percentage', 'value' => 10, 'description' => 'User fee (Example: 10%)', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
        ]);
    }
}
