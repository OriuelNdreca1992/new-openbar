<?php

namespace Database\Seeders;

use App\Models\Type;
use App\Models\Venue;
use App\Models\Company;
use App\Models\PremiumDrink;
use App\Models\Product\Product;
use Illuminate\Database\Seeder;
use App\Models\Product\Category;
use Database\Seeders\SliderSeeder;
use Database\Seeders\SettingSeeder;
use Database\Seeders\CartItemSeeder;
use Database\Seeders\AdministrativeDataSeeder;
use Database\Seeders\RolesAndPermissionsSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(CartItemSeeder::class);
        $this->call(AppUserTableSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(AdministrativeDataSeeder::class);
        $this->call(SliderSeeder::class);

        Type::factory(10)->create();
        Venue::factory(5)->create();
        Company::factory(5)->create();
        Category::factory(5)->create();
        Product::factory(5)->create();
        PremiumDrink::factory(5)->create();
        // Order::factory(10)->create();
    }
}
