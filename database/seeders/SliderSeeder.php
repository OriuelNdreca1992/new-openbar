<?php

namespace Database\Seeders;

use App\Models\Slider;
use Illuminate\Database\Seeder;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Slider::insert([
            ['title' => 'venues of the week', 'subtitle' => 'I migliori locali della settimana'],
            ['title' => 'new entry', 'subtitle' => 'Scopri i nuovi locali di OpenBar'],
        ]);
    }
}
