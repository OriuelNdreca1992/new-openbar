<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'admin']);
        $role2 = Role::create(['name' => 'digital-event']);
        $role3 = Role::create(['name' => 'venue']);
        $role4 = Role::create(['name' => 'company']);
        $role5 = Role::create(['name' => 'user-app']);
        $superAdminRole = Role::create(['name' => 'super-admin']);

        // create demo users
        $user = User::factory()->create([
            'first_name' => 'Oriuel',
            'last_name' => 'Ndreca',
            'email' => 'oriuel.n@rubik-technologies.al',
            'status' => true,
            'phone' => 0601234567,
            'role' => $superAdminRole->id,
            'password' => '$2y$10$CgkR7LPjSh4mF9rXfXKzwepHyW4yfdP0AqJDNhm61mES2Sg75thra', // password
            'remember_token' => \Str::random(10),
        ]);
        $user->assignRole($superAdminRole);
        $superAdminRole->givePermissionTo(Permission::all());

        $user = User::factory()->create([
            'first_name' => 'Joan',
            'last_name' => 'Kucuqi',
            'email' => 'joankucuqi@gmail.com',
            'status' => true,
            'phone' => 65421396325,
            'role' => $role1->id,
            'password' => Hash::make('123456'),
            'remember_token' => \Str::random(10),
        ]);
        $user->assignRole($role1);

        $user = User::factory()->create([
            'first_name' => 'Alessio',
            'last_name' => 'Puddu',
            'email' => 'alessio.puddu@rubik-technologies.al',
            'status' => true,
            'phone' => 6325412558,
            'role' => $role1->id,
            'password' => '$2y$10$CgkR7LPjSh4mF9rXfXKzwepHyW4yfdP0AqJDNhm61mES2Sg75thra', // password
            'remember_token' => \Str::random(10),
        ]);
        $user->assignRole($role1);

        $user = User::factory()->create([
            'first_name' => 'Aldi',
            'last_name' => 'Leka',
            'email' => 'aldi.l@rubik-technologies.al',
            'status' => true,
            'phone' => 96325874125,
            'role' => $role1->id,
            'password' => '$2y$10$CgkR7LPjSh4mF9rXfXKzwepHyW4yfdP0AqJDNhm61mES2Sg75thra', // password
            'remember_token' => \Str::random(10),
        ]);
        $user->assignRole($role1);

        $user = User::factory()->create([
            'first_name' => 'Nicola',
            'last_name' => 'Mazzucchelli',
            'email' => 'nicola@gmail.com',
            'status' => true,
            'phone' => 963258741,
            'role' => $role1->id,
            'password' => '$2y$10$CgkR7LPjSh4mF9rXfXKzwepHyW4yfdP0AqJDNhm61mES2Sg75thra', // password
            'remember_token' => \Str::random(10),
        ]);
        $user->assignRole($role1);

        $user = User::factory()->create([
            'first_name' => 'Cristiano',
            'last_name' => 'Ceretti',
            'email' => 'cristiano@gmail.com',
            'status' => true,
            'phone' => 147852369,
            'role' => $role1->id,
            'password' => '$2y$10$CgkR7LPjSh4mF9rXfXKzwepHyW4yfdP0AqJDNhm61mES2Sg75thra', // password
            'remember_token' => \Str::random(10),
        ]);
        $user->assignRole($role1);
    }
}
