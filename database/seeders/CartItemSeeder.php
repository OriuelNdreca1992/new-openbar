<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Offer;
use App\Models\PremiumDrink;
use App\Models\Checkout\Cart;
use Illuminate\Database\Seeder;
use App\Models\Checkout\CartItem;

class CartItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $premiumDrinks = PremiumDrink::factory(5)->create();
        $offerDrink = Offer::factory(5)->create();

        $userId = User::factory()->create(['role' => 5])->id;

        foreach ($premiumDrinks as $premium) {
            CartItem::query()->create([
                'used_at' => null,
                'venue_id' => $premium->venue_id,
                'cart_id' => Cart::factory()->create(['user_id' => $userId, 'type' => 'premium'])->id,
                'name' => $premium->product->name,
                'price' => $premium->price,
                'drinkable_id' => $premium->id,
                'drinkable_type' => PremiumDrink::class,
            ]);
        }

        foreach ($offerDrink as $offer) {
             CartItem::query()->create([
                'used_at' => null,
                'venue_id' => $offer->venue_id,
                'cart_id' => Cart::factory()->create(['user_id' => $userId, 'type' => '2x1'])->id,
                'name' => $offer->product->name,
                'price' => $offer->price,
                'drinkable_id' => $offer->id,
                'drinkable_type' => Offer::class,
            ]);
        }
    }
}
