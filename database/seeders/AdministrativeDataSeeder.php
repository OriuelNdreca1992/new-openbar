<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AdministrativeData;

class AdministrativeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdministrativeData::create([
            'name' => 'FlipYouApp Srl',
            'p_iva' => '03773160134',
            'address' => 'L.go Augusto 3 20122 Milano',
            'email' => 'info@openbar.life',
            'sdi' => 'USAL8PV',
            'bank_name' => 'BANCO BPM SPA',
            'abi' => '05034',
            'iban' => 'IT68M0503450245000000027269',
            'cbi' => '1579074W',
        ]);
    }
}
