<?php

namespace Database\Seeders;

use PDF;
use Carbon\Carbon;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Invoice;
use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $adminPath = '/documents/invoices/invoice_admin_'.uniqid().'.pdf';
        $venuePath = '/documents/invoices/invoice_venue_'.uniqid().'.pdf';
        $companyPath = '/documents/invoices/invoice_company_'.uniqid().'.pdf';

        $customPaper = array(0, 0, 900.00, 900.80);
        $adminPdf = PDF::loadView('admin.pdf-invoices.admin')->setPaper($customPaper, 'landscape')->save(public_path($adminPath));
        $venuePdf = PDF::loadView('admin.pdf-invoices.venue')->setPaper($customPaper, 'landscape')->save(public_path($venuePath));
        $companyPdf = PDF::loadView('admin.pdf-invoices.company')->setPaper($customPaper, 'landscape')->save(public_path($companyPath));

        Invoice::insert([
            ['subject_id' => 1, 'subject_type' => Venue::class, 'subject_invoice' => $venuePath, 'admin_invoice' => $adminPath, 'from' => Carbon::now(), 'to' => Carbon::now(), 'invoice_no' => 1, 'status' => false],
            ['subject_id' => 2, 'subject_type' => Venue::class, 'subject_invoice' => $venuePath, 'admin_invoice' => $adminPath, 'from' => Carbon::now(), 'to' => Carbon::now(), 'invoice_no' => 2, 'status' => false],
            ['subject_id' => 3, 'subject_type' => Venue::class, 'subject_invoice' => $venuePath, 'admin_invoice' => $adminPath, 'from' => Carbon::now(), 'to' => Carbon::now(), 'invoice_no' => 3, 'status' => false],
            ['subject_id' => 1, 'subject_type' => Company::class, 'subject_invoice' => $companyPath, 'admin_invoice' => $adminPath, 'from' => Carbon::now(), 'to' => Carbon::now(), 'invoice_no' => 4, 'status' => false],
            ['subject_id' => 2, 'subject_type' => Company::class, 'subject_invoice' => $companyPath, 'admin_invoice' => $adminPath, 'from' => Carbon::now(), 'to' => Carbon::now(), 'invoice_no' => 5, 'status' => false],
            ['subject_id' => 3, 'subject_type' => Company::class, 'subject_invoice' => $companyPath, 'admin_invoice' => $adminPath, 'from' => Carbon::now(), 'to' => Carbon::now(), 'invoice_no' => 6, 'status' => false],
        ]);
    }
}
