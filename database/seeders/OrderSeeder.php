<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Profile;
use App\Models\PremiumDrink;
use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $premiumDrinks = PremiumDrink::factory(5)->create();
        $offerDrink = Offer::factory(5)->create();

        $userId = User::factory()->create(['role' => 5])->id;

        foreach ($premiumDrinks as $premium) {
            Order::create([
                'drinkable_id' => $premium->id,
                'drinkable_type' => PremiumDrink::class,
                'user_id' => $userId,
                'venue_id' => $premium->venue_id,
                'type' => 1,
                'user_fee' => floatval(1),
                'expires_at' => Carbon::now()->addDays(5),
                'created_at' => Carbon::now()->subDays(4),
            ]);
        }

        foreach ($offerDrink as $offer) {
             Order::create([
                'drinkable_id' => $offer->id,
                'drinkable_type' => Offer::class,
                'user_id' => $userId,
                'venue_id' => $offer->venue_id,
                'type' => 0,
                'expires_at' => Carbon::now()->addDays(5),
                'created_at' => Carbon::now()->subDays(4),
            ]);
        }
    }
}
