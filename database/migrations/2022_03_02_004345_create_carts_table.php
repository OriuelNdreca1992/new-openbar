<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('offered_to')->index()->nullable();
            $table->foreign('offered_to')->references('id')->on('users')->onDelete('cascade');
            $table->decimal('amount');
            $table->decimal('user_fee')->default(0);
            $table->string('type', 10);
            $table->string('transaction_type', 10);
            $table->uuid('transaction_id');
            $table->string('payment_intent_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('last4', 4)->nullable();
            $table->string('brand', 30)->nullable();
            $table->string('message')->nullable();
            $table->timestamp('expires_at')->nullable();
            $table->timestamps();
            $table->softDeletes(); // ToDo
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
