<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministrativeDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrative_data', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('p_iva');
            $table->string('address');
            $table->string('email');
            $table->string('sdi');
            $table->string('bank_name');
            $table->string('abi');
            $table->string('iban');
            $table->string('cbi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrative_data');
    }
}
