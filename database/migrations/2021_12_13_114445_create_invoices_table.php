<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('subject_id');
            $table->string('subject_type');
            $table->string('subject_invoice');
            $table->string('admin_invoice');
            $table->date('from');
            $table->date('to');
            $table->decimal('open_bar_income')->default(0);
            $table->decimal('venue_income')->default(0);
            $table->bigInteger('invoice_no')->nullable();
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
    */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
