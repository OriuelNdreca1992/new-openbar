<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDigitalEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('de_events', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('card_id')->nullable();
            $table->foreign('card_id')->references('id')->on('de_cards')->onDelete('set null');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->tinyInteger('order')->nullable();
            $table->string('type',50);
            $table->string('format',50)->nullable();
            $table->string('promo',50)->nullable();
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();
            $table->smallInteger('budget')->default(0)->unsigned();
            $table->boolean('status')->default(false);
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->string('date_label')->nullable();
            $table->string('time_label')->nullable();
            $table->timestamps();
        });

        Schema::create('de_event_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')->references('id')->on('de_events')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('de_event_venues', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')->references('id')->on('de_events')->onDelete('cascade');
            $table->unsignedBigInteger('venue_id')->nullable();
            $table->foreign('venue_id')->references('id')->on('venues')->onDelete('cascade');
        });

        Schema::create('de_event_drinks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('event_id')->nullable();
            $table->foreign('event_id')->references('id')->on('de_events')->onDelete('cascade');
            $table->unsignedBigInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->double('price', 8, 2);
            $table->unsignedTinyInteger('quantity');
            $table->unsignedTinyInteger('user_quantity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('de_event_drinks');
        Schema::dropIfExists('de_event_venues');
        Schema::dropIfExists('de_event_details');
        Schema::dropIfExists('de_events');
    }
}
