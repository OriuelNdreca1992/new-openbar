<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('drinkable_id');
            $table->string('drinkable_type');
            $table->foreignId('user_id')->constrained();
            $table->foreignId('venue_id')->constrained();
            $table->tinyInteger('type');
            $table->decimal('user_fee')->nullable();
            $table->boolean('is_offered')->nullable();
            $table->foreignId('user_id_offered')->nullable()->constrained('users');
            $table->timestamp('expires_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
