<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('name');
            $table->string('email');
            $table->string('city');
            $table->string('address');
            $table->string('phone_number');
            $table->string('image_url')->nullable();
            $table->boolean('status')->default(false);
            $table->string('fiscal_name');
            $table->integer('event_setup_fee')->nullable();
            $table->integer('event_drink_fee')->nullable();
            $table->integer('event_venue_fee')->nullable();
            $table->string('fiscal_address');
            $table->string('fiscal_city');
            $table->string('fiscal_prov');
            $table->string('fiscal_cap');
            $table->string('fiscal_vat');
            $table->string('fiscal_code');
            $table->string('sdi_code');
            $table->string('certified_email');
            $table->string('bic');
            $table->string('iban');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
