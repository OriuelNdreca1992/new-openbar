<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheers', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->boolean('status')->default(false);
            $table->nullableMorphs('promoted');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->string('date_label')->nullable();
            $table->string('time_label')->nullable();
            $table->tinyInteger('order')->nullable();
            $table->timestamps();
        });

        Schema::create('cheers_paragraphs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cheers_id')->nullable();
            $table->foreign('cheers_id')->references('id')->on('cheers')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('video_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheers_paragraphs');
        Schema::dropIfExists('cheers');
    }
}
