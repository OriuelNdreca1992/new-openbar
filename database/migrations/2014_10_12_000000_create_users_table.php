<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable()->unique();
            $table->string('status')->default(0);
            $table->tinyInteger('role');
            $table->timestamp('phone_verified_at')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('email_verify_token', 60)->nullable();
            $table->string('phone_verify_token', 60)->nullable();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->date('birthdate')->nullable();
            $table->tinyInteger('gender')->default(0);
            $table->string('city')->nullable();
            $table->string('location')->nullable();
            $table->string('stripe_id')->nullable();
            $table->string('lang', 2)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
