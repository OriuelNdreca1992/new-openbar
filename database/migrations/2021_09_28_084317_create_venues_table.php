<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venues', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->string('name');
            $table->string('address');
            $table->string('email')->unique();
            $table->string('city');
            $table->string('postal_code');
            $table->string('state')->nullable();
            $table->string('country');
            $table->string('image_event_url')->nullable();
            $table->string('image_publish_url')->nullable();
            $table->string('phone_number');
            $table->text('description')->nullable();
            $table->integer('seats_number');
            $table->integer('premium_drink_fee')->nullable();
            $table->string('fiscal_name');
            $table->string('fiscal_address');
            $table->string('fiscal_country');
            $table->string('sdi_code')->nullable();
            $table->string('certified_email')->unique()->nullable();
            $table->string('fiscal_city');
            $table->string('fiscal_prov')->nullable();
            $table->string('fiscal_cap');
            $table->string('fiscal_vat');
            $table->string('fiscal_code')->nullable();
            $table->string('bic')->nullable();
            $table->string('iban')->nullable();
            $table->decimal('lat', 15, 13);
            $table->decimal('lng', 15, 12);
            $table->string('pin_code');
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venues');
    }
}
