<?php

namespace Database\Factories;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Order;
use App\Models\Venue;
use App\Models\PremiumDrink;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $drink = PremiumDrink::factory()->create();
        $userId = User::factory()->create(['role' => 5])->id;

        return [
            'drink_id' => $drink->id,
            'user_id' => $userId,
            'venue_id' => Venue::factory()->create()->id,
            'type' => $this->faker->numberBetween(0, 1),
            'status' => $this->faker->numberBetween(1, 3),
            'user_fee' => '1.00',
            'expires_at' => Carbon::now()->subDays(2),
            'created_at' => Carbon::now()->subDays(4),
        ];
    }
}
