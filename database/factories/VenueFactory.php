<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Venue;
use Illuminate\Database\Eloquent\Factories\Factory;

class VenueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Venue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'          => User::factory()->create(['role' => 3])->id,
            'name'             => $this->faker->company(),
            'address'          => $this->faker->address(),
            'city'             => $this->faker->city(),
            'email'            => $this->faker->safeEmail(),
            'postal_code'      => $this->faker->postcode(),
            'state'            => $this->faker->state(),
            'country'          => $this->faker->country(),
            'phone_number'     => $this->faker->phoneNumber(),
            'description'      => $this->faker->sentence(),
            'seats_number'     => $this->faker->numberBetween(1, 100),
            'fiscal_name'      => $this->faker->company(),
            'fiscal_address'   => $this->faker->address(),
            'fiscal_country'   => $this->faker->country(),
            'sdi_code'         => $this->faker->ein(),
            'certified_email'  => $this->faker->safeEmail(),
            'fiscal_cap'       => $this->faker->ein(),
            'fiscal_vat'       => $this->faker->ein(),
            'fiscal_code'      => $this->faker->ein(),
            'fiscal_prov'      => $this->faker->ein(),
            'fiscal_city'      => $this->faker->city(),
            'bic'              => $this->faker->swiftBicNumber(),
            'iban'             => $this->faker->iban(),
            'lat'              => $this->faker->latitude(),
            'lng'              => $this->faker->longitude(),
            'pin_code'         => $this->faker->numberBetween(100000, 999999),
            'status'           => rand(0, 1),
        ];
    }
}
