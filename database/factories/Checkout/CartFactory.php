<?php

namespace Database\Factories\Checkout;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Checkout\Cart;
use Illuminate\Database\Eloquent\Factories\Factory;

class CartFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cart::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create()->id,
            'offered_to' => User::factory()->create()->id,
            'amount' => 10,
            'type' => 'type',
            'user_fee' => 1,
            'transaction_type' => 'Visa',
            'transaction_id' => '5asd2a3sd15asd1',
            'payment_intent_id' => 'pm_asdkjhgasjhdg54asd31dsa',
            'expires_at' => Carbon::now()->addDays(5),
            'created_at' => Carbon::now()->subDays(4),
        ];
    }
}
