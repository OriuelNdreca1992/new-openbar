<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'         => User::factory()->create(['role'          => 4])->id,
            'name'            => $this->faker->company(),
            'email'           => $this->faker->safeEmail(),
            'city'            => $this->faker->city(),
            'address'         => $this->faker->address(),
            'phone_number'    => $this->faker->phoneNumber(),
            'fiscal_name'     => $this->faker->company(),
            'fiscal_address'  => $this->faker->address(),
            'fiscal_city'     => $this->faker->city(),
            'fiscal_prov'     => $this->faker->ein(),
            'event_setup_fee' => $this->faker->randomDigit(15, 20),
            'event_drink_fee' => $this->faker->randomDigit(15, 20),
            'event_venue_fee' => $this->faker->randomDigit(15, 20),
            'fiscal_cap'      => $this->faker->ein(),
            'fiscal_vat'      => $this->faker->ein(),
            'fiscal_code'     => $this->faker->ein(),
            'sdi_code'        => $this->faker->ein(),
            'certified_email' => $this->faker->safeEmail(),
            'bic'             => $this->faker->swiftBicNumber(),
            'iban'            => $this->faker->iban($countryCode = null),
        ];
    }
}
