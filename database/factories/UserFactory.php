<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        // $path = public_path('uploads/users/avatars');

        // if(! File::isDirectory($path)) {
        //     File::makeDirectory($path, 0777, true, true);
        // }
        // $fileName = $this->faker->image($path, 800, 600);

        return [
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'phone' => $this->faker->phoneNumber(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => \Hash::make('123456789'), // password
            'remember_token' => Str::random(10),
            // 'avatar' => basename($fileName),
            'birthdate' => $this->faker->date,
            'gender' => rand(0, 2),
            'city' => $this->faker->city(),
            'location' => $this->faker->streetAddress(),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
