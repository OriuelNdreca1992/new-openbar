<?php

namespace Database\Factories;

use Carbon\Carbon;
use App\Models\Venue;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class OfferFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => Product::factory()->create()->id,
            'venue_id' => Venue::factory()->create()->id,
            'price' => 100,
            'quantity' => 20,
            'status' =>  true,
            'start' => Carbon::now(),
            'end' => Carbon::now()->addDays(5),
            'all_the_time' => true,
            'description' => $this->faker->sentence(),
        ];
    }
}
