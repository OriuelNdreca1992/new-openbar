<?php

namespace Database\Factories\Product;

use App\Models\Product\Category;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
     /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        $path = public_path('uploads/categories');
        if(! File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        $fileName = $this->faker->image($path, 800, 600);

        return [
            'name' => $this->faker->word(),
            'image_url' => basename($fileName),
        ];
    }
}
