<?php

namespace Database\Factories;

use App\Models\Venue;
use App\Models\PremiumDrink;
use App\Models\Product\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class PremiumDrinkFactory extends Factory
{

     /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PremiumDrink::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'venue_id' => Venue::factory()->create()->id,
            'product_id' => Product::factory()->create()->id,
            'price' => 100,
            'promotional_price' => $this->faker->randomDigit(2, 9),
            'status' =>  true,
            'description' => $this->faker->sentence(),
        ];
    }
}
