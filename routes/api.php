<?php

use App\Http\Controllers\Api\ActivityController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Auth\ContactController;
use App\Http\Controllers\Api\Auth\ForgotPasswordController;
use App\Http\Controllers\Api\Auth\ResetPasswordController;
use App\Http\Controllers\Api\Auth\SocialController;
use App\Http\Controllers\Api\Auth\StatisticsController;
use App\Http\Controllers\Api\Auth\VerificationController;
use App\Http\Controllers\Api\CheersController;
use App\Http\Controllers\Api\Payment\StripeController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Api\PurchaseController;
use App\Http\Controllers\Api\ReviewController;
use App\Http\Controllers\Api\SliderController;
use App\Http\Controllers\Api\VenuesController;
use App\Http\Controllers\Api\WalletController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
    Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
        Route::get('token', [SocialController::class, 'login']);
        Route::post('login', [AuthController::class, 'login']);
        Route::post('signup', [AuthController::class, 'signUp']);
        Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
        Route::post('password/reset',  [ResetPasswordController::class, 'reset']);

        Route::group(['middleware' => ['auth:sanctum']], function() {
            Route::post('email/verify', [VerificationController::class, 'verifyEmail'])->name('verification.verify');
            Route::post('phone/verify', [VerificationController::class, 'verifyPhone'])->name('verification.verifyPhone');
            Route::get('email/resend', [VerificationController::class, 'resend'])->name('verification.resend');
            Route::get('phone/resend', [VerificationController::class, 'resendPhone'])->name('verification.resendPhone');
            Route::post('devices', [AuthController::class, 'saveDevice']);
        });

        Route::group(['middleware' => ['auth:sanctum']], function() {
            Route::delete('/', [AuthController::class, 'delete']);
            Route::get('logout', [AuthController::class, 'logout']);
            Route::get('user', [AuthController::class, 'user']);
            Route::put('update', [AuthController::class, 'update']);
            Route::get('statistics', [StatisticsController::class, 'general']);
            Route::post('language', [AuthController::class, 'saveLang']);
            Route::post('avatar/update', [AuthController::class, 'updateAvatar']);
            Route::post('phone/update', [AuthController::class, 'updatePhoneNumber']);
            Route::post('password/change', [AuthController::class, 'changePassword']);


            Route::get('purchases', [PurchaseController::class, 'index']);
            Route::get('purchases/{id}', [PurchaseController::class, 'show']);
        });
    });

    Route::group(['middleware' => ['auth:sanctum', 'verified']], function() {
        Route::get('venues/{id}/offers/{offerID}/use', [VenuesController::class, 'use2x1']);
        Route::post('venues/{id}/drinks', [PaymentController::class, 'buyDrinks']);

        Route::get('wallet/{id}/check', [WalletController::class, 'checkClaim']);

        Route::post('review/{id}', [ReviewController::class, 'review']);
    });

    Route::group(['middleware' => ['auth:sanctum']], function() {
        Route::get('wallet', [WalletController::class, 'wallet']);

        Route::get('activities', [ActivityController::class, 'index']);
        Route::get('activities/{id}', [ActivityController::class, 'show']);
        Route::post('activities/{id}/read', [ActivityController::class, 'read']);
        Route::post('activities/{id}/feedback', [ActivityController::class, 'feedback']);
    });

    Route::group(['middleware' => ['auth:sanctum', 'verified']], function() {
        Route::post('contacts', [ContactController::class, 'checkContacts']);
    });

    Route::get('venues', [VenuesController::class, 'index']); // @ToDo Legacy route
    Route::get('venues-v2', [VenuesController::class, 'index_v2']);
    Route::get('venues/{id}', [VenuesController::class, 'show']);

    Route::get('cheers', [CheersController::class, 'index']);
    Route::get('cheers/{id}', [CheersController::class, 'show']);

    Route::get('sliders/{position}', [SliderController::class, 'show']);

    Route::group(['middleware' => ['auth:sanctum']], function() {
        Route::get('stripe/payment/cards', [StripeController::class, 'getCards']);
        Route::post('stripe/payment/method', [StripeController::class, 'paymentMethod']);
        Route::post('stripe/payment/attach', [StripeController::class, 'attachCard']);
        Route::post('stripe/payment/detach', [StripeController::class, 'detachCard']);
    });
    Route::post('stripe/payment/webhook', [StripeController::class, 'webhook']);
});
