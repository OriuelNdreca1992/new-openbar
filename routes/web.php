<?php

use App\Http\Controllers\Admin\SendNotificationController;
use App\Http\Controllers\DrinkController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\CheersController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\Admin\InvoicesController;
use App\Http\Controllers\Admin\UploaderController;
use App\Http\Controllers\RegisterVenuesController;
use App\Http\Controllers\Admin\SliderOneController;
use App\Http\Controllers\Admin\SliderTwoController;
use App\Http\Controllers\Company\MyCompanyController;
use App\Http\Controllers\Admin\Venues\TypesController;
use App\Http\Controllers\Admin\Venues\OffersController;
use App\Http\Controllers\Admin\Venues\VenuesController;
use App\Http\Controllers\Admin\AdministratorsController;
use App\Http\Controllers\Admin\UserFeesReportController;
use App\Http\Controllers\Admin\Venues\CalendarController;
use App\Http\Controllers\Admin\Venues\PurchasesController;
use App\Http\Controllers\Admin\Products\ProductsController;
use App\Http\Controllers\Admin\Settings\PasswordController;
use App\Http\Controllers\Admin\Settings\SettingsController;
use App\Http\Controllers\Admin\AdministrativeDataController;
use App\Http\Controllers\Admin\Companies\CompaniesController;
use App\Http\Controllers\Admin\ExpiredDrinksReportController;
use App\Http\Controllers\Admin\Products\CategoriesController;
use App\Http\Controllers\Admin\Venues\PremiumDrinksController;
use App\Http\Controllers\Venue\VenueController as MyVenueController;
use App\Http\Controllers\Admin\Api\OrdersController as ApiOrdersController;
use App\Http\Controllers\Venue\OffersController as MyVenueOffersController;
use App\Http\Controllers\Venue\OrdersController as MyVenueOrdersController;
use App\Http\Controllers\Admin\Api\InvoicesController as ApiInvoicesController;
use App\Http\Controllers\Venue\CalendarController as MyVenueCalendarController;
use App\Http\Controllers\Venue\InvoicesController as MyVenueInvoicesController;
use App\Http\Controllers\Admin\Venues\OrdersController as VenueOrdersController;
use App\Http\Controllers\Admin\Venues\InvoicesController as VenueInvoicesController;
use App\Http\Controllers\Admin\Venues\ReviewsController as VenueReviewsController;
use App\Http\Controllers\Venue\PremiumDrinksController as MyVenuePremiumDrinksController;
use App\Http\Controllers\Admin\DigitalEvents\CardsController as DigitalEventsCardsController;
use App\Http\Controllers\Admin\DigitalEvents\EventsController as DigitalEventsEventsController;
use App\Http\Controllers\Admin\DigitalEvents\LabelsController as DigitalEventsLabelsController;
use App\Http\Controllers\Admin\ScheduledPushNotificationsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('pdf-invoices', function () {
    return view('admin.pdf-invoices.test-admin');
});

Route::get('venues/register', [RegisterVenuesController::class, 'showRegisterVenueForm'])->name('venues.register');
Route::post('venues', [RegisterVenuesController::class, 'store'])->name('venues.store');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [HomeController::class, 'index'])->name('dashboard');

    Route::group(['middleware' => 'role:super-admin|admin', 'prefix' => 'admin'], function () {
        //Administrators
        Route::resource('administrators', AdministratorsController::class, ['except' => 'destroy', 'as' => 'admin']);
        Route::delete('administrators/{administrator?}', [AdministratorsController::class, 'destroy'])->name('admin.administrators.destroy');

        Route::resource('users', UsersController::class, ['except' => 'destroy', 'as' => 'admin']);
        Route::delete('users/{user?}', [UsersController::class, 'destroy'])->name('admin.users.destroy');
        Route::get('users/{user}/gifted-items/{cart}', [UsersController::class, 'showGiftedItems'])->name('admin.users.showGiftedItems');
        Route::get('users/{user}/purchased-items/{cart}', [UsersController::class, 'showPurchasedItems'])->name('admin.users.showPurchasedItems');

        //User Purchased And Gifted API
        Route::post('purchased', [UsersController::class, 'getPurchased'])->name('admin.users.getPurchased');
        Route::post('users/{user}/purchased-items/{cart}', [UsersController::class, 'getPurchasedItems'])->name('admin.users.getPurchasedItems');
        Route::post('gifted', [UsersController::class, 'getGifted'])->name('admin.users.getGifted');
        Route::post('users/{user}/gidted-items/{cart}', [UsersController::class, 'getGiftedItems'])->name('admin.users.getGiftedItems');

        //Venues
        Route::resource('venues', VenuesController::class, ['except' => 'destroy', 'as' => 'admin']);
        Route::delete('venues/{venue?}', [VenuesController::class, 'destroy'])->name('admin.venues.destroy');

        Route::put('venues/{venue}/status', [VenuesController::class, 'updateStatus'])->name('admin.venues-status.update');

        //Venues Api
        Route::post('get-venues', [VenuesController::class, 'getVenues'])->name('getVenues');

        //Venue slider one
        Route::get('slider-one', [SliderOneController::class, 'index'])->name('admin.slider-one.index');
        Route::post('slider-one', [SliderOneController::class, 'getVenues'])->name('admin.slider-one.getVenues');
        Route::post('slider-one/add-venue', [SliderOneController::class, 'addVenue'])->name('admin.slider-one.addVenue');
        Route::post('slider-one/remove-venue', [SliderOneController::class, 'removeVenue'])->name('admin.slider-one.removeVenue');
        Route::patch('slider-one', [SliderOneController::class, 'update'])->name('admin.slider-one.update');

        //Venue slider two
        Route::get('slider-two', [SliderTwoController::class, 'index'])->name('admin.slider-two.index');
        Route::post('slider-two', [SliderTwoController::class, 'getVenues'])->name('admin.slider-two.getVenues');
        Route::post('slider-two/add-venue', [SliderTwoController::class, 'addVenue'])->name('admin.slider-two.addVenue');
        Route::post('slider-two/remove-venue', [SliderTwoController::class, 'removeVenue'])->name('admin.slider-two.removeVenue');
        Route::patch('slider-two', [SliderTwoController::class, 'update'])->name('admin.slider-two.update');

        //Companies
        Route::resource('companies', CompaniesController::class, ['except' => 'destroy', 'as' => 'admin']);
        Route::delete('companies/{company?}', [CompaniesController::class, 'destroy'])->name('admin.companies.destroy');

        //Company Api
        Route::post('get-companies', [CompaniesController::class, 'getCompanies'])->name('getCompanies');

        //Purchases
        Route::get('purchases', [PurchasesController::class, 'Index'])->name('admin.purchases.index');
        Route::get('purchases/{id}', [PurchasesController::class, 'show'])->name('admin.purchases.show');
        Route::get('purchases/{id}/drinks', [PurchasesController::class, 'drinks'])->name('admin.purchases.drinks');

        //Purchases Api
        Route::post('get-purchase/{id}', [PurchasesController::class, 'getPurchase'])->name('getPurchase');
        Route::post('get-purchases', [PurchasesController::class, 'getPurchases'])->name('getPurchases');
        Route::post('get-purchases/items', [PurchasesController::class, 'getPurchaseItems'])->name('getPurchaseItems');

        Route::group(['as' => 'admin.'], function () {
            Route::resource('products', ProductsController::class, ['except' => ['show']]);
            Route::resource('product-categories', CategoriesController::class, ['except' => ['show']]);
        });

        //Venue Premium Drinks
        Route::resource('venues/{venue}/premium-drinks', PremiumDrinksController::class, ['except' => ['show', 'destroy'], 'as' => 'admin.venue'])->parameters([
            'premium-drinks' => 'drink',
        ]);
        Route::delete('venues/{venue}/premium-drinks/{drink?}', [PremiumDrinksController::class, 'destroy'])->name('admin.venue.premium-drinks.destroy');

        //Two Per One drinks
        Route::resource('venues/{venue}/offers', OffersController::class, ['except' => ['show', 'destroy'], 'as' => 'admin.venue']);
        Route::delete('venues/{venue}/offers/{offer?}', [OffersController::class, 'destroy'])->name('admin.venue.offers.destroy');

        //Calendar
        Route::get('venues/{venue}/calendar', [CalendarController::class, 'index'])->name('admin.venue.calendar.index');
        Route::get('venues/{venue}/offers-calendar', [CalendarController::class, 'getOffers'])->name('admin.venue.calendar.offers');

        //Settings
        Route::put('settings/password/{user}', [PasswordController::class, 'update'])->name('admin.settings.password.update');

        Route::get('settings', [SettingsController::class, 'index'])->name('admin.settings.index');
        Route::get('settings/create', [SettingsController::class, 'create'])->name('admin.settings.create');
        Route::get('settings/{setting}/edit', [SettingsController::class, 'edit'])->name('admin.settings.edit');
        Route::patch('settings/{setting}', [SettingsController::class, 'update'])->name('admin.settings.update');

        //Administrative Data
        Route::get('administrative-data/edit', [AdministrativeDataController::class, 'edit'])->name('admin.administrative-data.edit');
        Route::patch('administrative-data/update', [AdministrativeDataController::class, 'update'])->name('admin.administrative-data.update');

        //Send Notification
        Route::get('send-notification/edit', [SendNotificationController::class, 'edit'])->name('admin.send-notification.edit');
        Route::patch('send-notification/update', [SendNotificationController::class, 'update'])->name('admin.send-notification.update');

        //Datatable Request Api
        Route::get('get-settings', [SettingsController::class, 'getSettings'])->name('admin.settings.getSettings');

        //Venue Types
        Route::resource('venue-types', TypesController::class, ['only' => ['index', 'store'], 'as' => 'admin'])->parameters([
            'venue-types' => 'type',
        ]);

        Route::patch('venue-types/{type?}',  [TypesController::class, 'update'])->name('admin.venue-types.update');
        Route::delete('venue-types/{type?}',  [TypesController::class, 'destroy'])->name('admin.venue-types.destroy');

        //Invoices
        Route::get('invoices', [InvoicesController::class, 'index'])->name('admin.invoices.index');
        Route::patch('invoices/{invoice?}', [InvoicesController::class, 'update'])->name('admin.invoices.update');

        //Orders
        Route::get('orders', [OrdersController::class, 'index'])->name('admin.orders.index');

        //Reports
        Route::get('expired-drinks-report', [ExpiredDrinksReportController::class, 'index'])->name('admin.expiredDrinks.index');
        Route::get('users-fee-report', [UserFeesReportController::class, 'index'])->name('admin.userFees.index');
        //Datatable APIs
        Route::post('expired-drinks-report', [ExpiredDrinksReportController::class, 'getReports'])->name('admin.expiredDrinks.getReports');
        Route::post('users-fee-report', [UserFeesReportController::class, 'getReports'])->name('admin.userFees.getReports');

        //Venue orders
        Route::get('venues/{venue}/orders', [VenueOrdersController::class, 'index'])->name('admin.venues.orders.index');
        Route::post('venues/{venue}/orders', [VenueOrdersController::class, 'getOrders'])->name('admin.venues.getOrders');

        //Venue Invoices
        Route::get('venues/{venue}/invoices', [VenueInvoicesController::class, 'index'])->name('admin.venues.invoices.index');
        Route::post('venues/{venue}/invoices', [VenueInvoicesController::class, 'getInvoices'])->name('admin.venue.getInvoices');

        //Venue Reviews
        Route::get('venues/{venue}/reviews', [VenueReviewsController::class, 'index'])->name('admin.venues.reviews.index');

        //Push Notifications
        Route::get('/push-notifications', [ScheduledPushNotificationsController::class, 'index'])->name('admin.push-notifications.index');
        Route::get('/push-notifications/create', [ScheduledPushNotificationsController::class, 'create'])->name('admin.push-notifications.create');
        Route::post('/push-notifications', [ScheduledPushNotificationsController::class, 'store'])->name('admin.push-notifications.store');
        Route::get('/push-notifications/{notification}', [ScheduledPushNotificationsController::class, 'edit'])->name('admin.push-notifications.edit');
        Route::patch('/push-notifications/{notification}', [ScheduledPushNotificationsController::class, 'update'])->name('admin.push-notifications.update');
        Route::delete('/push-notifications/{notification?}', [ScheduledPushNotificationsController::class, 'destroy'])->name('admin.push-notifications.destroy');
        //Push Notifications Api
        Route::get('/scheduled-push-notifications', [ScheduledPushNotificationsController::class, 'getScheduledPushNotifications'])->name('admin.scheduled-push-notifications.index');

        Route::get('/expired-push-notifications', [ScheduledPushNotificationsController::class, 'getExpiredPushNotifications'])->name('admin.expired-push-notifications.index');
    });

    Route::group(['middleware' => 'role:super-admin|admin|digital-event', 'as' => 'admin.', 'prefix' => 'admin'], function () {
        Route::group(['as' => 'digitalEvents.', 'prefix' => 'digital-events'], function () {
            Route::resource('cards', DigitalEventsCardsController::class);
            Route::resource('cards/{card}/labels', DigitalEventsLabelsController::class);
            Route::resource('events', DigitalEventsEventsController::class);
        });

        Route::resource('cheers', CheersController::class);
        Route::get('cheers/{cheer}/preview', [CheersController::class, 'preview'])->name('cheers.preview');
        Route::post('promoters', [CheersController::class, 'promoters'])->name('promoters');

        Route::post('files/delete/{id}', [UploaderController::class, 'destroy'])->name('files.destroy');
    });

    Route::group(['middleware' => ['role:venue', 'is.active']], function () {
        Route::get('my-venue', [MyVenueController::class, 'show'])->name('venue.show');
        Route::get('my-venue/edit', [MyVenueController::class, 'edit'])->name('venue.edit');
        Route::patch('my-venue', [MyVenueController::class, 'update'])->name('venue.update');

        Route::get('my-venue/offers/calendar', [MyVenueCalendarController::class, 'index'])->name('venue.calendar.index');
        Route::get('my-venue/offers/offers-calendar', [MyVenueCalendarController::class, 'getOffers'])->name('venue.calendar.getOffers');

        Route::resource('my-venue/offers', MyVenueOffersController::class, ['except' => ['show', 'destroy'], 'as' => 'venue']);
        Route::delete('my-venue/offers/{offer?}', [MyVenueOffersController::class, 'destroy'])->name('venue.offers.destroy');

        Route::resource('my-venue/premium-drinks', MyVenuePremiumDrinksController::class, ['except' => ['show', 'destroy'], 'as' => 'venue'])->parameters([
            'premium-drinks' => 'drink',
        ]);
        Route::delete('my-venue/premium-drinks/{drink?}', [MyVenuePremiumDrinksController::class, 'destroy'])->name('venue.premium-drinks.destroy');

        //Orders
        Route::get('my-venue/orders', [MyVenueOrdersController::class, 'index'])->name('venueOrders.index');
        Route::post('my-venue/orders', [MyVenueOrdersController::class, 'getOrders'])->name('venue.getOrders');

        //Invoices
        Route::get('my-venue/invoices', [MyVenueInvoicesController::class, 'index'])->name('venueInvoice.index');
        Route::post('my-venue/invoices', [MyVenueInvoicesController::class, 'getInvoices']);
    });

    Route::group(['middleware' => ['role:company', 'is.active']], function () {
        Route::get('my-company', [MyCompanyController::class, 'show'])->name('company.show');
        Route::get('my-company/edit', [MyCompanyController::class, 'edit'])->name('company.edit');
        Route::patch('my-company', [MyCompanyController::class, 'update'])->name('company.update');
    });

    // Requtes API
    Route::post('orders', [ApiOrdersController::class, 'getOrders']);
    Route::post('invoices', [ApiInvoicesController::class, 'getInvoices']);

    Route::post('media/delete', [HomeController::class, 'deleteMedia'])->name('media.delete');
});

Route::get('thank-you', [WelcomeController::class, 'thankYou'])->name('thank-you');

// Requtes API without authentication
Route::get('venue/type', [TypesController::class, 'getTypeById']);

// Claim drink
Route::match(['GET', 'POST'],'drink/{transaction}/{hash}', [DrinkController::class, 'details'])->name('drink.details');
