<?php return function ($fontDir, $rootDir) {
return array (
  'sans-serif' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $rootDir . '/lib/fonts/Helvetica',
    'bold' => $rootDir . '/lib/fonts/Helvetica-Bold',
    'italic' => $rootDir . '/lib/fonts/Helvetica-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold' => $rootDir . '/lib/fonts/ZapfDingbats',
    'italic' => $rootDir . '/lib/fonts/ZapfDingbats',
    'bold_italic' => $rootDir . '/lib/fonts/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $rootDir . '/lib/fonts/Symbol',
    'bold' => $rootDir . '/lib/fonts/Symbol',
    'italic' => $rootDir . '/lib/fonts/Symbol',
    'bold_italic' => $rootDir . '/lib/fonts/Symbol',
  ),
  'serif' => array(
    'normal' => $rootDir . '/lib/fonts/Times-Roman',
    'bold' => $rootDir . '/lib/fonts/Times-Bold',
    'italic' => $rootDir . '/lib/fonts/Times-Italic',
    'bold_italic' => $rootDir . '/lib/fonts/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $rootDir . '/lib/fonts/Courier',
    'bold' => $rootDir . '/lib/fonts/Courier-Bold',
    'italic' => $rootDir . '/lib/fonts/Courier-Oblique',
    'bold_italic' => $rootDir . '/lib/fonts/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSans-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSans-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSans-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSansMono-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSansMono-BoldOblique',
    'italic' => $rootDir . '/lib/fonts/DejaVuSansMono-Oblique',
    'normal' => $rootDir . '/lib/fonts/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $rootDir . '/lib/fonts/DejaVuSerif-Bold',
    'bold_italic' => $rootDir . '/lib/fonts/DejaVuSerif-BoldItalic',
    'italic' => $rootDir . '/lib/fonts/DejaVuSerif-Italic',
    'normal' => $rootDir . '/lib/fonts/DejaVuSerif',
  ),
  'poppins' => array(
    'normal' => $fontDir . '/poppins_normal_c885b15ae79e3daeece46b4ffe3f3d1b',
    '300' => $fontDir . '/poppins_300_ea8381d11630c33f2ab271919542aa73',
    '500' => $fontDir . '/poppins_500_05fb5a75e2d8c287d160b394607b8bee',
    '600' => $fontDir . '/poppins_600_c74f3fda4cb04848553a2345132469ca',
    'bold' => $fontDir . '/poppins_bold_948842cc44091c38acada365677354f5',
  ),
  'ki' => array(
    'normal' => $fontDir . '/ki_normal_e4ff80717bc5ccefd532a3e0b737d67f',
  ),
  'summernote' => array(
    'normal' => $fontDir . '/summernote_normal_2f793524e803436fa8c7eb3fbbd1054d',
  ),
  'line awesome brands' => array(
    'normal' => $fontDir . '/line_awesome_brands_normal_898c1ad0c42ade8d3182b1bdf9c936ee',
  ),
  'line awesome free' => array(
    'normal' => $fontDir . '/line_awesome_free_normal_c8a1d6d9d463bd6ad1ea9c18329979b9',
    '900' => $fontDir . '/line_awesome_free_900_745334d8cfa01e84c353ce1d4a531f96',
  ),
  'flaticon' => array(
    'normal' => $fontDir . '/flaticon_normal_5913d8d1678502647f365220503520ca',
  ),
  'flaticon2' => array(
    'normal' => $fontDir . '/flaticon2_normal_ad18d3e09e61ef64017cc7206555f34a',
  ),
  'socicon' => array(
    'normal' => $fontDir . '/socicon_normal_93960191d3791dbf6ef9ad4136fb9aa1',
  ),
  'font awesome 5 brands' => array(
    'normal' => $fontDir . '/font_awesome_5_brands_normal_d51defd07ea839af9d4247649a0d7dd5',
  ),
  'font awesome 5 free' => array(
    'normal' => $fontDir . '/font_awesome_5_free_normal_524616e78fc015290db6c07fe29ca98b',
    '900' => $fontDir . '/font_awesome_5_free_900_52915cb85f927ed4ba2dfa7460c43f7d',
  ),
  'montserrat' => array(
    '100' => $fontDir . '/montserrat_100_a2f86a1c67f005f099c6f14dfe4fd644',
    'normal' => $fontDir . '/montserrat_normal_31d154b17309e94407cadbaa528f867f',
    '500' => $fontDir . '/montserrat_500_7034f2b934852fc9cf836be356cf8b24',
  ),
);
}; ?>