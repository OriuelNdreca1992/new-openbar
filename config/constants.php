<?php

return [
    'select_one' => 'Select one',
    'status' => [
        'unpublish' => 'Unpublish',
        'publish' => 'Publish',
        'disable' => 'Disable',
        'enable' => 'Enable',
        'active' => 'Active',
        'no' => 'No',
        'yes' => 'Yes',
    ],
    'item_created_successfully'         => 'Item created successfully!',
    'item_updated_successfully'         => 'Item updated successfully!',
    'item_deleted_successfully'         => 'Item deleted successfully!',
    'item_not_created_successfully'     => 'Item not created successfully!',
    'item_not_updated_successfully'     => 'Item not updated successfully!',
    'item_not_deleted_successfully'     => 'Item not deleted successfully!',
    'data_processed_successfully'       => 'Data processed successfully!',
    'data_not_processed_successfully'   => 'Data not processed successfully!',
    'data_deleted_successfully'         => 'Data deleted successfully!',
    'data_not_deleted_successfully'     => 'Data not deleted successfully!',
];
