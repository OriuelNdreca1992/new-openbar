<?php

return [
    'base' => [
        'date_format' => 'd M Y',
        'datetime_format' => 'd M Y H:i',
    ],
];
