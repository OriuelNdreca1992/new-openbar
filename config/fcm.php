<?php

return [

    /**
     * Set the FCM Server Key.
     */
    'server_key' => env('FCM_SERVER_KEY'),

    /**
     * Set the Firebase tokens limit allowed in one request.
     */
    'firebase_tokens_limit' => env('FIREBASE_TOKENS_LIMIT', 1000),
];
