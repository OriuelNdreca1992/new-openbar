@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Premium Drinks', 'url' => route('venue.premium-drinks.index', request()->route('venue'))],
        ['title' => 'Create']
    ]
])

@section('content')
    <form
        class="form"
        action="{{ route('venue.premium-drinks.store', request()->route('venue')) }}"
        method="POST"
        enctype="multipart/form-data"
    >
    @csrf
        <!--begin::Content-->
        <div class="col-lg-10 offset-1">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Create Premium Drink</h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Form-->
                <div class="card-body">
                    @include('venue.premium-drinks.components.form', [
                        'drink' => new App\Models\PremiumDrink
                    ])

                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Create</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/pages/crud/file-upload/image-input.js') }}"></script>

    <script>
        $('select').on('change', function() {
            var productName = $('#product-id').find(":selected").text();
            productName = productName.replace(/^\s+|\s+$/gm,'');
            $('#name').val(productName);

            if(productName === 'Select Product') {
                $('#name').val('');
            }
        });
    </script>
@endpush




