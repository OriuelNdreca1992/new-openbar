@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Orders']
    ]
])

@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon">
                    <i class="flaticon2-delivery-package text-primary"></i>
                </span>
                <h3 class="card-label">Orders</h3>
            </div>
            <div class="card-toolbar">
               <div class="dropdown dropdown-inline">
                    <button type="button" class="btn btn-secondary btn-sm font-weight-bold" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="la la-download"></i>Tools
                    </button>
                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                        <ul class="navi flex-column navi-hover py-2">
                            <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">Export Tools</li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_print">
                                    <span class="navi-icon">
                                        <i class="la la-print"></i>
                                    </span>
                                    <span class="navi-text">Print</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_copy">
                                    <span class="navi-icon">
                                        <i class="la la-copy"></i>
                                    </span>
                                    <span class="navi-text">Copy</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_excel">
                                    <span class="navi-icon">
                                        <i class="la la-file-excel-o"></i>
                                    </span>
                                    <span class="navi-text">Excel</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_csv">
                                    <span class="navi-icon">
                                        <i class="la la-file-text-o"></i>
                                    </span>
                                    <span class="navi-text">CSV</span>
                                </a>
                            </li>
                            <li class="navi-item">
                                <a href="#" class="navi-link" id="export_pdf">
                                    <span class="navi-icon">
                                        <i class="la la-file-pdf-o"></i>
                                    </span>
                                    <span class="navi-text">PDF</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row mb-6">
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>Date:</label>
                    <div class="input-daterange input-group" id="kt_datepicker">
                        <input type="text" class="form-control datatable-input" name="start" placeholder="From" data-col-index="6" />
                        <div class="input-group-append">
                            <span class="input-group-text">
                                <i class="la la-ellipsis-h"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control datatable-input" name="end" placeholder="To" data-col-index="6" />
                    </div>
                </div>
                <div class="col-lg-3 mb-lg-0 mb-6">
                    <label>Type:</label>
                    <select class="form-control datatable-input" data-col-index="5" name="type">
                        <option value="">Select</option>
                        <option value="2x1">2x1</option>
                        <option value="premium">Premium</option>
                    </select>
                </div>
                 <div class="col-lg-3 mb-lg-0 mb-6">
                    <div class="row">
                        <div class="col-lg-6 mb-6">
                            <label>Drink ID:</label>
                            <input
                                type="text"
                                class="form-control datatable-input"
                                name="drink_id"
                                placeholder="Drink ID"
                                data-col-index="1"
                            />
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mt-8">
                    <div class="float-right">
                        <button class="btn btn-primary btn-primary--icon" id="kt_search">
                            <span>
                                <i class="la la-search"></i>
                                <span>Search</span>
                            </span>
                        </button>&#160;&#160;
                        <button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
                            <span>
                                <i class="la la-close"></i>
                                <span>Reset</span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable" id="order">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Drink ID</th>
                        <th>Drink Name</th>
                        <th>Drink Price</th>
                        <th>Owner ID</th>
                        <th>Type</th>
                        <th>Purchased At</th>
                        <th>Used At</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Drink ID</th>
                        <th>Drink Name</th>
                        <th>Drink Price</th>
                        <th>Owner ID</th>
                        <th>Type</th>
                        <th>Purchased At</th>
                        <th>Used At</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
@endsection

@push('scripts')
    <script>
        var getOrders = {!! json_encode(route('venue.getOrders')) !!}
    </script>
    <script src="{{ asset('js/venue/datatables/order.js') }}"></script>
@endpush
