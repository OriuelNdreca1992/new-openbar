@extends('layouts.app')

@push('styles')
    <style>
        .dt-body-center {
            text-align: center;
        }
    </style>
@endpush

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Reports']
    ]
])

@section('content')
     <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Reports</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="invoice">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Report</th>
                        <th>Report No.</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Report</th>
                        <th>Report No.</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
@endsection

@push('scripts')
  {{--   <script>
        var getInvoices = {!! json_encode(route('venueInvoice.index')) !!}
    </script> --}}
    <script src="{{ asset('js/venue/datatables/invoice.js') }}"></script>
@endpush
