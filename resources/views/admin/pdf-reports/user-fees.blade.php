@php use Carbon\Carbon; @endphp
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<style>
    body {
        font-family: 'Montserrat', sans-serif;
    }

    .flex-container {
        width: 100%;
        margin-top: 30px;
    }

    .flex-container > div {
        display: inline-block;
    }

    .flex-container-calculate > div {
        display: inline-block;
        font-size: 14px;
    }

    .center {
        text-align: center;
    }

    table {
        font-size: 12px;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<body>
<div class="flex-container center">
    <h1>OPENBAR USERS FEE REPORT</h1>
    <p>From {{ $from->format('Y-m-d') }} to {{ $to->format('Y-m-d') }}</p>
</div>
@if ($drinks->count())
    <div class="flex-container">
        <h2>PREMIUM DRINKS FEE</h2>
    </div>
    <table>
        <tr>
            <th>#</th>
            <th>Drink ID</th>
            <th>Drink</th>
            <th>Purchased</th>
            <th>Buyer</th>
            <th>Venue</th>
            <th>Price</th>
            <th>User Fee</th>
        </tr>
        @foreach ($drinks as $drink)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $drink->id }}</td>
                <td>{{ $drink->drink_name }}</td>
                <td>
                    {{ Carbon::parse($drink->created_at)->format('Y-m-d') }}
                    <br>
                    {{ Carbon::parse($drink->created_at)->format('H:i') }}
                </td>
                <td>
                    {{ $drink->first_name . '' . $drink->last_name }}
                    <br>
                    (ID: {{ $drink->buyer_id }})
                </td>
                <td>{{ $drink->venue_name }}</td>
                <td>{{ $drink->price }}</td>
                <td>&euro; {{ $drink->user_fee }}</td>
            </tr>
        @endforeach
    </table>
    <div style="width:40%;float:right;margin-top:40px;">
        <div class="flex-container-calculate">
            <div>
                <div style="font-weight:bold;">Total</div>
            </div>
            <div style="margin-left:60px;">
                <div style="font-weight:bold;">&euro; {{ sprintf("%0.2f", $drinks->sum('user_fee')) }}</div>
            </div>
        </div>
    </div>
@endif
</body>
</html>
