@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Settings', 'url' => route('admin.settings.index')],
        ['title' => 'Edit'],
    ]
])

@section('content')
    <form
        class="form"
        action="{{ route('admin.settings.update', $setting->id) }}"
        method="POST"
    >
    @method('PATCH')
    @csrf
        <!--begin::Content-->
        <div class="col-lg-10 offset-1">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Edit Setting</h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Form-->
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Name</label>
                        <div class="col-lg-9 col-xl-6">
                            <input
                                class="form-control form-control-solid"
                                type="text"
                                name="name"
                                value="{{ $setting->name }}"
                                readonly
                            />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Value</label>
                        <div class="col-lg-9 col-xl-6">
                            <input
                                class="form-control form-control-solid"
                                type="text"
                                name="value"
                                value="{{ $setting->value }}"
                            />
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Update</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </form>
@endsection
