@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Administrators', 'url' => route('admin.administrators.index')],
        ['title' => 'Edit'],
    ]
])

@section('content')
    <!--begin::Content-->
    <div class="col-lg-10 offset-1 mb-5">
        <!--begin::Card-->
        <div class="card card-custom">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Edit Administrative Data</h3>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form
                class="form"
                id="manage_administrator_form"
                action="{{ route('admin.administrative-data.update') }}"
                method="POST"
            >
                @method('PATCH')
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">Name</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="name"
                            value="{{ $administrativeData->name }}"
                        />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">P.Iva</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="p_iva"
                            value="{{ $administrativeData->p_iva }}"
                        />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">Address</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="address"
                            value="{{ $administrativeData->address }}"
                        />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">Email Address</label>
                    <div class="col-lg-9 col-xl-6">
                        <div class="input-group input-group-solid">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="la la-at"></i>
                                </span>
                            </div>
                            <input
                                class="form-control form-control-solid"
                                type="text"
                                name="email"
                                value="{{ $administrativeData->email }}"
                            />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">SDI</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="sdi"
                            value="{{ $administrativeData->sdi }}"
                        />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">Bank Name</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="bank_name"
                            value="{{ $administrativeData->bank_name }}"
                        />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">ABI</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="abi"
                            value="{{ $administrativeData->abi }}"
                        />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">IBAN</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="iban"
                            value="{{ $administrativeData->iban }}"
                        />
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">CBI</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid"
                            type="text"
                            name="cbi"
                            value="{{ $administrativeData->cbi }}"
                        />
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
            </form>
        <!--end::Form-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Content-->
@endsection

