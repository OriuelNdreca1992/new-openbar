@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Send Notification'],
    ]
])

@section('content')
    <div class="col-lg-10 offset-1 mb-5">
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <h3 class="card-label">Send Notification</h3>
                </div>
            </div>
            <form
                class="form"
                id="manage_administrator_form"
                action="{{ route('admin.send-notification.update') }}"
                method="POST"
            >
                @method('PATCH')
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label">
                            Mobiles <span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-9 col-xl-6">
                            <select name="users[]" class="form-control selectpicker" data-live-search="true" multiple required>
                                @foreach ($users as $user)
                                    <option
                                        value="{{ $user['id'] }}" {{ (old("users") !== null && in_array($user['id'], old("users")) ? "selected": "") }}>
                                        {{ $user['phone'] }} ({{$user['first_name']}} {{$user['last_name']}})
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label">
                            Title <span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-9 col-xl-6">
                            <input name="title" type="text"  class="form-control form-control-solid" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label">
                            Body <span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-9 col-xl-6">
                            <textarea name="body" rows="4" class="form-control border-0 form-control-solid" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <a href="{{ route('dashboard') }}" class="btn btn-light-primary font-weight-bold">Cancel</a>
                    <button type="submit" class="btn btn-primary font-weight-bold">Send</button>
                </div>
            </form>
        </div>
    </div>
@endsection
