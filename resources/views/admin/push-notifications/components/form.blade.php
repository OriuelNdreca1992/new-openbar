<div class="col-lg-8 col-xl-8">
    <div class="form-group">
        <label class="form-label">Notification Title</label>
        <input
            type="text"
            name="title"
            class="form-control border-0 form-control-solid"
            placeholder="Notification Title"
            value="{{ old('title', $notification->title) }}"
        />
    </div>
    <div class="form-group">
        <label class="form-label">Notification Body</label>
        <textarea
            class="form-control border-0 form-control-solid"
            rows="4"
            name="body"
            placeholder="Notification Body">{{ old('body', $notification->body) }}</textarea>
    </div>
</div>
<div class="col-lg-12 col-xl-12">
    <label class="form-label">Scheduled Time</label>
    <div class="form-group row">
        <div class="col-lg-4 col-md-9 col-sm-12">
            <div class="input-group date" id="kt_datetimepicker_1" data-target-input="nearest">
                <input
                    type="text"
                    name="scheduled_time"
                    class="form-control datetimepicker-input"
                    placeholder="Select date &amp; time"
                    data-target="#kt_datetimepicker_1"
                    value="{{ $notification->scheduled_time ? $notification->scheduled_time : old('scheduled_time') }}"
                />
                <div class="input-group-append" data-target="#kt_datetimepicker_1" data-toggle="datetimepicker">
                    <span class="input-group-text">
                        <i class="ki ki-calendar"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

