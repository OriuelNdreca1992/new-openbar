@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Push Notifications', 'url' => route('admin.push-notifications.index')],
        ['title' => 'Edit']
    ]
])

@section('content')
    <form
        class="form"
        action="{{ route('admin.push-notifications.update', $notification->id) }}"
        method="POST"
    >
    @method('PATCH')
    @csrf
        <!--begin::Content-->
        <div class="col-lg-10 offset-1">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title">
                        <h3 class="card-label font-weight-bolder text-dark">Edit Push Notification</h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Form-->
                <div class="card-body">
                    @include('admin.push-notifications.components.form')

                    <div class="card-footer text-right">
                        <a href="{{ route('admin.push-notifications.index') }}" class="btn btn-light-primary font-weight-bold">Cancel</a>
                        <button type="submit" class="btn btn-primary font-weight-bold">Update</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
@endpush

