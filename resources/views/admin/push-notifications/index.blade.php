@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Push Notifications'],
    ]
])

@section('content')
     <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Scheduled Push Notifications</h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{ route('admin.push-notifications.create') }}" class="btn btn-primary font-weight-bolder">
                    <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24" />
                            <circle fill="#000000" cx="9" cy="15" r="6" />
                            <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>Create
                </a>
                <!--end::Button-->
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="scheduled-push-notifications">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Scheduled Time</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Scheduled Time</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->

    <!--begin::Card-->
    <div class="card card-custom mt-10">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Expired Push Notifications</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="expired-push-notifications">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Expired At</th>
                        <th>Status</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Expired At</th>
                        <th>Status</th>
                        <th>Created At</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
   <div class="modal fade" id="deleteNotification" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
        @include('admin.push-notifications.components.modals.confirm')
    </div>
@endsection

@push('scripts')
    <script>
        var notificationsEdit = {!! json_encode(route('admin.push-notifications.edit', ':notification')) !!};

        $(document).on("click", "#delete-notification", function () {
            var id = $(this).data("id");
            if (id) {
                $('form').attr('action', function(i, value) {
                    return value + "/" + id;
                });
            }
        });
    </script>
    <script src="{{ asset('js/admin/datatables/push-notifications.js') }}"></script>
@endpush
