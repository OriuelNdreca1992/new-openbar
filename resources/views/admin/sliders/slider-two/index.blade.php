@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Slider Two']
    ]
])

@section('content')
     <!--begin::Content-->
    <div class="col-lg-12 mb-5">
        <!--begin::Card-->
        <div class="card card-custom">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Slider Two</h3>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form
                class="form"
                id="manage_administrator_form"
                action="{{ route('admin.slider-two.update') }}"
                method="POST"
            >
                @method('PATCH')
                @csrf
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Title</label>
                        <div class="col-lg-9 col-xl-6">
                            <input
                                class="form-control form-control-solid"
                                type="text"
                                name="title"
                                value="{{ $slider->title }}"
                            />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 text-right col-form-label">Subtitle</label>
                        <div class="col-lg-9 col-xl-6">
                            <input
                                class="form-control form-control-solid"
                                type="text"
                                name="subtitle"
                                value="{{ $slider->subtitle }}"
                            />
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                        <div class="col-lg-5 offset-3">
                            @if (count($errors))
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $message)
                                        <li class="ml-5">{{ $message }}</li>
                                    @endforeach
                                </ul>
                            @endif
                       </div>
                </div>
            </form>
        <!--end::Form-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Content-->
    <div class="card card-custom">
        <div class="card-header flex-wrap py-5">
            <div class="card-title">
                <h3 class="card-label">Venues</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="venue">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>City</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

@push('scripts')
    <script> let getVenues = {!! json_encode(route('admin.slider-two.getVenues')) !!}</script>
    <script> let addVenue = {!! json_encode(route('admin.slider-two.addVenue')) !!}</script>
    <script> let removeVenue = {!! json_encode(route('admin.slider-two.removeVenue')) !!}</script>
    <script> let venuesShow = {!! json_encode(route('admin.venues.show', ':venue')) !!}</script>
    <script src="{{ asset('js/admin/datatables/slider.js') }}"></script>
@endpush
