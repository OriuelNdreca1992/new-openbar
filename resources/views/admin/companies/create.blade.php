@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Companies', 'url' => route('admin.companies.index')],
        ['title' => 'Create']
    ]
])

@section('content')
    <form
        class="form"
        id="validate_company_request"
        action="{{ route('admin.companies.store') }}"
        method="POST"
        enctype="multipart/form-data"
    >
    @csrf
        <!--begin::Content-->
        <div class="col-lg-12">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Create Company</h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Form-->
                @include('admin.companies.components.form', [
                    'company' => new App\Models\Company,
                    'action' => 'Create',
                ])
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/pages/crud/file-upload/image-input.js') }}"></script>

        <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CompanyRequest', '#validate_company_request'); !!}
@endpush




