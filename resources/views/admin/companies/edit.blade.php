@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Companies', 'url' => route('admin.companies.index')],
        ['title' => 'Edit']
    ]
])

@section('content')
    <form
        class="form"
        action="{{ route('admin.companies.update', $company->id) }}"
        method="POST"
        id="validate_company_request"
        enctype="multipart/form-data"
    >
    @csrf
    @method('PATCH')
        <!--begin::Content-->
        <div class="col-lg-12 mb-5">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Edit Company</h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Form-->
                @include('admin.companies.components.form', [
                    'action' => 'Save Changes',
                ])
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </form>

    <!--begin::Content-->
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Password Settings</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update the password used for logging into company account.</span>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form
                class="form"
                id="password_validation"
                action="{{ route('admin.settings.password.update', $company->user->id) }}"
                method="POST"
                enctype="multipart/form-data"
            >
                @method('PUT')
                @csrf
                <div class="card-body">
                    @include('admin.administrators.components.password', [
                        'password' => 'New password'
                    ])

                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
            <!--end::Form-->
            </form>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Content-->
@endsection

@push('scripts')
    <script src="{{ asset('js/form/password-validate.js') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/file-upload/image-input.js') }}"></script>
     <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CompanyRequest', '#validate_company_request'); !!}
@endpush




