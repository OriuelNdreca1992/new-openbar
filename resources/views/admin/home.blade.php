@extends('layouts.app')

@push('styles')
    <style>
        .card.card-custom.card-stretch.card-stretch-half.gutter-b {
            height: calc(33% - 25px);
        }
    </style>
@endpush
@section('content')
    <!--begin::Row-->
    <div class="row">
        <div class="col-lg-8">
            <!--begin::Advance Table Widget 4-->
            <div class="card card-custom card-stretch gutter-b">
                <!--begin::Header-->
                <div class="card-header border-0 py-5">
                    <h3 class="card-title align-items-start flex-column">
                        <span class="card-label font-weight-bolder text-dark">Purchases</span>
                        <span class="text-muted mt-3 font-weight-bold font-size-sm">10 Most Purchased Beverages</span>
                    </h3>
                </div>
                <!--end::Header-->
                <!--begin::Body-->
                <div class="card-body pt-0 pb-3">
                    <div class="tab-content">
                        <!--begin::Table-->
                        <div class="table-responsive">
                            <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                                <thead>
                                    <tr class="text-left text-uppercase">
                                        <th style="min-width: 250px" class="pl-7">
                                            <span class="text-dark-75">Products</span>
                                        </th>
                                        <th style="min-width: 100px">Quantity Premium</th>
                                        <th style="min-width: 100px">Quantity 2x1</th>
                                        <th style="min-width: 100px">Venue</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($adminPurchasesStats as $purchase)
                                        <tr>
                                            <td class="pl-0 py-8">
                                                <div class="d-flex align-items-center">
                                                    <div class="symbol symbol-50 symbol-light mr-4">
                                                        <span class="symbol-label">
                                                            <img src="{{ asset('uploads/' . $purchase->product_image) }}" class="h-75 align-self-end" />
                                                        </span>
                                                    </div>
                                                    <div>
                                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{ $purchase->drink_name}}</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{ $purchase->total_premium }}</span>
                                            </td>
                                            <td>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{ $purchase->total_offer }}</span>
                                            </td>
                                            <td>
                                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{ $purchase->venue_name }}</span>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!--end::Table-->
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end::Advance Table Widget 4-->
        </div>
        <!--begin::Items-->
        <div class="col-lg-4 col-xxl-4">
            <div class="card card-custom card-stretch card-stretch-half gutter-b">
                <!--begin::Body-->
                <div class="card-body p-0">
                    <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                        <span class="symbol symbol-50 symbol-light-primary mr-2">
                            <span class="symbol-label">
                                <span class="svg-icon svg-icon-xl svg-icon-primary">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"/>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </span>
                        </span>
                        <div class="d-flex flex-column text-right">
                            <span class="text-dark-75 font-weight-bolder font-size-h3">+{{ $adminVenuesStats['new_venues'] ?? 0 }}</span>
                            <span class="text-muted font-weight-bold mt-2">New Venues</span>
                        </div>
                    </div>
                    <div id="venues_stats" class="card-rounded-bottom" data-color="primary" style="height: 150px"></div>
                </div>
                <!--end::Body-->
            </div>
            <div class="card card-custom card-stretch card-stretch-half gutter-b">
                <!--begin::Body-->
                <div class="card-body p-0">
                    <div class="d-flex align-items-center justify-content-between card-spacer flex-grow-1">
                        <span class="symbol symbol-50 symbol-light-primary mr-2">
                            <span class="symbol-label">
                                <span class="svg-icon svg-icon-xl svg-icon-primary">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Group.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24" />
                                            <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                            <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                            </span>
                        </span>
                        <div class="d-flex flex-column text-right">
                            <span class="text-dark-75 font-weight-bolder font-size-h3">+{{ $adminUsersStats['new_users'] ?? 0 }}</span>
                            <span class="text-muted font-weight-bold mt-2">New Users</span>
                        </div>
                    </div>
                    <div id="users_stats" class="card-rounded-bottom" data-color="primary" style="height: 150px"></div>
                </div>
                <!--end::Body-->
            </div>
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Premium Drinks Monthly Statistics</h3>
                    </div>
                </div>
                <div class="card-body">
                    <!--begin::Chart-->
                    <div id="premium_drinks" class="d-flex justify-content-center"></div>
                    <!--end::Chart-->
                </div>
            </div>
        </div>
    </div>
    <!--end::Row-->
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Premium Drinks Monthly Statistics</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="premium_drinks_stats"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">2x1 Drinks Monthly Statistics</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="offer_drinks_stats"></div>
                </div>
            </div>
        </div>
    </div>
        <!--end::Row-->
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Monthly Income Statistics (Venues)</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="admin_venue_income_stats"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">Monthly Fees Statistics</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div id="admin_op_income_stats"></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>var adminUsersStats = {!! json_encode($adminUsersStats) !!} </script>
    <script>var adminVenuesStats = {!! json_encode($adminVenuesStats) !!} </script>
    <script>var adminPremiumDrinksCurrentMonthStats = {!! json_encode($adminPremiumDrinksCurrentMonthStats) !!} </script>
    <script>var adminPremiumDrinksStats = {!! json_encode($adminPremiumDrinksStats) !!} </script>
    <script>var adminOfferDrinksStats = {!! json_encode($adminOfferDrinksStats) !!} </script>
    <script>var adminOpPremiumDrinksIncomeStats = {!! json_encode($adminOpPremiumDrinksIncomeStats) !!} </script>
    <script>var adminVenuePremiumDrinksIncomeStats = {!! json_encode($adminVenuePremiumDrinksIncomeStats) !!} </script>

    <script src="{{ asset('js/admin/stats/chart.js') }}"></script>
@endpush
