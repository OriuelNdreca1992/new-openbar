@extends('admin.metronic.layout.form')

@include('admin.metronic.cheers.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS, 'url' => route(INDEX['route'], INDEX['parameters'] )],
        ['title' =>  (isset($item)) ? 'Edit '.ITEM : 'New '.ITEM]
    ]
])

@section('card_body')
    <div class="card-body p-0 bg-gray-200">
        <div class="row">
            <div class="col-md-12">
                <!-- Cheers main info -->
                <div class="card mt-5">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                {{ Form::textGroup([
                                    'label' => 'Title',
                                    'name' => 'title',
                                    'id' => 'title',
                                    'field' => 'title',
                                    'value' => (isset($item)) ? $item->title : old('title'),
                                    'required' => true,
                                ], $errors) }}

                                {{ Form::textGroup([
                                    'label' => 'Subtitle',
                                    'name' => 'subtitle',
                                    'id' => 'subtitle',
                                    'field' => 'subtitle',
                                    'value' => (isset($item)) ? $item->subtitle : old('subtitle'),
                                    'required' => false,
                                ], $errors) }}

                                <div class="row">
                                    <div class="col-md-6">
                                        {{ Form::selectGroup([
                                            'label' => 'Type',
                                            'id' => 'promoted_type',
                                            'name' => 'promoted_type',
                                            'values' => (isset($item)) ? $item->type : old('promoted_type', 'openbar'),
                                            'multiple' => false,
                                            'required' => true,
                                            'data' => App\Models\Cheers::CHEER_TYPES
                                        ], $errors) }}
                                    </div>
                                    <div class="col-md-6">
                                        {{ Form::selectGroup([
                                            'label' => 'Promoted From',
                                            'id' => 'promoted_id',
                                            'name' => 'promoted_id',
                                            'values' => (isset($item)) ? $item->promoted_id : [],
                                            'multiple' => false,
                                            'required' => false,
                                            'data' => (isset($promoters)) ? $promoters : []
                                        ], $errors) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{ Form::dateTimeRangeGroup([
                                            'label_from' => 'From',
                                            'label_to' => 'To',
                                            'id_from' => 'start_date',
                                            'id_to' => 'end_date',
                                            'name_from' => 'start_date',
                                            'name_to' => 'end_date',
                                            'value_from' => (isset($item) && !empty($item->start_date))
                                                ? date('d.m.Y h:i A', strtotime($item->start_date)) : '',
                                            'value_to' => (isset($item) && !empty($item->end_date))
                                                ? date('d.m.Y h:i A', strtotime($item->end_date)) : '',
                                            'required_from' => true,
                                            'required_to' => false,
                                        ], $errors) }}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{ Form::textGroup([
                                            'label' => 'Date Label',
                                            'id' => 'date_label',
                                            'name' => 'date_label',
                                            'field' => 'date_label',
                                            'value' => (isset($item)) ? $item->date_label : old('date_label'),
                                            'required' => false,
                                        ], $errors) }}
                                    </div>

                                    <div class="col-md-6">
                                        {{ Form::textGroup([
                                            'label' => 'Time Label',
                                            'id' => 'time_label',
                                            'name' => 'time_label',
                                            'field' => 'time_label',
                                            'value' => (isset($item)) ? $item->time_label : old('time_label'),
                                            'required' => false,
                                        ], $errors) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{ Form::imageGroup([
                                    'id' => 'image',
                                    'label' => 'Image',
                                    'name' => 'image',
                                    'value' => (isset($image)) ? $image : '',
                                    'multiple' => false,
                                    'required' => false,
                                ], $errors) }}

                                {{ Form::switchGroup([
                                   'label' => 'Active',
                                   'name' => 'status',
                                   'value' => (isset($item)) ? $item->status : old('status'),
                                   'data-off' => config('constants.status.no'),
                                   'data-on' => config('constants.status.yes'),
                                ], $errors) }}
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cheers paragraphs -->
                <div class="card mt-5 mb-5">
                    <div class="card-header">
                        <h5 class="card-title m-0">Cheers paragraphs</h5>
                    </div>
                    <div class="card-body">
                        @include('admin.metronic.cheers.include.form_paragraphs')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('styles')
    {!! Html::style('assets/plugins/custom/cropper/cropper.bundle.css?v=1.5.9') !!}
@endpush

@push('scripts')
    {!! Html::script('assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js?v=7.2.9') !!}
    {!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
    {!! JsValidator::formRequest('App\Http\Requests\CheersRequest'); !!}

    {!! Html::script('assets/plugins/custom/cropper/cropper.bundle.js?v=1.5.9') !!}
    {!! Html::script('assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js?v=7.2.9') !!}

    <script>
        {!! File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
    </script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).ready(function () {
            let promoted_type = $('#promoted_type');
            promoted_type.bind('change',function(e) {
                let promoted_type = e.target.value;
                $.ajax({
                    url:"{{ route('admin.promoters') }}",
                    type:"POST",
                    data: {
                        type: promoted_type
                    },
                    success:function (data) {
                        let promoted_id = $('#promoted_id');
                        promoted_id.empty();
                        $.each(data,function(index, promoter) {
                            promoted_id.append('<option value="'+index+'">'+promoter+'</option>');
                        });
                        let firstVal = $('#promoted_id option:first').val();
                        let selectedVal = "{{ (isset($item)) ? $item->promoted_id : null }}";
                        let value = selectedVal ? selectedVal : firstVal;
                        promoted_id.selectpicker('refresh');
                        promoted_id.selectpicker('val', value);
                    }
                })
            });
            promoted_type.trigger('change');
        });
    </script>
@endpush
