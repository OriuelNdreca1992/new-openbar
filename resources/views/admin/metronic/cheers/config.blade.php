@php
    const ITEM = 'Article';
    const ITEMS = 'Cheers';
    const INDEX = ['route' => 'admin.cheers.index', 'parameters' => '' ];
    const CREATE = ['route' => 'admin.cheers.create', 'parameters' => '' ];

    if(isset($item)):
        define("UPDATE", ['route' => 'admin.cheers.update', 'parameters' => ['cheer' => $item->id] ]);
    else:
        define('STORE', ['route' => 'admin.cheers.store', 'parameters' => '' ]);
    endif;
@endphp
