<div id="kt_repeater_paragraphs">
    <div data-repeater-list="paragraphs">
        @if (isset($selected_paragraphs) and !empty($selected_paragraphs))
            @foreach ($selected_paragraphs as $key => $paragraph)
                @include('admin.metronic.cheers.include.form_paragraph_item')
            @endforeach
        @else
            @php($key = 0)
            @include('admin.metronic.cheers.include.form_paragraph_item')
        @endif
    </div>
    <div class="form-group row">
        <div class="col-md-12 text-center">
            <a href="javascript:;" data-repeater-create="" id="repeater-button"
                class="btn btn-sm font-weight-bolder btn-light-primary">
                <i class="la la-plus"></i>Add
            </a>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        // Class definition
        let KTFormRepeaterParagraphs = function() {
            // Private functions
            let paragraphs = function() {
                $('#kt_repeater_paragraphs').repeater({
                    // initEmpty: true,
                    show: function () {
                        let selfRepeaterItem = this;
                        // console.log(this);
                        // $(selfRepeaterItem).slideDown();
                        //
                        // let repeaterItems = $("div[data-repeater-item] > div.panel");
                        // $(selfRepeaterItem).attr('data-index', repeaterItems.length - 1);
                        $(selfRepeaterItem).find('.media-box').remove();
                        $(this).slideDown();
                    },
                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    },
                });
            }

            return {
                // public functions
                init: function() {
                    paragraphs();
                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormRepeaterParagraphs.init();
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        jQuery(document).on("click", ".delete-media", function (e) {
            e.preventDefault();
            if(confirm('Are you sure you want to permanently delete this image?')) {
                let object = $(this);
                let filename = $(this).attr("data-media");
                $.ajax({
                    url: "{{ route('media.delete') }}",
                    type: 'POST',
                    data: JSON.stringify({filename: filename}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (data) {
                            object.closest('.media-box').remove();
                        } else {
                            toastr.error('Error');
                        }
                    },
                    error: function (data) {
                        toastr.error('Error');
                    },
                });
            }
        });
    </script>
@endpush



@push('scripts')
    <script>


        $(document).on("click", "#delete-type", function () {
            var id = $(this).data("id");
            if (id) {
                $('form').attr('action', function(i, value) {
                    return value + "/" + id;
                });
            }
        });
    </script>
@endpush
