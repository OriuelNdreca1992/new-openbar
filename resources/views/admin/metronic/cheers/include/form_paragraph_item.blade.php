<div data-repeater-item class="example-preview mb-12">
    <div class="row">
        <input class="form-control" type="hidden" name="id" value="{{ isset($paragraph) ? $paragraph['id'] : old('id') }}" />
        @if( !isset($key) ) @php( $key = 0 ) @endif

        <div class="col-md-5">
            {{ Form::textGroup([
                'label' => 'Title',
                'id' => 'title',
                'name' => 'title',
                'field' => 'paragraphs.'.$key.'.title',
                'value' => isset($paragraph) ? $paragraph['title'] : old('title'),
                'required' => false,
            ], $errors) }}

            {{ Form::textareaGroup([
                'label' => 'Description',
                'id' => 'description',
                'name' => 'description',
                'field' => 'paragraphs.'.$key.'.description',
                'value' => isset($paragraph) ? $paragraph['description'] : old('description'),
                'required' => false,
            ], $errors) }}
        </div>
        <div class="col-md-5">
            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label class="form-label">Image</label>
                <input class="form-control" type="file" name="image" accept=".png, .jpg, .jpeg" />
                @if(isset($paragraph) && $paragraph['image'])
                    <div class="media-box" style="padding:5px">
                        <a class="media-name" target="_blank" href="{{ $paragraph['image'] }}">
                            {{ Str::of($paragraph['image'])->basename() }}
                        </a>
                        <a class="delete-media" href="#" data-media="{{ Str::of($paragraph['image'])->basename() }}">
                            <i class="ki ki-close icon-nm text-danger float-right"></i>
                        </a>
                    </div>
                @endif
                @if ($errors->has('image'))
                    <span class="error invalid-feedback">{{ $errors->first('image') }}</span>
                @endif
            </div>

            {{ Form::textGroup([
               'label' => 'Youtube Video ID',
               'id' => 'video_url',
               'name' => 'video_url',
               'field' => 'paragraphs.'.$key.'.video_url',
               'value' => isset($paragraph) ? $paragraph['video_url'] : old('video_url'),
               'required' => false,
           ], $errors) }}
        </div>
        <div class="col-md-2 d-flex align-items-center justify-content-center">
            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                <i class="la la-trash-o"></i>Delete
            </a>
        </div>
    </div>
</div>
