<html>
<head>
    <title>{{ $item->title }}</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&display=swap" rel="stylesheet">
    <link href="{{ asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/custom/bootstrap-modal/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/preview/preview.min.css') }}" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="cheers">
    <div class="vertical-box" style="margin-top: 15px;background-image: url('{{ $item->image }}')">
        <div class="box-textcontainer">
            <div class="text-banner-small-vertical"></div>
            <div class="text-banner-vertical">{{ $item->title }}</div>
            <div class="text-banner-vertical-bottom">{{ $item->subtitle }}</div>
        </div>
    </div>
    <div class="bottom-cheers">
        <div class="mid-banner">
            <span>
                <i class="ki ki-calendar-2 icon-sm"></i>
                @if ($item->date_label) {{ $item->date_label }}
                @else {{ $item->start_date->format('d/m') }} - {{ $item->end_date->format('d/m') }}
                @endif
            </span>
            <span>
                <i class="ki ki-clock icon-sm"></i>
                @if ($item->time_label) {{ $item->time_label }}
                @else {{ $item->start_date->format('H:i') }} - {{ $item->end_date->format('H:i') }}
                @endif
            </span>
        </div>

        @foreach($item->paragraphs as $paragraph)
            @if ($paragraph->title) <h2>{{ $paragraph->title }}</h2> @endif
            @if ($paragraph->description) <p>{{ $paragraph->description }}</p> @endif
            @if ($paragraph->video_url)
                <img class="img-cheers" alt="Image" width="100%" height="300px" data-toggle="modal" data-target="#myModal"
                     src="https://img.youtube.com/vi/{{ $paragraph->video_url }}/sddefault.jpg" style="cursor:pointer"/>
                <!-- Modal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-body" style="border:0;padding:0;">
                                <iframe width="100%" height="300px" src="https://www.youtube.com/embed/{{ $paragraph->video_url }}"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif($paragraph->image)
                <img src="{{ $paragraph->image }}" class="img-cheers" alt="Image" width="100%" height="auto"/>
            @endif
        @endforeach

        <div class="footer-cheers">
            <div>
                <h5>Promoted by</h5>
                <span>{{ Str::upper($promoted_by) }}</span>
            </div>
            @if($promoted_logo)
                <img src="{{ $promoted_logo }}" class="logo"  alt="Logo"/>
            @endif
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('assets/plugins/custom/bootstrap-modal/js/bootstrap.min.js') }}"></script>

</body>
</html>
