@include('admin.metronic.cheers.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS]
    ]
])

@extends('admin.metronic.layout.index', [
    'columns' => [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
		['data' => 'id', 'name' => 'id', 'orderable' => true, 'searchable' => false],
        ['data' => 'title', 'name' => 'title'],
        ['data' => 'status', 'name' => 'status', 'searchable' => false],
        ['data' => 'start_date', 'name' => 'start_date', 'searchable' => false],
        ['data' => 'end_date', 'name' => 'end_date', 'searchable' => false],
        ['data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false]
    ],
])

@section('thead')
    <tr>
        <th>#</th>
        <th>ID</th>
        <th>Title</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th style="min-width:150px">Actions</th>
    </tr>
@stop
