@php
    const ITEM = 'Category';
    const ITEMS = 'Categories';
    const INDEX = ['route' => 'admin.product-categories.index', 'parameters' => '' ];
    const CREATE = ['route' => 'admin.product-categories.create', 'parameters' => '' ];

    if(isset($item)):
        define("UPDATE", ['route' => 'admin.product-categories.update', 'parameters' => ['product_category' => $item->id] ]);
    else:
        define('STORE', ['route' => 'admin.product-categories.store', 'parameters' => '' ]);
    endif;
@endphp
