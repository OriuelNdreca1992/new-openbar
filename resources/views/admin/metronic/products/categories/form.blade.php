@extends('admin.metronic.layout.form')

@include('admin.metronic.products.categories.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS, 'url' => route(INDEX['route'], INDEX['parameters'] )],
        ['title' =>  (isset($item)) ? 'Edit '.ITEM : 'New '.ITEM]
    ]
])

@section('card_body')
    <div class="card-body p-0 bg-gray-200">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-5 mb-5">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-8 col-xl-8">
                                <div class="form-group">
                                    {{ Form::textGroup([
                                        'label' => 'Name',
                                        'name' => 'name',
                                        'id' => 'name',
                                        'field' => 'name',
                                        'value' => (isset($item)) ? $item->name : old('name'),
                                        'required' => true,
                                    ], $errors) }}
                                </div>
                            </div>
                            <div class="col-lg-4 col-xl-4">
                                <div class="text-center">
                                    <label>Image</label>
                                    <div class="form-group">
                                        <div class="col-lg-12 col-xl-12">
                                            <div class="image-input image-input-outline" id="product_category_image">
                                                <div class="image-input-wrapper" style="background-image: url({{ (isset($item) && $item->image_url) ? $item->image_url : asset('assets/media/no-image.png') }} )"></div>
                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Upload Image">
                                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                                    <input type="file" name="image_url" accept=".png, .jpg, .jpeg"/>
                                                </label>
                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel image">
                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                </span>
                                            </div>
                                            <span class="form-text">
                                                Upload Image 1080&times;960 <br>
                                                <small>(.png, .jpg, .jpeg)</small>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script src="{{ asset('assets/js/pages/crud/file-upload/image-input.js') }}"></script>
@endpush
