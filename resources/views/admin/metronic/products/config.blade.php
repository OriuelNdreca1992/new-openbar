@php
    const ITEM = 'Product';
    const ITEMS = 'Products';
    const INDEX = ['route' => 'admin.products.index', 'parameters' => '' ];
    const CREATE = ['route' => 'admin.products.create', 'parameters' => '' ];

    if(isset($item)):
        define("UPDATE", ['route' => 'admin.products.update', 'parameters' => ['product' => $item->id] ]);
    else:
        define('STORE', ['route' => 'admin.products.store', 'parameters' => '' ]);
    endif;
@endphp
