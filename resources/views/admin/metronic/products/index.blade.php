@include('admin.metronic.products.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS]
    ]
])

@extends('admin.metronic.layout.index', [
    'columns' => [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
		['data' => 'id', 'name' => 'id', 'orderable' => true, 'searchable' => false],
        ['data' => 'image_url', 'name' => 'image_url', 'orderable' => false, 'searchable' => false],
        ['data' => 'name', 'name' => 'name'],
        ['data' => 'category', 'name' => 'category', 'orderable' => false, 'searchable' => false],
        ['data' => 'status', 'name' => 'status', 'orderable' => true, 'searchable' => false],
        ['data' => 'created_at', 'name' => 'created_at', 'orderable' => true, 'searchable' => false],
        ['data' => 'updated_at', 'name' => 'updated_at', 'orderable' => true, 'searchable' => false],
        ['data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false]
    ],
])

@section('thead')
    <tr>
        <th>#</th>
        <th>ID</th>
        <th>Image</th>
        <th>Name</th>
        <th>Category</th>
        <th>Status</th>
        <th>Created At</th>
        <th>Updated At</th>
        <th style="min-width:150px">Actions</th>
    </tr>
@stop
