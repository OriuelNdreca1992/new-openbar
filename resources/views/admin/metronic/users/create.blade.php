@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Users', 'url' => route('admin.users.index')],
        ['title' => 'Create']
    ]
])

@section('content')
    <form class="form" action="{{ route('admin.users.store') }}" method="POST">
        @csrf
        <div class="col-lg-10 offset-1">
            <div class="card card-custom">
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Create App User</h3>
                    </div>
                </div>
                <div class="card-body">
                    @include('admin.metronic.users.components.form', ['user' => new App\Models\User])
                    @include('admin.metronic.users.components.password', ['password' => 'Password'])
                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Create</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
@endpush

