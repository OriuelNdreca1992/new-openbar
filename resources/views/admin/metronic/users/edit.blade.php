@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Users', 'url' => route('admin.users.index')],
        ['title' => 'Edit']
    ]
])

@section('content')
    <!-- User information -->
    <div class="col-lg-10 offset-1 mb-5">
        <div class="card card-custom">
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">User information</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update user informaiton</span>
                </div>
            </div>
            <form class="form" action="{{ route('admin.users.update', $user->id) }}" method="POST">
                @method('PATCH')
                @csrf
                <div class="card-body">
                    @include('admin.metronic.users.components.form')
                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Password Settings -->
    <div class="col-lg-10 offset-1">
        <div class="card card-custom">
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Password Settings</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update the password used for logging into account.</span>
                </div>
            </div>
            <form class="form" id="password_validation" action="{{ route('admin.settings.password.update', $user->id) }}" method="POST">
                @method('PUT')
                @csrf
                <div class="card-body">
                    @include('admin.administrators.components.password', ['password' => 'New password'])
                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/form/password-validate.js') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js') }}"></script>
@endpush
