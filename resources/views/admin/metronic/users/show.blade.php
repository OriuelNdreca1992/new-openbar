@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Users', 'url' => route('admin.users.index')],
        ['title' => 'User']
    ]
])

@section('content')
    <!--begin::Card-->
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <!--begin::Details-->
            <div class="d-flex mb-9">
                <!--begin: Pic-->
                <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                    <div class="symbol symbol-50 symbol-lg-120">
                        <img src="{{ $user->avatar['original'] ?: url('assets/media/users/default.jpg') }}" alt="image" />
                    </div>
                    <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                        <span class="font-size-h3 symbol-label font-weight-boldest">ON</span>
                    </div>
                </div>
                <!--end::Pic-->
                <!--begin::Info-->
                <div class="flex-grow-1">
                    <!--begin::Title-->
                    <div class="d-flex justify-content-between flex-wrap mt-1">
                        <div class="d-flex mr-3">
                            @if ($user->first_name or $user->last_name)
                                <span class="text-dark-75 font-size-h5 font-weight-bold mr-3">
                                    {{ $user->getFullName() }}
                                </span>
                            @endif
                            <span>
                                @if ($user->status)
                                    <i class="flaticon2-correct text-success font-size-h5"></i>
                                @else
                                    <i class="flaticon2-cancel text-danger font-size-h5"></i>
                                @endif
                            </span>
                        </div>
                    </div>
                    <!--end::Title-->
                    <!--begin::Content-->
                    <div class="d-flex flex-wrap justify-content-between mt-1">
                        <div class="d-flex flex-column flex-grow-1 pr-8">
                            <div class="text-dark-50 font-weight-bold">
                                @if ($user->email)
                                    <i class="flaticon2-new-email mr-2 font-size-lg"></i> {{ $user->email }} <br>
                                @endif
                                @if ($user->city)
                                    <i class="flaticon2-placeholder mr-2 font-size-lg"></i> {{ $user->city }} <br>
                                @endif
                                @if ($user->phone)
                                    <i class="flaticon2-phone mr-2 font-size-lg"></i> {{ $user->phone }} <br>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Info-->
            </div>
            <!--end::Details-->
            <div class="separator separator-solid"></div>
            <!--begin::Items-->
            <div class="d-flex align-items-center flex-wrap mt-8">
                <!--begin::Item-->
                <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                    <span class="mr-4">
                        <i class="flaticon2-user display-4 text-muted font-weight-bold"></i>
                    </span>
                    <div class="d-flex flex-column text-dark-75">
                        <span class="font-weight-bolder font-size-h5">Role</span>
                        <span class="font-weight-bolder font-size-sm">App User</span>
                    </div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                    <span class="mr-4">
                        <i class="fas fa-user icon-2x"></i>
                    </span>
                    <div class="d-flex flex-column text-dark-75">
                        <span class="font-weight-bolder font-size-h5">Gender</span>
                        <span class="font-weight-bolder font-size-sm">{{ $user->getGenderLabel() ?: '-' }}</span>
                    </div>
                </div>
                <!--end::Item-->
                <!--begin::Item-->
                <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                    <span class="mr-4">
                        <i class="flaticon-user-add display-4 text-muted font-weight-bold"></i>
                    </span>
                    <div class="d-flex flex-column text-dark-75">
                        <span class="font-weight-bolder font-size-h5">Created At</span>
                        <span class="font-weight-bolder font-size-sm">{{ $user->created_at->format('d.m.Y H:i') }}</span>
                    </div>
                </div>
                <!--end::Item-->
                 <!--begin::Item-->
                <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                    <span class="mr-4">
                        <i class="flaticon-user-ok display-4 text-muted font-weight-bold"></i>
                    </span>
                    <div class="d-flex flex-column text-dark-75">
                        <span class="font-weight-bolder font-size-h5">Updated At</span>
                        <span class="font-weight-bolder font-size-sm">{{ $user->updated_at->format('d.m.Y H:i') }}</span>
                    </div>
                </div>
                <!--end::Item-->
            </div>
            <!--begin::Items-->
        </div>
    </div>
    <!--end::Card-->
    <div class="card card-custom mt-10">
        <div class="card-header flex-wrap py-5">
            <div class="card-title">
                <h3 class="card-label">Drinks gifted to others</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="purchased_drink">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Purchase ID</th>
                        <th>Taker</th>
                        <th>Type</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Amount</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Purchase ID</th>
                        <th>Buyer</th>
                        <th>Type</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Amount</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
     <div class="card card-custom mt-10">
        <div class="card-header flex-wrap py-5">
            <div class="card-title">
                <h3 class="card-label">Drinks gifted to user</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="gifted_drink">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Purchase ID</th>
                        <th>Buyer</th>
                        <th>Type</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Amount</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Purchase ID</th>
                        <th>Buyer</th>
                        <th>Type</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Amount</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

@push('scripts')
    <script> let purchasedUrl = {!! json_encode(route('admin.users.getPurchased', ['id' => $user->id])) !!}</script>
    <script> let giftedUrl = {!! json_encode(route('admin.users.getGifted', ['id' => $user->id])) !!}</script>
    <script> let purchasesShow = {!! json_encode(route('admin.users.showPurchasedItems', ['user' => $user->id, 'cart' => ':cart'])) !!}</script>
    <script> let giftedShow = {!! json_encode(route('admin.users.showGiftedItems', ['user' => $user->id, 'cart' => ':cart'])) !!}</script>
    <script src="{{ asset('js/admin/datatables/purchased-drinks.js') }}"></script>
@endpush
