<div class="row">
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>First Name <span class="text-danger">*</span></label>
            <input type="text" name="first_name" class="form-control form-control-solid" placeholder="First Name" value="{{ old('first_name', $user->first_name) }}"/>
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>Last Name <span class="text-danger">*</span></label>
            <input type="text" name="last_name" class="form-control form-control-solid" placeholder="Last Name" value="{{ old('last_name', $user->last_name) }}"/>
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>Email <span class="text-danger">*</span></label>
            <input type="email" name="email" class="form-control form-control-solid" placeholder="Email" value="{{ old('email', $user->email) }}"/>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>Phone <span class="text-danger">*</span></label>
            <input type="text" name="phone" class="form-control form-control-solid" placeholder="Phone" value="{{ old('phone', $user->phone) }}"/>
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>Gender <span class="text-danger">*</span></label>
            <select class="form-control form-control-solid" name="gender">
                <option value="">Select Gender</option>
                <option value="1" {{ $user->gender === \App\Models\User::USER_GENDER_MALE ? 'selected' : '' }}>Male</option>
                <option value="2" {{ $user->gender === \App\Models\User::USER_GENDER_FEMALE ? 'selected' : '' }}>Female</option>
            </select>
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>Birthdate</label>
            <div class="input-group date">
                <input type="text" name="birthdate" class="form-control form-control-solid" id="kt_datepicker_2" placeholder="Select date" value="{{ $user->birthdate ?  : old('birthdate') }}"/>
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-calendar-check-o"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
