<div class="row">
    <div class="col-lg-6 col-xl-6">
        <div class="form-group">
            <label>Password <span class="text-danger">*</span></label>
            <input type="password" name="password" class="form-control form-control-solid" placeholder="Password" required/>
        </div>
    </div>
        <div class="col-lg-6 col-xl-6">
        <div class="form-group">
            <label>Confirm Password <span class="text-danger">*</span></label>
            <input type="password" name="password_confirmation" class="form-control form-control-solid" placeholder="Password" required/>
        </div>
    </div>
</div>
