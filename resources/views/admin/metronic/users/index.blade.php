@include('admin.metronic.users.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS]
    ]
])

@extends('admin.metronic.layout.index', [
    'columns' => [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
        ['data' => 'id', 'name' => 'id', 'orderable' => true, 'searchable' => false],
		['data' => 'first_name', 'name' => 'first_name'],
		['data' => 'last_name', 'name' => 'last_name'],
		['data' => 'status', 'name' => 'status', 'searchable' => false],
		['data' => 'email', 'name' => 'email'],
		['data' => 'birthdate', 'name' => 'birthdate', 'orderable' => true, 'searchable' => false],
		['data' => 'phone', 'name' => 'phone'],
		['data' => 'register_type', 'name' => 'register_type', 'orderable' => true, 'searchable' => false],
		['data' => 'created_at', 'name' => 'created_at', 'orderable' => true, 'searchable' => false],
        ['data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false]
    ],
])

@section('thead')
    <th>#</th>
    <th>User ID</th>
    <th>Name</th>
    <th>Surname</th>
    <th>Status</th>
    <th>E-Mail</th>
    <th>Birthday</th>
    <th>Phone</th>
    <th>Register Type</th>
    <th>Created At</th>
    <th>Actions</th>
@stop


