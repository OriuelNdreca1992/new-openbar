@php
    const ITEM = 'User';
    const ITEMS = 'Users';
    const INDEX = ['route' => 'admin.users.index', 'parameters' => '' ];
    const CREATE = ['route' => 'admin.users.create', 'parameters' => '' ];

    if(isset($item)):
        define("UPDATE", ['route' => 'admin.users.update', 'parameters' => ['user' => $item->id] ]);
    else:
        define('STORE', ['route' => 'admin.users.store', 'parameters' => '' ]);
    endif;
@endphp
