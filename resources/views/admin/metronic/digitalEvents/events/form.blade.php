@extends('admin.metronic.layout.form')

@include('admin.metronic.digitalEvents.events.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS, 'url' => route(INDEX['route'], INDEX['parameters'] )],
        ['title' =>  (isset($item)) ? 'Edit '.ITEM : 'New '.ITEM]
    ]
])

@section('card_body')
    <div class="card-body p-0 bg-gray-200">
        <div class="row">
            <div class="col-md-12">
                <!-- Event main info -->
                <div class="card mt-5">
                    <div class="card-header">
                        <h5 class="card-title m-0">Event main info</h5>
                    </div>
                    <div class="card-body">
                        @include('admin.metronic.digitalEvents.events.include.form_main_info')
                    </div>
                </div>
                <!-- Event details -->
                <div class="card mt-5">
                    <div class="card-header">
                        <h5 class="card-title m-0">Event details</h5>
                    </div>
                    <div class="card-body">
                        @include('admin.metronic.digitalEvents.events.include.form_details')
                    </div>
                </div>
                <!-- Event budget -->
                <div class="card mt-5">
                    <div class="card-header">
                        <h5 class="card-title m-0">Event budget</h5>
                    </div>
                    <div class="card-body">
                        @include('admin.metronic.digitalEvents.events.include.form_budget')
                    </div>
                </div>
                <!-- Event venues -->
                <div class="card mt-5">
                    <div class="card-header">
                        <h5 class="card-title m-0">Choose venues</h5>
                    </div>
                    <div class="card-body">
                        @include('admin.metronic.digitalEvents.events.include.form_venues')
                    </div>
                </div>
                <!-- Event drinks -->
                <div class="card mt-5 mb-5">
                    <div class="card-header">
                        <h5 class="card-title m-0">Drinks of the event</h5>
                    </div>
                    <div class="card-body">
                        @include('admin.metronic.digitalEvents.events.include.form_drinks')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('styles')
    {!! Html::style('assets/plugins/custom/cropper/cropper.bundle.css?v=1.5.9') !!}
@endpush

@push('scripts')
    {!! Html::script('assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js?v=7.2.9') !!}
    {!! Html::script('vendor/jsvalidation/js/jsvalidation.js') !!}
    {!! JsValidator::formRequest('App\Http\Requests\DigitalEvents\EventRequest'); !!}

    {!! Html::script('assets/plugins/custom/cropper/cropper.bundle.js?v=1.5.9') !!}
    {!! Html::script('assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js?v=7.2.9') !!}

    <script>
        {!! \File::get(base_path('vendor/unisharp/laravel-filemanager/public/js/stand-alone-button.js')) !!}
    </script>
    <script>
        var route_prefix = "/filemanager";
        $('#lfm').filemanager('image', {prefix: route_prefix});

        $(function() {
            $("#budget").on("changed.bs.select", function(e, clickedIndex, newValue, oldValue) {
                let display = 'none';
                if (this.value === @json(\App\Models\DigitalEvent\Event::BUDGET_CUSTOM)) {
                    display = 'block';
                }
                document.getElementById("custom-input").style.display = display;
            });
        });
    </script>
@endpush
