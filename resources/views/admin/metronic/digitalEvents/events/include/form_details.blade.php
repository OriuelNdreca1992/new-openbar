<div class="row">
    <div class="col-md-12">
        {{ Form::textareaGroup([
            'label' => 'Description',
            'id' => 'description',
            'name' => 'description',
            'class' => 'ckeditor',
            'field' => 'description',
            'value' => isset($item) ? $item->description : old('description'),
            'required' => false,
        ], $errors) }}
    </div>
</div>
