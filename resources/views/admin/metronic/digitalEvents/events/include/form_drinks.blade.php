<div id="kt_repeater_drinks">
    <div data-repeater-list="drinks">
        @if (isset($selected_drinks) and !empty($selected_drinks))
            @foreach ($selected_drinks as $drink)
                @include('admin.metronic.digitalEvents.events.include.form_drink_item')
            @endforeach
        @else
            @include('admin.metronic.digitalEvents.events.include.form_drink_item')
        @endif
    </div>
    <div class="form-group row">
        <div class="col-md-12 text-center">
            <a href="javascript:;" data-repeater-create="" id="repeater-button"
                class="btn btn-sm font-weight-bolder btn-light-primary">
                <i class="la la-plus"></i>Add
            </a>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        // Class definition
        let KTFormRepeaterDrinks = function() {
            // Private functions
            let drinks = function() {
                $('#kt_repeater_drinks').repeater({
                    show: function () {
                        $(this).slideDown();
                    },
                    hide: function (deleteElement) {
                        if(confirm('Are you sure you want to delete this element?')) {
                            $(this).slideUp(deleteElement);
                        }
                    },
                });
            }

            return {
                // public functions
                init: function() {
                    drinks();
                }
            };
        }();

        jQuery(document).ready(function() {
            KTFormRepeaterDrinks.init();
        });
    </script>
@endpush
