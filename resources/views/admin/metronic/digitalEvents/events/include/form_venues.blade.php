<div class="row">
    <div class="col-md-12">
        {{ Form::selectGroup([
            'label' => 'Choose Venues',
            'id' => 'venue_ids',
            'name' => 'venue_ids[]',
            'values' => (isset($selected_venues)) ? $selected_venues : [],
            'multiple' => true,
            'required' => true,
            'data' => (isset($venues)) ? $venues : []
        ], $errors) }}
    </div>
</div>
