<div data-repeater-item class="example-preview mb-12">
    <div class="row">
        <div class="col-md-6">
            {{ Form::textGroup([
                'label' => 'Title',
                'id' => 'title',
                'name' => 'title',
                'field' => 'title',
                'value' => isset($drink) ? $drink['title'] : old('title'),
                'required' => true,
            ], $errors) }}

            {{ Form::textareaGroup([
                'label' => 'Description',
                'id' => 'description',
                'name' => 'description',
                'field' => 'description',
                'value' => isset($drink) ? $drink['description'] : old('description'),
                'required' => false,
            ], $errors) }}
        </div>
        <div class="col-md-4">
            {{ Form::selectGroup([
                'label' => 'Choose Drink',
                'id' => 'product_id',
                'name' => 'product_id',
                'values' => isset($drink) ? $drink['product_id'] : [],
                'multiple' => false,
                'required' => true,
                'data' => (isset($products)) ? $products : []
            ], $errors) }}
        </div>
        <div class="col-md-2 d-flex align-items-center justify-content-center">
            <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                <i class="la la-trash-o"></i>Delete
            </a>
        </div>
    </div>
</div>
