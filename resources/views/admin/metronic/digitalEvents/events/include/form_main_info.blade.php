<div class="row">
    <div class="col-md-8">
        {{ Form::textGroup([
            'label' => 'Title',
            'name' => 'title',
            'id' => 'title',
            'field' => 'title',
            'value' => (isset($item)) ? $item->title : old('title'),
            'required' => true,
        ], $errors) }}

        {{ Form::textGroup([
            'label' => 'Subtitle',
            'name' => 'subtitle',
            'id' => 'subtitle',
            'field' => 'subtitle',
            'value' => (isset($item)) ? $item->subtitle : old('subtitle'),
            'required' => false,
        ], $errors) }}

        <div class="row">
            <div class="col-md-6">
                {{ Form::selectGroup([
                    'label' => 'Type',
                    'id' => 'type',
                    'name' => 'type',
                    'values' => (isset($item)) ? $item->type : [],
                    'multiple' => false,
                    'required' => true,
                    'data' => App\Models\DigitalEvent\Event::TYPOLOGY_TYPES
                ], $errors) }}
            </div>

            <div class="col-md-6">
                {{ Form::selectGroup([
                    'label' => 'Promo Label',
                    'id' => 'promo',
                    'name' => 'promo',
                    'values' => (isset($item)) ? $item->promo : [],
                    'multiple' => false,
                    'required' => false,
                    'data' => App\Models\DigitalEvent\Event::PROMO_TYPES
                ], $errors) }}
            </div>

            <div class="col-md-6">
                {{ Form::selectGroup([
                    'label' => 'Promoted From',
                    'id' => 'company_id',
                    'name' => 'company_id',
                    'values' => (isset($item)) ? $item->company_id : [],
                    'multiple' => false,
                    'required' => false,
                    'data' => (isset($companies)) ? $companies : []
                ], $errors) }}
            </div>

            <div class="col-md-6">
                {{ Form::selectGroup([
                    'label' => 'Card',
                    'id' => 'card_id',
                    'name' => 'card_id',
                    'values' => (isset($item)) ? $item->card_id : [],
                    'multiple' => false,
                    'required' => false,
                    'data' => (isset($cards)) ? $cards : []
                ], $errors) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {{ Form::dateTimeRangeGroup([
                    'label_from' => 'From',
                    'label_to' => 'To',
                    'id_from' => 'start_date',
                    'id_to' => 'end_date',
                    'name_from' => 'start_date',
                    'name_to' => 'end_date',
                    'value_from' => (isset($item) && !empty($item->start_date))
                        ? date('d.m.Y h:i A', strtotime($item->start_date)) : '',
                    'value_to' => (isset($item) && !empty($item->end_date))
                        ? date('d.m.Y h:i A', strtotime($item->end_date)) : '',
                    'required_from' => true,
                    'required_to' => false,
                ], $errors) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                {{ Form::textGroup([
                    'label' => 'Date Label',
                    'id' => 'date_label',
                    'name' => 'date_label',
                    'value' => (isset($item)) ? $item->date_label : old('date_label'),
                    'required' => false,
                ], $errors) }}
            </div>

            <div class="col-md-6">
                {{ Form::textGroup([
                    'label' => 'Time Label',
                    'name' => 'time_label',
                    'id' => 'time_label',
                    'value' => (isset($item)) ? $item->time_label : old('time_label'),
                    'required' => false,
                ], $errors) }}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        {{ Form::radioGroup([
            'label' => 'Format',
            'name' => 'format',
            'value' => (isset($item)) ? $item->format : '',
            'required' => true,
            'data' => App\Models\DigitalEvent\Event::FORMAT_TYPES
        ], $errors) }}

        {{ Form::imageGroup([
            'id' => 'image',
            'label' => 'Image',
            'name' => 'image',
            'value' => (isset($image)) ? $image : '',
            'multiple' => false,
            'required' => false,
        ], $errors) }}

        {{ Form::switchGroup([
           'label' => 'Active',
           'name' => 'status',
           'value' => (isset($item)) ? $item->status : old('status'),
           'data-off' => config('constants.status.no'),
           'data-on' => config('constants.status.yes'),
        ], $errors) }}
    </div>
</div>

