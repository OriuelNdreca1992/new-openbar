<div class="row">
    <div class="col-md-12">
        {{ Form::selectGroup([
            'label' => 'Choose Budget',
            'id' => 'budget',
            'name' => 'budget',
            'values' => isset($item) ? $item->budget : old('budget'),
            'multiple' => false,
            'required' => true,
            'data' => (isset($budgets)) ? $budgets : []
        ], $errors) }}
        <div id="custom-input" style="display:none">
            {{ Form::textGroup([
                'type' => 'number',
                'label' => 'Custom',
                'id' => 'custom',
                'name' => 'custom',
                'field' => 'custom',
                'value' => isset($item) ? $item->budget : old('custom'),
                'required' => true,
            ], $errors) }}
        </div>
    </div>
</div>
