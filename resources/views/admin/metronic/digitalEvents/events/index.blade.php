@include('admin.metronic.digitalEvents.events.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS]
    ]
])

@extends('admin.metronic.layout.index', [
    'columns' => [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
		['data' => 'id', 'name' => 'id', 'orderable' => true, 'searchable' => false],
        ['data' => 'type', 'name' => 'type'],
        ['data' => 'promo', 'name' => 'promo'],
        ['data' => 'title', 'name' => 'title'],
        ['data' => 'start_date', 'name' => 'start_date', 'searchable' => false],
        ['data' => 'end_date', 'name' => 'end_date', 'searchable' => false],
        ['data' => 'status', 'name' => 'status', 'searchable' => false],
        ['data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false]
    ],
])

@section('thead')
    <tr>
        <th>#</th>
        <th>ID</th>
        <th>Type</th>
        <th>Promo</th>
        <th>Title</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Active</th>
        <th style="min-width:150px">Actions</th>
    </tr>
@stop
