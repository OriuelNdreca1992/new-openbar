@php
    const ITEM = 'Event';
    const ITEMS = 'Events';
    const INDEX = ['route' => 'admin.digitalEvents.events.index', 'parameters' => '' ];
    const CREATE = ['route' => 'admin.digitalEvents.events.create', 'parameters' => '' ];

    if(isset($item)):
        define("UPDATE", ['route' => 'admin.digitalEvents.events.update', 'parameters' => ['event' => $item->id] ]);
    else:
        define('STORE', ['route' => 'admin.digitalEvents.events.store', 'parameters' => '' ]);
    endif;
@endphp
