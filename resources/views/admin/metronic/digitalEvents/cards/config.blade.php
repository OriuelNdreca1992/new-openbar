@php
    const ITEM = 'Card';
    const ITEMS = 'Cards';
    const INDEX = ['route' => 'admin.digitalEvents.cards.index', 'parameters' => '' ];
    const CREATE = ['route' => 'admin.digitalEvents.cards.create', 'parameters' => '' ];

    if(isset($item)):
        define("UPDATE", ['route' => 'admin.digitalEvents.cards.update', 'parameters' => ['card' => $item->id] ]);
    else:
        define('STORE', ['route' => 'admin.digitalEvents.cards.store', 'parameters' => '' ]);
    endif;
@endphp

