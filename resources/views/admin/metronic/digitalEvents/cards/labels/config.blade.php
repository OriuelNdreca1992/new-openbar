@php
    const ITEM = 'Label';
    const ITEMS = 'Labels';

    if(isset($card)):
        define("INDEX", ['route' => 'admin.digitalEvents.labels.index', 'parameters' => ['card' => $card] ]);
        define("CREATE", ['route' => 'admin.digitalEvents.labels.create', 'parameters' => ['card' => $card] ]);
    endif;

    if(isset($card) && isset($item)):
        define("UPDATE", ['route' => 'admin.digitalEvents.labels.update', 'parameters' => ['card' => $card, 'label' => $item->id] ]);
    elseif(isset($card)):
        define('STORE', ['route' => 'admin.digitalEvents.labels.store', 'parameters' => ['card' => $card] ]);
    endif;
@endphp
