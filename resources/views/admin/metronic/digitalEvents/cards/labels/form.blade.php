@extends('admin.metronic.layout.form')

@include('admin.metronic.digitalEvents.cards.labels.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Cards', 'url' => route('admin.digitalEvents.cards.index')],
        ['title' => ITEMS, 'url' => route(INDEX['route'], INDEX['parameters'] )],
        ['title' =>  (isset($item)) ? 'Edit '.ITEM : 'New '.ITEM]
    ]
])

@section('card_body')
    <div class="card-body p-0 bg-gray-200">
        <div class="row">
            <div class="col-md-12">
                <div class="card mt-5 mb-5">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6 offset-md-3">
                                        {{ Form::textGroup([
                                            'label' => 'Name',
                                            'name' => 'name',
                                            'id' => 'name',
                                            'field' => 'name',
                                            'value' => (isset($item)) ? $item->name : old('name'),
                                            'required' => true,
                                        ], $errors) }}
                                    </div>
                                    <div class="col-md-6 offset-md-3">
                                        {{ Form::switchGroup([
                                           'label' => 'Active',
                                           'name' => 'status',
                                           'value' => (isset($item)) ? $item->status : old('status'),
                                           'data-off' => config('constants.status.no'),
                                           'data-on' => config('constants.status.yes'),
                                       ], $errors) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                {{ Form::imageGroup([
                                    'id' => 'image',
                                    'label' => 'Icon',
                                    'name' => 'image',
                                    'value' => (isset($image)) ? $image : '',
                                    'multiple' => false,
                                    'required' => false,
                                ], $errors) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
