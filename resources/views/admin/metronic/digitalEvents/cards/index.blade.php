@include('admin.metronic.digitalEvents.cards.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => ITEMS]
    ]
])

@extends('admin.metronic.layout.index', [
    'columns' => [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
		['data' => 'id', 'name' => 'id', 'orderable' => true, 'searchable' => false],
        ['data' => 'name', 'name' => 'name'],
        ['data' => 'status', 'name' => 'status', 'searchable' => false],
        ['data' => 'action', 'name' => 'action', 'orderable' => false, 'searchable' => false]
    ]
])

@section('thead')
    <tr>
        <th>#</th>
        <th>ID</th>
        <th>Name</th>
        <th>Status</th>
        <th style="min-width: 150px;">Actions</th>
    </tr>
@stop
