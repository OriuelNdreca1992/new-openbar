@section('breadcrumbs')
    @if(isset($breadcrumbs) && !empty($breadcrumbs))
        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
            @foreach($breadcrumbs as $breadcrumb)
                <li class="breadcrumb-item text-muted">
                    @if(isset($breadcrumb['url']))
                        <a href="{{ $breadcrumb['url'] }}" class="text-muted"><b>{{ $breadcrumb['title'] }}</b></a>
                    @else
                        {{ $breadcrumb['title'] }}
                    @endif
                </li>
            @endforeach
        </ul>
    @endif
@stop
