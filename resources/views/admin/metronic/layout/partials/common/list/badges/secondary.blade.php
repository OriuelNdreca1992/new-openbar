@if(isset($value) && !empty($value))
    <span class="badge badge-secondary">{{$value}}</span>
@endif
