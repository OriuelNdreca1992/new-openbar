<span class="label label-lg font-weight-bold label-inline
    @if($status) label-light-success @else label-light-danger @endif">
    @if($status) {{ config('constants.status.active') }} @else {{ config('constants.status.disable') }} @endif
</span>
