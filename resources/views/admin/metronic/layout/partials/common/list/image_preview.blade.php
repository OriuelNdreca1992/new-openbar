<div class="symbol symbol-40 symbol-sm flex-shrink-0">
    <img
        alt="{{ ( isset($alt) && !empty($alt) ) ? $alt : '' }}"
        src="{{ ( isset($src) && !empty($src) ) ? $src : asset('assets/media/no-image.png') }}">
</div>
