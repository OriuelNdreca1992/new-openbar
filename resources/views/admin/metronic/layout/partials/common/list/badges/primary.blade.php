@if(isset($value) && !empty($value))
    <span class="badge badge-primary">{{$value}}</span>
@endif
