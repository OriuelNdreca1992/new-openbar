@switch($provider)
    @case('facebook')
        <i class="socicon-facebook text-primary"></i>
        @break

    @case('instagrambasic')
        <i class="socicon-instagram text-danger"></i>
        @break

    @default
        <i class="socicon-mail text-dark-50"></i>
@endswitch