@if(isset($value) && !empty($value))
    <span class="badge badge-dark">{{$value}}</span>
@endif
