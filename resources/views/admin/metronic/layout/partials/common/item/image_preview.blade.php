@php( (!(isset($src) && !empty($src))) ? $path = '/media/no-image.png' : $path = $src )
<div class="symbol symbol-250 symbol-sm flex-shrink-0">
    <img alt="{{ ($alt) ? $alt : '' }}" src="{{ $path }}">
</div>
