<div class="input-group-append">
    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="svg-icon">
            {{ Metronic::getSVG(config()->get('languages')[app()->getLocale()]['icon'], "svg-icon-sm") }}
        </span>
        {{ config()->get('languages')[app()->getLocale()]['name'] }}
    </button>
    <div class="dropdown-menu dropdown-menu-right" style="">
        @foreach (config()->get('languages') as $lang => $language)
            @if ($lang != app()->getLocale())
                <a class="btn dropdown-item" href="{{ route('admin.lang.switch', $lang) }}">
                    <span class="svg-icon">
                        {{ Metronic::getSVG($language['icon'], "svg-icon-sm") }}
                    </span>
                    {{ $language['name'] }}
                </a>
            @endif
        @endforeach
    </div>
</div>