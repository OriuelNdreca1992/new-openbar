@extends('admin.metronic.layout.default')

@section('content')
    <div class="row pb-3">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">{{ ITEM }}</h3>
                        </div>
                        @if(isset($item))
                            {{ Form::model($item, ['route' => [UPDATE['route'], UPDATE['parameters']], 'method' => 'patch', 'enctype' => 'multipart/form-data', 'novalidate']) }}
                        @else
                            {{ Form::open(['route' => [STORE['route'], STORE['parameters']], 'enctype' => 'multipart/form-data', 'novalidate']) }}
                        @endif
{{--                            <div class="card-body  bg-gray-100">--}}
                                @yield('card_body')
{{--                            </div>--}}
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-success mr-2">Submit</button>
                                            <a href="{{ route(INDEX['route'], INDEX['parameters'] ) }}" class="btn btn-secondary">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    {!! Html::style('plugins/custom/bootstrap-fileinput/css/fileinput.min.css') !!}
@endpush

@push('scripts')
    {!! Html::script('plugins/custom/bootstrap-fileinput/js/fileinput.min.js') !!}
    {!! Html::script('plugins/custom/bootstrap-fileinput/themes/fas/theme.min.js') !!}

    <!-- CKEditor init -->
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/ckeditor.js') !!}
    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.11/adapters/jquery.js') !!}
    <script>
        var route_prefix = "/filemanager";
        $('.ckeditor').ckeditor({
            height: 200,
            filebrowserImageBrowseUrl: route_prefix + '?type=Images',
            filebrowserImageUploadUrl: route_prefix + '/upload?type=Images&_token={{csrf_token()}}',
            filebrowserBrowseUrl: route_prefix + '?type=Files',
            filebrowserUploadUrl: route_prefix + '/upload?type=Files&_token={{csrf_token()}}'
        });
    </script>

@endpush
