@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($type)) @php( $type = Arr::get($params, 'type') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>

    <div class="radio-{{ !empty($type) ?: 'inline' }}">
        @foreach (Arr::get($params, 'data') as $key => $option)
            <label class="radio">
                <input type="radio" name="{{ $name }}"
                       value="{{$key}}"
                    {{ ($key === $value
                        OR (Arr::get($params, 'required', false)
                            AND !in_array($value, Arr::get($params, 'data'))
                            AND $loop->first)) ? 'checked="checked"' : '' }}
                />
                <span></span>{{ $option }}
            </label>
        @endforeach
    </div>

    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>
