@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif
<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $id }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <input
        type="file"
        id="{{ $id }}"
        name="{{ $name }}"
        class="{{ $errors->has($name) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }}"
        {{ Arr::get($params, 'multiple', false) ? 'multiple' : '' }}
        accept=".jpg, .jpeg, .gif, .png" >
    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>

<div class="modal fade" id="{{ $id }}Modal" role="dialog">
    <div class="modal-dialog modal-xl">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Crop area</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <!-- leftbox -->
                        <div class="col-12">
                            <div id="{{ $id }}Result"></div>
                        </div>
                        <!--rightbox-->
                        <div class="box-2 hide">
                            <!-- result of crop -->
                            <img id="{{ $id }}Cropped" src="" alt="">
                        </div>
                        <!-- input file -->
                        <div class="box">
                            <div class="options hide">
                                <input type="hidden" id="{{ $id }}Img-w" value="200" min="200" max="1200" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="{{ $id }}Save" type="button" class="btn btn-primary hide">Download</button>
            </div>
        </div>
    </div>
</div>

@php ( $initialPreview = [] )
@php ( $initialPreviewConfig = [] )

@if(isset($value) && !empty($value) )
    @foreach($value as $key => $image)
        @if(!empty($image))
            @php ( $data = [
                'caption' => $image->name,
                'filename' => $image->getFullUrl(),
                'downloadUrl' => $image->getFullUrl(),
                'url' => route("admin.files.destroy", ['id' => $image->id, '_token' => csrf_token()]),
                'size' => $image->size,
                'width' => '120px',
                'key' => $image->id ] )
            @php ( $initialPreviewConfig[] = $data )
        @endif
    @endforeach
@endif
@php ( $initialPreview = array_column($initialPreviewConfig, 'downloadUrl') )

@push('scripts')
    <script>
        $(function () {
            $("#{{$id}}").fileinput({
                showRemove: false,
                showUpload: false,
                showCancel: false,
                @if((isset($item) && !empty($item)))
                    ajaxSettings: {headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}},
                    ajaxDeleteSettings: {headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}},
                    initialPreviewShowDelete: true,
                    uploadUrl: "{{ Arr::get($params, 'uploadUrl') ? Arr::get($params, 'uploadUrl') : '' }}",
                @endif
                required: {{ Arr::get($params, 'required', 'false') ? 'true' : 'false' }},
                validateInitialCount: true,
                overwriteInitial: false,
                initialPreviewAsData: true,
                maxFileSize: 5120, //2MB
                maxTotalFileCount: {{ Arr::get($params, 'multiple', 1) ? 20 : 1 }},
                theme: 'fas',
                initialPreview: {!! json_encode($initialPreview) !!},
                initialPreviewConfig: {!! json_encode($initialPreviewConfig) !!},
                allowedFileExtensions: ['jpg', 'jpeg', 'gif', 'png'],
                fileActionSettings: {
                    showRemove: false,
                },
                layoutTemplates: {
                },
            });
            $("#{{$id}}").on("filepredelete", function() {
                if (confirm("Are you sure you want to permanently delete this image?")) {
                    return false;
                }
                return true; // you can also send any data/object that you can receive on `filecustomerror` event
            });

            let input = $("#{{ $id }}");
            let result = document.querySelector("#{{ $id }}Result");
            let img_w = document.querySelector("#{{ $id }}Img-w");
            let cropper = "";
            let save = $("#{{ $id }}Save");

            input.on('change', function (e) {
                if (e.target.files.length) {
                    // start file reader
                    let reader = new FileReader();
                    reader.onload = function(e) {
                        if (e.target.result) {
                            $("#{{ $id }}Modal").modal("show");
                            // create new image
                            let img = document.createElement("img");
                            img.id = "{{ $id }}Image";
                            img.src = e.target.result;
                            // clean result before
                            result.innerHTML = "";
                            // append new image
                            result.appendChild(img);
                            // init cropper
                            cropper = new Cropper(img, {
                                // autoCropArea: 1,
                                minContainerWidth: 300,
                                minContainerHeight: 300,
                                // viewMode: 2,
                                aspectRatio:  1080 / 1600,
                            });
                        }
                    };
                    reader.readAsDataURL(e.target.files[0]);
                }
            });
            // save on click
            save.on('click', function (e) {
                e.preventDefault();
                // get result to data uri
                let imgSrc = cropper.getCroppedCanvas({
                    width: img_w.value // input value
                }).toDataURL();

                downloadBase64File(imgSrc, "cropped.jpg");
            });

            function downloadBase64File(base64Data, fileName) {
                const downloadLink = document.createElement("a");
                downloadLink.href = base64Data;
                downloadLink.download = fileName;
                downloadLink.click();
            }
        });
    </script>
@endpush

@push('styles')
<style>
    .cropper-container{
        margin: auto !important;
    }
</style>
@endpush
