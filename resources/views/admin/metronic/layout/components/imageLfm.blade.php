@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $id }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>

    <div class="input-group">
            <span class="input-group-btn">
                <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                    <i class="fa fa-picture-o"></i> Choose
                </a>
            </span>
        <input id="thumbnail" class="form-control" type="text" name="filepath">
    </div>
    <div id="holder" style="margin-top:15px;max-height:100px;"></div>
    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>

@php ( $initialPreview = [] )
@php ( $initialPreviewConfig = [] )

@if(isset($value) && !empty($value) )
    @foreach($value as $key => $image)
        @if(!empty($image))
            @php ( $data = [
                'caption' => $image->name,
                'filename' => $image->getFullUrl(),
                'downloadUrl' => $image->getFullUrl(),
                'url' => route("admin.files.destroy", ['id' => $image->id, '_token' => csrf_token()]),
                'size' => $image->size,
                'width' => '120px',
                'key' => $image->id ] )
            @php ( $initialPreviewConfig[] = $data )
        @endif
    @endforeach
@endif
@php ( $initialPreview = array_column($initialPreviewConfig, 'downloadUrl') )

@push('scripts')
    <script>
        $(function () {
            $("#{{$id}}").fileinput({
                showRemove: false,
                showUpload: false,
                showCancel: false,
                @if((isset($item) && !empty($item)))
                    ajaxSettings: {headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}},
                    ajaxDeleteSettings: {headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}},
                    initialPreviewShowDelete: true,
                    uploadUrl: "{{ Arr::get($params, 'uploadUrl') ? Arr::get($params, 'uploadUrl') : '' }}",
                @endif
                required: {{ Arr::get($params, 'required', 'false') ? 'true' : 'false' }},
                validateInitialCount: true,
                overwriteInitial: false,
                initialPreviewAsData: true,
                maxFileSize: 5120, //2MB
                maxTotalFileCount: {{ Arr::get($params, 'multiple', 1) ? 20 : 1 }},
                theme: 'fas',
                initialPreview: {!! json_encode($initialPreview) !!},
                initialPreviewConfig: {!! json_encode($initialPreviewConfig) !!},
                allowedFileExtensions: ['jpg', 'jpeg', 'gif', 'png'],
                fileActionSettings: {
                    showRemove: false,
                },
                layoutTemplates: {
                },
            });
            $("#{{$id}}").on("filepredelete", function() {
                if (confirm("Are you sure you want to permanently delete this image?")) {
                    return false;
                }
                return true; // you can also send any data/object that you can receive on `filecustomerror` event
            });
        });
    </script>
@endpush
