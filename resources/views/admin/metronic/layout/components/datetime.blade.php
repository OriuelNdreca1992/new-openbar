@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $id }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="far fa-clock"></i></span>
        </div>
        <input
            type="text"
            autocomplete="off"
            class="form-control datepicker float-right {{ $errors->has($name) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }}"
            id="{{ $id }}"
            name="{{ $name }}"
            value="{{ $value }}"
            {{ Arr::get($params, 'required', false) ? 'required' : '' }}>
    </div>
    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>

@push('scripts')
    @parent
    <script>
        $(function() {
            $('#{{$id}}').daterangepicker({
                timePicker: true,
                autoUpdateInput: false,
                singleDatePicker: true,
                showDropdowns: true,
                minYear: 1990,
                maxYear: parseInt(moment().format('YYYY')),
                locale: {
                    format: 'DD-MM-YYYY hh:mm A'
                }
            }, function(chosen_date) {
                $('#{{$id}}').val(chosen_date.format('DD-MM-YYYY hh:mm A'));
            });
        });
    </script>
@endpush
