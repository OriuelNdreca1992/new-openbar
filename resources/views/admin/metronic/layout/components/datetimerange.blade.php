<div class="row">
    @if(!isset($id_from)) @php( $id_from = Arr::get($params, 'id_from') ) @endif
    @if(!isset($name_from)) @php( $name_from = Arr::get($params, 'name_from') ) @endif
    @if(!isset($value_from)) @php( $value_from = Arr::get($params, 'value_from') ) @endif
    <div class="col-md-6">
        <div class="form-group {{ $errors->has($name_from) ? 'has-error' : '' }}">
            <label for="{{ $id_from }}">
                {{ Arr::get($params, 'label_from') }} {!! Arr::get($params, 'required_from') ? '<span class="text-danger">*</span>' : '' !!}
            </label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar"></i></span>
                </div>
                <input
                    type="text"
                    autocomplete="off"
                    class="form-control float-right {{ $errors->has($name_from) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }}"
                    id="{{ $id_from }}"
                    name="{{ $name_from }}"
                    value="{{ $value_from }}"
                    {{ Arr::get($params, 'required_from', false) ? 'required' : '' }}>
            </div>
            @if ($errors->has($name_from))
                <span class="error invalid-feedback">{{ $errors->first($name_from) }}</span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        @if(!isset($id_to)) @php( $id_to = Arr::get($params, 'id_to') ) @endif
        @if(!isset($name_to)) @php( $name_to = Arr::get($params, 'name_to') ) @endif
        @if(!isset($value_to)) @php( $value_to = Arr::get($params, 'value_to') ) @endif

        <div class="form-group {{ $errors->has($name_to) ? 'has-error' : '' }}">
            <label for="{{ $id_to }}">
                {{ Arr::get($params, 'label_to') }} {!! Arr::get($params, 'required_to') ? '<span class="text-danger">*</span>' : '' !!}
            </label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar"></i></span>
                </div>
                <input
                    type="text"
                    autocomplete="off"
                    class="form-control float-right {{ $errors->has($name_to) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }}"
                    id="{{ $id_to }}"
                    name="{{ $name_to }}"
                    value="{{ $value_to }}"
                    {{ Arr::get($params, 'required_to', false) ? 'required' : '' }}>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        $(function () {
            if($('#{{$id_from}}, #{{$id_to}}').length) {
                // check if element is available to bind ITS ONLY ON HOMEPAGE
                let currentDate = moment().format("DD.MM.YYYY hh:mm A");

                $('#{{$id_from}}, #{{$id_to}}').daterangepicker({
                        timePicker: true,
                        locale: {
                        format: 'DD.MM.YYYY hh:mm A'
                    },
                    "alwaysShowCalendars": true,
                    "minDate": currentDate,
                    autoApply: true,
                    autoUpdateInput: false
                }, function(start, end, label) {
                    // Lets update the fields manually this event fires on selection of range
                    let selectedStartDate = start.format('DD.MM.YYYY hh:mm A'); // selected start
                    let selectedEndDate = end.format('DD.MM.YYYY hh:mm A'); // selected end

                    let checkinInput = $('#{{$id_from}}');
                    let checkoutInput = $('#{{$id_to}}');

                    // Setting the Selection of dates on calender on CHECKOUT FIELD (To get this it must be binded by Ids not Calss)
                    let checkOutPicker = checkoutInput.data('daterangepicker');
                    checkOutPicker.setStartDate(selectedStartDate);
                    checkOutPicker.setEndDate(selectedEndDate);

                    // Setting the Selection of dates on calender on CHECKIN FIELD (To get this it must be binded by Ids not Calss)
                    let checkInPicker = checkinInput.data('daterangepicker');
                    checkInPicker.setStartDate(selectedStartDate);
                    checkInPicker.setEndDate(selectedEndDate);

                    // Updating Fields with selected dates
                    checkinInput.attr('value', selectedStartDate);
                    checkoutInput.attr('value', selectedEndDate);
                });
            } // End DateRange Picker
        });
    </script>
@endpush
