@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $id }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <input
        type="file"
        id="{{ $id }}"
        name="{{ $name }}"
        class="{{ $errors->has($name) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }}"
        {{ Arr::get($params, 'multiple', false) ? 'multiple' : '' }}
        accept=".docx, .xlsx, .pptx, .jpg, .jpeg, .png, .pdf, .zip, .rar" >
    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>

@php ( $initialPreview = [] )
@php ( $initialPreviewConfig = [] )

@if(isset($value) && !empty($value) )
    @foreach($value as $key => $file)
        @php ( $data = [
            'type' => Storage::disk('storage')->mimeType($file->path),
            'caption' => $file->name,
            'filename' => Storage::disk('storage')->url($file->path),
            'downloadUrl' => Storage::disk('storage')->url($file->path),
            'url' => route("admin.files.destroy", $file->id),
            'size' => Storage::disk('storage')->size($file->path),
            'width' => '120px',
            'key' => $file->id ] )
        @php ( $initialPreviewConfig[] = $data )
    @endforeach
@endif
@php ( $initialPreview = array_column($initialPreviewConfig, 'downloadUrl') )

@push('scripts')
    <script>
        $(function () {
            $("#{{$id}}").fileinput({
                showRemove: false,
                showUpload: false,
                showCancel: false,
                @if((isset($item) && !empty($item)))
                    ajaxSettings: {headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}},
                    ajaxDeleteSettings: {headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}},
                    initialPreviewShowDelete: true,
                    uploadUrl: "{{ Arr::get($params, 'uploadUrl') ? Arr::get($params, 'uploadUrl') : '' }}",
                @endif
                required: {{ Arr::get($params, 'required', 'false') ? 'true' : 'false' }},
                validateInitialCount: true,
                overwriteInitial: false,
                initialPreviewAsData: true,
                maxFileSize: 2024, //2MB
                maxTotalFileCount: {{ Arr::get($params, 'multiple', 1) ? 20 : 1 }},
                theme: 'fas',
                initialPreview: {!! json_encode($initialPreview) !!},
                initialPreviewConfig: {!! json_encode($initialPreviewConfig) !!},
                allowedFileExtensions: ['docx', 'xlsx', 'pptx', 'jpg', 'jpeg', 'png', 'pdf', 'zip', 'rar'],
                fileActionSettings: {
                    showRemove: false,
                },
                layoutTemplates: {
                },
                uploadAsync: true,
                previewFileIcon: '<i class="fas fa-file"></i>',
                allowedPreviewTypes: null, // set to empty, null or false to disable preview for all types
                previewFileIconSettings: {
                    'docx': '<i class="fas fa-file-word text-primary"></i>',
                    'xlsx': '<i class="fas fa-file-excel text-success"></i>',
                    'pptx': '<i class="fas fa-file-powerpoint text-danger"></i>',
                    'jpg': '<i class="fas fa-file-image text-warning"></i>',
                    'jpeg': '<i class="fas fa-file-image text-warning"></i>',
                    'png': '<i class="fas fa-file-image text-warning"></i>',
                    'pdf': '<i class="fas fa-file-pdf text-danger"></i>',
                    'zip': '<i class="fas fa-file-archive text-muted"></i>',
                    'rar': '<i class="fas fa-file-archive text-muted"></i>',
                }
            });
            $("#{{$id}}").on("filepredelete", function() {
                if (confirm("Are you sure you want to permanently delete this image?")) {
                    return false;
                }
                return true; // you can also send any data/object that you can receive on `filecustomerror` event
            });
        });
    </script>
@endpush
