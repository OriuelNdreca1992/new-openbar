@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($values)) @php( $values = Arr::get($params, 'values') ) @endif

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <select id="{{ $id }}" name="{{ $name }}"
        class="form-control selectPicker {{ $errors->has($name) ? 'is-invalid' : '' }}"
        data-live-search="true"
        title="{{ config('constants.select_one') }}"
        {{ Arr::get($params, 'multiple', false) ? 'multiple' : '' }}
        {{ Arr::get($params, 'required', false) ? 'required' : '' }}
    >
        @foreach (Arr::get($params, 'data') as $key => $option)
            @if(is_array($values))
                <option {!! Arr::get($params, 'multiple', false) ? 'data-content="<span class=\'label label-light-dark label-inline\'>'.$option.'</span>"' : '' !!}
                    value="{{$key}}" {{ (in_array($key, $values, TRUE) || old($name) == $key) ? 'selected' : '' }}>{{ $option }}</option>
            @else
                <option value="{{$key}}" {{ ($key == $values || old($name) == $key) ? 'selected' : '' }}>{{ $option }}</option>
            @endif
        @endforeach
    </select>
    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>
