<div class="card"> {{--<div class="card card-primary card-outline card-outline-tabs">--}}
    <div class="card-header d-flex p-0"> {{--<div class="card-header p-0 border-bottom-0">--}}
        <ul class="nav nav-pills p-2" role="tablist">  {{--<ul class="nav nav-tabs">--}}
            @foreach(config()->get('translatable.locales') as $local)
                @php( $id = str_replace('LOCAL', $local, Arr::get($params, 'id') ) )
                @php( ($loop->first) ? $active = ' active' : $active = '' )
                <li class="nav-item">
                    <a class="nav-link{{$active}} clonable-increment-href"
                       data-toggle="tab"
                       role="tab"
                       href="#tabs_{{$id}}">{{ strtoupper($local) }}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content">
            @foreach(config()->get('translatable.locales') as $local)
                @php( $id = str_replace('LOCAL', $local, Arr::get($params, 'id') ) )
                @php( ($loop->first) ? $active = ' show active' : $active = '' )
                <div id="tabs_{{$id}}"
                     role="tabpanel"
                     class="tab-pane fade{{$active}} clonable-increment-id">
                    @include('admin.adminlte.components.textarea', [
                        'id' => str_replace('LOCAL', $local, Arr::get($params, 'id')),
                        'name' => str_replace('LOCAL', $local, Arr::get($params, 'name')),
                        'value' => (isset($item) && !empty($item) && $item->translate($local)) ?
                            $item->translate($local)->{Arr::get($params, 'field')} : ''
                    ])
                </div>
            @endforeach
        </div>
    </div>
</div>
