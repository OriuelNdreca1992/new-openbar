@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif

@php($id = str_replace(']', '', str_replace('[', '_', $name)) )

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label class="clonable-increment-for" for="{{ $id }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <div>
        <span class="switch switch-sm switch-icon">
            <label>
                <input type="checkbox"
                    id="{{ $id }}"
                    name="{{ $name }}"
                    class="bootstrap-switch {{ $errors->has($name) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }}"
                    @if($value) checked @endif />
                <span></span>
            </label>
        </span>
    </div>
    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>

