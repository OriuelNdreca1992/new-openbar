@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <textarea
        id="{{ $id }}"
        name="{{ $name }}"
        class="form-control form-control-solid {{ $errors->has($name) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }}"
        placeholder="{{ Arr::get($params, 'label') }}"
        rows="5"
        @if(Arr::get($params, 'options'))
            @foreach (Arr::get($params, 'options') as $key => $option)
                {{$key}} = "{{ $option }}"
            @endforeach
        @endif
        onkeyup="this.innerHTML = this.value;"
        {{ Arr::get($params, 'required', false) ? 'required' : '' }}>{{ $value }}</textarea>

    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>
