@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif

<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
    <label for="{{ $name }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <div class="row">
        @foreach($items as $key => $element)
            <div class="col-md-4 mb-3">
                <label class="checkbox checkbox-rounded" for="{{ $key }}">
                    <input
                        type="checkbox"
                        id="{{ $key }}"
                        name="{{ $name }}"
                        value="{{ $element }}"
                        @if( isset($value) && in_array($element, $value) ) checked @endif >
                    <span class="mr-2"></span> {{ $element }}
                </label>
            </div>
        @endforeach
    </div>
    @if ($errors->has($name))
        <span class="error invalid-feedback">{{ $errors->first($name) }}</span>
    @endif
</div>

