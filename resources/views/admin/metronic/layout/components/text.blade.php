@if(!isset($id)) @php( $id = Arr::get($params, 'id') ) @endif
@if(!isset($name)) @php( $name = Arr::get($params, 'name') ) @endif
@if(!isset($value)) @php( $value = Arr::get($params, 'value') ) @endif
@if(!isset($field)) @php( $field = Arr::get($params, 'field') ) @endif

<div class="form-group {{ $errors->has($field) ? 'has-error' : '' }}">
    <label class="for" for="{{ $name }}">
        {{ Arr::get($params, 'label') }} {!! Arr::get($params, 'required') ? '<span class="text-danger">*</span>' : '' !!}
    </label>
    <input
        type="{{ Arr::get($params, 'type', 'text') }}"
        name="{{ $name }}"
        id="{{ $id }}"
        class="form-control form-control form-control-solid {{ $errors->has($field) ? 'is-invalid' : '' }} {{ Arr::get($params, 'class') }} "
        value="{{ $value }}"
        placeholder="{{ Arr::get($params, 'label') }}"
        @if(Arr::get($params, 'options'))
            @foreach (Arr::get($params, 'options') as $key => $option)
                {{$key}} = "{{ $option }}"
            @endforeach
        @endif
        onkeyup="this.setAttribute('value', this.value);"
        {{ Arr::get($params, 'required', false) ? 'required' : '' }}
        {{ Arr::get($params, 'readonly', false) ? 'readonly' : '' }}
        {{ Arr::get($params, 'disabled', false) ? 'disabled' : '' }}
    >

    @if(Arr::get($params, 'description'))
        <small class="text-muted"><i>{{ Arr::get($params, 'description') }}</i></small>
    @endif

    @error($field)
        <span class="error invalid-feedback">{{ $message }}</span>
    @enderror
</div>
