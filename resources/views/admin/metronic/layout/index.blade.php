@extends('admin.metronic.layout.default')

@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap py-5">
            @if (defined('ITEMS'))
                <div class="card-title">
                    <h3 class="card-label">{{ ITEMS }}</h3>
                </div>
            @endif
            <div class="card-toolbar">
                @if (defined('LANGUAGE') && LANGUAGE)
                    <div class="ml-3">
                        @include('admin.metronic.layout.partials.common.item.language')
                    </div>
                @endif
                @if (defined('CREATE'))
                    <div class="ml-3">
                        <!--begin::Button-->
                        <a href="{{ route(CREATE['route'], CREATE['parameters']) }}" class="btn btn-primary font-weight-bolder">
                        <span class="svg-icon svg-icon-md">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <circle fill="#000000" cx="9" cy="15" r="6"/>
                                    <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3"/>
                                </g>
                            </svg>
                        </span>New {{ ITEM }}</a>
                        <!--end::Button-->
                    </div>
                @endif
            </div>
        </div>

        <div class="card-body">
            <table id="datatable" class="table table-separate table-head-custom table-checkable">
                <thead>@yield('thead')</thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
@endsection

@push('styles')
    {!! Html::style('plugins/custom/datatables/datatables.bundle.css') !!}
@endpush

@push('scripts')
    {!! Html::script('js/pages/crud/datatables/basic/basic.js') !!}

    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            let table = $('#datatable').DataTable({
                "language": {
                    searchPlaceholder: "Search...",
                    search: '<span><i class="flaticon2-search-1 text-muted"></i></span>',
                },
                processing: true,
                serverSide: true,
                order: [[ 1, "DESC" ]],
                ajax: "{{ route(INDEX['route'], INDEX['parameters']) }}",
                columns: {!! json_encode($columns) !!}
            });

            $('body').on('click', '.deleteItem', function () {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Do you want to delete this item?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    confirmButtonColor: '#dc3545',
                }).then(function(result) {
                    if (result.value) {
                        $.ajax({
                            url: "{{ route(INDEX['route'], INDEX['parameters']) }}" + "/" + id,
                            method: 'DELETE',
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "id": id
                            },
                            error: function (data) {
                                console.log('Error:', data);
                            },
                            complete: function (data) {
                                if (data.responseJSON.status !== 'undefined') {
                                    switch (data.responseJSON.status) {
                                        case 'success':
                                            toastr.success(data.responseJSON.message, 'Success');
                                            break;
                                        case 'warning':
                                            toastr.warning(data.responseJSON.message, 'Warning');
                                            break;
                                        default:
                                            toastr.error(data.responseJSON.message, 'Error');
                                    }
                                }
                                table.draw();
                            },
                        });
                    }
                });
            });
        })
    </script>
@endpush
