@extends('admin.metronic.layout.default')

@section('content')
    <div class="row pb-3">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            @yield('card_header')
                        </div>
                        <div class="card-body">
                            @yield('card_body')
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-12">
                                    <div class="float-right">
                                        @if(defined('INDEX'))
                                            <a href="{{ route(INDEX['route'], INDEX['parameters'] ) }}"
                                               class="btn btn-secondary">Back</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
