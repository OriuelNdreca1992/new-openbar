@php
    const ITEM = 'Review';
    const ITEMS = 'Reviews';

    if(isset($venue)):
        define("INDEX", ['route' => 'admin.venues.reviews.index', 'parameters' => ['venue' => $venue->id] ]);
    endif;
@endphp
