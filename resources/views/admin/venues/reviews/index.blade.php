@include('admin.venues.reviews.config')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard',    'url' => route('dashboard')],
        ['title' => 'Venues',       'url' => route('admin.venues.index')],
        ['title' => $venue->name,   'url' => route('admin.venues.show', request()->route('venue'))],
        ['title' => ITEMS]
    ]
])

@push('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
@endpush

@extends('admin.metronic.layout.index', [
    'columns' => [
        ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
        ['data' => 'user.first_name', 'name' => 'user.first_name'],
		['data' => 'user.last_name', 'name' => 'user.last_name'],
        ['data' => 'review', 'name' => 'review', 'searchable' => false],
        ['data' => 'created_at', 'name' => 'created_at'],
    ]
])

@section('thead')
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Review</th>
        <th>Created</th>
    </tr>
@stop
