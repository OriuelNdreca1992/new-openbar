<div class="modal-dialog modal-dialog-scrollable modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">{{ $modalTitle }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i aria-hidden="true" class="ki ki-close"></i>
            </button>
        </div>
        <div class="modal-body">
            <div data-height="100">
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 text-right col-form-label">Name</label>
                    <div class="col-lg-9 col-xl-6">
                        <input
                            class="form-control form-control-solid form-control-lg"
                            id="name"
                            name="name"
                            type="text"
                            required
                        />
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary font-weight-bold">{{ $modalAction }}</button>
        </div>
    </div>
</div>
