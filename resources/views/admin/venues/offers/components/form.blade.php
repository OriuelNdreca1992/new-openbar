<div class="row">
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>Select Default Product</label>
            <select
                class="form-control selectpicker"
                name="product_id"
                data-size="7"
                data-live-search="true"
            >
                <option>Select Product</option>
                @foreach ($products as $product)
                    <option
                        value="{{ $product->id }}" {{ $product->id == $offer->product_id ? 'selected' : '' }}
                        data-content='<span><div class="symbol symbol-250"><img src="{{ $product->image_url }}"></div><span style="margin-left:20px;">{{ $product->name }}</span></span>'
                    >
                     </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>
                Price per 2
                <span class="text-danger">*</span>
            </label>
            <input
                type="text"
                name="price"
                class="form-control form-control-solid"
                placeholder="Price"
                value="{{ old('price', $offer->price) }}"
            />
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>
                Quantity
                <span class="text-danger">*</span>
            </label>
            <input
                type="number"
                name="quantity"
                class="form-control form-control-solid"
                placeholder="Quantity"
                value="{{ old('quantity', $offer->quantity) }}"
            />
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-6 col-xl-6">
        <label class="col-form-label col-lg-12 col-sm-12">Date Range</label>
        <div class="col-lg-12 col-xl-12">
            <div class="input-daterange input-group" id="kt_datepicker_5">
                <input
                    id="start"
                    type="text"
                    class="form-control form-control-solid"
                    name="start"
                    value="{{ $offer->start ? $offer->start : old('start') }}"
                />
                <div class="input-group-append">
                    <span class="input-group-text">
                        <i class="la la-ellipsis-h"></i>
                    </span>
                </div>
                <input
                    id="end"
                    type="text"
                    class="form-control form-control-solid"
                    name="end"
                    value="{{ $offer->end ? $offer->end : old('end') }}"
                />
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 mt-4">
            <div class="form-group">
                <label class="checkbox mb-0">
                    <input
                        id="all_the_time"
                        type="checkbox"
                        name="all_the_time"
                        {{ old('all_the_time') || $offer->all_the_time == 1 ? 'checked' : '' }}
                        onclick="allTheTime()"
                    />
                    <span></span>
                    <div class="ml-2">Available all the time</div>
                </label>
            </div>
        </div>
    </div>
  <div class="form-group col-lg-6 col-xl-6">
    <label class="col-form-label col-lg-3 col-sm-12">Days</label>
    <div class="col-lg-12 col-md-12 col-sm-12">
        <select
            class="form-control select2"
            id="days_select"
            multiple="multiple"
            name="days[]"
        >
            @foreach (App\Models\Offer::DAYS as $key => $day)
                @if(in_array($key, $offer->offerDays->pluck('week_day')->toArray()))
                    <option value="{{ $key }}" selected="selected">{{ $day }}</option>
                @else
                    <option value="{{ $key }}">{{ $day }}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label class="col-3 col-form-label">Status</label>
            <div class="col-3">
                <span class="switch switch-icon">
                    <label>
                        <input type="checkbox" name="status" {{ old('status') || $offer->status == 1 ? 'checked' : '' }} />
                        <span></span>
                    </label>
                </span>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-xl-8">
        <label class="col-3 col-form-label">Description</label>
        <div class="form-group">
            <textarea
                class="form-control border-0 form-control-solid"
                rows="4"
                name="description"
                placeholder="Description">{{ old('description', $offer->description) }}</textarea>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        let allTheTimeCheckBox = document.getElementById("all_the_time");
        let start = document.getElementById("start");
        let end = document.getElementById("end");
        function allTheTime() {
            if (allTheTimeCheckBox.checked) {
                start.value = null;
                start.disabled = true;
                end.value = null;
                end.disabled = true;
            } else {
                start.disabled = false;
                end.disabled = false;
            }
        }
        allTheTime();
    </script>
@endpush
