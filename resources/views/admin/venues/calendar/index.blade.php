@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Venues'],
        ['title' => $venue->name, 'url' => route('admin.venues.show', request()->route('venue'))],
        ['title' => 'Calendar',]
    ]
])

@section('content')
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <h3 class="card-label">Calendar</h3>
            </div>
            <div class="card-toolbar">
                <a href="{{ route('admin.venue.offers.create', request()->route('venue')) }}" class="btn btn-light-primary font-weight-bold">
                <i class="ki ki-plus icon-md mr-2"></i>Add Offer</a>
            </div>
        </div>
        <div class="card-body">
            <div id="kt_calendar"></div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        var offersCalendar = {!! json_encode(route('admin.venue.calendar.offers', $venue->id)) !!}
    </script>
    <script src="{{ asset('js/calendar/offers-calendar.js') }}"></script>
@endpush
