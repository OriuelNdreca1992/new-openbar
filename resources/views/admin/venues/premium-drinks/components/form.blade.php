<div class="row">
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>Select Default Product</label>
            <select
                class="form-control selectpicker"
                name="product_id"
                data-size="7"
                data-live-search="true"
            >
                <option>Select Product</option>
                @foreach ($defaultProducts as $product)
                    <option
                        value="{{ $product->id }}" {{ $product->id == $drink->product_id ? 'selected' : '' }}
                        data-content='<span><div class="symbol symbol-250"><img src="{{ $product->image_url }}"></div><span style="margin-left:20px;">{{ $product->name }}</span></span>'
                    >
                     </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>
                Price
                <span class="text-danger">*</span>
            </label>
            <input
                type="text"
                name="price"
                class="form-control form-control-solid"
                placeholder="Price"
                value="{{ old('price', $drink->price) }}"
            />
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label>
                Promotional Price
                <span class="text-danger"></span>
            </label>
            <input
                type="text"
                name="promotional_price"
                class="form-control form-control-solid"
                placeholder="Promotional Price"
                value="{{ old('promotional_price', $drink->promotional_price) }}"
            />
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-8 col-xl-8">
        <label class="col-3 col-form-label">Description</label>
        <div class="form-group">
            <textarea
                class="form-control border-0 form-control-solid"
                rows="4"
                name="description"
                placeholder="Description">{{ old('description', $drink->description) }}</textarea>
        </div>
    </div>
    <div class="col-lg-4 col-xl-4">
        <div class="form-group">
            <label class="col-3 col-form-label">Status</label>
            <div class="col-3">
                <span class="switch switch-icon">
                    <label>
                        <input type="checkbox" name="status" {{ old('status') || $drink->status == 1 ? 'checked' : '' }} />
                        <span></span>
                    </label>
                </span>
            </div>
        </div>
    </div>
</div>
