@extends('layouts.app')
@push('styles')
    <style>
        .nav-item {
            width:14%;
        }
    </style>
@endpush

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Venues', 'url' => route('admin.venues.index')],
        ['title' => 'Create']
    ]
])

@section('content')
    <form
        class="form"
        id="validate_venue_request"
        action="{{ route('admin.venues.store') }}"
        method="POST"
        enctype="multipart/form-data"
    >
    @csrf
        <!--begin::Content-->
        <div class="col-lg-12">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Create Venue</h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Form-->
                <div class="card-body">
                    <div class="mb-8">
                       <h5 class="card-label text-dark">User Information</h5>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    First Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="first_name"
                                    class="form-control form-control-solid"
                                    placeholder="First Name"
                                    value="{{ old('first_name') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Last Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="last_name"
                                    class="form-control form-control-solid"
                                    placeholder="Last Name"
                                    value="{{ old('last_name') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Phone
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="phone"
                                    class="form-control form-control-solid"
                                    placeholder="Phone"
                                    value="{{ old('phone') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Email
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="email"
                                    name="email"
                                    class="form-control form-control-solid"
                                    placeholder="Email"
                                    value="{{ old('email') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Password
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="password"
                                    name="password"
                                    class="form-control form-control-solid"
                                    placeholder="Password"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Password Confirmation
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="password"
                                    name="password_confirmation"
                                    class="form-control form-control-solid"
                                    placeholder="Password Confirmation"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="mb-8 mt-8">
                       <h5 class="card-label text-dark">Venue Information</h5>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4 text-center offset-2">
                            <div class="form-group row">
                                <div class="col-lg-12 col-xl-12">
                                    <div class="image-input image-input-outline" id="venue_event_image_url">
                                        <div class="image-input-wrapper" style="background-image: url({{ asset('assets/media/companies/default_logo.jpg') }})"></div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Upload Image">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input
                                                type="file"
                                                name="image_event_url"
                                                accept=".png, .jpg, .jpeg"
                                                value="{{ old('image_event_url') }}"
                                            />
                                        </label>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                    <span class="form-text">Upload Image 1080x1300</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-xl-3 text-center">
                            <div class="form-group row">
                                <div class="col-lg-12 col-xl-12">
                                    <div class="image-input image-input-outline" id="venue_publish_image_url">
                                        <div class="image-input-wrapper" style="background-image: url({{ asset('assets/media/companies/default_logo.jpg') }})"></div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Upload Image">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="image_publish_url" accept=".png, .jpg, .jpeg" />
                                        </label>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                    <span class="form-text">Upload Image 1000x750</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 offset-2">
                        <input type="hidden" id="lat" name="lat" value="{{ old('lat') }}">
                        <input type="hidden" id="lng" name="lng" value="{{ old('lng') }}">
                        <div class="form-group">
                            <label>
                                Cerca il tuo locale con Google
                                <span class="text-danger">*</span>
                            </label>
                            <input
                                type="text"
                                id="pac-input"
                                class="form-control form-control-solid"
                                name="address"
                                id="address"
                                placeholder="Cerca il tuo locale con Google"
                                value="{{ old('address') }}"
                            />
                        </div>
                    </div>
                    <div class="col-lg-8 offset-2">
                        <div class="card card-custom gutter-b">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3 class="card-label">Venue Location</h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="map" style="height:300px;width:100%;"></div>
                                <div id="infowindow-content">
                                    <span id="place-address"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Venue Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    class="form-control form-control-solid"
                                    placeholder="Venue Name"
                                    value="{{ old('name') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    City
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="city"
                                    id="city"
                                    class="form-control form-control-solid"
                                    placeholder="City"
                                    value="{{ old('city') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Postal Code
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="postal_code"
                                    id="postal_code"
                                    class="form-control form-control-solid"
                                    placeholder="Postal Code"
                                    value="{{ old('postal_code') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Stato/Regione
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="state"
                                    id="state"
                                    class="form-control form-control-solid"
                                    placeholder="Stato/Regione"
                                    value="{{ old('state') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Country
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="country"
                                    id="country"
                                    class="form-control form-control-solid"
                                    placeholder="Country"
                                    value="{{ old('country') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Phone Number
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="phone_number"
                                    id="phone_number"
                                    class="form-control form-control-solid"
                                    placeholder="Phone Number"
                                    value="{{ old('phone_number') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>Seats Number</label>
                                <select
                                    class="form-control form-control-solid"
                                    id="seats_number"
                                    name="seats_number"
                                >
                                    @for ($i = 1; $i <= 100; $i++)
                                        <option value="{{ $i }}" {{ (old("seats_number") == $i ? "selected" : "") }}>
                                            {{ $i }}
                                        </option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>Types</label>
                                <select
                                    style="width: 100%"
                                    class="form-control form-control-solid"
                                    id="kt_select2_11"
                                    multiple="multiple"
                                    name="types[]"
                                    id="types"
                                >
                                    @foreach ($types as $type)
                                        <option
                                            value="{{ $type->id }}" {{ (old("types") !== null && in_array($type->id, old("types")) ? "selected": "") }}>
                                            {{ $type->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>Status</label>
                                <span class="switch switch-icon">
                                    <label>
                                        <input type="checkbox" name="status" {{ old('status') ? 'checked' : '' }} />
                                        <span></span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <label>Description</label>
                            <div class="form-group">
                                <textarea
                                    class="form-control border-0 form-control-solid"
                                    rows="4"
                                    name="description"
                                    placeholder="Description">{{ old('description') }}
                                </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="mb-8 mt-8">
                       <h5 class="card-label text-dark">Fiscal Data</h5>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_name"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Name"
                                    value="{{ old('fiscal_name') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Email
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="venue_email"
                                    class="form-control form-control-solid"
                                    placeholder="Email"
                                    value="{{ old('venue_email') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Address
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_address"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Address"
                                    value="{{ old('fiscal_address') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal City
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_city"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal City"
                                    value="{{ old('fiscal_city') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Country
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_country"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Country"
                                    value="{{ old('fiscal_country') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Prov
                                    <span class="text-danger"></span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_prov"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Prov"
                                    value="{{ old('fiscal_prov') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Cap
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_cap"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Cap"
                                    value="{{ old('fiscal_cap') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal VAT
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_vat"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal VAT"
                                    value="{{ old('fiscal_vat') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Code
                                    <span class="text-danger"></span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_code"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Code"
                                    value="{{ old('fiscal_code') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    SDI Code
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="sdi_code"
                                    class="form-control form-control-solid"
                                    placeholder="SDI Code"
                                    value="{{ old('sdi_code') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    PEC
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="certified_email"
                                    class="form-control form-control-solid"
                                    placeholder="PEC"
                                    value="{{ old('certified_email') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    BIC/SWIFT
                                    <span class="text-danger"></span>
                                </label>
                                <input
                                    type="text"
                                    name="bic"
                                    class="form-control form-control-solid"
                                    placeholder="BIC/SWIFT"
                                    value="{{ old('bic') }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    IBAN
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="iban"
                                    class="form-control form-control-solid"
                                    placeholder="IBAN"
                                    value="{{ old('iban') }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="mb-8 mt-8">
                       <h5 class="card-label text-dark">Opening Hours</h5>
                    </div>
                    <div class="card card-custom gutter-b">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="monday-tab" data-toggle="tab" href="#monday">
                                    <span class="nav-text">Monday</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="tuesday-tab" data-toggle="tab" href="#tuesday" aria-controls="tuesday">
                                    <span class="nav-text">Tuesday</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="wednesday-tab" data-toggle="tab" href="#wednesday" aria-controls="wednesday">
                                    <span class="nav-text">Wednesday</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="thursday-tab" data-toggle="tab" href="#thursday" aria-controls="thursday">
                                    <span class="nav-text">Thursday</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="friday-tab" data-toggle="tab" href="#friday" aria-controls="friday">
                                    <span class="nav-text">Friday</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="saturday-tab" data-toggle="tab" href="#saturday" aria-controls="saturday">
                                    <span class="nav-text">Saturday</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="sunday-tab" data-toggle="tab" href="#sunday" aria-controls="sunday">
                                    <span class="nav-text">Sunday</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content mt-5" id="myTabContent">
                            <div class="tab-pane fade show active" id="monday" role="tabpanel" aria-labelledby="monday-tab">
                                <div class="form-group row">
                                    <label class="col-form-label text-right col-lg-1">From</label>
                                    <div class="col-lg-3 col-md-3">
                                        <input
                                            class="form-control form-control-solid"
                                            name="opening_hours[1][start_hour]"
                                            id="kt_timepicker_1_modal"
                                            placeholder="Select time"
                                            type="text"
                                        />
                                    </div>
                                    <label class="col-form-label text-right col-lg-1 offset-2">To</label>
                                    <div class="col-lg-3 col-md-3">
                                        <input
                                            class="form-control form-control-solid"
                                            name="opening_hours[1][end_hour]"
                                            id="kt_timepicker_1_modal"
                                            placeholder="Select time"
                                            type="text"
                                        />
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tuesday" role="tabpanel" aria-labelledby="tuesday-tab">
                                <div class="tab-pane fade show active" id="monday" role="tabpanel" aria-labelledby="monday-tab">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-1">From</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[2][start_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                        <label class="col-form-label text-right col-lg-1 offset-2">To</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[2][end_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="wednesday" role="tabpanel" aria-labelledby="wednesday-tab">
                                <div class="tab-pane fade show active" id="monday" role="tabpanel" aria-labelledby="monday-tab">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-1">From</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[3][start_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                        <label class="col-form-label text-right col-lg-1 offset-2">To</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[3][end_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="thursday" role="tabpanel" aria-labelledby="thursday-tab">
                                <div class="tab-pane fade show active" id="monday" role="tabpanel" aria-labelledby="monday-tab">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-1">From</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[4][start_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                        <label class="col-form-label text-right col-lg-1 offset-2">To</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[4][end_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="friday" role="tabpanel" aria-labelledby="friday-tab">
                                <div class="tab-pane fade show active" id="friday" role="tabpanel" aria-labelledby="monday-tab">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-1">From</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[5][start_hour]"
                                                id="kt_timepicker_1_modal"
                                                readonly="readonly"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                        <label class="col-form-label text-right col-lg-1 offset-2">To</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[5][end_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="saturday" role="tabpanel" aria-labelledby="saturday-tab">
                                <div class="tab-pane fade show active" id="monday" role="tabpanel" aria-labelledby="monday-tab">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-1">From</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[6][start_hour]"
                                                id="kt_timepicker_1_modal"
                                                readonly="readonly"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                        <label class="col-form-label text-right col-lg-1 offset-2">To</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[6][end_hour]"
                                                id="kt_timepicker_1_modal"
                                                readonly="readonly"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="sunday" role="tabpanel" aria-labelledby="sunday-tab">
                                <div class="tab-pane fade show active" id="monday" role="tabpanel" aria-labelledby="monday-tab">
                                    <div class="form-group row">
                                        <label class="col-form-label text-right col-lg-1">From</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[7][start_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                        <label class="col-form-label text-right col-lg-1 offset-2">To</label>
                                        <div class="col-lg-3 col-md-3">
                                            <input
                                                class="form-control form-control-solid"
                                                name="opening_hours[7][end_hour]"
                                                id="kt_timepicker_1_modal"
                                                placeholder="Select time"
                                                type="text"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold" id="btn_upload">Create</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </form>
@endsection

@push('scripts')
    <script>
        var oldLat = {!! json_encode(old('lat')) !!};
        var oldLng = {!! json_encode(old('lng')) !!};
        var oldAddress = {!! json_encode(old('address')) !!};
    </script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.js') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/file-upload/image-input.js') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/bootstrap-timepicker.js') }}"></script>

    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\VenueRequest', '#validate_venue_request'); !!}

    <script src="{{ asset('js/map/map.js') }}"></script>
@endpush




