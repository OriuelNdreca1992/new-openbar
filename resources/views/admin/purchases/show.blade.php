@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Purchases']
    ]
])

@section('content')
    <div class="card card-custom">
        <div class="card-header flex-wrap py-5">
            <div class="card-title">
                <h3 class="card-label">Purchases</h3>
            </div>
        </div>
        <div class="card-body">
            <table class="table table-separate table-head-custom table-foot-custom table-checkable" id="purchase">
                <thead>
                    <tr>
                        <th>Purchase ID</th>
                        <th>Buyer</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Status</th>
                        <th>Total Amount</th>
                        <th>User Fee</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $cart->id }}</td>
                        <td>{{ $cart->user?->getFullNameWithID() }}</td>
                        <td>{{ $cart->transaction_type }}</td>
                        <td>{{ $cart->transaction_id }}</td>
                        <td>{{ $cart->payment_intent_id }}</td>
                        <td>
                            @php
                                $status = match ($cart->status) {
                                    'failed'    => ['status' => 'Failed', 'style' => 'danger'],
                                    'pending'   => ['status' => 'Pending', 'style' => 'default'],
                                    'succeeded' => ['status' => 'Succeeded', 'style' => 'success'],
                                    default     => ['status' => 'undefined', 'style' => 'warning'],
                                };
                            @endphp
                            <span class="font-weight-bold text-{{ $status['style'] }}">{{ $status['status'] }}</span>
                        </td>
                        <td>{{ number_format($cart->amount + $cart->user_fee, 2, '.', ',') }}</td>
                        <td>{{ number_format($cart->user_fee, 2, '.', ',') }}</td>
                        <td>{{ $cart->created_at?->format('Y-m-d H:i') }}</td>
                        <td>
                            <a href="{{ route('admin.purchases.drinks', $cart->id) }}" class="btn btn-sm btn-clean btn-icon" title="View Drinks">
                                <i class="fa fa-glass-cheers icon-md"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Purchase ID</th>
                        <th>Buyer</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Status</th>
                        <th>Total Amount</th>
                        <th>User Fee</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
