@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Purchases']
    ]
])

@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap py-5">
            <div class="card-title">
                <h3 class="card-label">Purchases</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-foot-custom table-checkable" id="purchase">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Purchase ID</th>
                        <th>Buyer</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Status</th>
                        <th>Total Amount</th>
                        <th>User Fee</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Purchase ID</th>
                        <th>Buyer</th>
                        <th>Transaction Type</th>
                        <th>Transaction ID</th>
                        <th>Payment Intent ID</th>
                        <th>Status</th>
                        <th>Total Amount</th>
                        <th>User Fee</th>
                        <th>Purchased At</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        let getPurchases = {!! json_encode(route('getPurchases')) !!};
        let purchaseDrinks = {!! json_encode(route('admin.purchases.drinks', ':id')) !!};
    </script>
    <script src="{{ asset('js/admin/datatables/purchase.js') }}"></script>
@endpush
