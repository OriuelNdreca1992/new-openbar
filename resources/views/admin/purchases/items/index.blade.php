@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Purchases', 'url' => route('admin.purchases.index')],
        ['title' => 'Items']
    ]
])

@section('content')
    <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap py-5">
            <div class="card-title">
                <h3 class="card-label">Items</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-separate table-head-custom table-foot-custom table-checkable" id="purchase-item">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Drink ID</th>
                        <th>Drink Name</th>
                        <th>Taker</th>
                        <th>Venue</th>
                        <th>Price</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Purchased At</th>
                        <th>Used/Expired At</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Drink ID</th>
                        <th>Drink Name</th>
                        <th>Taker</th>
                        <th>Venue</th>
                        <th>Price</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Purchased At</th>
                        <th>Used/Expired At</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        let getPurchaseItems = {!! json_encode(route('getPurchaseItems', ['id' => request()->route('id')])) !!}
    </script>
    <script src="{{ asset('js/admin/datatables/purchase-item.js') }}"></script>
@endpush
