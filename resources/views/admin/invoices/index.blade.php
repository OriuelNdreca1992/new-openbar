@extends('layouts.app')

@push('styles')
    <style>
        .dt-body-center {
            text-align: center;
        }
    </style>
@endpush

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Reports']
    ]
])

@section('content')
     <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Reports</h3>
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="invoice">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Report</th>
                        <th>Report No.</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Status</th>
                        <th>Paid/Unpaid</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Report</th>
                        <th>Report No.</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Status</th>
                        <th>Paid/Unpaid</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
        <div class="modal fade" id="confirmStatus" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
            @include('admin.invoices.components.modals.confirm')
        </div>
    </div>
    <!--end::Card-->
@endsection

@push('scripts')
    <script> var venuesShow = {!! json_encode(route('admin.venues.show', ':venue')) !!}</script>
    <script src="{{ asset('js/admin/datatables/invoice.js') }}"></script>
    <script>
        $(document).on("change", "#confirm-status", function () {
            var id = $(this).data("id");
            var checkboxValue = this.checked;
            if (id) {
                $("#status").val(checkboxValue);
                $('form').attr('action', function(i, value) {
                    return value + "/" + id;
                });
            }
        });
    </script>
@endpush
