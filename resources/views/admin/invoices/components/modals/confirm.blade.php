<form
    class="form pt-9"
    action="{{ route('admin.invoices.update') }}"
    method="POST"
>
    @method('PATCH')
    @csrf
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Invoice Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div data-scroll="true" data-height="30">
                    <h5>Are you sure you want to change status of invoice?</h5>
                    <div class="col-lg-9 col-xl-6">
                        <input id="status" name="status" class="form-control" type="hidden" />
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-light-success font-weight-bold">Update</button>
            </div>
        </div>
    </div>
</form>
