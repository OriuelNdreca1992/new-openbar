@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Administrators', 'url' => route('admin.administrators.index')],
        ['title' => 'Edit'],
    ]
])

@section('content')
    <!--begin::Content-->
    <div class="col-lg-10 offset-1 mb-5">
        <!--begin::Card-->
        <div class="card card-custom">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Administrator information</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update administrator informaiton</span>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form
                class="form"
                id="manage_administrator_form"
                action="{{ route('admin.administrators.update', $administrator->id) }}"
                method="POST"
            >
                @method('PATCH')
                @csrf
                <div class="card-body">
                    @include('admin.administrators.components.form')

                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
            </form>
        <!--end::Form-->
        </div>
        <!--end::Card-->
    </div>
    <!--end::Content-->

    <!--begin::Content-->
    <div class="col-lg-10 offset-1">
        <!--begin::Card-->
        <div class="card card-custom">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Password Settings</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update the password used for logging into administrator account.</span>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form
                class="form"
                id="password_validation"
                action="{{ route('admin.settings.password.update', $administrator->id) }}"
                method="POST"
            >
                @method('PUT')
                @csrf
                <div class="card-body">
                    @include('admin.administrators.components.password', [
                        'password' => 'New password'
                    ])

                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
            <!--end::Form-->
            </form>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Content-->
@endsection

@push('scripts')
    <script src="{{ asset('js/form/form-validate.js') }}"></script>
@endpush



