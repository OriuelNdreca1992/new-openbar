<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label">{{ $password }}</label>
    <div class="col-lg-9 col-xl-6">
        <div class="input-group input-group-solid">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="la la-lock"></i>
                </span>
            </div>
            <input
                type="password"
                class="form-control form-control-solid"
                name="password"
            />
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label">Confirm Password</label>
    <div class="col-lg-9 col-xl-6">
        <div class="input-group input-group-solid">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="la la-lock"></i>
                </span>
            </div>
            <input
                type="password"
                class="form-control form-control-solid"
                name="password_confirmation"
            />
        </div>
    </div>
</div>
