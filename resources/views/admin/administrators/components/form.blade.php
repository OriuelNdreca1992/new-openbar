<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label">First Name</label>
    <div class="col-lg-9 col-xl-6">
        <input
            class="form-control form-control-solid"
            type="text"
            name="first_name"
            value="{{ old('first_name', $administrator->first_name) }}"
        />
    </div>
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label">Last Name</label>
    <div class="col-lg-9 col-xl-6">
        <input
            class="form-control form-control-solid"
            type="text"
            name="last_name"
            value="{{ old('last_name', $administrator->last_name) }}"
        />
    </div>
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label">Email Address</label>
    <div class="col-lg-9 col-xl-6">
        <div class="input-group input-group-solid">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="la la-at"></i>
                </span>
            </div>
            <input
                class="form-control form-control-solid"
                type="text"
                name="email"
                value="{{ old('email', $administrator->email) }}"
            />
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label">Phone</label>
    <div class="col-lg-9 col-xl-6">
        <div class="input-group input-group-solid">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <i class="la la-phone"></i>
                </span>
            </div>
            <input
                class="form-control form-control-solid"
                type="text"
                name="phone"
                value="{{ old('phone', $administrator->phone) }}"
            />
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 text-right col-form-label">Role</label>
    <div class="col-lg-9 col-xl-6">
        <select
            class="form-control selectpicker"
            name="role"
        >
            <option value="">Select a Role</option>
            <option value="1" {{ $administrator->role == 1 || old("role") == 1 ? 'selected' : '' }}>Admin</option>
            <option value="2" {{ $administrator->role == 2 || old("role") == 2 ?'selected' : '' }}>Digital Event</option>
        </select>
    </div>
</div>
