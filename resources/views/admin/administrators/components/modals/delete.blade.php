<form
    class="form pt-9"
    id="form"
    action="{{ route('admin.administrators.destroy') }}"
    method="POST"
>
    @method('DELETE')
    @csrf
    <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detele Administrator</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <div data-scroll="true" data-height="30">
                    <h5>Are you sure you want to delete this administrator?</h5>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-danger font-weight-bold">Delete</button>
            </div>
        </div>
    </div>
</form>
