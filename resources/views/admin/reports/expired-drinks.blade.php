@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Expired Drinks Reports'],
    ]
])

@section('content')
     <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Expired Drinks Reports
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="expired-drinks">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Report Pdf</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Report Pdf</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
@endsection

@push('scripts')
    <script>
        var getReports = {!! json_encode(route('admin.expiredDrinks.getReports')) !!}
    </script>
    <script src="{{ asset('js/admin/datatables/expired-drinks.js') }}"></script>
@endpush
