@extends('layouts.app')

@include('admin.metronic.layout.partials.breadcrumbs', [
    'breadcrumbs' => [
        ['title' => 'Dashboard', 'url' => route('dashboard')],
        ['title' => 'Users Fee Reports'],
    ]
])

@section('content')
     <!--begin::Card-->
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Users Fee Reports
            </div>
        </div>
        <div class="card-body">
            <!--begin: Datatable-->
            <table class="table table-bordered table-hover table-checkable mt-10" id="user-fees">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Report Pdf</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Report Pdf</th>
                        <th>From</th>
                        <th>To</th>
                    </tr>
                </tfoot>
            </table>
            <!--end: Datatable-->
        </div>
    </div>
    <!--end::Card-->
@endsection

@push('scripts')
    <script>
        var getReports = {!! json_encode(route('admin.userFees.getReports')) !!}
    </script>
    <script src="{{ asset('js/admin/datatables/user-fee.js') }}"></script>
@endpush
