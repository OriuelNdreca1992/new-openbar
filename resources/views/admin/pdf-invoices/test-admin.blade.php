<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;500&display=swap" rel="stylesheet">
</head>
<style>

    body {
         font-family: 'Montserrat', sans-serif;
    }
    .flex-container {
        width: 100%;
        margin-top: 30px;
    }

   .flex-container > div {
        display: inline-block;
    }

    .flex-container-calculate > div {
        display: inline-block;
        font-size: 19px;
    }

    .center {
      text-align: center;
    }

    table {
        border-collapse: collapse;
        border: none !important;
        width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    .custom-border {
        border-top: none;
        border-left: 1px solid #000000;
        border-right: 1px solid #000000;
        border-bottom: 1px solid #000000;
    }

    tr:nth-child(even) {
        background-color: #dddddd8c;
    }

    .invoice-container {
        font-size: 19px;
        margin-top: 60px;
        width: 100%;
    }

    .container {
        font-size: 19px;
        margin-top: 60px;
        width: 45%;
        float: right;
    }

    .f-bold {
        font-weight: bold;
    }

    .right {
        width: 33%;
        float: right;
    }

    .left {
        float: left;
        width: 50%;
    }

    .invoice-right {
        width: 30%;
        float: right;
    }

    .invoice-left {
        float: left;
        width: 70%;
    }

    .logo {
        max-height: 200px;
        max-width: 300px;
        margin-bottom: 60px;
    }
</style>
<body>
    <!-- <div class="flex-container">
        <div style="width: 45%; font-size: 18px; line-height: 23px; margin-bottom: 20px;">
            <p class="f-bold" style="font-size: 24px;">Venue Name</p>
            <div>Venue Address</div>
            <div>Venue Address</div>
            <div style="margin-top: 10px;">P.IVA: 321472398042983</div>
        </div>
        <div style="width: 50%; text-align: right;">
            <img src="{{ asset('assets/media/logos/openbar_logo_pdf.png') }}" class="logo" alt="OpenBar Logo"/>
            <p class="f-bold" style="font-size: 24px;">flip</p>
            <div>flip</div>
            <div>address</div>
            <div>P.IVA: 32874298374</div>
        </div>
    </div> -->
    <div class="invoice-container page-break">
        <div class="invoice-left">
            <p class="f-bold" style="font-size: 24px;">Venue Name</p>
            <div>Venue Address</div>
            <div>Venue Address</div>
            <div style="margin-top: 10px;">P.IVA: 321472398042983</div>
       </div>
        <div class="invoice-right">
            <div style="float: right;">
                <img src="{{ asset('assets/media/logos/openbar_logo_pdf.png') }}" class="logo" alt="OpenBar Logo"/>
            <p class="f-bold" style="font-size: 24px;">flip</p>
            <div>flip</div>
            <div>address</div>
            <div>P.IVA: 32874298374</div>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 100px;">
        <div class="flex-container">
        <h1>WEEKLY REPORT</h1>
        <h3>WEEK 9 | from 2022-02-27 to 2022-03-07 included</h3>
    </div>
<!--         <div class="flex-container">
            <div style="width: 45%; font-size: 18px; line-height: 23px; margin-bottom: 40px;">
                <p style="font-weight: bold; font-size: 24px;">Venue Name</p>
                <div>Venue Adress Line 1 (street, n°)</div>
                <div>Venue Adress line 2 ( Cap, City )</div>
            </div>
            <div style="width: 50%; text-align: right;">
                <img src="{{ asset('assets/media/logos/openbar_logo_pdf.png') }}" style="max-height: 200px; max-width: 300px; margin-bottom: 60px;" alt="OpenBar Logo"/>
            </div>
        </div> -->
            <div class="flex-container">
                <div style="width: 45%;">
                    <h2>2x1 Drinks</h2>
                </div>
            </div>
            <table>
                <tr>
                    <th class="custom-border">#</th>
                    <th class="custom-border">Offer ID</th>
                    <th class="custom-border">Drink Name</th>
                    <th class="custom-border">Consumed Date</th>
                    <th class="custom-border">Consumed Time</th>
                    <th class="custom-border">Price (€)</th>
                </tr>
                    <tr>
                        <td>1</td>
                        <td>2x1-A-000001</td>
                        <td>2x1 Amaro</td>
                        <td>2022/02/27</td>
                        <td>18:03</td>
                        <td>10,00</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>2x1-A-000001</td>
                        <td>2x1 Wine</td>
                        <td>2022/02/27</td>
                        <td>18:03</td>
                        <td>10,00</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>2x1-A-000001</td>
                        <td>2x1 Raki</td>
                        <td>2022/02/27</td>
                        <td>18:03</td>
                        <td>10,00</td>
                    </tr>
            </table>
            <div style="width: 45%; float: right; margin-top: 40px;  padding: 10px 6px 2px 8px; background-color: #ECF6EB">
                <div class="flex-container-calculate">
                    <div>
                        <div style="font-weight: bold;">Totale 2x1</div>
                    </div>
                    <div style="margin-left: 235px;">
                        <div style="font-weight: bold;">€ 30</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-container">
            <div style="width: 45%;">
                <h2>Premium Drink</h2>
            </div>
        </div>
        <table>
            <tr>
                <th>#</th>
                <th>Purchase ID</th>
                <th>Drink ID</th>
                <th>Drink Name</th>
                <th>Consumed Date</th>
                <th>Consumed Time</th>
                <th>Price</th>
            </tr>
                <tr>
                    <td>1</td>
                    <td>Pu-A-000001</td>
                    <td>Pr-A-000001</td>
                    <td>Amaro</td>
                    <td>02/03/2022</td>
                    <td>34:03</td>
                    <td>5.00</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Pu-A-000001</td>
                    <td>Pr-A-000001</td>
                    <td>Amaro</td>
                    <td>02/03/2022</td>
                    <td>34:03</td>
                    <td>5.00</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Pu-A-000001</td>
                    <td>Pr-A-000001</td>
                    <td>Amaro</td>
                    <td>02/03/2022</td>
                    <td>34:03</td>
                    <td>5.00</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Pu-A-000001</td>
                    <td>Pr-A-000001</td>
                    <td>Amaro</td>
                    <td>02/03/2022</td>
                    <td>34:03</td>
                    <td>5.00</td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Pu-A-000001</td>
                    <td>Pr-A-000001</td>
                    <td>Amaro</td>
                    <td>02/03/2022</td>
                    <td>34:03</td>
                    <td>5.00</td>
                </tr>
        </table>
<!--         <div style="width: 50%; float: left; margin-top: 100px;">
            <div class="flex-container-calculate">
                <div style="float: right;">
                    <div>Servizio Openbar Premium Drink</div>
                </div>
            </div>
        </div> -->
<!--         <div style="width: 45%; float: right; margin-top: 60px; padding: 10px 6px 2px 8px;">
            <div class="flex-container-calculate">
                <div>
                    <div style="font-weight: bold;">Totale Premium Drinks</div>
                    <div>Iva 10% </div>
                    <div style="margin-bottom: 20px;">Imponibile</div>
                    <div>OB Fee 15%</div>
                    <div style="margin-bottom: 20px;">Iva 22%</div>
                    <div style="font-weight: bold; margin-bottom: 20px;">Total OB Fee</div>
                    <div style="font-weight: bold;">Total to Venue</div>
                </div>
                <div style="margin-left: 115px;">
                    <div style="font-weight: bold;">€ 100.00</div>
                    <div>€ 9.09</div>
                    <div style="margin-bottom: 20px;">€ 90.91</div>
                    <div>€ 13.64</div>
                    <div style="margin-bottom: 20px;">€ 3,00</div>
                    <div style="margin-bottom: 20px;">€ 16,64</div>
                    <div>€ 83,36</div>
                </div>
            </div>
        </div> -->

        <div class="container">
            <div class="right">
                <div style="font-weight: bold;">€ 100.00</div>
                <div>€ 9.09</div>
                <div>€ 90.91</div>
                <div>€ 13.64</div>
                <div>€ 3,00</div>
                <div>€ 16,64</div>
                <div>€ 83,36</div>

            </div>
            <div class="left">
                <div style="font-weight: bold;">Totale Premium Drinks</div>
                <div>Iva 10% </div>
                <div>Imponibile</div>
                <div>OB Fee 15%</div>
                <div>Iva 22%</div>
                <div style="font-weight: bold;">Total OB Fee</div>
                <div style="font-weight: bold;">Total to Venue</div>
           </div>
       </div>
</body>
</html>
