<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
</head>
<style>
    .flex-container {
        width: 100%;
        margin-top: 30px;
    }

   .flex-container > div {
        display: inline-block;
        /*text-align: center;*/
    }

    .flex-container-calculate > div {
        display: inline-block;
        font-size: 19px;
    }

    table {
      font-family: arial, sans-serif;
      border-collapse: collapse;
      width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd;
    }
</style>
<body>
    <div class="flex-container">
        <div style="width: 45%; font-size: 18px; line-height: 23px; margin-bottom: 40px;">
            <div style="font-weight: bold;">Company Invoice</div>
            <div>Address: </div>
            <div>P.IVA: </div>
            <div>CF: </div>
            <div>SDI: </div>
            <div>PEC: </div>
            <div>Tel: </div>
        </div>
        <div style="width: 50%; text-align: right;">
            <img src="{{ asset('assets/media/logos/openbar_logo.png') }}" style="max-height: 85px; max-width: 85px; margin-bottom: 60px;" alt="OpenBar Logo"/>
        </div>
    </div>
    <div class="flex-container">
        <div style="width: 45%;">
            <h4>Report Settimannale: From 22-jan To 29-jan</h4>
        </div>
    </div>
</body>
</html>
