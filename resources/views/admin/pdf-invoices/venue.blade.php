<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;500&display=swap" rel="stylesheet">
</head>
<style>
    body {
         font-family: 'Montserrat', sans-serif;
    }
    .flex-container {
        width: 100%;
        margin-top: 30px;
    }

   .flex-container > div {
        display: inline-block;
    }

    .flex-container-calculate > div {
        display: inline-block;
        font-size: 14px;
    }

    .center {
      text-align: center;
    }

    .logo {
        max-height: 150px;
        max-width: 250px;
    }

    .row {
        display: flex;
        margin-top: 85px;
    }

    .column {
        flex: 50%;
        padding: 10px;
    }

    .invoice-container {
        font-size: 14px;
        margin-top: 60px;
        width: 100%;
    }

    .invoice-right {
        width: 30%;
        float: right;
    }

    .invoice-left {
        float: left;
        width: 70%;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
      border: 1px solid #dddddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #dddddd66;
    }

    .container {
        font-size: 14px;
        margin-top: 60px;
        width: 45%;
        float: right;
    }

    .right {
        width: 33%;
        float: right;
    }

    .left {
        float: left;
        width: 67%;
    }

    .page-break {
        page-break-after: avoid;
    }

    .page-break-always {
        page-break-after: always;
    }

    .f-bold {
        font-weight: bold;
    }

    .custom-border {
        font-size: 12px;
        border-top: none;
        border-left: 1px solid #000000;
        border-right: 1px solid #000000;
        border-bottom: 1px solid #000000;
    }

        .p-b-red {
        background-color: #FBE5F1;
    }

    .b-green {
        background-color: #ECF6EB;
    }

    .p-5 {
        padding: 5px;
    }

    .f-bold {
        font-weight: bold;
    }

    .w-50 {
        width: 45%;
    }

    .page-break {
        page-break-inside: avoid;
    }
</style>
<body>
    @if ($premiumDrinkOrders->count())
        <div class="invoice-container">
            <div class="invoice-left">
                <p class="f-bold" style="font-size: 16px;">{{ $venue->fiscal_name }}</p>
                <div>{{ $venue->fiscal_address }}</div>
                <div>{{ $venue->city }}</div>
                <div>P.IVA: {{ $venue->fiscal_vat }}</div>
           </div>
            <div class="invoice-right">
                <div style="float: right;">
                    <img src="{{ asset('assets/media/logos/openbar_logo_pdf.png') }}" class="logo" alt="OpenBar Logo"/>
                    <p class="f-bold" style="font-size: 16px;">FlipYouApp S.r.l.</p>
                    <div>{{ $openBarAdministrativeData->name }}</div>
                    <div>{{ $openBarAdministrativeData->address }}</div>
                    <div style="margin-top: 15px;">P.IVA: {{ $openBarAdministrativeData->p_iva }}</div>
                </div>
            </div>
        </div>
    @endif
   <div>
        <div class="flex-container">
            <h1>WEEKLY REPORT</h1>
            <h3>WEEK {{ $week }} | from {{ $from->format('Y-m-d') }} to {{ $to->format('Y-m-d') }} included</h3>
        </div>
        @if($premiumDrinkOrders->count() == 0)
            <div class="flex-container">
                <div style="width: 45%; font-size: 16px; line-height: 23px; margin-bottom: 20px;">
                    <p class="f-bold" style="font-size: 20px;">{{ $venue->name }}</p>
                    <div>{{ $venue->address }}</div>
                    <div>{{ $venue->city }}</div>
                </div>
                <div style="width: 50%; text-align: right;">
                    <img src="{{ asset('assets/media/logos/openbar_logo_pdf.png') }}" class="logo" alt="OpenBar Logo"/>
                </div>
            </div>
        @endif
        @if ($twoPerOneOrders->count())
            <div style="margin-bottom:200px;">
                <div class="flex-container">
                    <div class="w-45">
                        <h2>2x1 Double Drink</h2>
                    </div>
                </div>
                <table>
                    <tr>
                        <th>#</th>
                        <th>Drink ID</th>
                        <th>Consumed Date</th>
                        <th>Consumed Time</th>
                        <th>Product</th>
                        <th>Price (&euro;)</th>
                    </tr>
                    @php $offerOrderTotal = 0; @endphp
                    @foreach ($twoPerOneOrders as $offerOrder)
                        @php $offerOrderTotal += $offerOrder->price; @endphp
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $offerOrder->id }}</td>
                            <td>{{ $offerOrder->used_at?->format('Y-m-d') }}</td>
                            <td>{{ $offerOrder->used_at?->format('H:i') }}</td>
                            <td>{{ $offerOrder->name }}</td>
                            <td>{{ $offerOrder->price }}</td>
                        </tr>
                    @endforeach
                </table>
                <div class="container page-break">
                    <div class="left">
                        <div class="b-green p-5 f-bold">Totale 2x1</div>
                   </div>
                    <div class="right">
                        <div class="b-green p-5 f-bold">&euro; {{ sprintf("%0.2f", $offerOrderTotal) }}</div>
                    </div>
                </div>
            </div>
        @endif
        @if ($premiumDrinkOrders->count())
            <div class="flex-container">
                <div class="w-45">
                    <h2>Premium Drinks</h2>
                </div>
            </div>
            <table>
                <tr>
                    <th>#</th>
                    <th>Drink ID</th>
                    <th>Consumed Date</th>
                    <th>Consumed Time</th>
                    <th>Product</th>
                    <th>Price (&euro;)</th>
                </tr>
                @php $premiumOrderTotal = 0; @endphp
                @foreach($premiumDrinkOrders as $premiumOrder)
                    @php $premiumOrderTotal += $premiumOrder->price; @endphp
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $premiumOrder->id }}</td>
                        <td>{{ $premiumOrder->used_at?->format('Y-m-d') }}</td>
                        <td>{{ $premiumOrder->used_at?->format('H:i') }}</td>
                        <td>{{ $premiumOrder->name }}</td>
                        <td>{{ $premiumOrder->price }}</td>
                    </tr>
                @endforeach
            </table>
            <div class="container page-break">
                <div class="left">
                    <div class="b-green p-5 f-bold">Total Premium Drink</div>
               </div>
                <div class="right">
                    <div class="b-green p-5 f-bold" >&euro; {{ sprintf("%0.2f", $premiumOrderTotal) }}</div>
                </div>
           </div>
        @endif
   </div>
</body>
</html>
