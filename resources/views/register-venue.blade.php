@extends('layouts.app')

@push('styles')
    <link href="{{ asset('assets/css/pages/wizard/wizard-3.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('auth_template')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body p-0">
                        <div class="text-center mb-10 mt-10">
                            <img style="width:10vw" src="{{ asset('assets/media/logos/openbar_logo_800x300.png') }}" alt="">
                        </div>
                        <div class="text-center mb-5 mt-15">
                            <span class="font-size-h4">Registra Il Tuo Locale</span>
                        </div>
                        <!--begin: Wizard-->
                        <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first" data-wizard-clickable="true">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav">
                                <div class="wizard-steps px-8 py-8 px-lg-15 py-lg-3">
                                    <!--begin::Wizard Step 1 Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span>1.</span>Sign Up
                                            </h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 1 Nav-->
                                    <!--begin::Wizard Step 2 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                            <span>2.</span>Venue Data</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 2 Nav-->
                                    <!--begin::Wizard Step 3 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                            <span>3.</span>Billing Info</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 3 Nav-->
                                </div>
                            </div>
                            <!--end: Wizard Nav-->
                            <!--begin: Wizard Body-->

                            <div class="row justify-content-center py-10 px-8 py-lg-12 px-lg-10">
                                <div class="col-xl-12 col-xxl-11">
                                    <!--begin: Wizard Form-->
                                    <form
                                        class="form"
                                        id="kt_form"
                                        action="{{ route('venues.store') }}"
                                        method="POST"
                                    >
                                    @csrf

                                        <!--begin: Wizard Step 1-->
                                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                            <div class="row">
                                                <!--begin::Input-->
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Nome <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="first_name"
                                                            placeholder="Nome"
                                                            value="{{ old('first_name"') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <!--end::Input-->
                                                <!--begin::Input-->
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Cognome <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="last_name"
                                                            placeholder="Cognome"
                                                            value="{{ old('last_name') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <!--end::Input-->
                                                <div class="col-lg-4">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Cellulare <span class="text-danger">*</span></label>
                                                        <input
                                                            type="tel"
                                                            class="form-control"
                                                            name="phone"
                                                            placeholder="Cellulare"
                                                            value="{{ old('phone') }}"
                                                        />
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!--begin::Input-->
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Email <span class="text-danger">*</span></label>
                                                        <input
                                                            type="email"
                                                            class="form-control"
                                                            name="email"
                                                            placeholder="Email"
                                                            value="{{ old('email') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <!--end::Input-->
                                                <!--begin::Input-->
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Password <span class="text-danger">*</span></label>
                                                        <input
                                                            type="password"
                                                            class="form-control"
                                                            name="password"
                                                            id="password"
                                                            placeholder="Password"
                                                            value="{{ old('password') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <!--end::Input-->
                                                <!--begin::Input-->
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Conferma password <span class="text-danger">*</span></label>
                                                        <input
                                                            type="password"
                                                            class="form-control"
                                                            name="password_confirmation"
                                                            placeholder="Conferma password"
                                                            value="{{ old('confirm_password') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <!--end: Wizard Step 1-->
                                             @if (count($errors))
                                                <ul class="alert alert-danger">
                                                    @foreach ($errors->all() as $message)
                                                        <li>{{ $message }}</li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                        <!--begin: Wizard Step 2-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <div class="row">
                                              {{--   <input type="hidden" id="lat" name="lat" value="{{ old('lat') }}">
                                                <input type="hidden" id="lng" name="lng" value="{{ old('lng') }}"> --}}
                                                <div id="map" style="display: none;"></div>
                                                <!--begin::Input-->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Cerca il tuo locale con Google <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            id="pac-input"
                                                            class="form-control"
                                                            name="address"
                                                            placeholder="Cerca il tuo locale con Google"
                                                            value="{{ old('address') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <!--end::Input-->
                                                <!--begin::Input-->
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Nome Locale <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="name"
                                                            id="name"
                                                            placeholder="Nome Locale"
                                                            value="{{ old('name') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <!--end::Input-->
                                            </div>
                                            <div class="row">
                                                <input type="hidden" id="lat" name="lat" value="{{ old('lat') }}">
                                                <input type="hidden" id="lng" name="lng" value="{{ old('lng') }}">
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Città <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="city"
                                                            id="city"
                                                            placeholder="Città"
                                                            value="{{ old('city') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Codice Postale <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="postal_code"
                                                            id="postal_code"
                                                            placeholder="Codice Postale"
                                                            value="{{ old('postal_code') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Stato/Regione</label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="state"
                                                            id="state"
                                                            placeholder="Stato/Regione"
                                                            value="{{ old('state') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group">
                                                        <label>Nazione <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="country"
                                                            id="country"
                                                            placeholder="Nazione"
                                                            value="{{ old('country') }}"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Nr Telefono Venue <span class="text-danger">*</span></label>
                                                        <input
                                                            type="tel"
                                                            class="form-control"
                                                            name="phone_number"
                                                            id="phone_number"
                                                            placeholder="Nr Telefono Venue"
                                                            value="{{ old('phone_number') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <input type="hidden" name="opening_hours[1][start_hour]" value="{{ old('opening_hours.1.start_hour') }}"/>
                                                <input type="hidden" name="opening_hours[1][end_hour]" value="{{ old('opening_hours.1.end_hour') }}"/>
                                                <input type="hidden" name="opening_hours[2][start_hour]" value="{{ old('opening_hours.2.start_hour') }}"/>
                                                <input type="hidden" name="opening_hours[2][end_hour]" value="{{ old('opening_hours.2.end_hour') }}"/>
                                                <input type="hidden" name="opening_hours[3][start_hour]" value="{{ old('opening_hours.3.start_hour') }}"/>
                                                <input type="hidden" name="opening_hours[3][end_hour]" value="{{ old('opening_hours.3.end_hour') }}"/>
                                                <input type="hidden" name="opening_hours[4][start_hour]" value="{{ old('opening_hours.4.start_hour') }}"/>
                                                <input type="hidden" name="opening_hours[4][end_hour]" value="{{ old('opening_hours.4.end_hour') }}"/>
                                                <input type="hidden" name="opening_hours[5][start_hour]" value="{{ old('opening_hours.5.start_hour') }}"/>
                                                <input type="hidden" name="opening_hours[5][end_hour]" value="{{ old('opening_hours.5.end_hour') }}"/>
                                                <input type="hidden" name="opening_hours[6][start_hour]" value="{{ old('opening_hours.6.start_hour') }}"/>
                                                <input type="hidden" name="opening_hours[6][end_hour]" value="{{ old('opening_hours.6.end_hour') }}"/>
                                                <input type="hidden" name="opening_hours[7][start_hour]" value="{{ old('opening_hours.7.start_hour') }}"/>
                                                <input type="hidden" name="opening_hours[7][end_hour]" value="{{ old('opening_hours.7.end_hour') }}"/>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Posti a Sedere <span class="text-danger">*</span></label>
                                                        <select
                                                            class="form-control selectpicker"
                                                            id="seats_number"
                                                            name="seats_number"
                                                        >
                                                            @for ($i = 1; $i <= 100; $i++)
                                                                <option value="{{ $i }}" {{ (old("seats_number") == $i ? "selected" : "") }}>
                                                                    {{ $i }}
                                                                </option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Tipologia <span class="text-danger">*</span></label>
                                                        <select
                                                            style="width: 100%"
                                                            class="form-control"
                                                            id="kt_select2_11"
                                                            multiple="multiple"
                                                            name="types[]"
                                                            id="types"
                                                        >
                                                            @foreach ($types as $type)
                                                                <option
                                                                    value="{{ $type->id }}" {{ (old("types") !== null && in_array($type->id, old("types")) ? "selected": "") }}>
                                                                    {{ $type->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                             @if (count($errors))
                                                <ul class="alert alert-danger">
                                                    @foreach ($errors->all() as $message)
                                                        <li>{{ $message }}</li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </div>
                                        <!--end: Wizard Step 2-->
                                        <!--begin: Wizard Step 3-->
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Ragione Sociale <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="fiscal_name"
                                                            placeholder="Ragione Sociale"
                                                            value="{{ old('fiscal_name') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Partita Iva <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="fiscal_vat"
                                                            placeholder="Partita Iva"
                                                            value="{{ old('fiscal_vat') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Indirizzo di fatturazione <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="fiscal_address"
                                                            placeholder="Indirizzo di fatturazione"
                                                            value="{{ old('fiscal_address') }}"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="row">
                                                 <div class="col-lg-4">
                                                     <div class="form-group">
                                                         <label>SDI</label>
                                                         <input
                                                             type="text"
                                                             class="form-control"
                                                             name="sdi_code"
                                                             placeholder="SDI"
                                                             value="{{ old('sdi_code') }}"
                                                         />
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>PEC</label>
                                                        <input
                                                            type="email"
                                                            class="form-control"
                                                            name="certified_email"
                                                            placeholder="PEC"
                                                            value="{{ old('certified_email') }}"
                                                        />
                                                    </div>
                                                </div>
                                                 <div class="col-lg-4">
                                                     <div class="form-group">
                                                         <label>IBAN</label>
                                                         <input
                                                             type="text"
                                                             class="form-control"
                                                             name="iban"
                                                             placeholder="IBAN"
                                                             value="{{ old('iban') }}"
                                                         />
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Città <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="fiscal_city"
                                                            placeholder="Città"
                                                            value="{{ old('fiscal_city') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Nazione <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="fiscal_country"
                                                            placeholder="Nazione"
                                                            value="{{ old('fiscal_country') }}"
                                                        />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label>Cap <span class="text-danger">*</span></label>
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            name="fiscal_cap"
                                                            placeholder="Cap"
                                                            value="{{ old('fiscal_cap') }}"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="checkbox mb-0">
                                                            <input type="checkbox" name="terms_and_conditions" value="{{ old('terms_and_conditions') }}" @if( old('terms_and_conditions') ) checked='checked' @endif/>
                                                            <span></span>
                                                            <div class="ml-2">
                                                                Ho letto e accetto i <a target="_blank" href="https://www.openbar.life/termini-e-condizioni-venue">Termini e Condizioni</a>
                                                                e dichiaro espressamente di accettare <a target="_blank" href="https://www.openbar.life/Clausole-vessatorie-Esercenti.pdf">le Clausole Vessatorie</a>
                                                                anche per i finidi cui agli art. 1341 e 1342 del Codice Civile <span class="text-danger">*</span>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="checkbox mb-0">
                                                            <input type="checkbox" name="privacy_policy" value="{{ old('privacy_policy') }}" @if( old('privacy_policy') ) checked='checked' @endif/>
                                                            <span></span>
                                                            <div class="ml-2">
                                                                Ho letto e accetto la <a target="_blank" href="https://www.openbar.life/privacy-policy">Privacy Policy</a>
                                                                <span class="text-danger">*</span>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Wizard Step 3-->
                                        <!--begin: Wizard Actions-->
                                        <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                            <div class="mr-2">
                                                <button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-prev">Previous</button>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-submit">Submit</button>
                                                <button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-next">Next</button>
                                            </div>
                                        </div>
                                        <!--end: Wizard Actions-->
                                    </form>
                                    <!--end: Wizard Form-->
                                </div>
                            </div>
                            <!--end: Wizard Body-->
                        </div>
                        <!--end: Wizard-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        let oldLat = {!! json_encode(old('lat')) !!};
        let oldLng = {!! json_encode(old('lng')) !!};
        let oldAddress = {!! json_encode(old('address')) !!};
    </script>
    <script src="{{ asset('js/form/venue-validate.js') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/forms/widgets/select2.js') }}"></script>
    <script src="{{ asset('js/map/map.js') }}"></script>
@endpush

