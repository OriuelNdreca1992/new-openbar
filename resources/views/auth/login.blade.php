@extends('layouts.app')

@push('styles')
    <link href="{{ asset('assets/css/pages/login/classic/login-5.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .btn-ob-primary {border:1px solid transparent;color:white;background-image:linear-gradient(to right, #d71b73, #e3252a);}
        .btn-ob-primary:hover {color:white;border:1px solid white;}
    </style>
@endpush
@section('auth_template')
<!--begin::Login-->
<div class="login login-5 login-signin-on d-flex flex-row-fluid" id="kt_login">
    <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid" style="background-image: url({{ asset('assets/media/bg/bg-2.jpg') }});">
        <div class="login-form text-center text-white p-7 position-relative overflow-hidden">
            <!--begin::Login Header-->
            <div class="d-flex flex-center mb-10">
                <a href="#">
                    <img src="{{ asset('assets/media/logos/openbar.png') }}" class="max-h-75px"  alt=""/>
                </a>
            </div>
            <!--end::Login Header-->

            <!--begin::Login Sign in form-->
            <div class="login-signin">
                <div class="mb-20">
                    <h3 class="opacity-40 font-weight-normal">Login</h3>
                </div>
                <form method="POST" action="{{ route('login') }}" class="form" id="kt_login_signin_form">
                    @csrf
                    <div class="form-group">
                        <input
                            id="email"
                            type="email"
                            class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8 @error('email') is-invalid @enderror"
                            name="email"
                            placeholder="Email"
                            value="{{ old('email') }}"
                            autocomplete="email"
                        />

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input
                            id="password"
                            type="password"
                            class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8 @error('password') is-invalid @enderror"
                            name="password"
                            placeholder="Password"
                            autocomplete="current-password"
                        />
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group d-flex flex-wrap justify-content-between align-items-center px-8 opacity-60">
                        <div class="checkbox-inline">
                            <label class="checkbox checkbox-outline checkbox-white text-white m-0">
                            <input type="checkbox" name="remember" />
                            <span></span>Remember me</label>
                        </div>
                        <a href="javascript:;" id="kt_login_forgot" class="text-white font-weight-bold">Forget Password ?</a>
                    </div>
                    <div class="form-group text-center mt-10">
                        <button type="submit" class="btn btn-pill btn-outline-white opacity-90 px-15 py-3">{{ __('Sign In') }}</button>
                    </div>
                </form>
                <div class="mt-10">
                    <a href="{{ route('venues.register') }}" class="btn btn-pill btn-ob-primary px-15 py-3">
                        {{ __('Register New Venue') }}
                    </a>
                </div>
            </div>
            <!--end::Login Sign in form-->

            <!--begin::Login forgot password form-->
            <div class="login-forgot">
                <div class="mb-20">
                    <h3 class="opacity-40 font-weight-normal">Forgotten Password ?</h3>
                    <p class="opacity-40">Enter your email to reset your password</p>
                </div>
                <form method="POST" action="{{ route('password.email') }}" id="kt_login_forgot_form">
                    @csrf
                    <div class="form-group mb-10">
                        <input
                            id="email"
                            type="email"
                            class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8 @error('email') is-invalid @enderror"
                            name="email"
                            placeholder="Email"
                            value="{{ old('email') }}"
                            autocomplete="email"
                        >
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-pill btn-outline-white opacity-90 px-15 py-3 m-2">Request</button>
                        <button id="kt_login_forgot_cancel" class="btn btn-pill btn-outline-white opacity-70 px-15 py-3 m-2">Cancel</button>
                    </div>
                </form>
            </div>
            <!--end::Login forgot password form-->
        </div>
    </div>
</div>
<!--end::Login-->
@endsection

@push('scripts')
    <script src="{{ asset('assets/js/pages/custom/login/login-general.js') }}"></script>
@endpush
