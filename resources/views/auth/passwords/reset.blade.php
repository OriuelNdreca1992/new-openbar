@extends('layouts.app')

@push('styles')
    <link href="{{ asset('assets/css/pages/login/classic/login-5.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('auth_template')
    <div class="login login-5 login-signin-on d-flex flex-row-fluid" id="kt_login">
        <div class="d-flex flex-center bgi-size-cover bgi-no-repeat flex-row-fluid" style="background-image: url({{ asset('assets/media/bg/bg-2.jpg') }});">
            <div class="login-form text-center text-white p-7 position-relative overflow-hidden">
                <!--begin::Reset Password form-->
                <div class="login-signin">
                    <div class="mb-20">
                        <h3 class="opacity-40 font-weight-normal">Reset Password</h3>
                    </div>
                    <form method="POST" action="{{ route('password.update') }}" class="form" id="kt_login_signin_form">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <input
                                id="email"
                                type="email"
                                name="email"
                                class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8 @error('email') is-invalid @enderror"
                                value="{{ $email ?? old('email') }}"
                                placeholder="Email"
                                autocomplete="email"
                                autofocus
                            >

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input
                                id="password"
                                type="password"
                                class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8 @error('password') is-invalid @enderror"
                                name="password"
                                placeholder="Password"
                                autocomplete="current-password"
                            />
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input
                                id="password-confirm"
                                type="password"
                                class="form-control h-auto text-white bg-white-o-5 rounded-pill border-0 py-4 px-8"
                                name="password_confirmation"
                                placeholder="Confirm Password"
                                autocomplete="new-password"
                            />
                        </div>
                        <div class="form-group text-center mt-10">
                            <button type="submit" class="btn btn-pill btn-outline-white opacity-90 px-15 py-3">Reset Password</button>
                        </div>
                    </form>
                </div>
                <!--end::Reset Password form-->
            </div>
        </div>
    </div>
@endsection
