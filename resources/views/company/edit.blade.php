@extends('layouts.app')

@section('content')
    <form
        class="form"
        action="{{ route('company.update') }}"
        method="POST"
        id="validate_company_request"
         enctype="multipart/form-data"
    >
    @method('PATCH')
    @csrf
        <!--begin::Content-->
        <div class="col-lg-12 mb-5">
            <!--begin::Card-->
            <div class="card card-custom">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">Edit Company</h3>
                    </div>
                </div>
                <!--end::Header-->
                <!--begin::Form-->
                <div class="card-body">
                    <div class="mb-8">
                       <h5 class="card-label text-dark">Update User Information</h5>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    First Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="first_name"
                                    class="form-control form-control-solid"
                                    placeholder="First Name"
                                    value="{{ old('first_name', $company->user->first_name) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    Last Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="last_name"
                                    class="form-control form-control-solid"
                                    placeholder="Last Name"
                                    value="{{ old('last_name', $company->user->last_name) }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    Phone
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="phone"
                                    class="form-control form-control-solid"
                                    placeholder="Phone"
                                    value="{{ old('phone', $company->user->phone) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Email
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="email"
                                    name="email"
                                    class="form-control form-control-solid"
                                    placeholder="Email"
                                    value="{{ old('email', $company->user->email) }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="mb-8 mt-8">
                       <h5 class="card-label text-dark">Company Information</h5>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Company Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="name"
                                    class="form-control form-control-solid"
                                    placeholder="Company Name"
                                    value="{{ old('name', $company->name) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Company Email
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="email"
                                    name="company_email"
                                    class="form-control form-control-solid"
                                    placeholder="Company Email"
                                    value="{{ old('company_email', $company->email) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Address
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="address"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Address"
                                    value="{{ old('address', $company->address) }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                         <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    City
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="city"
                                    class="form-control form-control-solid"
                                    placeholder="City"
                                    value="{{ old('city', $company->city) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Phone
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="phone_number"
                                    class="form-control form-control-solid"
                                    placeholder="Phone"
                                    value="{{ old('phone_number', $company->phone_number) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group row">
                                <label class="col-xl-3 col-lg-3 col-form-label text-right">Logo</label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="image-input image-input-outline" id="company_image">
                                        <div
                                            class="image-input-wrapper"
                                            style="background-image: url({{ $company->imageUrl() ?? asset('assets/media/companies/default_logo.jpg') }} )">
                                        </div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Upload Logo">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input
                                                type="file"
                                                name="image_url"
                                                accept=".png, .jpg, .jpeg"
                                            />
                                            <input type="hidden" name="image_url_remove" />
                                        </label>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel logo">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mb-8 mt-8">
                       <h5 class="card-label text-dark">Fiscal Data</h5>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Name
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_name"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Name"
                                    value="{{ old('fiscal_name', $company->fiscal_name) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Address
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_address"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Address"
                                    value="{{ old('fiscal_address', $company->fiscal_address) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal City
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_city"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal City"
                                    value="{{ old('fiscal_city', $company->fiscal_city) }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Prov
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_prov"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Prov"
                                    value="{{ old('fiscal_prov', $company->fiscal_prov) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Cap
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_cap"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Cap"
                                    value="{{ old('fiscal_cap', $company->fiscal_cap) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal VAT
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_vat"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal VAT"
                                    value="{{ old('fiscal_vat', $company->fiscal_vat) }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    Fiscal Code
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="fiscal_code"
                                    class="form-control form-control-solid"
                                    placeholder="Fiscal Code"
                                    value="{{ old('fiscal_code', $company->fiscal_code) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    SDI Code
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="sdi_code"
                                    class="form-control form-control-solid"
                                    placeholder="SDI Code"
                                    value="{{ old('sdi_code', $company->sdi_code) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label>
                                    PEC
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="email"
                                    name="certified_email"
                                    class="form-control form-control-solid"
                                    placeholder="PEC"
                                    value="{{ old('certified_email', $company->certified_email) }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    BIC/SWIFT
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="bic"
                                    class="form-control form-control-solid"
                                    placeholder="BIC/SWIFT"
                                    value="{{ old('bic', $company->bic) }}"
                                />
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label>
                                    IBAN
                                    <span class="text-danger">*</span>
                                </label>
                                <input
                                    type="text"
                                    name="iban"
                                    class="form-control form-control-solid"
                                    placeholder="IBAN"
                                    value="{{ old('iban', $company->iban) }}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Card-->
        </div>
        <!--end::Content-->
    </form>

    <!--begin::Content-->
    <div class="col-lg-12">
        <!--begin::Card-->
        <div class="card card-custom">
            <!--begin::Header-->
            <div class="card-header py-3">
                <div class="card-title align-items-start flex-column">
                    <h3 class="card-label font-weight-bolder text-dark">Password Settings</h3>
                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update the password used for logging into company account.</span>
                </div>
            </div>
            <!--end::Header-->
            <!--begin::Form-->
            <form
                class="form"
                id="password_validation"
                action="{{ route('admin.settings.password.update', $company->user->id) }}"
                method="POST"
                enctype="multipart/form-data"
            >
                @method('PUT')
                @csrf
                <div class="card-body">
                    @include('admin.administrators.components.password', [
                        'password' => 'New password'
                    ])

                    <div class="card-footer text-right">
                        <button type="button" class="btn btn-light-primary font-weight-bold">Cancel</button>
                        <button type="submit" class="btn btn-primary font-weight-bold">Save Changes</button>
                    </div>
                    <div class="col-lg-5 offset-3">
                        @if (count($errors))
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $message)
                                    <li class="ml-5">{{ $message }}</li>
                                @endforeach
                            </ul>
                        @endif
                   </div>
                </div>
            <!--end::Form-->
            </form>
        </div>
        <!--end::Card-->
    </div>
    <!--end::Content-->
@endsection

@push('scripts')
    <script src="{{ asset('js/form/password-validate.js') }}"></script>
    <script src="{{ asset('assets/js/pages/crud/file-upload/image-input.js') }}"></script>
     <!-- Laravel Javascript Validation -->
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {!! JsValidator::formRequest('App\Http\Requests\CompanyRequest', '#validate_company_request'); !!}
@endpush




