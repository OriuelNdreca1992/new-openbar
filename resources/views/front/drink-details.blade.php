<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Claim Drink</title>
    @if($item->cart->offered->avatar['thumb'])
        @php $avatar = $item->cart->offered->avatar['thumb'] @endphp
    @else
        @php $avatar = asset('assets/media/icons/avatar.jpg') @endphp
    @endif
    <style>
        body {margin:0;padding:0;font-family:Arial,serif;width:100vw;}
        .container {width:100vw;}
        .venue_name {background-color:#35383f;color:#fff;font-weight:bold;font-size:7.3vw;padding:12vw 6.6vw;width:100vw;box-sizing:border-box;}
        .arrow {background-image:url({{ asset('assets/media/icons/arrow.jpg') }});background-repeat:repeat-x;height:3vw;width:100vw;background-size:3vw;}
        .order_number {text-align:right;font-weight:bold;font-size:4.7vw;margin-right:7vw;margin-top:3.5vw;margin-bottom:1vw;}
        .avatar {background: url({{$avatar}}) no-repeat center;background-size:cover;width:21.5vw;height:21.5vw;border-radius:12vw;border:0.2vw solid #ff0164;margin-left:9vw;margin-top:4vw;}
        .avatar, .info, .article, .quantity {float:left}
        .label {font-size:3vw;color:#676f86;font-weight:normal;}
        .text {font-size:4.2vw;color:#000;font-weight:bold;margin-bottom:4vw;margin-top:1vw;}
        .info {margin-top:4vw;margin-left:6vw;}
        .items {margin:6vw 10vw;}
        .items .text {font-size:5vw;max-width:63vw;margin-bottom:1vw;}
        .quantity {float:right;}
        .quantity .text {text-align:right;}
        .article_quantity {border-bottom:1px solid;margin-bottom:2.5vw;}
        .item {margin-bottom:3vw;}
        .right {float:right;}
        .left {float:left;}
        .price .text {text-align:right;font-weight:normal;font-size:4vw;margin-bottom:0;}
        .price .lbl {width:63vw;}
        .confirm, .cancel {text-align:center;}
        .confirm input {width:80vw;margin-bottom:1vw;height:14vw;background-color:#ff0064;border:none;color:#fff;font-weight:bold;font-size:5vw;border-radius:4vw;}
        .cancel input {width:49vw;margin-bottom:10vw;height:8vw;background-color:#fff;border:none;color:#676f86;font-size:4.5vw;border-radius:2vw;margin-top:2vw;}
        input {-webkit-appearance:none;-moz-appearance:none;appearance:none;}
    </style>
</head>
<body>
<div class="container">
    <div class="venue_name"> {{ $item->venue->name }} </div>
    <div class="arrow"></div>
    <div class="order_number">
        n ordine: {{ str_pad($item->cart->id, 6, '0', STR_PAD_LEFT) }}
    </div>
    <div class="info_user">
        <div class="avatar"></div>
        <div class="info">
            <div class="label">APP USER</div>
            <div class="app_user text">{{ $item->cart->offered->first_name }} {{ $item->cart->offered->last_name }}</div>
            <div class="label">DATA E ORA ACQUISTO</div>
            <div class="order_date text">{{ $item->cart->created_at->format('d/m/Y H:i') }}</div>
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="items">
        <div class="item">
            <div class="article_quantity">
                <div class="article">
                    <div class="label">ARTICOLO</div>
                    <div class="text">{{ $item->name }}</div>
                </div>
                <div class="quantity">
                    <div class="label">QUANTIT&Agrave;</div>
                    <div class="text">x 1</div>
                </div>
                <div style="clear:both"></div>
            </div>
            <div class="price">
                <div class="left text lbl">Prezzo del drink:</div>
                <div class="right text">{{ $item->price }} &euro;</div>
                <div style="clear:both"></div>
            </div>
        </div>
        <div class="item">
            <div class="article_quantity">
                <span class="label">
                    <i>Scade il: {{ $item->cart->expires_at?->format('d/m/Y H:i') }}</i>
                </span>
            </div>
        </div>
        <div class="item" style="margin-bottom:8vw;">
            <div class="article_quantity" style="border:none">
                <div class="article">
                    <div class="text">
                        <img style="width:6vw" src="{{ asset('assets/media/logos/openbar.png') }}" alt="">
                    </div>
                </div>
                <div class="quantity">
                    <div class="text">
                        <img style="width: 25vw;margin-top: 0.5vw;" src="{{ asset('assets/media/icons/pagato.png') }}" alt="">
                    </div>
                </div>
                <div style="clear:both"></div>
            </div>
        </div>
    </div>
    <div class="confirm">
        <form action="{{ Request::url() }}" method="post">
            @csrf
            <input type="hidden" name="confirm" value="1">
            <input type="submit" value="Conferma">
        </form>
    </div>
    <div class="cancel">
        <form action="{{ Request::url() }}" method="post">
            @csrf
            <input type="hidden" name="error_message" value="Operazione annullata.">
            <input type="submit" value="Annulla">
        </form>
    </div>
</div>
</body>
</html>
