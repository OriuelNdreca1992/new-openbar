<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="robots" content="noindex" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Claim Drink</title>
    <style>
        body {margin:0;padding:0;font-family:Arial,serif;width:100vw;}
        .container {width:100vw;}
        .icon {text-align:center;margin-top:50vw;}
        .text1 {text-align:center;font-size:9.2vw;color:#000;font-weight:bold;margin-bottom:4vw;margin-top:8vw;}
        .text2 {text-align:center;font-size:5.2vw;color:#35383f;font-weight:normal;margin-bottom:4vw;margin-top:1vw;}
    </style>
</head>
<body>
<div class="container">
    @if (isset($error_message) && $error_message)
        <div class="icon">
            <img style="width: 25vw;margin-top: 0.5vw;" src="{{ asset('assets/media/icons/icon_ko.png') }}" alt="">
        </div>
        <div class="text1">Oh no!</div>
        <div class="text2">{{ $error_message }}</div>
    @else
        <div class="icon">
            <img style="width: 25vw;margin-top: 0.5vw;" src="{{ asset('assets/media/icons/icon_ok.png') }}" alt="">
        </div>
        <div class="text1">Ok!</div>
        <div class="text2">Ora puoi servire il drink.</div>
    @endif
</div>
</body>
</html>
