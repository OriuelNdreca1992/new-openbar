@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Signature --}}
    @slot('signature')
        @component('mail::signature')
        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            @if(isset($footer_email))
                <p>If you have any questions, contact us <a href="mailto:{{ $footer_email }}">{{ $footer_email }}</a></p>
            @else
                <p>If you have any questions, contact us <a href="mailto:support@openbar.life">support@openbar.life</a></p>
            @endif
            <p class="copyright">Copyright FlipYouApp S.r.l. © {{ date('Y') }}, @lang('All rights reserved.')</p>
        @endcomponent
    @endslot
@endcomponent
