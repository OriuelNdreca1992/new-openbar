@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Signature --}}
    @slot('signature')
        @component('mail::signature')
        @endcomponent
    @endslot

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer_v2')
            <p>
                Ricevi questa email perché ti sei registrato sul nostro sito e hai dato il consenso a ricevere comunicazioni email da parte nostra. <br>
                FlipYouApp Srl, Largo Augusto 3 , Milano, 20122 IT Milano <br>
                OpenBar www.openbar.life 3392379314
            </p>
        @endcomponent
    @endslot
@endcomponent
