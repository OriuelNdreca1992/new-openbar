<tr>
    <td>
        <table class="footer footer_v2 content-cell" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            {{-- Socials --}}
            <tr>
                <td>
                    <table width="150px" align="center" cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td>
                                <div class="text-center">
                                    <a href="https://www.facebook.com/openbarlife/" target="_blank">
                                        <img width="32" height="32" alt="Facebook" title="Facebook" src="http://a3f0i3.mailupclient.com/images/social/circle-blue/facebook.png">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="text-center">
                                    <a href="https://www.instagram.com/openbar.app/" target="_blank">
                                        <img width="32" height="32" alt="Instagram" title="Instagram" src="http://a3f0i3.mailupclient.com/images/social/circle-blue/instagram@2x.png">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="text-center">
                                    <a href="https://www.linkedin.com/company/openbar-app/" target="_blank">
                                        <img width="32" height="32" alt="LinkedIn" title="LinkedIn" src="http://a3f0i3.mailupclient.com/images/social/circle-blue/linkedin@2x.png">
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            {{-- Copyright --}}
            <tr>
                <td align="center" style="padding-top:25px;">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
