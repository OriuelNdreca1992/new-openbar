<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="https://laravel.com/img/notification-logo.png" class="logo" alt="Laravel Logo">
@else
<img src="{{asset('assets/media/mail/logo.png')}}" class="logo" alt="OpenBar" style="width:120px;height:auto;">
@endif
</a>
</td>
</tr>
