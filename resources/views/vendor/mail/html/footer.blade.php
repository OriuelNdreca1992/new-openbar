<tr>
    <td>
        <table class="footer content-cell" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            {{-- Socials --}}
            <tr>
                <td>
                    <table width="150px" align="center" cellpadding="0" cellspacing="0" role="presentation">
                        <tr>
                            <td>
                                <div class="text-center">
                                    <a href="https://www.facebook.com/openbarlife/" target="_blank">
                                        <img width="25" height="25" alt="Facebook" title="Facebook" src="https://openbar.img.musvc5.net/static/136093/images/social/colored/facebook@2x.png">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="text-center">
                                    <a href="https://www.instagram.com/openbar.app/" target="_blank">
                                        <img width="25" height="25" alt="Instagram" title="Instagram" src="https://openbar.img.musvc5.net/static/136093/images/social/colored/instagram@2x.png">
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="text-center">
                                    <a href="https://www.linkedin.com/company/openbar-app/" target="_blank">
                                        <img width="25" height="25" alt="LinkedIn" title="LinkedIn" src="https://openbar.img.musvc5.net/static/136093/images/social/colored/linkedin@2x.png">
                                    </a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            {{-- Copyright --}}
            <tr>
                <td align="center" style="padding-top:20px;">
                    {{ Illuminate\Mail\Markdown::parse($slot) }}
                </td>
            </tr>
        </table>
    </td>
</tr>
