@extends('layouts.app')


@section('auth_template')
<div class="error error-6 d-flex flex-row-fluid bgi-size-cover bgi-position-center" style="background-image: url('{{ asset('assets/media/bg/background-image.png') }}');">
    <!--begin::Content-->
    <div class="d-flex flex-column flex-row-fluid text-center">
        <h1 class="font-weight-boldest text-white mb-12" style="margin-top: 12rem;">
            Grazie per la tua iscrizione
        </h1>
        <h3 class="font-weight-bold text-white">
            La tua candidatura è arrivata correttamente, il nostro team commerciale sarà in contatto con te al più presto.
        </h3>
        <div class="col-md-2 offset-5">
            <a href="https://www.openbar.life" class="btn btn-block btn-sm btn-light-primary font-weight-bolder text-uppercase py-4">
                Visita il nostro sito
            </a>
        </div>
    </div>
    <!--end::Content-->
</div>
@endsection
