@component('mail::message')
# Open Bar
New Company has registered on Open Bar

@component('mail::panel')
User name: {{ $company->user->first_name }} {{ $company->user->last_name }}
@endcomponent
@component('mail::panel')
Company name: {{ $company->name }}
@endcomponent

@component('mail::panel')
Email: {{ $company->user->email }}
@endcomponent
@component('mail::panel')
Password: {{ $password }}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
