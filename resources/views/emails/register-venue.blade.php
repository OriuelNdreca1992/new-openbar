@component('mail::message_v2')
<p>
    <img src="{{asset('assets/media/mail/welcome_01.jpg')}}" alt="Image" style="width:100%;height:auto;">
</p>
<h1> Grazie per la tua iscrizione </h1>
<p>
    <i>Ciao {{ $venue->name }},</i>
</p>
<p>
    La tua candidatura è arrivata correttamente, il nostro team commerciale sarà in contatto con te al più presto.
    Se hai bisogno di aiuto non esitare a contattarci scrivendo a <a href="mailto:support@openbar.life">support@openbar.life</a>
</p>
@endcomponent
