@component('mail::message_v2')
    <p>
        <i>Ciao {{ $venue->name }},</i>
    </p>
    <p>
        Da questo link che trovi di seguito puoi scaricare il tuo report della settimana.
    </p>
    <p>
        @component('mail::button', ['url' => url($docPath), 'color' => 'primary'])
            Scarica report
        @endcomponent
    </p>
@endcomponent
