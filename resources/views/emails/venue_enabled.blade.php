@component('mail::message_v2')
    <p>
        <img src="{{asset('assets/media/mail/bocche_portfolio_01.jpg')}}" alt="Image" style="width:100%;height:auto;">
    </p>
    <h1> Benvenuto su OpenBar </h1>
    <p>
        <i>Ciao {{ $venue->name }},</i>
    </p>
    <p>
        Il tuo profilo è stato attivato.
    </p>
    <p>
        Fai login con le tue credenziali sul tuo pannello di controllo cliccando il link di seguito:
    </p>
    <p>
        <a href="https://my.openbar.life/">my.openbar.life</a>
    </p>
    <p>
        Se non hai i tuoi dati di accesso utilizza la mail con cui ti sei registrato e puoi recuperare la password cliccando il link <i>“forget password”</i>,
        controlla che le tue info siano corrette e inizia ad attivare i servizi disponibili. <br>
        Scarica da questo link le istruzioni per familiarizzare con il pannello di controllo.
    </p>
    <p>
        @component('mail::button', ['url' => 'https://www.openbar.life/OB_VENUE_2023_Tutorial.pdf', 'color' => 'primary'])
            Venue User Manual
        @endcomponent
    </p>
    <p>
        Se hai bisogno di assistenza contattaci a <a href="mailto:supprt@openbar.life">supprt@openbar.life</a>
    </p>
@endcomponent
