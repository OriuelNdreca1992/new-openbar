@component('mail::message_v2')
<p>
    <i>Ciao {{ $fullName }},</i>
</p>
<p>
    Per resettare la tua password usa il codice di verifica qui sotto.
</p>
<p>
    Codice di verifica: <strong>{{ $token }}</strong>
</p>
<p>
    Questo codice di verifica scadrà tra {{ $count }} minuti.
</p>
<p>
    Se non hai richiesto il reset della password puoi ignorare questa mail.
</p>
@endcomponent
