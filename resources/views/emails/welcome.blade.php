@component('mail::message_v2', ['footer_email' => $footer_email])
<p>
    Ciao {{ $user->first_name }} {{ $user->last_name }},
</p>
<p>
    Benvenuto in Openbar!
</p>
<p>
    Da oggi puoi offrire e ricevere da bere con la nostra app.
</p>
<p>
    Inoltre, controlla le offerte 2x1 disponibili e divertiti con i tuoi amici.
</p>
<p>
    Offrire da bere è sempre la cosa giusta da fare!
</p>
@endcomponent
