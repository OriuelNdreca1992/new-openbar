@component('mail::message_v2')
    <p>
        <i>Ciao {{ $venue->name }},</i>
    </p>
    <p>
        La tua venue è stata disabilitata dall’amministratore di Openbar.
    </p>
    <p>
        Per maggiori informazioni puoi contattarci a <a href="mailto:support@openbar.life">support@openbar.life</a><br>
    </p>
@endcomponent
