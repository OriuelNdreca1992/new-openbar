@component('mail::message_v2')
<p>
    <i>Ciao {{ $fullName }},</i>
</p>
<p>
    Per resettare la tua password clicca al link di seguito.
</p>
@component('mail::button', ['url' => $url])
    Resetta la password
@endcomponent
<p>
    Questo link per resettare la password scadrà tra {{ $count }} minuti.
</p>
<p>
    Se non hai richiesto il reset della password puoi ignorare questa mail.
</p>
@endcomponent
