<?php

if (! function_exists('menu_item_active')) {
    /**
     * Determines if the given routes are active.
     */
    function menu_item_active($routes): string
    {
        return call_user_func_array([app('router'), 'is'], (array) $routes) ? 'menu-item-open menu-item-here' : '';
    }
}

if (! function_exists('active')) {
    /**
     * Determines if the given routes are active.
    */
    function active($routes): string
    {
        return call_user_func_array([app('router'), 'is'], (array) $routes) ? 'menu-item-active' : '';
    }
}

