<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'sent_code' => 'We have e-mailed your password reset code!',
    'verify_email' => 'We have e-mailed you the verification code.',
    'verify_phone' => 'We have texted you the verification code.',
    'phone_verification_code_is' => 'Your OpenBar verification code is :token',
    'email_verified' => 'E-mail successfully verified.',
    'phone_verified' => 'Mobile number successfully verified.',
    'email_already_verified' => 'E-mail already verified.',
    'phone_already_verified' => 'Phone number already verified.',
    'verify_email_invalid' => 'The verification code is invalid.',
    'verify_phone_invalid' => 'The verification code is invalid.',
    'throttled' => 'Please wait before retrying.',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",
    'not_matches' => 'Your current password does not matches with the password you provided.',
    'not_same' => 'New Password cannot be same as your current password. Please choose a different password.',
    'changed' => 'Password changed successfully!',

];
