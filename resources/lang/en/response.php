<?php

return [

    '201' => 'Successfully created!',
    '204' => 'Successfully deleted!',
    '401' => 'Unauthorized!',
    '404' => 'Not found!',
    '422' => 'The given data was invalid.',
    '500' => 'Something went wrong!',
    'phone_email_taken' => 'The phone number or email has already been taken.',
    'phone_taken' => 'The phone number has already been taken.',
    'logged_out' => 'Successfully logged out.',

    'venue' => [
        '404' => 'Venue not found!',
    ],
    'offer' => [
        '409' => 'Already used!',
    ],
    'premium' => [
        '409' => 'Already used!',
    ],
    'cheers' => [
        '404' => 'Cheer not found!',
    ],
    'activity' => [
        '404' => 'Activity not found!',
    ],
    'drink' => [
        '404' => 'Drink not found!',
    ],
    'review' => [
        '409' => 'Already reviewed!',
    ],
];
