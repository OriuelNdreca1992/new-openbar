<?php

return [
    'welcome' => [
        'title' => 'Welcome to Openbar',
        'body' => '',
    ],
    'premium' => [
        'title' => ':drink_name for you!',
        'body' => ':full_name gifted you a :drink_name :venue_name.',
    ],
    'premium_multi' => [
        'title' => 'Drinks for you!',
        'body' => ':full_name gifted you some drinks :venue_name.',
    ],
    'feedback' => [
        'title' => 'Thanks for :drink_name! :emoji',
        'body' => ':full_name thanks for your :drink_name.',
    ],
    'feedback_multi' => [
        'title' => 'Thanks for the drinks! :emoji',
        'body' => ':full_name thanks for your drinks.',
    ],
    'reminder' => [
        'title' => 'Hey, a drink is expiring..',
        'body' => 'Your :drink_name :venue_name expires in :days days.',
    ],
];
