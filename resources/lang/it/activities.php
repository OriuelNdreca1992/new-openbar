<?php

return [
    'welcome' => [
        'title' => 'Benvenuto su Openbar',
        'body' => '',
    ],
    'premium' => [
        'title' => ':drink_name in arrivo!',
        'body' => ':full_name ti ha regalato un :drink_name :venue_name.',
    ],
    'premium_multi' => [
        'title' => 'Hai dei drink in arrivo!',
        'body' => ':full_name ti ha regalato dei drink :venue_name.',
    ],
    'feedback' => [
        'title' => 'Grazie per :drink_name! :emoji',
        'body' => ':full_name ti ringrazia per :drink_name.',
    ],
    'feedback_multi' => [
        'title' => 'Grazie per i drink! :emoji',
        'body' => ':full_name ti ringrazia per i drink.',
    ],
    'reminder' => [
        'title' => 'Hey, un drink sta per scadere...',
        'body' => 'Il tuo :drink_name :venue_name scade tra :days giorni.',
    ],
];
